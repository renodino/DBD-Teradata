#
#    DBD::Teradata = Perl DBI Driver for Teradata
#    Copyright (C) 2001-2009, Presicient Corp., USA
#    email: info@presicient.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; Refer to the included LICENSE file, or the LICENSE section 
#    of the DBD::Teradata documentation, for details. If neither is available,
#    write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
package DBD::Teradata::Parcels;

use Exporter;
our @ISA = qw(Exporter);

BEGIN {
use constant PclREQUEST		=>  1;
use constant PclRSUP		=>  2;
use constant PclDATA        =>  3;
use constant PclRESP        =>  4;
use constant PclKEEPRESP    =>  5;
use constant PclABORT       =>  6;
use constant PclCANCEL      =>  7;
use constant PclSUCCESS     =>  8;
use constant PclFAILURE     =>  9;
use constant PclRECORD      =>  10;
use constant PclENDSTATEMENT =>   11;
use constant PclENDREQUEST  =>  12;
use constant PclFMREQ       =>  13;
use constant PclFMRSUP		=>   14;
use constant PclVALUE       =>  15;
use constant PclNULLVALUE   =>  16;
use constant PclOK          =>  17;
use constant PclFIELD       =>  18;
use constant PclNULLFIELD   =>  19;
use constant PclTITLESTART  =>  20;
use constant PclTITLEEND    =>  21;
use constant PclFORMATSTART =>  22;
use constant PclFORMATEND   =>  23;
use constant PclSIZESTART   =>  24;
use constant PclSIZEEND     =>  25;
use constant PclSIZE        =>  26;
use constant PclRECSTART    =>  27;
use constant PclRECEND      =>  28;
use constant PclPROMPT      =>  29;
use constant PclENDPROMPT   =>  30;
use constant PclREWIND      =>  31;
use constant PclNOP         =>  32;
use constant PclWITH        =>  33;
use constant PclPOSITION    =>  34;
use constant PclENDWITH     =>  35;
use constant PclLOGON       =>  36;
use constant PclLOGOFF      =>  37;
use constant PclRUN         =>  38;
use constant PclRUNRESP     =>  39;
use constant PclUCABORT     =>  40;
use constant PclHOSTSTART   =>  41;
use constant PclCONFIG      =>  42;
use constant PclCONFIGRESP  =>  43;
use constant PclSTATUS      =>  44;
use constant PclIFPSWITCH   =>  45;
use constant PclPOSSTART    =>  46;
use constant PclPOSEND      =>  47;
use constant PclBULKRESP    =>  48;
use constant PclERROR       =>  49;
use constant PclDATE        =>  50;
use constant PclROW         =>  51;
use constant PclHUTCREDBS   =>  52;
use constant PclHUTDBLK     =>  53;
use constant PclHUTDELTBL   =>  54;
use constant PclHUTINSROW   =>  55;
use constant PclHUTRBLK     =>  56;
use constant PclHUTSNDBLK   =>  57;
use constant PclENDACCLOG	=>  58;
use constant PclHUTRELDBCLK =>  59;
use constant PclHUTNOP      =>  60;
use constant PclHUTBLD      =>  61;
use constant PclHUTBLDRSP   =>  62;
use constant PclHUTGETDDT   =>  63;
use constant PclHUTGETDDTRSP =>   64;
use constant PclHUTIDx      =>  65;
use constant PclHUTIDxRsp   =>  66;
use constant PclFieldStatus =>  67;
use constant PclINDICDATA   =>  68;
use constant PclINDICREQ    =>  69;
use constant PclDATAINFO    =>  71;
use constant PclIVRSUP		=>   72;
use constant PclOPTIONS     =>  85;
use constant PclPREPINFO    =>  86;
use constant PclCONNECT     =>  88;
use constant PclLSN         =>  89;
use constant PclCOMMIT		=>  90;
use constant PclASSIGN		=>  100;
use constant PclASSIGNRSP	=> 101;
use constant PclMLOADCTRL	=> 102;
use constant PclMLOADREC	=>  104;
use constant PclERRORCNT    =>  105;
use constant PclSESSINFO    =>  106;
use constant PclSESSINFORESP =>   107;
use constant PclSESSOPT     =>  114;
use constant PclVOTEREQUEST =>  115;
use constant PclVOTETERM    =>  116;
use constant PclCMMT2PC     =>  117;
use constant PclABRT2PC     =>  118;
use constant PclFORGET      =>  119;
use constant PclCURSORHOST	=>  120;
use constant PclCURSORDBC	=>  121;
use constant PclFLAGGER		=>  122;
use constant PclXINDICREQ	=>	123;
use constant PclPREPINFOX	=>	125;
use constant PclMULTITSR	=>  128;
use constant PclSPOPTIONS	=>  129;
#
#	SSO (needed for encryption)
#
use constant PclSSOAUTHREQ	=> 130;
use constant PclSSOAUTHRESP => 131;
use constant PclSSOREQ		=> 132;
use constant PclSSODOMAIN	=> 133;
use constant PclSSORESP		=> 134;
use constant PclSSOAUTHINFO => 135;
use constant PclUSERNAMEREQ => 136;
use constant PclUSERNAMERESP => 137;
#
#	new for v2r5
#
use constant PclMULTIPARTDATA	=> 140;
use constant PclENDMULTIPARTDATA	=> 141;
use constant PclMULTIPARTINDICDATA	=> 142;
use constant PclENDMULTIPARTINDICDATA	=> 143;
use constant PclMULTIPARTREC	=> 144;
use constant PclENDMULTIPARTREC	=> 145;
use constant PclDATAINFOX	=> 146;
use constant PclMULTIPARTRSUP	=> 147;
use constant PclMULTIPARTREQ	=> 148;
use constant PclELICITDATAMAILBOX	=> 149;
use constant PclELICITDATA	=> 150;
use constant PclELICITFILE	=> 151;
use constant PclELICITDATARECVD	=> 152;
use constant PclBIGRESP	=> 153;
use constant PclBIGKEEPRESP	=> 154;
use constant PclSETPOSITION => 157;
use constant PclROWPOSITION => 158;
use constant PclOFFSETPOSITION => 158;
use constant PclRESULTSUMMARY => 163;
use constant PclERRORINFO => 164;
use constant PclGTWCONFIG => 165;
use constant PclCLIENTCONFIG => 166;
use constant PclAUTHMECH => 167;

use constant PclLASTPCL => 167;

our @EXPORT    = ();		    # we export nothing by default
our @EXPORT_OK = q();
our %EXPORT_TAGS = (

	tdat_parcels => [
	qw/PclREQUEST PclRSUP PclDATA PclRESP PclKEEPRESP PclABORT
	PclCANCEL PclSUCCESS PclFAILURE PclRECORD PclENDSTATEMENT
	PclENDREQUEST PclFMREQ PclFMRSUP PclVALUE PclNULLVALUE PclOK
	PclFIELD PclNULLFIELD PclTITLESTART PclTITLEEND PclFORMATSTART
	PclFORMATEND PclSIZESTART PclSIZEEND PclSIZE PclRECSTART PclRECEND
	PclPROMPT PclENDPROMPT PclREWIND PclNOP PclWITH PclPOSITION
	PclENDWITH PclLOGON PclLOGOFF PclRUN PclRUNRESP PclUCABORT
	PclHOSTSTART PclCONFIG PclCONFIGRESP PclSTATUS PclIFPSWITCH
	PclPOSSTART PclPOSEND PclBULKRESP PclERROR PclDATE PclROW
	PclHUTCREDBS PclHUTDBLK PclHUTDELTBL PclHUTINSROW PclHUTRBLK
	PclHUTSNDBLK PclENDACCLOG PclHUTRELDBCLK PclHUTNOP PclHUTBLD
	PclHUTBLDRSP PclHUTGETDDT PclHUTGETDDTRSP PclHUTIDx PclHUTIDxRsp
	PclFIELDSTATUS PclINDICDATA PclINDICREQ PclDATAINFO PclIVRSUP
	PclOPTIONS PclPREPINFO PclCONNECT PclLSN PclCOMMIT PclASSIGN
	PclASSIGNRSP PclMLOADCTRL PclMLOADREC PclERRORCNT PclSESSINFO
	PclSESSINFORESP PclSESSOPT PclVOTEREQUEST PclVOTETERM PclCMMT2PC
	PclABRT2PC PclFORGET PclCURSORHOST PclCURSORDBC PclFLAGGER
	PclXINDICREQ PclPREPINFOX PclMULTITSR PclSPOPTIONS PclSSOAUTHREQ
	PclSSOAUTHRESP PclSSOREQ PclSSODOMAIN PclSSORESP PclSSOAUTHINFO
	PclUSERNAMEREQ PclUSERNAMERESP PclMULTIPARTDATA PclENDMULTIPARTDATA
	PclMULTIPARTINDICDATA PclENDMULTIPARTINDICDATA PclMULTIPARTREC
	PclENDMULTIPARTREC PclDATAINFOX PclMULTIPARTRSUP PclMULTIPARTREQ
	PclELICITDATAMAILBOX PclELICITDATA PclELICITFILE PclELICITDATARECVD
	PclBIGRESP PclBIGKEEPRESP PclSETPOSITION PclROWPOSITION PclOFFSETPOSITION
	PclRESULTSUMMARY PclERRORINFO PclGTWCONFIG PclCLIENTCONFIG
	PclAUTHMECH PclLASTPCL/
	],
);
#
#	add the tags
#
Exporter::export_tags(keys %EXPORT_TAGS);
};

1;