#
#    DBD::Teradata = Perl DBI Driver for Teradata
#    Copyright (C) 2001-2009, Presicient Corp., USA
#    email: info@presicient.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; Refer to the included LICENSE file, or the LICENSE section 
#    of the DBD::Teradata documentation, for details. If neither is available,
#    write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
package DBD::Teradata::ThreadedUtil;

use Config;
use DBI;
use DBD::Teradata;
use DBI qw(:sql_types);
use DBD::Teradata qw(
	:tdat_impl_codes
	:tdat_sth_codes
	:tdat_part_codes);

use threads;
use threads::shared;	# so we can share data
use Thread::Queue;	# a thread-safe shared  queue!

use DBD::Teradata::Utility;
use base ('DBD::Teradata::Utility');

use strict;
use warnings;
use bytes;

our $VERSION = '12.001';
our $copyright = "Copyright(c) 2001-2008, Presicient Corporation, USA";
#
#	constructor and all other methods provided by DBD::Teradata::Utility;
#	DBD::Teradata::Utility will act as factory for this class if
#	Perl is built with threads
#
#	Thread for threaded MP
#
sub ThrdThread {
	my ($mstrdbh, $lbdbh, $id, $offset, $stsq, $cmdq, $attrs, $sql, $lbsth, $jobcount) = @_;
	my $sqlstmt = $attrs->{SQL};
	my $io_sub = ($attrs->{Utility} eq 'EXPORT') ? $attrs->{Target} :
		$attrs->{Loopback} ? undef : $attrs->{Source};
	my $ctxt = $attrs->{Context};

	my $i = 0;

	print "Thread $id: logging on...\n" if $mstrdbh->{_debug};
	$attrs->{Report}->("Logging on thread $id") if $attrs->{Report};

	my ($dbh,$sth, $expdbh, $expsth);
	my $no_cli = (!$mstrdbh->{tdat_uses_cli});
	my $no_bigint = $mstrdbh->{tdat_no_bigint};
	$dbh = DBI->connect('dbi:Teradata:' . $mstrdbh->{Name},
		 $mstrdbh->{USER}, $mstrdbh->{tdat_password},
		{
			PrintError => 0,
			RaiseError => 0,
			AutoCommit => 0,
			tdat_lsn => $mstrdbh->{tdat_lsn},
			tdat_utility => $attrs->{Utility},
			tdat_charset => $mstrdbh->{tdat_charset},
			tdat_reqsize => $attrs->{RequestSize},
			tdat_no_cli => $no_cli,
			tdat_no_bigint => $no_bigint,
		}
	);

	if ($dbh) {
		DBD::Teradata::Utility::_announceConnection($attrs->{Report}, $dbh, $attrs->{Utility})
			if $attrs->{Report};
		print "Thread $id: preparing...\n" if $dbh->{_debug};
		$sth = ($attrs->{Utility} eq 'EXPORT') ?
			$dbh->prepare(';', { tdat_clone => $sql }) :
			$dbh->prepare($sql, { tdat_mlseq => [ $id, $offset, $jobcount ] });
			$sth->{tdat_raw_in} = 'IndicatorMode'
				if $sth && $attrs->{Loopback};
	}
	else {
#
#	send status up to master if error
#
		print "Thread $id: logon failed...\n" if $mstrdbh->{_debug};
		$stsq->enqueue("$id ERROR " . $DBI::err . ' ' . $DBI::errstr);
	}
#
#	send status up to master
#
	if ($dbh && (! $sth)) {
		print "Thread $id: prepare failed\n" if $dbh->{_debug};
		$stsq->enqueue("$id ERROR " . $dbh->err . ' ' . $dbh->errstr);
	}

 	if ($dbh && $attrs->{Loopback}) {
		$no_cli = (!$lbdbh->{tdat_uses_cli});
		$no_bigint = $lbdbh->{tdat_no_bigint};
 		$expdbh = DBI->connect('dbi:Teradata:' . $lbdbh->{Name},
			 $lbdbh->{USER}, $lbdbh->{tdat_password},
			{
				PrintError => 0,
				RaiseError => 0,
				AutoCommit => 0,
				tdat_lsn => $lbdbh->{tdat_lsn},
				tdat_utility => 'EXPORT',
				tdat_charset => $lbdbh->{tdat_charset},
				tdat_no_cli => $no_cli,
				tdat_no_bigint => $no_bigint,
			}
		);
#
#	send status up to master if error
#
		unless ($expdbh) {
			print "Thread $id: logon failed...\n" if $mstrdbh->{_debug};
			$stsq->enqueue("$id ERROR " . $DBI::err . ' ' . $DBI::errstr);
		}
		else {
			DBD::Teradata::Utility::_announceConnection($attrs->{Report}, $expdbh, 'EXPORT')
				if $attrs->{Report};
			print "Thread $id: preparing...\n" if $expdbh->{_debug};
			$expsth = $expdbh->prepare(';', {
				tdat_clone => $lbsth,
				tdat_raw_out => 'IndicatorMode' });
		}
#
#	send status up to master
#
		if ($expdbh && (! $expsth)) {
			print "Thread $id: prepare failed\n" if $dbh->{_debug};
			$stsq->enqueue("$id ERROR " . $expdbh->err . ' ' . $expdbh->errstr);
		}
	}
	$stsq->enqueue("$id READY")
		if $dbh && $sth &&
			((! $attrs->{Loopback}) || ($expdbh && $expsth));
	($expdbh, $expsth) = ($dbh, $sth) if ($attrs->{Utility} eq 'EXPORT');
#
#	now wait for a command
#
	my ($okcnt,$errcnt) = 0;
	my $initio = undef;
	my $rc;
	my ($qnum, $seqnum) = (1, $id+1);
	my ($stmtno, $stmthash, $stmtinfo);
	my $total_sent = 0;
	my @colary = ();
	my $rowcount = 0;
	print "Thread $id: waiting for command\n" if $mstrdbh->{_debug};
	my $export_started = 0;
	while (1) {
		$_ = $cmdq->dequeue();
		chomp;
		print "Thread $id: Got $_ command...\n" if $mstrdbh->{_debug};
		last if ($_ eq 'EXIT');
#
#	since the offset we are provided might not be the final
#	offset (due to using up the AMPs), we have toget the
#	real offset here
		$offset = $1,
		$sth->{tdat_mlseq} = [ $id, $offset, $jobcount ],
		next
			if (/^OFFSET (\d+)$/i);

		if (/^GO(\s+(\d+))?$/) {
			$rowcount = $2;
			unless ($initio) {
#
#	init the IO sub for the first GO
#
				if ($io_sub) {
					$rc = $io_sub->('INIT', $sth, $id, $rowcount, $ctxt);
					$stsq->enqueue("$id ERROR " . $sth->err . ' ' . $sth->errstr),
#					print "ERROR on $id:", $sth->errstr, "\n" and
					next
						unless $rc;
				}
				$initio = 1;
			}
			my $status = 0;
#
#	if we set a checkpoint in loopback mode, then we need to be
#	able to restart the export processing...from where we left off
#
			if ($expdbh) {
#
#	2 params need to be bound: the query number, and the sequence number
#
				unless ($export_started) {
					no strict;
					$expsth->bind_param_inout(1, \$qnum, SQL_INTEGER);
					$expsth->bind_param_inout(2, \$seqnum, SQL_INTEGER);
					use strict;
					$expsth->{tdat_nowait} = 1;

					$expsth->tdat_BindColArray(1, \@colary, 10000),
					$sth->bind_param_array(1, \@colary)
						if $attrs->{Loopback};

					$rc = $expsth->execute;

					$stsq->enqueue("$id ERROR " . $expsth->err . ' ' . $expsth->errstr),
#					print "ERROR on $id:", $sth->errstr, "\n" and
					next
						unless defined($rc);

					$seqnum += $offset;
					$export_started = 1;
				}
#
#	we fetch segments until our rowcount is used up
#
				$total_sent = 0;
				$okcnt = 0;
				while ((!defined($rowcount)) || ($rowcount > $total_sent)) {
#
#	have to handle things this way otherwise we fall into an infinite recursion
#
					last if defined($expsth->err) && ($expsth->err == 2588);
					$rc = $expsth->tdat_Realize();
					unless (defined($rc)) {
						$attrs->{Report}->("Export: error on thread $id " . $expsth->errstr)
							if $attrs->{Report} && ($expsth->err != 2588);
						$status = 128 unless ($expsth->err == 2588);
						last;
					}
					$seqnum += $offset;
#
#	get the result info
#
#					$stmtno = $expsth->{tdat_stmt_num};
#					$stmtinfo = $expsth->{tdat_stmt_info};
#					warn "stmtno $stmtno"
#						unless $stmtinfo->[$stmtno]{ActivityCount};

					$rc = 0;
#
#	if in loopback mode, then directly apply the rows to the
#	output; we'd liketo reuse the same buffer, but MLOADREC
#	parcels require some extra header stuff, so we're screwed
#	also, our export response size may cause overflow on MLOAD,
#	so we'll need to add a 2nd execute if things overflow,
#	OR restrict the response buffer size so we don't overflow
#
					if ($attrs->{Loopback}) {
						$rc = $expsth->fetch;
						unless (defined($rc)) {
							$attrs->{Report}->("Export: error on thread $id " . $expsth->errstr)
								if $attrs->{Report} && ($expsth->err != 2588);
							$status = 128 unless ($expsth->err == 2588);
							last;
						}

						$attrs->{Report}->("Loopback: sending $#colary rows on thread $id")
							if $attrs->{Report};
						my $rowlim = $#colary;
						if ($attrs->{Utility} eq 'MLOAD') {
#
#	sum up size of available rows and limit to max we can handle w/ mload headers
#	also duplicate records for each job, and add job ID array
#
							$i = 0;
							while ($i <= $#colary) {
								my $ttl = 0;
								my $len = 0;
								my @jobary = ();

								while (($i <= $#colary) && ($ttl < 64000)) {
									$len = (length($colary[$i]) + 11) * $jobcount;
									last
										unless ($ttl + $len < 64000);
									$ttl += $len;
									push @jobary, $colary[$i];
									$i++;
								}
								$sth->bind_param_array(1, \@jobary);
								$rc = $sth->execute;

								$total_sent += $rc if $rc;

								$rc = $dbh->commit
									if defined($rc);

								unless (defined($rc)) {
									$status = 128;
									print "Thread: sth error ", $sth->errstr, "\n"
										if $dbh->{_debug};
									last;
								}
								$stmtinfo = $sth->{tdat_stmt_info};
								$okcnt += $stmtinfo->[1]{ActivityCount};
							} # end while records left
						} # endif MLOAD
						else { # FASTLOAD is much easier...
							$rc = $sth->execute;

							$total_sent += $rc if $rc;

							$rc = $dbh->commit
								if defined($rc);

							unless (defined($rc)) {
								$status = 128;
								print "Thread: sth error ", $sth->errstr, "\n"
									if $dbh->{_debug};
								last;
							}
							$stmtinfo = $sth->{tdat_stmt_info};
							$okcnt += $stmtinfo->[1]{ActivityCount};
						}
					}
					else {
						my $sthpriv = $expsth->{_private};
						while ($expsth->fetch && defined($rc)) {
							$total_sent += ($sthpriv->[TD_STH_COLARY]) ?
								$#{$sthpriv->[TD_STH_COLARY][0]}+1 : 1;
							$rc = $io_sub->('MOREDATA', $expsth, $id,
								($sthpriv->[TD_STH_COLARY]) ?
									$#{$sthpriv->[TD_STH_COLARY][0]}+1 : 1, $ctxt);
							$attrs->{Report}->("Saved $rc records on thread $id")
								if ($rc && $attrs->{Report});
							$okcnt += $rc if $rc;
						}

						$status = 128,
						last
							unless defined($rc);
					}
				} # end while rowcount
			} # endif export
			else {
#
#	fastload/mload processing
#	possibly only do limited chunk per pass and check
#	the cmdfd for commands intermittently, in case of
#	errlimit or other failure
#
				$total_sent = 0;
				$okcnt = 0;
				while ($rowcount > $total_sent) {
					$rc = $io_sub->('MOREDATA', $sth, $id, $rowcount - $total_sent, $ctxt, $offset-1)
						unless $attrs->{Loopback};
					$status = 128, last
						unless defined($rc);

					last
						unless $rc;

					$attrs->{Report}->("Sent $rc records on thread $id")
						if $attrs->{Report};

					$rc = $sth->execute;

					unless (defined($rc)) {
						print "Thread: sth error ", $sth->errstr, "\n"
							if $dbh->{_debug};
						$status = 128;
						last;
					}

					$total_sent += $rc;
					$rc = $dbh->commit;

					$stmtinfo = $sth->{tdat_stmt_info};
					$okcnt += $stmtinfo->[1]{ActivityCount};
				} # end while rowcount
#
#	should we check the cmdq between msgs ?
#	for now, no
#
			} # endif not export
			$expsth->finish if ($expsth && $status);
			$stsq->enqueue($status ?
				"$id ERROR " . $dbh->err . ' ' . $dbh->errstr :
				"$id READY $okcnt " . ($total_sent - $okcnt));
#			print "ERROR on $id:", $dbh->errstr, "\n"
#				if $status;
			next;
		} # end if GO

		if ($_ eq 'FINISH') {
			$io_sub->('FINISH', undef, $id, 0, $ctxt)
				if $io_sub;
			$attrs->{Report}->("FINISH on thread $id")
				if $attrs->{Report};
			next;
		}

		if ($_ eq 'CHECKPOINT') {
			$io_sub->('CHECKPOINT', undef, $id, 0, $ctxt)
				if $io_sub;
			$attrs->{Report}->("CHECKPOINT on thread $id")
				if $attrs->{Report};
		}
	}
	$dbh->disconnect if $dbh && ((! defined($expdbh)) || ($dbh ne $expdbh));
	$expdbh->disconnect if $expdbh;
	print "Thread $id: exitting...\n" if $mstrdbh->{_debug};
}
#
#	threaded loader
#
sub ThrdUtilityLogon {
	my ($obj, $dbh, $dbhary, $sesscnt, $attrs, $sql, $csth, $jobcount) = @_;
	my $iobj = $dbh->{_iobj};
#
#	logon error session
#
	$dbh->{_expdbh} = $attrs->{Source}
		if $attrs->{Loopback};

	my $no_cli = (!$dbh->{tdat_uses_cli});
	my $no_bigint = $dbh->{tdat_no_bigint};
	my $errdbh = DBI->connect('dbi:Teradata:' . $dbh->{Name}, $dbh->{USER},
		$dbh->{tdat_password},
	{
		PrintError => 0,
		RaiseError => 0,
		AutoCommit => 1,
		tdat_charset => $dbh->{tdat_charset},
		tdat_no_cli => $no_cli,
		tdat_no_bigint => $no_bigint,
	});
	return $iobj->io_set_error("UtilitySetup: Can't logon error session: $DBI::errstr")
		unless $errdbh;
	DBD::Teradata::Utility::_announceConnection($attrs->{Report}, $errdbh, 'Error')
		if $attrs->{Report};
#
#	create status and response queues
#	spawn a thread for each session
#	wait for it to return status
#
	my $rspq = Thread::Queue->new();
	@$dbhary = ();
	my ($i, $j);
	foreach $i (0..$sesscnt-1) {
		$$dbhary[$i] = {
			OkCount => 0,
			ErrCount => 0,
			Status => 0,
			CmdQ => Thread::Queue->new()
		};
	}

	my $failcnt = 0;
	my ($status, $failed, $err, $len);
#
#	since all existing DBI entities get trashed in the new thread,
#	we need to make copies of the relevant bits here
#
	my $mstrdbh =
		{	# master dbh attrs
			Name => $dbh->{Name},
			USER => $dbh->{USER},
			tdat_password => $dbh->{tdat_password},
			tdat_lsn => $dbh->{tdat_lsn},
			tdat_charset => $dbh->{tdat_charset},
			tdat_uses_cli => $dbh->{tdat_uses_cli},
			tdat_no_bigint => $no_bigint,
		};
	$mstrdbh->{_debug} = $dbh->{_debug};	# obfus issue

	my $lbdbh = $attrs->{Loopback} ?
		{ # loopback dbh attrs
			Name => $attrs->{Source}{Name},
			USER => $attrs->{Source}{USER},
			tdat_password => $attrs->{Source}{tdat_password},
			tdat_lsn => $attrs->{Source}{tdat_lsn},
			tdat_charset => $attrs->{Source}{tdat_charset},
			tdat_uses_cli => $attrs->{Source}{tdat_uses_cli},
			tdat_no_bigint => $attrs->{Source}{tdat_no_bigint},
		} : undef;
# either EXPORT or loopback STH attrs for cloning
	$sql = DBD::Teradata::Utility::st->new($sql) if (ref $sql);
	$csth = DBD::Teradata::Utility::st->new($csth) if ($csth && (ref $csth));
	foreach $i (0..$sesscnt-1) {
		$dbhary->[$i]{Thread} = threads->create(\&ThrdThread,
			$mstrdbh, $lbdbh, $i, $sesscnt, $rspq, $dbhary->[$i]{CmdQ},
			$attrs, $sql, $csth, $jobcount);

		unless (defined($dbhary->[$i]{Thread})) {

			$iobj->io_set_error("Can't spawn thread: $!");
#
#	kill off existing threads
#
			foreach $j (0..$i-1) {
				$dbhary->[$j]{CmdQ}->enqueue('EXIT');
			}

			foreach $j (0..$i-1) {
				$dbhary->[$j]{Thread}->join;
				delete $dbhary->[$j]{Thread};
			}
			$errdbh->disconnect;
			$dbh->{_expdbh} = undef;
			@$dbhary = ();
			return (undef, undef);
		}
		print "Spawned thread $i\n" if $dbh->{_debug};
#
#	get threads startup response
#	if failure, exit
#	if all amps full, just shutdown the failed thread
#		and continue
#
		$status = $rspq->dequeue;
		print "got status of $status\n"
			if $dbh->{_debug};
		next unless ($status=~/^(\d+)\s+ERROR\s+(\d+)\s+(.+)$/);
		my $thrdid = $1;
		$err = $2;
#
#	see if we just ran out of AMPs
#
		$failcnt++,
		$iobj->io_set_error($err, "Failure $err: $3"),
		last
			unless ($err == 2632);
#
#	all amps are full, so just shutdown the failed thread
#
		pop(@$dbhary);	# remove the null entry
		$#$dbhary = $i - 1;	# shorten array
		last;
	}

	unless ($failcnt) {
#
#	since the final sesscnt may be less than original,
#	we need to reset the rowscan offsets
#
		$_->{CmdQ}->enqueue("OFFSET " . (scalar @$dbhary))
			foreach (@$dbhary);

		return ($errdbh, $rspq)
	}
#
#	if any failures, kill everybody
#
	foreach (@$dbhary) {
		$_->{CmdQ}->enqueue('EXIT');
		$_->{Thread}->join if $_->{Thread};
		delete $_->{Thread};
	}

	$errdbh->disconnect;
	$dbh->{_expdbh} = undef;
	@$dbhary = ();
	return undef;
}

sub ThrdUtilityLoad {
	my ($obj, $dbh, $errdbh, $dbhary, $stsq, $sths, $attrs, $jobcount) = @_;

	my $chkpt = $attrs->{Checkpoint};
	my $chkpt_sub = $attrs->{CheckpointCallback};
	my $ctxt = $attrs->{Context};
	my $errlim = $attrs->{ErrorLimit};
	$errlim = 1000000 unless $errlim;
	my $i = 0;
	my $rc;
	my ($errstr, $estate, $err);
	my $active = 0;
	my @avail = (1) x scalar @$dbhary;
	my $bundle;
	my $iobj = $dbh->{_iobj};
#
#	execute each statement
#
	$dbh->{_loading} = 1;
	$dbh->{AutoCommit} = 0;
	foreach (@$sths) {
		$rc = $_->execute;
		return undef unless defined($rc);
	}

	$dbh->commit
		if ($attrs->{Utility} eq 'MLOAD');

	$rc = $dbh->do('CHECKPOINT LOADING;');
	return undef unless defined($rc);

	my $maxrows = ($chkpt) ? $chkpt : 1000000;
	my $status;
	my $len;
	my ($total_sent, $total_accepted) = (0,0);
	my ($id, $result, $dmy, $okcnt, $errcnt);
	while (1) {
#
#	start each thread with a fixed number of records
#
		foreach $i (@avail) { $active += $i };
		last unless $active;

		$bundle = int($maxrows/$active);
		$bundle = 1 unless $bundle;

		foreach $i (0..$#$dbhary) {
			next unless $avail[$i];
			$dbhary->[$i]{CmdQ}->enqueue("GO $bundle");
		}
#
#	now wait for them all to reply
#
		while ($active) {
			$status = $stsq->dequeue();
			chomp $status;
#			print "\n *** Status is $status\n";
			($id, $dmy, $okcnt, $errcnt, $dmy, $err, $errstr) =
				($status=~/^(\d+)\s+(READY\s+(\d+)\s+(\d+))|(ERROR\s+(\d+)\s+(.+))$/);
			$active--;
#
#	not certain what to do with failures at this point
#
			$avail[$id] = defined($okcnt) ? 1 : 0;
			next unless $avail[$id];

			$$dbhary[$id]{OkCount} += $okcnt;
			$$dbhary[$id]{ErrCount} += $errcnt;
			$total_sent += ($okcnt + $errcnt);
			$total_accepted += $okcnt;
			print "Master: sess $id reports $okcnt loaded, $errcnt failed, total $total_sent\n"
				if $dbh->{_debug};
			$avail[$id] = 0
				if (($okcnt + $errcnt) == 0); # source exhausted
		}
#
#	issue checkpoint
#
		$rc = $dbh->do('CHECKPOINT LOADING;');
		return undef unless defined($rc);
#
#	notify each instance, and the app of the checkpoint
#
		foreach $i (0..$#$dbhary) {
			$dbhary->[$i]{CmdQ}->enqueue('CHECKPOINT')
				if $avail[$i];
		}

		$rc = $chkpt_sub->('CHECKPOINT', $total_sent, $ctxt)
			if $chkpt_sub;
#
#	check if we've exceed error limit
#
		LoadCleanup($dbh, $errdbh, $dbhary, $stsq, $attrs),
		$iobj->io_set_error('ThrdUtilityLoad: ErrorLimit exceeded.'),
		return ($total_sent, $total_accepted)
			if ($errlim < $total_sent - $total_accepted);

		$maxrows = ($chkpt) ? $chkpt : 1000000;

	}	# end while active sessions...

	return ($total_sent, $total_accepted);
}

1;