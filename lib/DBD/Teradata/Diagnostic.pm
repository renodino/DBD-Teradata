#
#    DBD::Teradata = Perl DBI Driver for Teradata
#    Copyright (C) 2001-2009, Presicient Corp., USA
#    email: info@presicient.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; Refer to the included LICENSE file, or the LICENSE section 
#    of the DBD::Teradata documentation, for details. If neither is available,
#    write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
package DBD::Teradata::Diagnostic;
use Time::HiRes qw(time);
use Exporter;
our @ISA = qw(Exporter);

use strict;
use warnings;
use bytes;

our @EXPORT    = qw(
	io_hexdump
	io_bytedump
	io_hdrdump
	io_pcldump);

our @EXPORT_OK = qw(
	@td_pclstrings
	%td_msgkind_map);

#pragma begin_protect
our @td_pclstrings = qw(
Unknown PclREQUEST PclRSUP PclDATA PclRESP PclKEEPRESP PclABORT PclCANCEL
PclSUCCESS PclFAILURE PclRECORD PclENDSTATEMENT PclENDREQUEST PclFMREQ
PclFMRSUP PclVALUE PclNULLVALUE PclOK PclFIELD PclNULLFIELD PclTITLESTART
PclTITLEEND PclFORMATSTART PclFORMATEND PclSIZESTART PclSIZEEND PclSIZE
PclRECSTART PclRECEND PclPROMPT PclENDPROMPT PclREWIND PclNOP PclWITH
PclPOSITION PclENDWITH PclLOGON PclLOGOFF PclRUN PclRUNRESP PclUCABORT
PclHOSTSTART PclCONFIG PclCONFIGRESP PclSTATUS PclIFPSWITCH PclPOSSTART
PclPOSEND PclBULKRESP PclERROR PclDATE PclROW PclHUTCREDBS PclHUTDBLK
PclHUTDELTBL PclHUTINSROW PclHUTRBLK PclHUTSNDBLK PclENDACCLOG PclHUTRELDBCLK
PclHUTNOP PclHUTBLD PclHUTBLDRSP PclHUTGETDDT PclHUTGETDDTRSP PclHUTIDX
PclHUTIDXRSP PclFIELDSTATUS PclINDICDATA PclINDICREQ Unknown PclDATAINFO
PclIVRSUP Unknown Unknown Unknown Unknown Unknown Unknown Unknown Unknown
Unknown Unknown Unknown Unknown PclOPTIONS PclPREPINFO Unknown PclCONNECT
PclLSN PclCOMMIT Unknown Unknown Unknown Unknown Unknown Unknown Unknown
Unknown Unknown PclASSIGN PclASSIGNRSP PclMLOADCTRL Unknown PclMLOAD PclERRORCNT
PclSESSINFO PclSESSINFORESP Unknown Unknown Unknown Unknown Unknown Unknown
PclSESSOPT PclVOTEREQUEST PclVOTETERM PclCMMT2PC PclABRT2PC PclFORGET
PclCURSORHOST PclCURSORDBC PclFLAGGER PclXINDICREQ Unknown PclPREPINFOX Unknown
Unknown PclMULTITSR PclSPL PclSSOAUTHREQ PclSSOAUTHRESP PclSSOREQ PclSSODOMAIN
PclSSORESP PclSSOAUTHINFO PclUSERNAMEREQ PclUSERNAMERESP Unknown Unknown
PclMULTIPARTDATA PclENDMULTIPARTDATA PclMULTIPARTINDICDATA PclENDMULTIPARTINDICDATA
PclMULTIPARTREC PclENDMULTIPARTREC PclDATAINFOX PclMULTIPARTRSUP PclMULTIPARTREQ
PclELICITDATAMAILBOX PclELICITDATA PclELICITFILE PclELICITDATARECVD PclBIGRESP
PclBIGKEEPRESP Unknown Unknown PclSETPOSITION PclROWPOSITION PclOFFSETPOSITION
Unknown Unknown Unknown PclRESULTSUMMARY PclERRORINFO PclGTWCONFIG PclCLIENTCONFIG
PclAUTHMECH
);

our %td_msgkind_map = qw(
1 COPKINDASSIGN 2 COPKINDREASSIGN 3 COPKINDCONNECT 4 COPKINDRECONNECT 5 COPKINDSTART
6 COPKINDCONTINUE 7 COPKINDABORT 8 COPKINDLOGOFF 9 COPKINDTEST 10 COPKINDCONFIG
11 COPKINDAUTHMETHOD 12 COPKINDSSOREQ 13 COPKINDELICITDATA 255 COPKINDDIRECT
);
#pragma end_protect

our @ebcdics = (	# flag valid EBCDIC chars
((0) x 64), 1, ((0) x 9), ((1) x 5), 0, 1,
((0) x 9), ((1) x 8), ((0) x 8), ((1) x 6),
((0) x 10), ((1) x 6), 0, ((1) x 9), ((0) x 7),
((1) x 9), ((0) x 7), ((1) x 9), 0,0,0,1, ((0) x 15),
1,0,0, ((1) x 10), ((0) x 6), ((1) x 10), ((0) x 6),
1, 0, ((1) x 8), ((0) x 6), ((1) x 10), ((0) x 6)
);

sub io_hexdump {
	return join('', $_[0], ":\n", _hexdump(0, length($_[1]), $_[1]), "\n");
}

sub _fractime {
	my @t = split(/\./, time());
	my $p = scalar localtime($t[0]);
	$p=~s/^\w+\s+(.+?)\s+\d+$/At $1.$t[1]/;
	return $p;
}

sub io_hdrdump {

	my ($req, $kind) = unpack('CC', substr($_[1], 1, 2));
	$req = ($req == 1) ? 'Sending' : 'Received';
	$kind = $td_msgkind_map{$kind} || 'Unknown kind';
	my ($seg, $len) = unpack('Cxxxn', substr($_[1], 4, 6));
	$len += ($seg << 16);
	my ($tdsess, $tdauth1, $tdauth2, $reqno) = unpack('N LL N', substr($_[1], 20));

	return join('',
		_fractime(),
		"\n$_[0] $req $kind Session $tdsess Request $reqno: $len bytes:\n",
		_hexdump(0, length($_[1]), $_[1]));
}

sub io_pcldump {
	my $len = $_[1];
	my $encrypted = $_[2];
#
#	buf in $_[0]
#
	my ($flavor, $pcllen) = (0,0);
	my $outstr = '';
	my $pos = 0;
	my $pclhdrsz = 4;
	my $aph = undef;
	while ($pos < $len) {
		$aph = "\n";
		return join('', $outstr, "Encrypted Data:\n", _hexdump($pos, $len, $_[0]), "\n")
			if $encrypted;

		($flavor, $pcllen) = unpack('SS', substr($_[0], $pos, 4));
#
#	new for v2r5: msb indicates its a modified parcel w/ 4 byte length
#
		$pclhdrsz = 4;
		$flavor &= 0x00FF,
		$aph = "\n(APH)",
		$pcllen = unpack('L', substr($_[0], $pos+4, 4)),
		$pclhdrsz = 8
			if ($flavor & 0x8000);
#
#	if the flavor is unknown, or size is too big,
#	then just hexdump the rest of the buffer
#
		return join('', $outstr,
			"Unknown parcel $flavor length $pcllen; dumping buffer:\n",
			_hexdump($pos, $len - $pos, $_[0]), "\n")
			if ($flavor > $#td_pclstrings) || ($pos + $pcllen > $len);

		$outstr .= "$aph Parcel $td_pclstrings[$flavor] length $pcllen:\n";

		$pcllen -= $pclhdrsz;
		$pos += $pclhdrsz;
		$outstr .= _hexdump($pos, $pcllen, $_[0]);
		$pos += $pcllen;
	}
	return $outstr . "\n";
}

sub _hexdump {
	my $pos = shift;
	my $len = shift;
#
#	buffer in $_[0]
#
	my $outstr = '';
	my $hexbuf = '';
	my $alphabuf = '';
	my $c;
	$len += $pos - 1;
	foreach ($pos..$len) {
		$outstr .= "$hexbuf	$alphabuf\n",
		($hexbuf, $alphabuf) = ('', '')
			unless ($_ - $pos) % 16;
		$c = substr($_[0], $_, 1);
		$hexbuf .= ' ' . unpack('H2', $c);
		$alphabuf .= ((ord($c) > 127) || (ord($c) < 32)) ? '.' : $c;
	}
	$outstr .= $hexbuf . (' ' x (48 - length($hexbuf))) . "\t" . $alphabuf . "\n"
		if length($hexbuf);
	return $outstr;
}

#pragma begin_redact
sub io_bytedump {
	return join('', $_[0], ":\n", _bytedump(0, length($_[1]), $_[1]), "\n");
}

sub _bytedump {
	my $pos = shift;
	my $len = shift;
#
#	buffer in $_[0]
#
	my $outstr = '';
	my $hexbuf = '';
	my $c;
	$len += $pos - 1;
	foreach ($pos..$len) {
		$outstr .= "$hexbuf\n",
		$hexbuf = ''
			unless ($_ - $pos) % 16;
		$c = substr($_[0], $_, 1);
		$hexbuf .= ((ord($c) > 127) ? ' (byte)0x' : ' 0x') . unpack('H2', $c) . ',';
	}
	$outstr .= $hexbuf . "\n"
		if length($hexbuf);
	return $outstr;
}
#pragma end_redact


1;