#
#    DBD::Teradata = Perl DBI Driver for Teradata
#    Copyright (C) 2001-2009, Presicient Corp., USA
#    email: info@presicient.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; Refer to the included LICENSE file, or the LICENSE section 
#    of the DBD::Teradata documentation, for details. If neither is available,
#    write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#	tdat_UtilitySetup connects several utility sessions using the current
#	sessions's user, password, and LSN, and prepares statement handles
#	on each of them using the input statement and attributes
#
require 5.008;

package DBD::Teradata::Utility;

use Config;
use DBI;
use DBD::Teradata;
use DBI qw(:sql_types);
use DBD::Teradata qw(
	:tdat_impl_codes
	:tdat_sth_codes
	:tdat_part_codes);

use strict;
use warnings;
use bytes;

our $VERSION = '12.001';
our $copyright = "Copyright(c) 2001-2007, Presicient Corporation, USA";
our $has_threads;

BEGIN {
	if ($Config{useithreads} && ($Config{useithreads} eq 'define') &&
		(!$ENV{TDAT_DBD_NOTHREADS})) {
		eval {
			require DBD::Teradata::ThreadedUtil;
		};
		$has_threads = 1 unless $@;
	}
	else {
		$has_threads = 0;
	}
}

sub new {
	my ($class, $dbh, $attrs) = @_;
	my $obj = { };
#
#	if we've got threads, then use our subclass
#
	$class = 'DBD::Teradata::ThreadedUtil'
		if $has_threads;

	bless $obj, $class;

	$obj->{_dbh} = $dbh;
	$obj->{_attrs} = $attrs;
#
#	%$attr contains the following:
#
#	SQL => SQL statement for utility, or arrayref of statements
#			for MLOAD
#	Sessions => max num of sessions to logon
#	Utility => FASTLOAD | MLOAD | EXPORT
#	Checkpoint => Num of rows per checkpoint
#	ErrorLimit => max number of errors before pausing
#	CheckpointCallback => \&chkpt_sub
#	Source => one of :
#		a coderef callback to provide data for fastload/mload
#		a string input file description
#		a DBI connection handle to be used as control session for
#			EXPORT in loopback mode
#		a DBI stmt handle - eventually
#	SourceDescription => a USING clause string
#	Target => one of:
#		a coderef callback to recv data for export
#		a string output file description
#		a DBI stmt handle - eventually
#	LogTables => arrayref of logtables; for MLOAD,
#		a hashref mapping tablenames to arrayrefs of
#		[ worktable, errortable, uvtable ]
#	Context => application level context
#	MP => 1 = multiprocess, w/ 1 session/process
#	Loopback => SELECT statement used as source
#		from FastExport feeding into Fastload/Mload
#		w/ 1 export + 1 load session per process
#	Report => a coderef to receive progress notifications
#		from internal rawfile and vartext file, or loopback callbacks;
#		ignored if a coderef source/target is specified
#	RequestSize => integer size of request buffer in bytes; fastload/mload only
#	Skip => integer number of records to skip from beginning of an
#		input rawmode or vartext file; ignored unless Source is
#		a file specification
#	Retry => if scalar, the number of seconds to wait between retry attempts
#		if arrayref, the number of seconds, and the max number of retry attempts
#
#	(Source | Target ) => subroutine ref |
#		"VARTEXT <separator>|INDICDATA|DATA|INMOD|OUTMOD|AXSMOD filename" |
#		DBI connection handle | DBI stmt handle
#
	my $iobj = $dbh->{_iobj};

	return $iobj->io_set_error('UtilitySetup: SQL attribute not provided.')
		unless $$attrs{SQL};

	return $iobj->io_set_error('UtilitySetup: Utility attribute not provided.')
		unless $$attrs{Utility};

	return $iobj->io_set_error('UtilitySetup: Unrecognized Utility attribute value (must be either FASTLOAD, MLOAD, or EXPORT).')
		if ($$attrs{Utility}!~/^(FASTLOAD|MLOAD|EXPORT)$/i);

	return $iobj->io_set_error('UtilitySetup: Utility attribute value must be either FASTLOAD or MLOAD in Loopback mode).')
		if ($$attrs{Loopback} && ($$attrs{Utility}!~/^(FASTLOAD|MLOAD)$/i));

	return $iobj->io_set_error('UtilitySetup: Invalid MP attribute value.')
		if $$attrs{MP} && ($$attrs{MP} ne '1') &&
			(lc $$attrs{MP} ne 'threads') && (lc $$attrs{MP} ne 'nothreads');

	return $iobj->io_set_error('UtilitySetup: Invalid MP attribute value: threads not available.')
		if $$attrs{MP} && ($class ne 'DBD::Teradata::ThreadedUtil') &&
			(lc $$attrs{MP} eq 'threads');

	return $iobj->io_set_error('UtilitySetup: MP attribute requested for MsWin32, but Perl built without thread support.')
		if $$attrs{MP} && ($^O eq 'MSWin32') &&
			($class ne 'DBD::Teradata::ThreadedUtil');

	my $sqlstmt = $$attrs{SQL};
	my $partition = $$attrs{Utility};

	my $chkpt = undef;
	$chkpt = $1
		if $$attrs{Checkpoint} && ($$attrs{Checkpoint}=~/^(\d+)$/);
	return $iobj->io_set_error('UtilitySetup: Invalid Checkpoint attribute; needs integer > 0.')
		if $$attrs{Checkpoint} && (! $chkpt);

	return $iobj->io_set_error('UtilitySetup: Invalid ErrorLimit value: must be integer > 0.')
		if  $$attrs{ErrorLimit} && ($$attrs{ErrorLimit} <= 0);

	return $iobj->io_set_error('UtilitySetup: CheckpointCallback attribute requires subroutine reference.')
		if $$attrs{CheckpointCallback} &&
			(ref $$attrs{CheckpointCallback} ne 'CODE');

	return $iobj->io_set_error('UtilitySetup: No LSN assigned to control session.')
		unless $dbh->{tdat_lsn};

	return $iobj->io_set_error('UtilitySetup: TERADATA mode required for Control session.')
		if ($dbh->{tdat_mode} ne 'TERADATA');

	return $iobj->io_set_error('UtilitySetup: ActivityCounts attribute must be HASHREF.')
		if $attrs->{ActivityCounts} && ($partition eq 'MLOAD') &&
			((! ref $attrs->{ActivityCounts}) || (ref $attrs->{ActivityCounts} ne 'HASH'));
#
#	silently adjust RequestSize
#
	$attrs->{RequestSize} = 65000
		unless (! $dbh->{tdat_uses_cli}) &&
			$attrs->{RequestSize} && ($attrs->{RequestSize} >= 64256) &&
			($attrs->{RequestSize} < 1048000);

	if ($attrs->{Loopback}) {
		return $iobj->io_set_error('UtilitySetup: Source attribute must be database handle for Loopback.')
			unless $attrs->{Source} && (ref $attrs->{Source} eq 'DBI::db');

		return $iobj->io_set_error('UtilitySetup: No LSN assigned to loopback control session.')
			unless $attrs->{Source}{tdat_lsn};

		return $iobj->io_set_error('UtilitySetup: TERADATA mode required for Control session.')
			unless ($attrs->{Source}{tdat_mode} eq 'TERADATA');
	}

	my $logtbls = $attrs->{LogTables};
	return $iobj->io_set_error('UtilitySetup: LogTables attribute requires arrayref for EXPORT and FASTLOAD.')
		if ($partition ne 'MLOAD') && defined($logtbls) &&
			((! ref $logtbls) || (ref $logtbls ne 'ARRAY'));

	return $iobj->io_set_error('UtilitySetup: LogTables attribute requires hashref for MLOAD.')
		if ($partition eq 'MLOAD') && defined($logtbls) && ((! ref $logtbls) || (ref $logtbls ne 'HASH'));

	return $iobj->io_set_error('UtilitySetup: Report attribute must be a coderef.')
		if $attrs->{Report} && ((! ref $attrs->{Report}) || (ref $attrs->{Report} ne 'CODE'));

	if ($attrs->{Retry}) {
		return $iobj->io_set_error('UtilitySetup: Invalid value for Retry attribute; must be scalar integer > 0, or arrayref of 2 scalar integers.')
			unless (((! ref $attrs->{Retry}) && ($attrs->{Retry}=~/^\d+$/)) ||
				(ref $attrs->{Retry} &&
					(ref $attrs->{Retry} eq 'ARRAY') &&
					($#{$attrs->{Retry}} == 1) &&
					($attrs->{Retry}[0]=~/^\d+$/) &&
					($attrs->{Retry}[1]=~/^\d+$/)));
	}

	unless ($partition eq 'EXPORT') {
		return $iobj->io_set_error('UtilitySetup: Source attribute not provided.')
			unless ($$attrs{Source} || $$attrs{Loopback});

		return $iobj->io_set_error('UtilitySetup: Source attribute requires subroutine reference or filename descriptor.')
			if (ref $$attrs{Source}) && (ref $$attrs{Source} ne 'CODE') && (! $attrs->{Loopback});

		return $iobj->io_set_error('UtilitySetup: FASTLOAD SQL statement requires USING clause.')
			if ($partition eq 'FASTLOAD') && ($sqlstmt!~/^\s*USING/i);

		return $iobj->io_set_error('UtilitySetup: LogTables attribute requires 2 names for FASTLOAD.')
			if ($partition eq 'FASTLOAD') && $logtbls && ($#$logtbls != 1);
#
#	if its a filename, open and init things
#
		unless (ref $$attrs{Source}) {
			my ($type, $sep, $fname) =
	($$attrs{Source}=~/^\s*(VARTEXT(\s+['"].['"])?|INDICDATA|DATA)\s+(.+)\s*$/i);

			return $iobj->io_set_error("UtilitySetup: Invalid Source specification for $partition.")
				unless (($fname && $type) || $$attrs{Loopback});

			$$attrs{Context}{_Filename} = $fname;
			if ($type=~/^VARTEXT/i) {
				$$attrs{Source} = \&LoadVartext;
				$$attrs{Context}{_Separator} =
					($sep && ($sep=~/^\s+['"](.)['"]/)) ? $1 : '|';
			}
			else {
				$$attrs{Source} = \&LoadRawFile ;
				$$attrs{Context}{Mode} =
					(uc $type eq 'DATA') ? 'RecordMode' : 'IndicatorMode';
			}
			$$attrs{Context}{_JobCount} = 1 if ($partition eq 'MLOAD');
			$$attrs{Context}{_Skip} = $attrs->{Skip};
		}
	}
	else {
		return $iobj->io_set_error('UtilitySetup: Target attribute not provided.')
			unless $$attrs{Target};

#		return $iobj->io_set_error('UtilitySetup: LogTables attribute not provided.')
#			unless $logtbls;

		return $iobj->io_set_error('UtilitySetup: Target attribute requires subroutine reference or filename descriptor.')
			if (ref $$attrs{Target}) && (ref $$attrs{Target} ne 'CODE');

		unless (ref $$attrs{Target}) {
			my ($type, $sep, $fname) =
	($$attrs{Target}=~/^\s*(VARTEXT(\s+['"].['"])?|INDICDATA|DATA)\s+(.+)\s*$/i);

			return $iobj->io_set_error('UtilitySetup: Invalid Target specification for EXPORT.')
				unless $fname && $type;

			return $iobj->io_set_error('UtilitySetup: Filename Target not supported in MP mode.')
				unless $fname && $type;

			$$attrs{Context}{_Filename} = $fname;
			if ($type=~/^VARTEXT/i) {
				$$attrs{Target} =  \&SaveVartext;
				$$attrs{Context}{_Separator} =
					($sep && ($sep=~/^\s+['"](.)['"]/)) ? $1 : '|';
			}
			else {
				$$attrs{Target} = \&SaveRawFile ;
				$$attrs{Context}{Mode} =
					(uc $type eq 'DATA') ? 'RecordMode' : 'IndicatorMode';
			}
		}
	}

	$obj->{_sesscnt} = $$attrs{Sessions} ||= 24;

	return $obj;
}
#
#	now do utility specific processing
#
sub run {
	my $obj = shift;
	return ($obj->{_attrs}{Utility} eq 'FASTLOAD') ?
		$obj->FloadRun($obj->{_dbh}, $obj->{_sesscnt}, $obj->{_attrs}) :
		($obj->{_attrs}{Utility} eq 'MLOAD') ?
			$obj->MloadRun($obj->{_dbh}, $obj->{_sesscnt}, $obj->{_attrs}) :
		$obj->ExportRun($obj->{_dbh}, $obj->{_sesscnt}, $obj->{_attrs});
}

sub _announceConnection {
	my ($report, $dbh, $type) = @_;
	$report->(join('',
		"$type session connected using charset ",
		$dbh->{tdat_charset},
		($dbh->{tdat_uses_cli} ? ' via CLI ' : ' via pure Perl '),
		' with DECIMAL conversion via ',
		($dbh->{tdat_no_bigint} ? 'floating point math' : 'Math::BigInt')
	));
}

sub MPUtilityLogon {
	my ($obj, $dbh, $dbhary, $sesscnt, $attrs, $sql, $lbsth, $jobcount) = @_;
#
#	MP doesn't work too well on Win32 yet (need to test newer Perls)
#
	return undef if ($^O eq 'MSWin32');
	my $iobj = $dbh->{_iobj};
#
#	logon error session
#
	$dbh->{_expdbh} = $attrs->{Source}
		if $attrs->{Loopback};
#
#	be sure to inherit settings from the control connection
#
	my $no_cli = (!$dbh->{tdat_uses_cli});
	my $no_bigint = $dbh->{tdat_no_bigint};
	my $errdbh = DBI->connect('dbi:Teradata:' . $dbh->{Name}, $dbh->{USER},
		$dbh->{tdat_password},
	{
		PrintError => 0,
		RaiseError => 0,
		AutoCommit => 1,
		tdat_charset => $dbh->{tdat_charset},
		tdat_no_cli => $no_cli,
		tdat_no_bigint => $no_bigint,
	});

	return $iobj->io_set_error(-1,	"UtilitySetup: Can't logon error session: $DBI::errstr", $DBI::state)
		unless $errdbh;

	_announceConnection($attrs->{Report}, $errdbh, 'Error')
		if $attrs->{Report};
#
#	logon sessions till limit or AMPs full
#	fork each session into its own process
#	create status pipe
#
	my ($stsrd, $stswrt);
	pipe ($stsrd, $stswrt);
	binmode $stsrd;
	binmode $stswrt;
#
#	autoflush on the status pipe
	my $old = select $stswrt;
	$| = 1;
	select $old;
#
#	we may need to catch these in future
#
	$SIG{CHLD} = 'IGNORE';	# ignore SIGCHILD
	@$dbhary = ();
	my $i;
	map { $$dbhary[$_] = { OkCount => 0, ErrCount => 0, Status => 0 }; } 0..$sesscnt-1;

	my $master = 1;
	my $failcnt = 0;
	my ($status, $fd, $failed, $err, $len);
	foreach $i (0..$sesscnt-1) {
		pipe ($$dbhary[$i]{MyPipe}, $$dbhary[$i]{CmdPipe});

		my $old = select $dbhary->[$i]{CmdPipe};
		$| = 1;
		select $old;

		my $pid = fork();
		unless (defined($pid)) {
#
#	all existing children should die once we close the pipe
#
			$iobj->io_set_error("Can't fork child: $!");
			close $stsrd;

			map { $fd = $_->{CmdPipe}; close $fd; } @$dbhary;

			$errdbh->disconnect;
			$dbh->{_expdbh} = undef;
			@$dbhary = ();
			return (undef, undef);
		}
		unless ($pid) {
#
#	in child now
#
			$master = 0;
			close $$dbhary[$i]{CmdPipe};
			close $stsrd;
			my $rc = MPChild($dbh, $i, $sesscnt, $stswrt,
				$dbhary->[$i]{MyPipe}, $attrs, $sql, $lbsth, $jobcount);
#
#	exit doesn't seem to work properly on Win32 ???
#
			exit(1);
		}
#
#	in master:
#	cleanup and save ctxt
#	wait for thread to respond
#	get thread status
#	if failure, exit
#	if all amps full, just shutdown the failed thread
#		and continue
#
		close $$dbhary[$i]{MyPipe};
		$$dbhary[$i]{Pid} = $pid;
		print "Forked $pid\n" if $dbh->{_debug};
		$fd = $dbhary->[$i]{CmdPipe};
#
#	kill all existing children if we get a pipe failure
#
		$iobj->io_set_error("Can't read pipe: $!"),
		$failcnt++,
		last
			unless read($stsrd, $status, 1, 0);

		print "got status of $status\n"
			if $dbh->{_debug};
		$status = unpack('C', $status);
		next unless ($status > 127);
#
#	see if we just ran out of AMPs
#
		print "getting failed child status\n" if $dbh->{_debug};
		print $fd "REPORT\n";

		$iobj->io_set_error("Can't read pipe: $!"),
		$failcnt++,
		last
			unless read($stsrd, $status, 4, 0);
#
#	get status text msg
#
		($err, $len) = unpack('SS', $status);
		$iobj->io_set_error("Can't read pipe: $!"),
		$failcnt++,
		last
			unless read($stsrd, $status, $len, 0);

		$failcnt++,
		$iobj->io_set_error($err, "Failure $err: $status"),
		last
			unless ($err == 2632);
#
#	all amps are full, so just shutdown the failed child
#
		print $fd "FINISH\nEXIT\n";
		pop(@$dbhary) if ($err == 2632);	# remove the null entry
		close $fd;
		$#$dbhary = $i - 1;	# shorten array
		last;
	}

	unless ($failcnt) {
#
#	since the final sesscnt may be less than original,
#	we need to reset the rowscan offsets
#
		foreach (@$dbhary) {
			$fd = $_->{CmdPipe};
			print $fd "OFFSET ", scalar @$dbhary, "\n";
		}
		return ($errdbh, $stsrd)
	}
#
#	if any failures, kill everybody
#
	foreach (@$dbhary) {
		$fd = $_->{CmdPipe};
		next unless $fd;
		print $fd "EXIT\n";
		close $fd;
	}

	close $stsrd;

	$errdbh->disconnect;
	$dbh->{_expdbh} = undef;
	@$dbhary = ();
	return undef;
}


sub UtilityLogon {
	my ($obj, $dbh, $dbhary, $partition, $sesscnt, $attrs) = @_;
	my $iobj = $dbh->{_iobj};
#
#	logon error session
#
	my $no_cli = (!$dbh->{tdat_uses_cli});
	my $no_bigint = $dbh->{tdat_no_bigint};
	my $errdbh = DBI->connect('dbi:Teradata:' . $dbh->{Name}, $dbh->{USER},
		$dbh->{tdat_password},
	{
		PrintError => 0,
		RaiseError => 0,
		AutoCommit => 1,
		tdat_charset => $dbh->{tdat_charset},
		tdat_no_cli => $no_cli,
		tdat_no_bigint => $no_bigint,
	});
	return $iobj->io_set_error(-1, "UtilitySetup: Can't logon error session: $DBI::errstr", $DBI::state),
		unless $errdbh;
	_announceConnection($attrs->{Report}, $errdbh, 'Error')
		if $attrs->{Report};
#
#	logon sessions till limit or AMPs full
#
	@$dbhary = ();
	my $i = 0;
	foreach $i (0..$sesscnt-1) {
		$$dbhary[$i] = DBI->connect('dbi:Teradata:' . $dbh->{Name},
			$dbh->{USER}, $dbh->{tdat_password},
			{
				PrintError => 0,
				RaiseError => 0,
				AutoCommit => 0,
				tdat_lsn => $dbh->{tdat_lsn},
				tdat_utility => $partition,
				tdat_charset => $dbh->{tdat_charset},
				tdat_reqsize => $attrs->{RequestSize},
				tdat_no_cli => $no_cli,
				tdat_no_bigint => $no_bigint,
			});

		pop @$dbhary,
		last
			unless $$dbhary[$i];

		_announceConnection($attrs->{Report}, $$dbhary[$i], $partition)
			if $attrs->{Report};
#
#	make sure we can select() on it!
#
#		$$dbhary[$i]->disconnect,
#		pop @$dbhary,
#		last
#			if (fileno($$dbhary[$i]{_iobj}[TD_IMPL_CONNFD]) > 31);
	}
#
#	check for failures
#
	return ($errdbh, undef)
		unless (@$dbhary < $sesscnt) && defined($DBI::err) &&
			($DBI::err != 2632) && ($DBI::err != 0);

	$iobj->io_set_error($DBI::err, $DBI::errstr, $DBI::state);
	map { $_->disconnect; } @$dbhary;
	$errdbh->disconnect;
	@$dbhary = ();
	return undef;
}

sub UtilityLoad {
	my ($obj, $dbh, $errdbh, $dbhary, $sths, $attrs, $sql, $sthattrs) = @_;

	my $chkpt = $attrs->{Checkpoint};
	my $chkpt_sub = $attrs->{CheckpointCallback};
	my $io_sub = $attrs->{Source};
	my $ctxt = $attrs->{Context};
	my $errlim = $attrs->{ErrorLimit};
	$errlim = 1000000 unless $errlim;
	my $i = 0;
	my $rc;
	my ($errstr, $estate, $err);
	my $iobj = $dbh->{_iobj};
#
#	the std non-MP way
#	prepare the stmt on each util session
#	NOTE: prepare multiple for multiload
#
	my @sthary = ();
	foreach $i (0..$#$dbhary) {
		$sthary[$i] = $dbhary->[$i]->prepare($sql, $sthattrs);

		LoadCleanup($dbh, $errdbh, $dbhary, $attrs),
		return $iobj->io_set_error($dbhary->[$i]->err, 'UtilitySetup: ' . $dbhary->[$i]->errstr, $dbhary->[$i]->state)
			unless $sthary[$i];
	}
#
#	execute each statement, providing stmt number for each
#
	$dbh->{_loading} = 1;
	$dbh->{AutoCommit} = 0;
	foreach (@$sths) {
		$rc = $_->execute;
		return undef unless defined($rc);
	}

	$dbh->commit
		if ($attrs->{Utility} eq 'MLOAD');

	$rc = $dbh->do('CHECKPOINT LOADING;');
	return undef unless defined($rc);
#
#	tell app how many sessions we logged on
#
	$chkpt_sub->('INIT', $#$dbhary + 1, $ctxt) if $chkpt_sub;

	my $total_sent = 0;
	my @outlist = (0..$#$dbhary);
	my @actv_src = (1) x scalar @$dbhary;
	my $active = 0;
	my $maxrows = $chkpt || 1000000;
	my $drh = $dbh->{Driver};
	my $total_accepted = 0;
	foreach $i (@outlist) {
#
#	io callback uses provided STH to bind row data
#
		$attrs->{Report}->("Initializing for session $i") if $attrs->{Report};
		$rc = $io_sub->('INIT', $sthary[$i], $i, $maxrows, $ctxt);
		$actv_src[$i] = undef unless defined($rc);
	}

	while (1) {
		@outlist = $drh->tdat_FirstAvailList($dbhary);
		warn "Can't get a completed handle\n" and
		sleep 5, next
			unless  @outlist;

		foreach $i (@outlist) {
			if ($dbhary->[$i]{tdat_active}) {
				$rc = $sthary[$i]->tdat_Realize();
				warn $sthary[$i]->errstr
					unless defined($rc);
#
#	check number of rows processed and see if error limit
#	exceeded
#
				$active--;
				my $stmtinfo = $sthary[$i]{tdat_stmt_info};
				my $stmthash = $$stmtinfo[1];
				$total_accepted += $stmthash->{ActivityCount};
				$maxrows = 0, last
					if ($total_sent - $total_accepted > $errlim);
			}
#
#	if this source is no longer active, skip it
#
			next unless $actv_src[$i];
#
#	io callback uses provided STH to bind row data
#
			$rc = $io_sub->('MOREDATA', $sthary[$i], $i, $maxrows, $ctxt);
			$actv_src[$i] = undef,
			next unless $rc && ($rc > 0);
			$attrs->{Report}->("Sent $rc records for session $i") if $attrs->{Report};

			$total_sent += $rc;
			$maxrows -= $rc if $chkpt;
			$rc = $sthary[$i]->execute;
			$rc = $dbhary->[$i]->commit
				if defined($rc);

			return $iobj->io_set_error($dbhary->[$i]->err, 'UtilityLoad: ' . $dbhary->[$i]->errstr, $dbhary->[$i]->state)
				unless defined($rc);

			$active++;
			last if ($maxrows <= 0);
		}

		last unless $active;
#
#	see if its time for a checkpoint
#
		next unless ($maxrows <= 0);
#
#	wait for all active sessions
#
		while ($active) {

			foreach $i (0..$#$dbhary) {
				next unless $dbhary->[$i]{tdat_active};
				$rc = $sthary[$i]->tdat_Realize();
				$active--;
				next unless $rc;
#
#	check number of rows processed
#
				my $stmtinfo = $sthary[$i]{tdat_stmt_info};
				my $stmthash = $$stmtinfo[1];
				$total_accepted += $stmthash->{ActivityCount};
			}
		}
#
#	issue checkpoint
#
		$rc = $dbh->do('CHECKPOINT LOADING;');
		return undef unless defined($rc);
#
#	notify the app of the checkpoint
#
		foreach (0..$#$dbhary) {
			$attrs->{Report}->("Checkpointing session $_") if $attrs->{Report};
			$rc = $io_sub->('CHECKPOINT', undef, $_, 0, $ctxt);
		}

		$rc = $chkpt_sub->('CHECKPOINT', $total_sent, $ctxt)
			if $chkpt_sub;
#
#	check if we've exceed error limit
#
		LoadCleanup($dbh, $errdbh, $dbhary, $attrs),
		$iobj->io_set_error('UtilityLoad: ErrorLimit exceeded.'),
		return ($total_sent, $total_accepted)
			if ($errlim < $total_sent - $total_accepted);
#
#	re-activate the mload sessions
#
		$maxrows = $chkpt || 1000000;
	}
	$dbh->{_loading} = 0;
	return ($total_sent, $total_accepted);
}
#
#	multiprocess loader
#
sub MPUtilityLoad {
	my ($obj, $dbh, $errdbh, $dbhary, $stsfd, $sths, $attrs, $jobcount) = @_;

	my $chkpt = $attrs->{Checkpoint};
	my $chkpt_sub = $attrs->{CheckpointCallback};
	my $ctxt = $attrs->{Context};
	my $errlim = $attrs->{ErrorLimit};
	$errlim = 1000000 unless $errlim;
	my $i = 0;
	my $rc;
	my ($errstr, $estate, $err);
	my $active = 0;
	my @avail = (1) x scalar @$dbhary;
	my $bundle;
	my $iobj = $dbh->{_iobj};
#
#	execute each statement
#
	$dbh->{_loading} = 1;
	$dbh->{AutoCommit} = 0;
	foreach (@$sths) {
		$rc = $_->execute;
		return undef unless defined($rc);
	}

	$dbh->commit
		if ($attrs->{Utility} eq 'MLOAD');

	$rc = $dbh->do('CHECKPOINT LOADING;');
	return undef unless defined($rc);

	my $maxrows = $chkpt || 1000000;
	my $fd;
	my $status;
	my $len;
	my ($total_sent, $total_accepted) = (0,0);
	while (1) {
#
#	start each child with a fixed number of records
#
		map { $active += $_ } @avail;
		last unless $active;

		$bundle = int($maxrows/$active);
		$bundle = 1 unless $bundle;

		foreach (0..$#$dbhary) {
			$fd = $dbhary->[$_]{CmdPipe},
			print $fd "GO $bundle\n"
				if $avail[$_];
		}
#
#	now wait for them all to reply
#
		while ($active) {
			$iobj->io_set_error("Can't read pipe: $!"),
			return (undef, undef)
				unless read $stsfd, $status, 1, 0;
			$status = unpack('C', $status);
			my $id = $status & 127;
			$active--;
			$avail[$id] = ($status & 128) ? $status : 1;
		}

		foreach my $id (0..$#avail) {
			next unless $avail[$id];
#
#	get the OK/Error count
#
			$status = $avail[$id];
			$fd = $$dbhary[$id]{CmdPipe};

			if ($status & 128) {
				print $fd "REPORT\n";
				$iobj->io_set_error("Can't read pipe: $!"),
				return (undef, undef)
					unless read $stsfd, $status, 4, 0;

				($err, $len) = unpack('SS', $status);
				$iobj->io_set_error("Can't read pipe: $!"),
				return (undef, undef)
					unless read $stsfd, $errstr, $len, 0;
#
#	not certain what to do with failures at this point
#
				$avail[$id] = 0;
				next;
			}

			print $fd "STATUS\n";
			$iobj->io_set_error("Can't read pipe: $!"),
			return (undef, undef)
				unless read $stsfd, $status, 8, 0;
			my ($okcnt, $errcnt) = unpack('LL', $status);
			$$dbhary[$id]{OkCount} += $okcnt;
			$$dbhary[$id]{ErrCount} += $errcnt;
			$total_sent += ($okcnt + $errcnt);
			$total_accepted += $okcnt;
			print "Master: sess $id reports $okcnt loaded, $errcnt failed, total $total_sent\n"
				if $dbh->{_debug};
			$avail[$id] = 0
				unless ($okcnt + $errcnt); # source exhausted
		}
#
#	issue checkpoint
#
		$rc = $dbh->do('CHECKPOINT LOADING;');
		return undef unless defined($rc);
#
#	notify each instance, and the app of the checkpoint
#
		foreach (0..$#$dbhary) {
			$fd = $dbhary->[$_]{CmdPipe},
			print $fd "CHECKPOINT\n"
				if $avail[$_];
		}

		$rc = $chkpt_sub->('CHECKPOINT', $total_sent, $ctxt)
			if $chkpt_sub;
#
#	check if we've exceed error limit
#
		LoadCleanup($dbh, $errdbh, $dbhary, $stsfd, $attrs),
		$iobj->io_set_error('MPUtilityLoad: ErrorLimit exceeded.'),
		return ($total_sent, $total_accepted)
			if ($errlim < $total_sent - $total_accepted);

		$maxrows = $chkpt || 1000000;

	}	# end while active sessions...

	return ($total_sent, $total_accepted);
}

sub FloadRun {
	my ($obj, $dbh, $sesscnt, $attrs) = @_;
	my $sqlstmt = $attrs->{SQL};
	my $chkpt = $attrs->{Checkpoint};
	my $chkpt_sub = $attrs->{CheckpointCallback};
	my $io_sub = $attrs->{Source};
	my $ctxt = $attrs->{Context};
	my $errlim = $attrs->{ErrorLimit};
	my $logtbls = $attrs->{LogTables};
	my $iobj = $dbh->{_iobj};
	$errlim = 1000000 unless $errlim;
	my $i = 0;

	my ($ldtbl, $errtbl1, $errtbl2);

	$sqlstmt=~tr/\t\n\r/ /;
	$ldtbl = $3
		if ($sqlstmt=~/^\s*USING\s+.+\s+(INSERT|INS)\s+(INTO\s+)?(\w+)(\.\w+)?[\s\(]/i);
	$ldtbl .= $4 if $4;

	return $iobj->io_set_error('FloadRun: Invalid INSERT statement.')
		unless $ldtbl;
#
#	form error tables from the load table; make sure the name fits
#
	$errtbl1 = ($logtbls) ? $$logtbls[0] :
		((length($ldtbl) > 25) ? substr($ldtbl, 0, 25) : $ldtbl) . '_err1';
	$errtbl2 = ($logtbls) ? $$logtbls[1] :
		((length($ldtbl) > 25) ? substr($ldtbl, 0, 25) : $ldtbl)  . '_err2';

	my $beginstmt = join ' ', 'BEGIN LOADING', $ldtbl, 'ERRORFILES',
		$errtbl1, ',', $errtbl2;
#
#	reset any prior fastload
#
	my $rc = $dbh->do("DROP TABLE $errtbl1");
	LoadCleanup($dbh, undef, undef, undef, $attrs),
	return undef
		unless (defined($rc) || ($dbh->err == 3807));

	$rc = $dbh->do("DROP TABLE $errtbl2");
	LoadCleanup($dbh, undef, undef, undef, $attrs),
	return undef
		unless (defined($rc) || ($dbh->err == 3807));
#
#	clear out old errors
#
	$dbh->DBI::set_err(undef);
#
#	in loopback, we need to prep the source SQL
#
	my $expsth;
	my $expdbh;
	if ($attrs->{Loopback}) {
		$expdbh = $attrs->{Source};
		$expsth = $expdbh->prepare($attrs->{Loopback}, { tdat_keepresp => 1 });
		return $iobj->io_set_error($expdbh->{_iobj}[TD_IMPL_LASTERR],
			$expdbh->{_iobj}[TD_IMPL_LASTEMSG], $expdbh->{_iobj}[TD_IMPL_LASTSTATE])
			unless $expsth;
	}
	my @dbhary = ();
	my ($retrysecs, $retries) =
		$attrs->{Retry} ?
			(ref $attrs->{Retry} ? @{$attrs->{Retry}} : ($attrs->{Retry}, -1)) :
			(0, 0);
	my ($errdbh, $stsfd);
	while (1) {
		($errdbh, $stsfd) = $attrs->{MP} ?
			($obj->isa('DBD::Teradata::ThreadedUtil') && (lc $attrs->{MP} eq 'threads')) ?
			$obj->ThrdUtilityLogon($dbh, \@dbhary, $sesscnt, $attrs, $sqlstmt, $expsth) :
			$obj->MPUtilityLogon($dbh, \@dbhary, $sesscnt, $attrs, $sqlstmt, $expsth) :
			$obj->UtilityLogon($dbh, \@dbhary, 'FASTLOAD', $sesscnt, $attrs);
#
#	if "No more tasks" && Retry defined, try again in a bit
#
		last if $errdbh;
		return undef
			unless ($dbh->{_iobj}[TD_IMPL_LASTERR] == 2633) && $retries;

		$attrs->{Report}->("Too many load/unload tasks; retrying in $retrysecs secs.")
			if $attrs->{Report};
		sleep $retrysecs;
		$retries-- if ($retries > 0);
	}
#
#	now start fastloading
#
	$dbh->{AutoCommit} = 0;
	$rc = $dbh->do($beginstmt);

	my ($errstr, $estate, $err);
	LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
	return undef
		unless defined($rc);

	my $sth = $dbh->prepare($sqlstmt);

	LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
	return undef
		unless $sth;
#
#	if loopback, fire up the export now
#
	if ($attrs->{Loopback}) {
#
#	send BT;BEGIN FASTEXPORT; on control session
#
		$expdbh->{AutoCommit} = 0;
		$rc = $expdbh->do('BEGIN FASTEXPORT;');
		LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
		return undef
			unless defined($rc);
#
#	Execute fastexport query on control session with KEEPRESP parcel.
#
		$rc = $expsth->execute;
		$iobj->io_set_error($expdbh->{_iobj}[TD_IMPL_LASTERR],
			$expdbh->{_iobj}[TD_IMPL_LASTEMSG],
			$expdbh->{_iobj}[TD_IMPL_LASTSTATE]),
		LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
		return undef unless defined($rc);

		$dbh->{_export_started} = 1;
	}

	my ($total_sent, $total_accepted) = $attrs->{MP} ?
		($obj->isa('DBD::Teradata::ThreadedUtil') && (lc $attrs->{MP} eq 'threads')) ?
		$obj->ThrdUtilityLoad($dbh, $errdbh, \@dbhary, $stsfd, [ $sth ], $attrs) :
		$obj->MPUtilityLoad($dbh, $errdbh, \@dbhary, $stsfd, [ $sth ], $attrs) :
		$obj->UtilityLoad($dbh, $errdbh, \@dbhary, [ $sth ], $attrs, $sqlstmt, { tdat_nowait => 1});

	$dbh->{_loading} = 0;

	LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
	return undef
		unless $total_sent;
#
#	Start END LOADING phase
#
	LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs, $total_accepted);
#
#	retrieve the error records and report them here
#
	$dbh->{AutoCommit} = 1;
#
#	if there were errors, return the number (as a negative value)
#	and leave error tables intact
#
	return ($total_accepted - $total_sent)
		if ($total_sent - $total_accepted != 0);
#
#	check how many rows actually made it
#
	my $dupsth = $dbh->prepare("SELECT COUNT(*) from $ldtbl");
	my $dupcnt = 0;
	$dupsth->bind_col(1, \$dupcnt);
	$dupsth->execute;
	$dupsth->fetch;
#
#	return the actual number of rows in the table,
#	which may be different than the number sent in case of dups
#
	$dbh->do("DROP TABLE $errtbl2");
	$dbh->do("DROP TABLE $errtbl1");

	return $dupcnt;
}

sub split_stmts {
	my ($str, $sqls) = @_;

	my @strlits;
	my @starts;
#
#	replace newlines with CRs
#	and locate all literal strings
#
	$str=~tr/\n/\r/;

	push(@strlits, $1),
	push(@starts, $-[1])
		while ($str=~/('[^']+')/g);
#
#	replace literal strings with whitespace
#
	foreach (0..$#starts) {
		substr($str, $starts[$_], length($strlits[$_])) = ' ' x length($strlits[$_]) if $strlits[$_];
	}
#
#	break string into individual stmts
#
	@$sqls = split(';', $str);
#
#	restore the literals...if any
#
	if (scalar @starts) {
		my $pos = 0;
		my $i = 0;
		foreach my $j (0..$#$sqls) {
			substr($$sqls[$j], $starts[$i] - $pos, length($strlits[$i])) = $strlits[$i],
			$i++
				while ($i <= $#starts) &&
					($starts[$i] >= $pos) && ($starts[$i] < $pos + length($$sqls[$j]));
#
#	adjust offset (including the semicolon)
#
			$pos += length($$sqls[$j]) + 1;
		}
#
#	trim everything
#
	}

	map { $sqls->[$_]=~s/^\s*(.+)\s*$/$1/; } 0..$#$sqls;

	return 1;
}
#
#	Multiload controller
#
sub MloadRun {
	my ($obj, $dbh, $sesscnt, $attrs) = @_;
	my $sqlstmt = $attrs->{SQL};
	my $ctxt = $attrs->{Context};
	my $srcdesc = $attrs->{SourceFields};
	my $i = 0;
	my $iobj = $dbh->{_iobj};

	return $iobj->io_set_error('MloadRun: SQL attribute must be arrayref of SQL requests.')
		unless $attrs->{SQL} && (ref $attrs->{SQL}) && (ref $attrs->{SQL} eq 'ARRAY');

	my $logtables = { };
	if ($attrs->{LogTables}) {
		foreach my $t (keys %{$attrs->{LogTables}}) {
			return $iobj->io_set_error('UtilitySetup: LogTables entries must be arrayrefs for MLOAD.')
				unless (ref $attrs->{LogTables}{$t}) &&
					(ref $attrs->{LogTables}{$t} eq 'ARRAY') &&
					($#{$attrs->{LogTables}{$t}} == 2);
			$logtables->{uc $t} = $attrs->{LogTables}{$t};
		}
	}
#
#	process mload directives within the SQL
#
	my @sqls = ();
	my @mloadflags = ();
	my @sqljobs = ();
	foreach my $s (0..$#{$attrs->{SQL}}) {
#
#	break into individual stmts
#
		split_stmts($attrs->{SQL}[$s], \@sqls);
		$sqljobs[$s] = [ ];
		$mloadflags[$s] = [ ];
#
#	setup flags for each job
#
		my $mlflags = pack('CCCCCCC', 1, 1, 1, 1, 0, 0, 0);
		my $upsert = 0;

		foreach my $q (0..$#sqls) {
			if ($sqls[$q]=~/^\s*(MARK|IGNORE)\s+(DUPLICATE|MISSING)\s+((INSERT|UPDATE|DELETE)\s+)?ROWS\s*$/i) {
#
#	validate/process directive
#
				my $val = ($1 eq 'MARK') ? 1 : 0;
				my $type = ($2 eq 'DUPLICATE') ? 0 : 2;
				my $qrytype = $4;

				return $iobj->io_set_error("UtilitySetup: Invalid MLOAD $1 directive.")
					if (((! $type) && ($qrytype eq 'DELETE')) ||
						($type && ($qrytype eq 'INSERT')));

				$type += 1
					if (((! $type) && ($qrytype eq 'UPDATE')) ||
						($type && ($qrytype eq 'DELETE')));
				substr($mlflags, $type, 1) = pack('C', $val);
				next;
			}
#
#	validate/process UPSERT
#
			substr($mlflags, 4, 1) = pack('C', 1),
			substr($mlflags, 2, 1) = pack('C', 0),
			$upsert = 1,
			next
				if ($sqls[$q]=~/^\s*DO\s+INSERT\s+FOR\s+(MISSING\s+UPDATE\s+)?ROWS\s*$/i);
#
#	(minimally) verify SQL and extract target table
#
			return $iobj->io_set_error('UtilitySetup: Invalid SQL statement for MLOAD.')
				unless (($sqls[$q]=~/^\s*(UPDATE)\s+(\S+)\s+/i) ||
					($sqls[$q]=~/^\s*INSERT\s+(INTO\s+)?([^\(\s]+)/i) ||
					($sqls[$q]=~/^\s*(DELETE)\s+FROM\s+([^\s;]+)/i));

			return $iobj->io_set_error("UtilitySetup: Table reference $2 not found in LogTables.")
				if ($attrs->{LogTables} && (! $logtables->{uc $2}));

			unless ($attrs->{LogTables} || $logtables->{uc $2}) {
				my $tbl = uc $2;
				$logtables->{$tbl} = [  ];
				my ($dbase, $tblstem) = ($tbl=~/^(\w+\.)?(\w+)$/);
				$tblstem = substr($tblstem, 0, 27) if (length($tblstem) > 27);
				$logtables->{$tbl} = ($dbase ? [
					$dbase . 'WT_' . $tblstem,
					$dbase . 'ET_' . $tblstem,
					$dbase . 'UV_' . $tblstem ] :
					[
					'WT_' . $tblstem,
					'ET_' . $tblstem,
					'UV_' . $tblstem ]);
			}
#
#	cluster statements into jobs
#
			push(@{$sqljobs[$s]}, $sqls[$q]);
		}
#
#	validate UPSERT job
#
		return $iobj->io_set_error('UtilitySetup: Invalid SQL for MLOAD UPSERT operation.')
			if ($upsert && (($#{$sqljobs[$s]} != 1) ||
				($sqljobs[$s][0]!~/^\s*UPDATE\s+/i) ||
				($sqljobs[$s][1]!~/^\s*INSERT\s+/i)));
#
#	generate final flags/SQL for each stmt in job
#
		push (@{$mloadflags[$s]}, pack('CCCa7', 1, $s+1, $_+1, $mlflags)),
		$sqljobs[$s][$_] = $attrs->{SourceFields} . ' ' . $sqljobs[$s][$_]
			foreach (0..$#{$sqljobs[$s]});
	}
#
#	generate control stmts
#
	my @loadtables = keys %$logtables;
	my $beginstmt = 'BEGIN MLOAD ' . join(', ', @loadtables) . ';';
	my $execstmt = 'EXEC MLOAD ' . join('; EXEC MLOAD ', @loadtables) . ';';
	my @mloadstmts = ();
	map { push(@mloadstmts,
		join('', "MLOAD $_ WITH ", $logtables->{$_}[0],
			' ERRORTABLES ', $logtables->{$_}[1], ', ',
			$logtables->{$_}[2], ';')); } @loadtables;

	$iobj->io_set_error('UtilitySetup: More than 5 tables used in MLOAD.')
		if ($#mloadstmts > 4);

	map {
		$dbh->do('drop table ' . $logtables->{$_}[0]);
		$dbh->do('drop table ' . $logtables->{$_}[1]);
		$dbh->do('drop table ' . $logtables->{$_}[2]);
	} @loadtables;
#
#	clear out old errors from drop tables
#
	$dbh->DBI::set_err(undef);

	$dbh->{_release_stmt} = 'RELEASE MLOAD ' . join(', ', @loadtables) . ';';
#
#	provide jobcount to our internal IO subs
#
	$ctxt->{_JobCount} = scalar @sqljobs
		if $ctxt->{_JobCount};
#
#	validation done, logon the utility sessions
#
	my $expdbh;
	my $expsth;
	if ($attrs->{Loopback}) {
#
#	prepare the fastexport SELECT now since we can't do it after
#	BEGIN FASTEXPORT; besides, this gives us the column names
#	and types
#
		$expdbh = $attrs->{Source};
		$expsth = $expdbh->prepare($attrs->{Loopback}, { tdat_keepresp => 1 });
		return $iobj->io-set_error($expdbh->{_iobj}[TD_IMPL_LASTERR],
			$expdbh->{_iobj}[TD_IMPL_LASTEMSG], $expdbh->{_iobj}[TD_IMPL_LASTSTATE])
			unless $expsth;
	}

	my @dbhary = ();
	my ($retrysecs, $retries) =
		$attrs->{Retry} ?
			(ref $attrs->{Retry} ? @{$attrs->{Retry}} : ($attrs->{Retry}, -1)) :
			(0, 0);
	my ($errdbh, $stsfd);
	while (1) {
		($errdbh, $stsfd) = $attrs->{MP} ?
			($obj->isa('DBD::Teradata::ThreadedUtil') && (lc $attrs->{MP} eq 'threads')) ?
			$obj->ThrdUtilityLogon($dbh, \@dbhary, $sesscnt, $attrs,
				$attrs->{SourceFields} . ' INSERT INTO', $expsth, scalar @sqljobs) :
			$obj->MPUtilityLogon($dbh, \@dbhary, $sesscnt, $attrs,
				$attrs->{SourceFields} . ' INSERT INTO', $expsth, scalar @sqljobs) :
			$obj->UtilityLogon($dbh, \@dbhary, 'MLOAD', $sesscnt, $attrs);
#
#	if "No more tasks" && Retry defined, try again in a bit
#
		last if $errdbh;
		return undef
			unless ($dbh->{_iobj}[TD_IMPL_LASTERR] == 2633) && $retries;

		$attrs->{Report}->("Too many load/unload tasks; retrying in $retrysecs secs.")
			if $attrs->{Report};
		sleep $retrysecs;
		$retries-- if ($retries > 0);
	}

# our record sequencing values and job count
	my @mlseqary = (1, 1, scalar @sqljobs);

	$dbh->{AutoCommit} = 0;

	my @sths = ();
	foreach my $s (0..$#sqljobs) {
		map {
			push @sths, $dbh->prepare($sqljobs[$s][$_],
				{ tdat_mload => $mloadflags[$s][$_] });
		} 0..$#{$sqljobs[$s]};
	}
#
#	now start multiloading
#
	$dbh->{AutoCommit} = 0;
	my $rc = $dbh->do($beginstmt);

	LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
	return undef
		unless defined($rc);

	$dbh->{_loading} = 1;

	foreach my $s (@mloadstmts) {
		$rc = $dbh->do($s);
		LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
		return undef
			unless defined($rc);
	}
#
#	if loopback, fire up the export now
#
	if ($attrs->{Loopback}) {
#
#	send BT;BEGIN FASTEXPORT; on control session
#
		$expdbh->{AutoCommit} = 0;
		$rc = $expdbh->do('BEGIN FASTEXPORT;');
		LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
		return undef
			unless defined($rc);
#
#	Execute fastexport query on control session with KEEPRESP parcel.
#
		$rc = $expsth->execute;
		$iobj->io_set_error($expdbh->{_iobj}[TD_IMPL_LASTERR],
			$expdbh->{_iobj}[TD_IMPL_LASTEMSG],
			$expdbh->{_iobj}[TD_IMPL_LASTSTATE]),
		LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
		return undef
			unless defined($rc);

		$dbh->{_export_started} = 1;
	}

	my ($total_sent, $total_accepted) = $attrs->{MP} ?
		($obj->isa('DBD::Teradata::ThreadedUtil')  && (lc $attrs->{MP} eq 'threads')) ?
			$obj->ThrdUtilityLoad($dbh, $errdbh, \@dbhary, $stsfd, \@sths, $attrs) :
			$obj->MPUtilityLoad($dbh, $errdbh, \@dbhary, $stsfd, \@sths, $attrs) :
		$obj->UtilityLoad($dbh, $errdbh, \@dbhary, \@sths, $attrs,
			$attrs->{SourceFields} . ' INSERT INTO',
			{ tdat_nowait => 1, tdat_mlseq => \@mlseqary });

	$dbh->{_loading} = 0;

	LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
	return undef
		unless $total_sent;
#
#	Start EXEC MLOAD phase (apply phase)
#
	$dbh->do('CHECKPOINT LOADING END;');
	$dbh->commit;
	$dbh->{AutoCommit} = 1;
#
#	EXEC MLOAD behaves like single row select; supply each loaded table
#	this starts apply phase, we eventually need a way to notify app
#	and maybe run this async...
#	note that this will look like a multistmt request
#
	$attrs->{Report}->("MLOAD Acquisition complete, starting apply phase.")
		if $attrs->{Report};
	my $sth = $dbh->prepare($execstmt);
	$sth->execute;

	while ($sth->{tdat_more_results}) {
		my $starts = $sth->{tdat_stmt_info}[$sth->{tdat_stmt_num}]{StartsAt};
		my $row;
		while ($row = $sth->fetchrow_arrayref) {
			$attrs->{ActivityCounts}{shift @loadtables} =
				{
					Inserts => $$row[$starts],
					Updates => $$row[$starts+1],
					Deletes => $$row[$starts+2]
				}
				if $attrs->{ActivityCounts};

		}
	}

	$dbh->{AutoCommit} = 0;
	$dbh->do('END MLOAD;');
	$dbh->commit;
	$attrs->{Report}->("MLOAD Apply phase complete.")
		if $attrs->{Report};
#
#	we should do something with the returned rows....
#
	$dbh->{AutoCommit} = 1;
	LoadCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs, $total_sent);
#
#	remove work tables
#
	my $errcnt = 0;
	my $dupcnt = 0;
	my $ec = 0;
	foreach my $tbl (@loadtables) {
		$dbh->do('drop table ' . $logtables->{$tbl}[0]);
#
#	if there were errors, return the number (as a negative value)
#	and leave error tables intact
#
		$sth = $dbh->prepare('select count(*) from ' . $logtables->{$tbl}[1]);
		$sth->bind_col(1, \$ec),
		$sth->execute,
		$sth->fetch,
		$errcnt += $ec
			if $sth;

		$sth = $dbh->prepare('select count(*) from ' . $logtables->{$tbl}[2]);
		$sth->bind_col(1, \$ec),
		$sth->execute,
		$sth->fetch,
		$dupcnt += $ec
			if $sth;
	}
#
#	keep tables if there were errors or dups
#
	foreach (@loadtables) {
		$dbh->do('drop table ' . $logtables->{$_}[1]) unless $errcnt;
		$dbh->do('drop table ' . $logtables->{$_}[2]) unless $dupcnt;
	}

	return $errcnt ? ($errcnt * -1) : $total_sent ;
}

sub LoadCleanup {
	my ($dbh, $errdbh, $dbhary, $stsfd, $attrs, $total) = @_;

	my ($errstr, $estate, $err) = ($dbh->{_iobj}[TD_IMPL_LASTEMSG],
		$dbh->{_iobj}[TD_IMPL_LASTSTATE], $dbh->{_iobj}[TD_IMPL_LASTERR]);
#
#	how do PAUSES work ?
#
	$dbh->{_expdbh}->do('END EXPORT;'),
	$dbh->{_expdbh}->commit
		if $dbh->{_export_started};

	unless ($dbh->{_loading} || $dbh->{_release_stmt}) {
		$dbh->do('CHECKPOINT LOADING END;');
		$dbh->commit;
#
#	fastload only:
		$dbh->do('END LOADING;');
		$dbh->commit();

		$dbh->{AutoCommit} = 1;
	}

	$errdbh->disconnect if $errdbh;

	my $i;
	my $fd;
	if ($attrs->{MP} && $stsfd) {
		if (ref $stsfd eq 'Thread::Queue') {
			map {
				$_->{CmdQ}->enqueue('FINISH');
				$_->{CmdQ}->enqueue('EXIT');
			} @$dbhary;
			foreach (@$dbhary) {
				$_->{Thread}->join if $_->{Thread};
			}
		}
		else {
			map {
				$fd = $_->{CmdPipe};
				print $fd "FINISH\nLOGOFF\nEXIT\n";
				close $fd;
			} @$dbhary;
			close $stsfd;
		}
	}
	else {
		$_->disconnect
			foreach (@$dbhary);

		$attrs->{Source}->('FINISH', undef, $_, 0, $attrs->{Context})
			foreach (0..$#$dbhary);
	}
#
#	mload only; have to wait for MLOAD sessions to disconnect
#
	$dbh->do($dbh->{_release_stmt})
		if $dbh->{_loading} && $dbh->{_release_stmt};

	$attrs->{CheckpointCallback}->('FINISH', $total, $attrs->{Context})
		if $attrs->{CheckpointCallback};

	($dbh->{_iobj}[TD_IMPL_LASTERR], $dbh->{_iobj}[TD_IMPL_LASTSTATE],
		$dbh->{_iobj}[TD_IMPL_LASTEMSG]) = ($err, $estate, $errstr)
		if $err;
}

#
#	dummy iosub if input handle was a file
#
sub LoadRawFile {
	my ($function, $sth, $sessnum, $maxrows, $ctxt, $offset) = @_;

	my $rec;
	if ($function eq 'INIT') {
		$sth->{tdat_raw_in} = $ctxt->{Mode};
		return -1 if $ctxt->{_Handle};
#
#	only open if not already open
#
		my $fd;
		return undef
			unless open($fd, '<' . $ctxt->{_Filename}) ;
		$ctxt->{_Handle} = $fd;
		binmode $fd;
		$ctxt->{_NextRecord} = [ ];	# to save overflow records for each session
#
#	skip over records as needed
#
		LoadReadRaw($fd, $ctxt->{_Skip}-1, \$rec, $ctxt, $sessnum)
			if $ctxt->{_Skip};
		return -1;
	}

	if ($function eq 'FINISH') {
		close $ctxt->{_Handle} if $ctxt->{_Handle};
		return -1;
	}

	return -1
		if ($function eq 'CHECKPOINT');

	my ($rc, $rowcnt) = (0,0);
	my @ary = ();
	my $fd = $ctxt->{_Handle};
	my $len;
	my $pad = ($sth->{_private}[TD_STH_IOBJ][TD_IMPL_SESPART] == TD_PART_MLOAD) ? 11 : 1;
	my $skip = defined($offset) ? ($ctxt->{_InitFile} ? $offset : $sessnum) : 0;
	my $jobcnt = $ctxt->{_JobCount} || 1;
	$ctxt->{_InitFile} = 1;
	my $dbh = $sth->{Database};
	my $avail = $dbh->{tdat_uses_cli} ? 64000 :
		($sth->{Database}{tdat_reqsize} || 64000);
	$avail -= 100;
	while ($rowcnt < $maxrows) {
#
#	read records from the source file until maxrows or EOF or IO error
#	or we know we've reached the limit of the output buffer
#
		$rc = LoadReadRaw($fd, $skip, \$rec, $ctxt, $sessnum);
		return undef unless defined($rc);
		last unless $rc;
#
#	if this will exceed out buffer, save it for next time
#
		$len = ($rc + $pad) * $jobcnt;
		$ctxt->{_NextRecord}[$sessnum] = $rec,
		last
			if ($len > $avail);
		push @ary, $rec;
		$rowcnt++;

		$avail -= $len;
		$skip = defined($offset) ? $offset : 0;
	}
#
#	now bind it and return
#
	$sth->bind_param_array(1, \@ary,
		{ TYPE => SQL_VARBINARY, PRECISION => 64000 });
	return $#ary + 1;
}

sub LoadReadRaw {
	my ($fd, $skip, $rec, $ctxt, $sessnum) = @_;
#
#	read next record from the source file
#
	my ($rc, $len);
#
#	if we had a leftover last time, use it now
#
	$$rec = $ctxt->{_NextRecord}[$sessnum],
	$ctxt->{_NextRecord}[$sessnum] = undef,
	return length($$rec)
		if $ctxt->{_NextRecord}[$sessnum];

	while (1) {
		$rc = read $fd, $len, 2, 0;
		return undef unless defined($rc);
		return 0 unless $rc;	# EOF

		$len = unpack('S', $len);
		$len++;	# for newline
		$rc = read $fd, $$rec, $len, 0;
		return undef unless ($rc || ($rc == $len));

		$skip--, next if $skip;

		$len--;
		$$rec = pack('Sa*', $len, $$rec);
		return $rc + 2;
	}
}

sub LoadVartext {
	my ($function, $sth, $sessnum, $maxrows, $ctxt, $offset) = @_;

	my $rec;
	if ($function eq 'INIT') {
		$sth->{tdat_vartext_in} = $ctxt->{_Separator};
		return -1 if $ctxt->{_Handle};
#
#	only open if not already open
#
		my $fd;
#
#	For UTF8: open with PerlIO encoding
#
		my $arrow = ($sth->tdat_CharSet eq 'UTF8') ? '<:utf8' : '<';
		return undef
			unless open($fd, $arrow, $ctxt->{_Filename}) ;
		$ctxt->{_Handle} = $fd;
		$ctxt->{_NextRecord} = [ ];	# to save overflow records for each session
#
#	skip over records as needed
#
		LoadReadVartext($fd, $ctxt->{_Skip}-1, \$rec, $ctxt, $sessnum)
			if $ctxt->{_Skip};
		return -1;
	}

	if ($function eq 'FINISH') {
		close $ctxt->{_Handle} if $ctxt->{_Handle};
		return -1;
	}

	return -1
		if ($function eq 'CHECKPOINT');

	my ($rc, $rowcnt) = (0,0);
	my @ary = ();
	my $i = 0;
	my $numflds;
	my $fd = $ctxt->{_Handle};
	my $len;
	my $skip = defined($offset) ? ($ctxt->{_InitFile} ? $offset : $sessnum) : 0;
	$ctxt->{_InitFile} = 1;
	my $dbh = $sth->{Database};
	my $avail = $dbh->{tdat_uses_cli} ? 64000 :
		($sth->{Database}{tdat_reqsize} || 64000);
	$avail -= 100;
	my $numparms = $sth->{NUM_OF_PARAMS};
	my $jobcnt = $ctxt->{_JobCount} || 1;
	my $fixedpart = 4 +					# parcel header
				(2 * $numparms) +		# space for varchar hdr
				int($numparms/8) + 1 -	# and indicbits
				(length($ctxt->{_Separator}) * ($numparms-1)); # minus separator

	$fixedpart += 10 if ($sth->{_private}[TD_STH_IOBJ][TD_IMPL_SESPART] == TD_PART_MLOAD);
	while ($rowcnt < $maxrows) {
#
#	read records from the source file until maxrows or EOF or IO error
#	or we know we've reached the limit of the output buffer
#
		$rc = LoadReadVartext($fd, $skip, \$rec, $ctxt, $sessnum);
		return undef unless defined($rc);
		last unless ($rc);
#
#	compute size-in-buffer of record:
#
		$len = ($fixedpart + length($rec)) * $jobcnt;
#
#	if this will exceed our buffer, save it for next time
#
		$ctxt->{_NextRecord}[$sessnum] = $rec,
		last
			if ($len > $avail);

		push @ary, $rec;
		$rowcnt++;

		$avail -= $len;
		$skip = defined($offset) ? $offset : 0;
	}
#
#	bind and return
#
	$sth->bind_param_array(1, \@ary) if $rowcnt;
	return $#ary + 1;
}

sub LoadReadVartext {
	my ($fd, $skip, $rec, $ctxt, $sessnum) = @_;
#
#	read next record from the source file
#
	my ($rc, $len);
#
#	if we had a leftover last time, use it now
#
	$$rec = $ctxt->{_NextRecord}[$sessnum],
	$ctxt->{_NextRecord}[$sessnum] = undef,
	return length($$rec)
		if $ctxt->{_NextRecord}[$sessnum];

	while (1) {
		$$rec = <$fd>;
		return 0 unless defined($$rec);

		chomp $$rec,
		return length($$rec) unless $skip;
		$skip--;
	}
}

sub ExportCleanup {
	my ($dbh, $errdbh, $dbhary, $stsfd, $attrs, $total) = @_;
	my ($errstr, $estate, $err) = ($dbh->{_iobj}[TD_IMPL_LASTEMSG],
		$dbh->{_iobj}[TD_IMPL_LASTSTATE], $dbh->{_iobj}[TD_IMPL_LASTERR]);

	$dbh->do('END EXPORT;');
	$dbh->commit;

	$errdbh->disconnect;

	my $i;
	my $fd;
	if ($stsfd) {
		if (ref $stsfd eq 'Thread::Queue') {
			$_->{CmdQ}->enqueue('FINISH'),
			$_->{CmdQ}->enqueue('EXIT')
				foreach (@$dbhary);

			foreach (@$dbhary) {
				$_->{Thread}->join if $_->{Thread};
			}
		}
		else {
			foreach (@$dbhary) {
				$fd = $_->{CmdPipe};
				print $fd "FINISH\nLOGOFF\nEXIT\n";
				close $fd;
			}
			close $stsfd;
		}
	}
	else {
		$_->disconnect
			foreach (@$dbhary);

		$attrs->{Target}->('FINISH', undef, $_, 0, $attrs->{Context})
			foreach (0..$#$dbhary);

		$attrs->{CheckpointCallback}->('FINISH', $total, $attrs->{Context})
			if $attrs->{CheckpointCallback};
	}

	($dbh->{_iobj}[TD_IMPL_LASTERR], $dbh->{_iobj}[TD_IMPL_LASTSTATE],
		$dbh->{_iobj}[TD_IMPL_LASTEMSG]) = ($err, $estate, $errstr)
		if $err;
}

sub ExportRun {
	my ($obj, $dbh, $sesscnt, $attrs) = @_;
	my $sqlstmt = $attrs->{SQL};
	my $logtbl = $attrs->{LogTables};
	my $chkpt_sub = $attrs->{CheckpointCallback};
	my $io_sub = $attrs->{Target};
	my $ctxt = $attrs->{Context};
	my $i = 0;
	my $iobj = $dbh->{_iobj};
#
#	prepare the fastexport SELECT now since we can't do it after
#	BEGIN FASTEXPORT; besides, this gives us the column names
#	and types
#
	my $expsth = $dbh->prepare($sqlstmt, { tdat_keepresp => 1 });
	return undef unless $expsth;
#
#	logon utility sessions
#
	my @dbhary = ();
	my ($retrysecs, $retries) =
		$attrs->{Retry} ?
			(ref $attrs->{Retry} ? @{$attrs->{Retry}} : ($attrs->{Retry}, -1)) :
			(0, 0);
	my ($errdbh, $stsfd);
	while (1) {
		($errdbh, $stsfd) = $attrs->{MP} ?
			($obj->isa('DBD::Teradata::ThreadedUtil') && (lc $attrs->{MP} eq 'threads')) ?
			$obj->ThrdUtilityLogon($dbh, \@dbhary, $sesscnt, $attrs, $expsth) :
			$obj->MPUtilityLogon($dbh, \@dbhary, $sesscnt, $attrs, $expsth) :
			$obj->UtilityLogon($dbh, \@dbhary, 'EXPORT', $sesscnt, $attrs);
#
#	if "No more tasks" && Retry defined, try again in a bit
#
		last if $errdbh;
		return undef
			unless ($dbh->{_iobj}[TD_IMPL_LASTERR] == 2633) && $retries;

		$attrs->{Report}->("Too many load/unload tasks; retrying in $retrysecs secs.")
			if $attrs->{Report};
		sleep $retrysecs;
		$retries-- if ($retries > 0);
	}
#
#	send BT;BEGIN FASTEXPORT; on control session
#
	$dbh->{AutoCommit} = 0;

	my $rc = $dbh->do('BEGIN FASTEXPORT;');
	ExportCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
	return undef
		unless defined($rc);
#
#	Execute fastexport query on control session with KEEPRESP parcel.
#
	$rc = $expsth->execute;
	ExportCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
	return undef
		unless defined($rc);
#
#	if loopback mode, return here
#
	return $errdbh if $attrs->{Loopback};
#
#	send START on each export session with PclREQUEST body set to
#		 (integer 1)(integer ordinal export part number) and a max
#		RESP parcel.
#
#	DBMS returns response with PclSUCCESS followed by N PclRECORD parcels
#
	$chkpt_sub->('INIT', $#dbhary + 1, $ctxt) if $chkpt_sub;

	my $total = 0;
	my $activecnt = 0;
	my $status = 0;
	my $fd;
	my ($err, $len, $errstr);
	if ($attrs->{MP}) {
		my $usethrds = ($attrs->{MP} eq 'threads');
		my @avail = (1) x scalar(@dbhary);
		my $bundle = 0;
		while (1) {
#
#	start each child with a fixed number of records
#
			$activecnt = 0;
			foreach $i (@avail) { $activecnt += $i };
			last unless $activecnt;

			foreach (0..$#dbhary) {
				next unless $avail[$_];
				if ($usethrds) {
					$dbhary[$_]{CmdQ}->enqueue('GO');
				}
				else {
					$fd = $dbhary[$_]{CmdPipe};
					print $fd "GO\n";
				}
			}
#
#	now wait for them to reply
#
			my ($id, $dmy, $okcnt, $errcnt, $err, $errstr);
			if ($usethrds) {
				while ($activecnt) {
					$status = $stsfd->dequeue;
					chomp $status;
					($id, $dmy, $okcnt, $errcnt, $dmy, $err, $errstr) =
				($status=~/^(\d+)\s+(READY\s+(\d+)\s+(\d+))|(ERROR\s+(\d+)\s+(.+))$/);
					$activecnt--;
					$avail[$id] = defined($err) ? 0 : 1;
					next unless $avail[$id];

					$dbhary[$id]{OkCount} += $okcnt;
					$dbhary[$id]{ErrCount} += $errcnt;
					$total += ($okcnt + $errcnt);
					print "Export Master: sess $id reports $okcnt recvd, $errcnt failed, total $total\n"
						if $dbh->{_debug};
					$avail[$id] = 0
						unless ($okcnt + $errcnt); # source exhausted
				}
			}
			else { # process mode
				while ($activecnt) {
					$iobj->io_set_error("Can't read pipe: $!"),
					last
						unless read $stsfd, $status, 1, 0;
					$status = unpack('C', $status);
					$id = $status & 127;
					$activecnt--;
					$avail[$id] = ($status & 128) ? $status : 1;
				}

				foreach my $id (0..$#avail) {
					next unless $avail[$id];
					$status = $avail[$id];
#
#	get the OK/Error count
#
					$fd = $dbhary[$id]{CmdPipe};

					if ($status & 128) {
						print $fd "REPORT\n";
						$iobj->io_set_error("Can't read pipe: $!"),
						last
							unless read $stsfd, $status, 4, 0;

						($err, $len) = unpack('SS', $status);
						$iobj->io_set_error("Can't read pipe: $!"),
						last
							unless read $stsfd, $errstr, $len, 0;
#
#	not certain what to do with failures at this point
#
						$avail[$id] = 0;
						next;
					}

					print $fd "STATUS\n";
					$iobj->io_set_error("Can't read pipe: $!"),
					last
						unless read $stsfd, $status, 8, 0;

					($okcnt, $errcnt) = unpack('LL', $status);
					$dbhary[$id]{OkCount} += $okcnt;
					$dbhary[$id]{ErrCount} += $errcnt;
					$total += ($okcnt + $errcnt);
					print "Master: sess $id reports $okcnt recvd, $errcnt failed, total $total\n"
						if $dbh->{_debug};
					$avail[$id] = 0
						unless ($okcnt + $errcnt); # source exhausted
				} # end foreach avail
			} # end if usethrds...else...
		}	# end while active sessions...
	}
	else {
#
#	single process mode:
#
		my @sthary = ();
		my $seqnum = 1;
		my $qnum = 1;
		my @outlist = (0);
		foreach $i (0..$#dbhary) {
			$sthary[$i] = $dbhary[$i]->prepare(';', {
				tdat_nowait => 1,
				tdat_clone => $expsth
			});
			ExportCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs),
			return undef
				unless defined($sthary[$i]);
#
#	init everybody
#
			$attrs->{Report}->("Initializing session $i") if $attrs->{Report};
			$rc = $io_sub->('INIT', $sthary[$i], $i, 0, $ctxt);
#
#	2 params need to be bound: the query number, and the sequence number
#
			no strict;
			$sthary[$i]->bind_param_inout(1, \$qnum, SQL_INTEGER);
			$sthary[$i]->bind_param_inout(2, \$seqnum, SQL_INTEGER);
			use strict;

			$sthary[$i]->execute;
			$activecnt++;
			$seqnum++;
		}

		my $done = 0;
		my $drh = $dbh->{Driver};
		while ($activecnt > 0) {
			@outlist = $drh->tdat_FirstAvailList(\@dbhary);
			last unless @outlist;

			foreach $i (@outlist) {
				next unless defined($i) && $dbhary[$i]{tdat_active};
				$rc = $sthary[$i]->tdat_Realize();

				my $sthpriv = $sthary[$i]{_private};

				$activecnt--,
				$done ||= (defined($sthary[$i]->err) && ($sthary[$i]->err == 2588)),
				next
					unless defined($rc);
#
#	continuously fetch and send to callback
#	(smart apps will bind column arrays so we only fetch() once!)
#
				$sthary[$i]->finish,
				$activecnt--,
				next
					if $sthpriv->[TD_STH_FAILED];

				my $stmtno = $sthary[$i]{tdat_stmt_num};
				my $stmtinfo = $sthary[$i]{tdat_stmt_info};
				my $stmthash = $$stmtinfo[$stmtno];
				warn "stmtno $stmtno"
					unless $stmthash->{ActivityCount};

				$total += $stmthash->{ActivityCount};
				while ($sthary[$i]->fetch) {
					$rc = $io_sub->('MOREDATA', $sthary[$i], $i,
						($sthpriv->[TD_STH_COLARY] ?
							$#{$sthpriv->[TD_STH_COLARY][0]} + 1 : 1), $ctxt);
					$sthary[$i]->finish,
					$activecnt--,
					last unless ($rc);
					$attrs->{Report}->("Saved $rc records from session $i")
						if $attrs->{Report};
				}
				last unless $activecnt;
				$seqnum++;
			}
		}
	}

	ExportCleanup($dbh, $errdbh, \@dbhary, $stsfd, $attrs);
	return $total;
}
#
#	dummy iosub if target was a file
#
sub SaveRawFile {
	my ($function, $sth, $sessnum, $maxrows, $ctxt, $offset) = @_;

	if ($function eq 'INIT') {
		$sth->{tdat_raw_out} = $ctxt->{Mode};
		$ctxt->{_ColAry} = [ ] unless $ctxt->{_ColAry};
		$ctxt->{_ColAry}[$sessnum] = [ ];
		$sth->tdat_BindColArray(1, $ctxt->{_ColAry}[$sessnum], 1000);
		return -1 if $ctxt->{_Handle};

		my $fd;
		return undef unless open($fd, '>' . $ctxt->{_Filename}) ;
		binmode $fd;
		$ctxt->{_Handle} = $fd;
		return -1;
	}

	if ($function eq 'FINISH') {
		close $ctxt->{_Handle};
		return -1;
	}

	return -1
		if ($function eq 'CHECKPOINT');

	my $rowcnt = 0;
	my $fd = $ctxt->{_Handle};
	my $ary = $ctxt->{_ColAry}[$sessnum];
#
#	currently no way to skip for MP case; need to just
#	open multiple files and then append them all when done
#
	$rowcnt++,
	print $fd $_
		foreach (@$ary);

	return $rowcnt;
}

sub SaveVartext {
	my ($function, $sth, $sessnum, $maxrows, $ctxt, $offset) = @_;

	return -1
		if ($function eq 'CHECKPOINT');

	if ($function eq 'INIT') {
		$sth->{tdat_vartext_out} = $ctxt->{_Separator};
		$ctxt->{_ColAry} = [ ] unless $ctxt->{_ColAry};
		$ctxt->{_ColAry}[$sessnum] = [ ];
		$sth->tdat_BindColArray(1, $ctxt->{_ColAry}[$sessnum], 1000);
		return -1 if $ctxt->{_Handle};

		my $fd;
		my $arrow = ($sth->tdat_CharSet eq 'UTF8') ? '>:utf8' : '>';
		return undef unless open($fd, $arrow, $ctxt->{_Filename}) ;
		$ctxt->{_Handle} = $fd;
		return -1;
	}

	if ($function eq 'FINISH') {
		close $ctxt->{_Handle};
		return -1;
	}

	my $rowcnt = $ctxt->{_RowCount};
	my $fd = $ctxt->{_Handle};
	my $ary = $ctxt->{_ColAry}[$sessnum];
#
#	currently no way to skip for MP case; need to just
#	open multiple files and then append them all when done
#
	$rowcnt++,
	print $fd $$ary[$_], "\n"
		foreach (0..$maxrows-1);

	return $rowcnt;
}

sub MPChild {
	my ($mstrdbh, $id, $offset, $stsfd, $cmdfd, $attrs, $sql, $lbsth, $jobcount) = @_;
	my $sqlstmt = $attrs->{SQL};
	my $io_sub = ($attrs->{Utility} eq 'EXPORT') ? $attrs->{Target} :
		$attrs->{Loopback} ? undef : $attrs->{Source};
	my $ctxt = $attrs->{Context};
	my $lbdbh;
	$lbdbh = $attrs->{Source} if $attrs->{Loopback};
	my $i = 0;
#
#	make sure status pipe autoflushes
#
	my $old = select $stsfd;
	$| = 1;
	select $old;
#
#	we might have inherited some pre-existing DBH's, so
#	flag them as "virtual" so we don't try sending something
#	on them
#
	my $drh = $DBD::Teradata::drh;
	foreach my $dbh (values %{$drh->{_connections}}) {
		$dbh->{_ignore_destroy} = 1
			if $dbh;
	}

	print "Child $id: logging on...\n" if $mstrdbh->{_debug};
	$attrs->{Report}->("Logging on child $id") if $attrs->{Report};

	my ($dbh,$sth, $expdbh, $expsth);
	my $no_cli = (!$mstrdbh->{tdat_uses_cli});
	my $no_bigint = $mstrdbh->{tdat_no_bigint};
	$dbh = DBI->connect('dbi:Teradata:' . $mstrdbh->{Name},
		 $mstrdbh->{USER}, $mstrdbh->{tdat_password},
		{
			PrintError => 0,
			RaiseError => 0,
			AutoCommit => 0,
			tdat_lsn => $mstrdbh->{tdat_lsn},
			tdat_utility => $attrs->{Utility},
			tdat_charset => $mstrdbh->{tdat_charset},
			tdat_reqsize => $attrs->{RequestSize},
			tdat_no_cli => $no_cli,
			tdat_no_bigint => $no_bigint,
		}
	);

	if ($dbh) {
		_announceConnection($attrs->{Report}, $dbh, $attrs->{Utility})
			if $attrs->{Report};
		print "Child $id: preparing...\n" if $dbh->{_debug};
		$sth = ($attrs->{Utility} eq 'EXPORT') ?
			$dbh->prepare(';', { tdat_clone => $sql }) :
			$dbh->prepare($sql, { tdat_mlseq => [ $id, $offset, $jobcount ] });
			$sth->{tdat_raw_in} = 'IndicatorMode' if ($sth && $attrs->{Loopback});
	}
	else {
#
#	send status up to master if error
#
		print "Child $id: logon failed...\n" if $mstrdbh->{_debug};
		print $stsfd pack('C', $id + 128)
	}
#
#	send status up to master
#
	if ($dbh && (! $sth)) {
		print "Child $id: prepare failed\n" if $dbh->{_debug};
		print $stsfd pack('C', $id + 128);
	}

 	if ($dbh && $attrs->{Loopback}) {
		$no_cli = (!$lbdbh->{tdat_uses_cli});
		$no_bigint = $lbdbh->{tdat_no_bigint};
 		$expdbh = DBI->connect('dbi:Teradata:' . $lbdbh->{Name},
			 $lbdbh->{USER}, $lbdbh->{tdat_password},
			{
				PrintError => 0,
				RaiseError => 0,
				AutoCommit => 0,
				tdat_lsn => $lbdbh->{tdat_lsn},
				tdat_utility => 'EXPORT',
				tdat_charset => $lbdbh->{tdat_charset},
				tdat_no_cli => $no_cli,
				tdat_no_bigint => $no_bigint,
			}
		);
#
#	send status up to master if error
#
		unless ($expdbh) {
			print "Child $id: logon failed...\n" if $mstrdbh->{_debug};
			print $stsfd pack('C', $id + 128)
		}
		else {
			_announceConnection($attrs->{Report}, $expdbh, 'EXPORT')
				if $attrs->{Report};
			print "Child $id: preparing...\n" if $expdbh->{_debug};
			$expsth = $expdbh->prepare(';', {
				tdat_clone => $lbsth,
				tdat_raw_out => 'IndicatorMode' });
		}
#
#	send status up to master
#
		if ($expdbh && (! $expsth)) {
			print "Child $id: prepare failed\n" if $dbh->{_debug};
			print $stsfd pack('C', $id + 128);
		}
	}
	print $stsfd pack('C', $id)
		if (($dbh && $sth) &&
			((! $attrs->{Loopback}) || ($expdbh && $expsth)));
	($expdbh, $expsth) = ($dbh, $sth) if ($attrs->{Utility} eq 'EXPORT');
#
#	now wait for a command
#
	my ($okcnt,$errcnt) = 0;
	my $initio = undef;
	my $rc;
	my ($qnum, $seqnum) = (1, $id+1);
	my ($stmtno, $stmthash, $stmtinfo);
	my $total_sent = 0;
	my @colary = ();
	my $rowcount = 0;
	print "Child $id: waiting for command\n" if $mstrdbh->{_debug};
	my $export_started = 0;
	while (<$cmdfd>) {
		chomp;
		print "Child $id: Got $_ command...\n" if $mstrdbh->{_debug};
		last if ($_ eq 'EXIT');
#
#	since the offset we are provided might not be the final
#	offset (due to using up the AMPs), we have toget the
#	real offset here
		if (/^OFFSET (\d+)$/i) {
			$offset = $1;
			$sth->{tdat_mlseq} = [ $id, $offset, $jobcount ];
			next;
		}
#
#	force logoff; required for MP w/ CLI since child will kill
#	control session connections <sigh/>, so we have to sequence
#	shutdown differently
#
		if ($_ eq 'LOGOFF') {
			$dbh->disconnect
				if $dbh && ((! defined($expdbh)) || ($dbh ne $expdbh));
			$expdbh->disconnect
				if $expdbh;
			next;
		}

		if (/^GO(\s+(\d+))?$/) {
			$rowcount = $2;
			unless ($initio) {
#
#	init the IO sub for the first GO
#
				if ($io_sub) {
					$rc = $io_sub->('INIT', $sth, $id, $rowcount, $ctxt);
					print $stsfd pack('C', $id + 128) and
					next
						unless $rc;
				}
				$initio = 1;
			}
			my $status = 0;
#
#	if we set a checkpoint in loopback mode, then we need to be
#	able to restart the export processing...from where we left off
#
			if ($expdbh) {
#
#	2 params need to be bound: the query number, and the sequence number
#
				unless ($export_started) {
					no strict;
					$expsth->bind_param_inout(1, \$qnum, SQL_INTEGER);
					$expsth->bind_param_inout(2, \$seqnum, SQL_INTEGER);
					use strict;
					$expsth->{tdat_nowait} = 1;

					$expsth->tdat_BindColArray(1, \@colary, 10000),
					$sth->bind_param_array(1, \@colary)
						if $attrs->{Loopback};

					$rc = $expsth->execute;

					$status = 128,
					print $stsfd pack('C', $id + $status),
					next
						unless defined($rc);

					$seqnum += $offset;
					$export_started = 1;
				}
#
#	we fetch segments until our rowcount is used up
#
				$total_sent = 0;
				$okcnt = 0;
				while ((!defined($rowcount)) || ($rowcount > $total_sent)) {
#
#	have to handle things this way otherwise we fall into an infinite recursion
#
					last if defined($expsth->err) && ($expsth->err == 2588);
					$rc = $expsth->tdat_Realize();
					unless (defined($rc)) {
						$attrs->{Report}->("Export: error on child $id "
							. $expsth->errstr)
							if $attrs->{Report} && ($expsth->err != 2588);
						$status = 128 unless ($expsth->err == 2588);
						last;
					}
					$seqnum += $offset;
#
#	get the result info
#
#					$stmtno = $expsth->{tdat_stmt_num};
#					$stmtinfo = $expsth->{tdat_stmt_info};
#					warn "stmtno $stmtno"
#						unless $stmtinfo->[$stmtno]{ActivityCount};

					$rc = 0;
#
#	if in loopback mode, then directly apply the rows to the
#	output; we'd liketo reuse the same buffer, but MLOADREC
#	parcels require some extra header stuff, so we're screwed
#	also, our export response size may cause overflow on MLOAD,
#	so we'll need to add a 2nd execute if things overflow,
#	OR restrict the response buffer size so we don't overflow
#
					if ($attrs->{Loopback}) {
						$rc = $expsth->fetch;
						unless (defined($rc)) {
							$attrs->{Report}->("Export: error on child $id "
								. $expsth->errstr)
								if $attrs->{Report} && ($expsth->err != 2588);
							$status = 128 unless ($expsth->err == 2588);
							last;
						}

						$attrs->{Report}->("Loopback: sending $#colary rows on child $id")
							if $attrs->{Report};
						my $rowlim = $#colary;
						if ($attrs->{Utility} eq 'MLOAD') {
#
#	sum up size of available rows and limit to max we can handle w/ mload headers
#	also duplicate records for each job, and add job ID array
#
							$i = 0;
							while ($i <= $#colary) {
								my $ttl = 0;
								my $len = 0;
								my @jobary = ();

								while (($i <= $#colary) && ($ttl < 64000)) {
									$len = (length($colary[$i]) + 11) * $jobcount;
									last
										unless ($ttl + $len < 64000);
									$ttl += $len;
									push @jobary, $colary[$i];
									$i++;
								}
								$sth->bind_param_array(1, \@jobary);
								$rc = $sth->execute;

								$total_sent += $rc if $rc;

								$rc = $dbh->commit
									if defined($rc);

								unless (defined($rc)) {
									$status = 128;
									print "Child: sth error ", $sth->errstr, "\n"
										if $dbh->{_debug};
									last;
								}
								$stmtinfo = $sth->{tdat_stmt_info};
								$okcnt += $stmtinfo->[1]{ActivityCount};
							} # end while records left
						} # endif MLOAD
						else { # FASTLOAD is much easier...
							$rc = $sth->execute;

							$total_sent += $rc if $rc;

							$rc = $dbh->commit
								if defined($rc);

							unless (defined($rc)) {
								$status = 128;
								print "Child: sth error ", $sth->errstr, "\n"
									if $dbh->{_debug};
								last;
							}
							$stmtinfo = $sth->{tdat_stmt_info};
							$okcnt += $stmtinfo->[1]{ActivityCount};
						}
					}
					else {
						my $sthpriv = $expsth->{_private};
						while ($expsth->fetch && defined($rc)) {
							$total_sent += ($sthpriv->[TD_STH_COLARY]) ?
								$#{$sthpriv->[TD_STH_COLARY][0]}+1 : 1;
							$rc = $io_sub->('MOREDATA', $expsth, $id,
								($sthpriv->[TD_STH_COLARY]) ?
									$#{$sthpriv->[TD_STH_COLARY][0]}+1 : 1, $ctxt);
							$attrs->{Report}->("Saved $rc records on child $id")
								if ($rc && $attrs->{Report});
							$okcnt += $rc if $rc;
						}

						$status = 128,
						last
							unless defined($rc);
					}
				} # end while rowcount
			} # endif export
			else {
#
#	fastload/mload processing
#	possibly only do limited chunk per pass and check
#	the cmdfd for commands intermittently, in case of
#	errlimit or other failure
#
				$total_sent = 0;
				$okcnt = 0;
				while ($rowcount > $total_sent) {
					$rc = $io_sub->('MOREDATA', $sth, $id, $rowcount - $total_sent, $ctxt, $offset-1)
						unless $attrs->{Loopback};
					$status = 128, last
						unless defined($rc);

					last
						unless $rc;

					$attrs->{Report}->("Sent $rc records on child $id")
						if $attrs->{Report};

					$rc = $sth->execute;

					$total_sent += $rc if $rc;

					$rc = $dbh->commit
						if defined($rc);

					unless (defined($rc)) {
						print "Child: sth error ", $sth->errstr, "\n"
							if $dbh->{_debug};
						$status = 128;
						last;
					}

					$stmtinfo = $sth->{tdat_stmt_info};
					$okcnt += $stmtinfo->[1]{ActivityCount};
				} # end while rowcount
#
#	should we check the cmdpipe between msgs ?
#	for now, no
#
			} # endif not export
			$expsth->finish if $expsth && $status;
			print $stsfd pack('C', $id + $status);
			next;
		} # end if GO

		print $stsfd pack('LL', $okcnt, $total_sent - $okcnt) and next
			if ($_ eq 'STATUS');

		print $stsfd pack('SS/a*', $DBI::err, $DBI::errstr) and next
			if ($_ eq 'REPORT');

		if ($_ eq 'FINISH') {
			$io_sub->('FINISH', undef, $id, 0, $ctxt)
				if $io_sub;
			$attrs->{Report}->("FINISH on child $id") if $attrs->{Report};
			next;
		}

		if ($_ eq 'CHECKPOINT') {
			$io_sub->('CHECKPOINT', undef, $id, 0, $ctxt)
				if $io_sub;
			$attrs->{Report}->("CHECKPOINT on child $id") if $attrs->{Report};
		}
	}
	print "Child $id: exitting...\n" if $mstrdbh->{_debug};
}

1;

package DBD::Teradata::Utility::st;

use DBD::Teradata qw(:tdat_sth_codes);

sub new {
	my ($class, $csth) = @_;
	my $lbsth = {
		tdat_stmt_info => [ undef,
		{
			StartsAt => $csth->{tdat_stmt_info}[1]{StartsAt},
			EndsAt => $csth->{tdat_stmt_info}[1]{EndsAt},
		} ],
		NUM_OF_FIELDS => $csth->{NUM_OF_FIELDS},
		NAME => [ @{$csth->{NAME}} ],
		TYPE => [ @{$csth->{TYPE}} ],
		tdat_TITLE => [ @{$csth->{tdat_TITLE}} ],
		tdat_FORMAT => [ @{$csth->{tdat_FORMAT}} ],
		tdat_TYPESTR => [ @{$csth->{tdat_TYPESTR}} ],
		PRECISION => [ @{$csth->{PRECISION}} ],
		SCALE => [ @{$csth->{SCALE}} ],
		NULLABLE => [ @{$csth->{NULLABLE}} ],
	};
#
#	another obfus issue...
#
	$lbsth->{_private}[TD_STH_UNPACKSTRS] = [ @{$csth->{_private}[TD_STH_UNPACKSTRS]} ];
	$lbsth->{_private}[TD_STH_USENAMES] = $csth->{_private}[TD_STH_USENAMES] ?
		[ @{$csth->{_private}[TD_STH_USENAMES]} ] : undef;

	bless $lbsth, $class;
	return $lbsth;
}

1;
