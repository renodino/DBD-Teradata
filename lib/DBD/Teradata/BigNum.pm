#
#    DBD::Teradata = Perl DBI Driver for Teradata
#    Copyright (C) 2001-2009, Presicient Corp., USA
#    email: info@presicient.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; Refer to the included LICENSE file, or the LICENSE section 
#    of the DBD::Teradata documentation, for details. If neither is available,
#    write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
package DBD::Teradata::BigNum;

use Math::BigInt;
use Exporter;
use Scalar::Util qw(looks_like_number);

use DBD::Teradata qw(
	@td_decszs
);

use base ('Exporter');

@EXPORT = qw(
	big_rand 
	big_rand_bin
	cvt_dec2flt 
	cvt_dec2bigint 
	cvt_int64tobigint 
	cvt_flt2dec
	cvt_bigint2int64
	cvt_bigint2dec);

use strict;
use warnings;

our $VERSION = '12.001';
#
#	decimal pack map, indexed by precision

our @td_decstrs = (
	'c', 'c', 'c',
	's', 's',
	'l', 'l', 'l', 'l', 'l');

our @td_decscales = (
	1.0, 1.0E-1, 1.0E-2, 1.0E-3, 1.0E-4, 1.0E-5, 1.0E-6,
	1.0E-7, 1.0E-8, 1.0E-9, 1.0E-10, 1.0E-11, 1.0E-12,
	1.0E-13, 1.0E-14, 1.0E-15, 1.0E-16, 1.0E-17, 1.0E-18,
	1.0E-19, 1.0E-20, 1.0E-21, 1.0E-22, 1.0E-23, 1.0E-24,
	1.0E-25, 1.0E-26, 1.0E-27, 1.0E-28, 1.0E-29, 1.0E-30,
	1.0E-31, 1.0E-32, 1.0E-33, 1.0E-34, 1.0E-35, 1.0E-36,
	1.0E-37, 1.0E-38
);

our @td_decfactors = (
	1.0, 1.0E1, 1.0E2, 1.0E3, 1.0E4, 1.0E5, 1.0E6,
	1.0E7, 1.0E8, 1.0E9, 1.0E10, 1.0E11, 1.0E12,
	1.0E13, 1.0E14, 1.0E15, 1.0E16, 1.0E17, 1.0E18,
	1.0E19, 1.0E20, 1.0E21, 1.0E22, 1.0E23, 1.0E24,
	1.0E25, 1.0E26, 1.0E27, 1.0E28, 1.0E29, 1.0E30,
	1.0E31, 1.0E32, 1.0E33, 1.0E34, 1.0E35, 1.0E36,
	1.0E37, 1.0E38
	);

my $bigend;
my $int64pack;
my $int128pack;
my ($declo, $dechi);
BEGIN {
	$bigend = (unpack('S', pack('n', 1234)) == 1234);
	$int64pack = $bigend ? 'l L' : 'L l';
	$int128pack = $bigend ? 'l LLL' : 'LLL l';
	($dechi, $declo) = $bigend ? (0, 1) : (1, 0);
}


#
#	conversion functions for DECIMAL types
#	Perl numbers are always stored internally as
#	double precision floats, so we have to do
#	some bit twiddling here...
#
#	!!!NOTE: update in 8.101 to prepare for VARDECIMAL
#
#	convert DECIMAL from binary to Perl via float
#
sub cvt_dec2flt { # decstring, precision, scale
	my ($decstr, $prec, $scale) = @_;

	$@ = "cvt_dec2flt: Precision > 18 unsupported.",
	return undef
		unless ($prec <= 18);	# we can't reliably use this for big decimal

	my $fmt = join('', '%', $prec + 2, '.', $scale, 'f');
	my $val;
	if ($prec <= 9) {
		 $val = ((unpack($td_decstrs[$prec], $decstr)) * $td_decscales[$scale]);
	}
	else {
		my @ival = $bigend ?
			unpack('l L', $decstr) : unpack('L l', $decstr);
		$val = $bigend ?
			((($ival[0]*(2.0**32)) + $ival[1]) * $td_decscales[$scale]) :
			(($ival[1]*(2.0**32)) + $ival[0]) * $td_decscales[$scale];
	}
	$val = sprintf $fmt, $val;
	$val=~tr/ //d;
	return $val;
}

#
#	convert DECIMAL from Perl to binary via BigInt
#
our @b2d_packstr = (
	undef,
	'c',
	'c',
	's',
	's',
	'l',
	'l',
	'l',
	'l',
	'l',
	'H16',
	'H16',
	'H16',
	'H16',
	'H16',
	'H16',
	'H16',
	'H16',
	'H16',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',
	'H32',	
);

our @b2d_unpack = (
	undef,
	undef, 
	undef,
	undef, 
	undef, 
	undef,
	undef,
	undef,
	undef,
	undef,
	16,
	16,
	16,
	16,
	16,
	16,
	16,
	16,
	16,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	32,	
);

sub cvt_bigint2dec {
	return cvt_flt2dec(@_) if (index(uc $_[0], 'E') >= 0);

	$@ = "cvt_bigint2dec: Not a number",
	return undef 
		unless looks_like_number($_[0]);
	my $val = shift;
	my ($prec, $scale) = (($_[0] >> 8) & 255, $_[0] & 255);

	my $sign = (substr($val, 0, 1) eq '-');
	$val = substr($val, 1) if $sign;

	my ($w, $f) = split /\./, $val;
	if ($scale) {
		$f = '' unless defined $f;
		$f .= '0' x ($scale - length($f))
			if (length($f) < $scale);
		$val = join('', $w, substr($f, 0, $scale));
	}
	else {
		$val = $w;
	}
	
	$@ = "cvt_bigint2dec: Value length exceeds supplied precision $prec",
	return undef 
		if (length($val) > $prec);
	
	my ($packstr, $zerolen) = ($b2d_packstr[$prec], $b2d_unpack[$prec]);

	return pack($packstr, $sign ? -$val : $val) 
		unless ($prec > 9);

	my $x = substr(($sign 
		? Math::BigInt->new("$val")->bdec()->as_hex()
		: Math::BigInt->new("$val")->as_hex()),
		2);

	use bytes;
	my @y =	$bigend
		? split '', pack($packstr, ('0' x ($zerolen - length($x))) . $x)
		: reverse split '', pack($packstr, ('0' x ($zerolen - length($x))) . $x);

	@y = map ~$_, @y
		if $sign;

	$x = join('', @y);
	no bytes;
	return $x;
}

our @d2b_packstr = (
	undef,
	'c',
	'c',
	's',
	's',
	'l',
	'l',
	'l',
	'l',
	'l',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X',

	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
	'0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
);

our @d2b_unpack = (
	undef,
	undef, 
	undef,
	undef, 
	undef, 
	undef,
	undef,
	undef,
	undef,
	undef,
	'C8',
	'C8',
	'C8',
	'C8',
	'C8',
	'C8',
	'C8',
	'C8',
	'C8',	
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
	'C16',
);

#
#	convert DECIMAL from binary to Perl via BigInt
#
sub cvt_dec2bigint {
	my ($val, $prec, $scale) = @_;
	
	my ($packstr, $unpack) = ($d2b_packstr[$prec], $d2b_unpack[$prec]);

	my $sign = '';
	if ($prec <= 9) {
		$val = unpack($packstr, $val);
		$sign = '-', $val = substr($val, 1)
			if ($val < 0);
		$val = ('0' x ($scale + 1 - length($val))) . $val
			if (length($val) < $scale + 1);
		substr($val, -$scale, 0, '.') if $scale;
		return join('', $sign, $val);
	}
	
	use bytes;
	my @y = $bigend
		? unpack($unpack, $val)
		: reverse unpack($unpack, $val);
	no bytes;

	$sign = '-',
	@y = map { ~$_ & 255 } @y
		if ($y[0] & 0x80);
	
	my $x = $sign
		? Math::BigInt->from_hex(sprintf($packstr, @y))->binc()->bstr()
		: Math::BigInt->from_hex(sprintf($packstr, @y))->bstr();
	
	if ($scale) {
		$x = ('0' x ($scale + 1 - length($x))) . $x
			if (length($x) <= $scale);
		substr($x, -$scale, 0, '.');
	}
	return join('', $sign, $x);
}

#
#	convert BIGINT from binary to Perl via BigInt
#
sub cvt_int64tobigint {
#	my @ival = unpack($int64pack, $_[0]);
#	return Math::BigInt->new("$ival[$dechi]")->blsft(32)->badd("$ival[$declo]")->bstr();

	my $sign = '';
	use bytes;
	my @y = $bigend
		? unpack('C8', $_[0])
		: reverse unpack('C8', $_[0]);
	no bytes;

	$sign = '-',
	@y = map { ~$_ & 255 } @y
		if ($y[0] & 0x80);
	
	my $x = $sign
		? Math::BigInt->from_hex(sprintf('0x%02X%02X%02X%02X%02X%02X%02X%02X', @y))->binc()->bstr()
		: Math::BigInt->from_hex(sprintf('0x%02X%02X%02X%02X%02X%02X%02X%02X', @y))->bstr();
	
	return join('', $sign, $x);
}

#
#	convert DECIMAL from Perl to binary via float
#
sub cvt_flt2dec { # floatval, precision, scale
	my ($dval, $packed) = @_;
	my ($prec, $scale) = (($packed >> 8) & 255, $packed & 255);

	$dval = int($dval * $td_decfactors[$scale]);
	return pack($td_decstrs[$prec], $dval) if ($prec <= 9);
#print "!!! cvt_flt2dec: prec $prec: $dval\n";
	my @ival = (int($dval/(2**32)));
	$ival[0]-- if ($dval < 0);
	$ival[1] = int($dval - ($ival[0]*(2.0**32)));
	@ival = reverse @ival unless $bigend;
	return pack($int64pack, @ival);
}

#
#	convert BIGINT from Perl to binary via BigInt
#
sub cvt_bigint2int64 {
	my $val = shift;
	
	my $sign = '';
	$sign = '-',
	$val = substr($val, 1)
		if (substr($val, 0, 1) eq '-');

	my ($packstr, $zerolen) = ('H16', 16);

	my $x = substr(($sign 
		? Math::BigInt->new("$val")->bdec()->as_hex()
		: Math::BigInt->new("$val")->as_hex()),
		2);

	use bytes;
	my @y =	$bigend
		? split '', pack($packstr, ('0' x ($zerolen - length($x))) . $x)
		: reverse split '', pack($packstr, ('0' x ($zerolen - length($x))) . $x);

	@y = map ~$_, @y
		if $sign;

	$x = join('', @y);
	no bytes;
	return $x;
}

sub big_rand_bin {
	my ($p, $s) = @_;
	$p ||= 18;
	$s ||= 0;
	return cvt_bigint2dec(big_rand($p, $s), ($p << 8) | $s);
}

sub big_rand {
	my ($prec, $scale) = @_;
	$prec ||= 18;
	$scale ||= 0;
	my ($bytes, $fmt) = ($prec <= 18) 
		? (4, '0x%04X%04X%04X%04X')
		: (8, '0x%04X%04X%04X%04X%04X%04X%04X%04X');
	my @x = map int(rand(65536)), 1..$bytes;
	$x[0] -= 32768 if ($x[0] > 32767);
	my $a = Math::BigInt->from_hex(sprintf($fmt, @x))->bstr();
	if ($prec < length($a)) {
		$a = substr($a, 0, $prec);
	}
	elsif ($scale >= length($a)) {
		$a .= ('0' x ($scale - length($a) + 1));
	}
	substr($a, -$scale, 0, '.')
		if $scale;

	return (rand() > 0.49) ? $a : "-$a";
}

1;
