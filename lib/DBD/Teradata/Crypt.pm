#
#    DBD::Teradata = Perl DBI Driver for Teradata
#    Copyright (C) 2001-2009, Presicient Corp., USA
#    email: info@presicient.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; Refer to the included LICENSE file, or the LICENSE section 
#    of the DBD::Teradata documentation, for details. If neither is available,
#    write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#
#	Encryption factory class
#
package DBD::Teradata::Crypt;

use strict;
use warnings;

our $VERSION = "12.001";

sub new {
	my $class = shift;
	my $version = shift;
	return ($version < 5010100) ? DBD::Teradata::WeakCrypt->new(@_) :
		($version < 6000100) ? DBD::Teradata::TD1Crypt->new(@_) :
		DBD::Teradata::TD2Crypt->new(@_);
}

1;
#
#	Encryption setup for V2R5.1.0 (weak)
#
{
package DBD::Teradata::WeakCrypt;

#use Math::BigInt lib => 'GMP';
use Math::BigInt;

use DBD::Teradata qw(
	:tdat_parcels
	:tdat_msgkinds
	:tdat_misc_codes
	:tdat_platforms
	:tdat_impl_codes
	);

use strict;
use warnings;
#
#	pregenerated values that should be good
#
# the random values we generated our keys from
#
my @starts = ();
#
#	the intermediate values
#
my @abiginteger = qw();
#
#	our final key values
#
my @finals = qw();

my $cryptpack;

sub new {
	my ($class, $io, $inplatform, $reqkey) = @_;

	my $talg = 'Crypt::Blowfish';
	my $self = 'A';

	eval "require $talg;";

	if ($@) {
		$talg .= '_PP';
		eval "require $talg;";
		$self = 'B';
	}
	return $io->io_set_error(
"Unable to locate Crypt::Blowfish or Crypt::Blowfish_PP (required for encryption support). Check your installation.")
		if $@;

	$self .= ("\0" x 16);
	bless \$self, $class;
#
#	instantiate the class members
#
	my $platpack = (($inplatform == COPFORMATINTEL8086) ? 'L' : 'V') . '13';
 	$cryptpack = ($inplatform == COPFORMATINTEL8086) ? 'N' : 'V';

	@starts = ();
	@finals = ();

	push @starts, int(rand(2**31))
		foreach (0..12);
#
#	don't know why, but on Linux, wo/ this eval, we silently
#	die on return from $drh->connect()
#
	eval {
		map { push @abiginteger, Math::BigInt->new($_)->bmod(7919); } @starts;

		@finals = map { $_ = Math::BigInt->new(4068); } 0..12;

		map { $finals[$_]->bmodpow($abiginteger[$_], 7919); } 0..12;
	};
#
#	send authentication types request...we might skip this and
#	see if tdat burps if we just force it, since we can key off of
#	the version info returned by ASSIGN/CONFIGRESP
#
	my $reqmsg = $io->io_buildtdhdr(COPKINDAUTHMETHOD, 4);
	substr($reqmsg, TDAT_HDRSZ, 4) = pack('SS', PclSSOAUTHREQ, 4);

	return undef
		unless defined($io->io_tdsend($reqmsg));

	my $resp = $io->io_gettdresp;
	return undef unless $resp;

	return undef
		unless (unpack('C', substr($$resp, 2, 1)) == COPKINDAUTHMETHOD)
			&& (unpack('S', substr($$resp, TDAT_HDRSZ, 2)) == PclSSOAUTHRESP);
#
#	look for blowfish code
#
	my $len = unpack('S', substr($$resp, TDAT_HDRSZ+2, 2)) - 4;
	my $can_blowfish;

	foreach (unpack("C$len", substr($$resp, TDAT_HDRSZ+4, $len))) {
		$can_blowfish = 1, last
			if ($_ == 8);	# magic blowfish method code
	}
	return undef unless $can_blowfish;
#
#	blowfish supported, build 1st request key
#
#	initial context msg format:
#		Library/DBMS Major version
#		Library/DBMS Minor Version
#		Library/DBMS Maintenance version
#		Library/DBMS Emergency release
#		Method Major version
#		Method Minor version
#		Method Maintenance version
#		Method Efix version
#		Endian type
#		Encryption mode ???
#		Some other flag ???
#
	$reqkey ||= "\5\1\0\0\1\0\0\0\0\0\0";
	$reqkey .= "\0" x 53;

    my $reqlen = 4 + 4 + 64;
    $reqmsg = $io->io_buildtdhdr(COPKINDSSOREQ, $reqlen);
    substr($reqmsg, TDAT_HDRSZ, $reqlen) =
    	pack('SS C C S a*',
    		PclSSOREQ, $reqlen,
    		8, # encryption method code
    		0, # trip count
    		64, # keylen
    		$reqkey);
#
#	send it and get the response
#
	return undef
		unless $io->io_tdsend($reqmsg);

	$resp = $io->io_gettdresp;
	return undef unless $resp;

	return undef
		unless (unpack('S', substr($$resp, TDAT_HDRSZ, 2)) == PclSSORESP);
#
#	bail now if we're using a different algorithm
#
	return (\$self, $resp, $platpack, $cryptpack)
		unless ($class eq 'DBD::Teradata::WeakCrypt');
#
#	got SSOResp, but we don't seem to use it for anything ????
#
	$len = unpack('S', substr($$resp, TDAT_HDRSZ+2, 2));
#
#	some stuff we don't really care about, tho I guess we should
#	validate for the sake of true authentication...naaahhhh
#
#	method = abyte0[i++];
#	code = abyte0[i++];
#	trip = abyte0[i++];
#	byte byte0 = abyte0[i++];	# MBZ
#
#	this is in intel format, we need to
#	swizzle on non-intel platforms
#
#	my ($authlsb, $authmsb) = unpack('C2', substr($resp, TDAT_HDRSZ + 8, 2));
#
#	make sure its in the right byte order
#
	my $authdata = "\0" x 52;
	$authdata = pack($platpack, @finals);

    $reqlen = 4 + 4 + 52;
    $reqmsg = $io->io_buildtdhdr(COPKINDSSOREQ, $reqlen);
    substr($reqmsg, TDAT_HDRSZ, $reqlen) =
    	pack('SS C C S a*',
    		PclSSOREQ, $reqlen,
    		8, # encryption method code
    		2, # trip count
    		52, # keylen
    		$authdata);
#
#	send 2nd key and get the response
#
	return undef
		unless $io->io_tdsend($reqmsg);

	$resp = $io->io_gettdresp;
	return undef unless $resp;

	return undef
		unless (unpack('S', substr($$resp, TDAT_HDRSZ, 2)) == PclSSORESP);
#
#	some stuff we don't really care about, tho I guess we should
#	validate for the sake of true authentication...naaahhhh
#
#	method = abyte0[i++];
#	code = abyte0[i++];
#	trip = abyte0[i++];
#	byte byte0 = abyte0[i++];	# MBZ
#
# we actually know this will always be 52...
#
#	my ($authlsb, $authmsb) = unpack('C2', substr($$resp, TDAT_HDRSZ + 8, 2));
#	$authlen = ($authmsb * 256) + $authlsb;
#
#	now extract the keys from the authentication data
#
	$authdata = substr($$resp, TDAT_HDRSZ + 10, 52);
	my $keydata = "\0" x 52;
#
#	get native platform version
#
	my @bigkeys;

	eval {
		@bigkeys = map { $_ = Math::BigInt->new($_); } unpack($platpack, $authdata);
	};
#
#	and generate our encryption key
#
#	print "Generated key:\n";
#
#	don't know why, but on Linux, wo/ this eval, we silently
#	die on return from $drh->connect()
#
	eval {
		map { $bigkeys[$_]->bmodpow($abiginteger[$_], 7919); } 0..12;
	};
	substr($self, 1, 16, pack($platpack, @bigkeys));
	return \$self;
}

sub _getBigIntBytes {
#
#	retrieve as hex value, and grab 2 char chunks,
#	starting at the end
#
	my $bi;
	eval {
		$bi = Math::BigInt->new($_[1])->as_hex();
	};
	substr($bi, 0, 2, '');	# get rid of leading '0x'
	my $len = length($bi);
	my $bibytes = pack("H$len", $bi);
	return $bibytes;
}

sub encrypt_request {
#
#	object is $_[0], I/O object is $_[1], request is $_[2]; note that request will be
#	modified on output
#	encrypt $req and then send it
#	(currently only encrypt connection msgs,
#	but eventually we might support full encryption)
#
	return undef
		unless (unpack('C', substr($_[2], 2, 1)) == COPKINDCONNECT);

	my $bf = (substr(${$_[0]}, 0, 1) eq 'A') ?
		Crypt::Blowfish->new(substr(${$_[0]}, 1)) :
		Crypt::Blowfish_PP->new(substr(${$_[0]}, 1));
#
#	pad request to 8 byte chunks
#	encrypted part is msg body + last 28 bytes of header
#
	my $msglen = (unpack('n', substr($_[2], 3, 2)) << 16) +
		unpack('n', substr($_[2], 8, 2));
	my $enclen = (($msglen + 28 + 7) >> 3) * 8;
	my $enclen2 = $enclen - 28;
#
#	pad with zeros as needed
#
	my $pad = $enclen2 + 4 - $msglen;
	$_[2] .= "\0" x $pad;
#
#	swizzling bytes to normalize ??? seems pointless, but OK...
#
	my $lcnt = $enclen2 >> 2;
	substr($_[2], 24, (4 * $lcnt)) =
		pack("$cryptpack$lcnt", unpack("L$lcnt", substr($_[2], 24, (4 * $lcnt))));
#
#	###### encrypt the data ######
#
	my $cipher = substr($_[2], 0, 24);
	my $i = 24;
	$cipher .= $bf->encrypt(substr($_[2], $i, 8)),
	$i += 8
		while ($i < $enclen + 24);
#
#	then unswizzle
#
	substr($cipher, 24, (4 * $lcnt)) =
		pack("$cryptpack$lcnt", unpack("L$lcnt", substr($cipher, 24, (4 * $lcnt))));
	$cipher .= pack('L', 0);	# add pad count
#
#	updates the tail of the msg with the pad delta
#
	$pad = $enclen2 - $msglen;
	$msglen = $enclen2 + 4;
	substr($cipher, $msglen + 52 - 1, 1) = pack('C', $pad);
#
#	updates the msg header with the new length
#
	substr($cipher, 1, 1) = pack('C', 0x81);

	substr($cipher, 3, 2) = pack('n', (($msglen >> 16) & 0xFFFF));
	substr($cipher, 8, 2) = pack('n', ($msglen & 0xFFFF));

	return $cipher;
}
#
#	decrypt LAN msg
#
sub decrypt_response {
#
#	object is $_[0], IO object is $_[1], request is $_[2]; note that request will be
#	modified on output
#
	return $_[2]
		unless (unpack('C', substr($_[2], 1, 1)) & 0x80);

	my $bf = (substr(${$_[0]}, 0, 1) eq 'A') ?
		Crypt::Blowfish->new(substr(${$_[0]}, 1)) :
		Crypt::Blowfish_PP->new(substr(${$_[0]}, 1));

	my $msglen = length($_[2]) - 52;
	my $pad = unpack('C', substr($_[2], length($_[2]) - 1, 1));

	my $size = length($_[2]) - 24 - 4;

	$_[1]->io_set_error("Msg not a multiple of 8 bytes long")
		if ($size & 7);
	$size = (($size + 7) >> 3) * 8;
	my $cnt  = $size/4;
#
#	byte swizzle
#
	substr($_[2], 24, $size) =
		pack("$cryptpack$cnt", unpack("L$cnt", substr($_[2], 24, $size)));
#
#	extract padding length and trim the excess
#
	my $cleartext = substr($_[2], 0, 24) . ('\0' x ($cnt * 4));
	my $offset = 24;

	substr($cleartext, $offset, 8) = $bf->decrypt(substr($_[2], $offset, 8)),
	$offset += 8
		while ($offset < $size + 24);

	substr($cleartext, 24, $size) =
		pack("$cryptpack$cnt", unpack("L$cnt", substr($cleartext, 24, $size)));
#
#	truncate the padding (if any)
#
	$msglen -= $pad;
	$msglen -= 4;
	$cleartext = substr($cleartext, 0, ($msglen + 52));

	substr($cleartext, 1, 1) = 2;
	substr($cleartext, 3, 2) = pack('n', ($msglen >> 16) & 0xFFFF);
	substr($cleartext, 8, 2) = pack('n', ($msglen & 0xFFFF));
	return $cleartext;
}

1;
}

{
#
#	Encryption setup for V2R5.1.1 to V2R6.0.x (DH + Blowfish)
#
package DBD::Teradata::TD1Crypt;

use Math::BigInt;
#use Math::BigInt lib => 'GMP';
use base ('DBD::Teradata::WeakCrypt');

use DBD::Teradata qw(
	:tdat_parcels
	:tdat_msgkinds
	:tdat_misc_codes
	:tdat_platforms
	:tdat_impl_codes
	);

use strict;
use warnings;
#
#	pregenerated values that should be good
#
# the random values we generated our keys from
#
my @starts = ();
#
#	the intermediate values
#
my @abiginteger = qw();
#
#	our final key values
#
my @finals = qw();

#
#	P Key sets for DH key generation
#
my @primes = (
'4076653813596228070915447416944725510807416354818957791449753629083622523532685785748661925083661511663809188009153909841098212825371782955390163815581154783689562226720304611253908799093111623',
'4543428899644695710384732245926731695876557322098793434647511685992214165721078665730595689442205178518498814206142423102745506708967008109414433946689963212903256123752480054063656020634398523',
'2457891474326718270885714373919767186517372561749975956207528756158733023938854986008618918605097131105317988581226266569706764191585544612792439446398093173071471899449839022656387048438434563',
'2921711216168042301695959507093398903108549588429630917278597641007842623051173046958563092081791115218247448081313637940589899088275198785730285808048935983956822947462732825161932860780570303',
'4435990417951338133615569741082219719790296994515195823744005761204966402006024493723842276147967581207229857149637372847774583630030068852890823456570871930062314348510231980830813613491437663',
'4402312708483987981664923142285834090026037770665637915486115532189620885184199600239644652640348574830876693852009329969630928114626902833502310806794656804066953623716269905296458360521253123',
'2621566096112058907728599834919914837380554003767890178335629044930425991761079595258523445426027373728725104930505299318186622055740797458608767704915138116052071869340013504057136914821067963'
);

my $g_seed_str =
'89110168312933500364085382923833814939320869282198436144124853865220218109544480205193609596042410151926607608859265767786888764089364023403372291400824495864296770983598924806306136567316480';

#
#	these are the randoms
#
my @seeds = (
'1445292499673026165283904717251144723120075351949354656238343476376696298158805558820719498064978483968383613752107164963415132972377503792057862277161016646925813978731188026151429435829622264',
'2722116176447965526280275836494121266788595842372342947330442856247315575627282738239032073414751495305484907063836613782503383063949800352002357326416576125045001180754310014764980121114059642',
'433538620516897943233034240747673614029629958108856668222020979875634769819034609562539271201307654888601743482135617981606484371669483365600995744609049708256713968580417619259094467242287733',
'1137404432990159414490554969831882501678790234024794793481761187655842721136385681504494454902624236869627962451769038182070154607965655009644053606272901623706963235452879754006365271274299703',
'3287883528020093528890525064303423117536435017037216701921887692749186151727280482638076323223744320527340922640409687314773631362512084845512861867255642219915606150323126984392939104640952788',
'379241114140412512558050398231686864970192968813005813165323189662187993828911786860778997526931440789776078067355128439130933117001222413964089631600992938582380790415342856950785982235597191',
'2247510455737353435030472410265005967262770964110079704484761917147361907942269121031412087314573988287129252768008710799878257434413824506239512248390010081215221414150939599149941775155188676',
);
#
#	and the randoms modpow'ed with the generator
#
my @modpow_seeds = (
'2251946400012396752436611069623018415421334021060535734507511611987127715301413480774362720079286467071709895669097357492737038608970101117566942593275129309092317231680897429502239488665596237',
'2299962307415192152959233718003365711933365615009515617477593275828951225194568177501293574683296910765057612601341519009197445756971828476522770016287595827239622613914688455045111276059353474',
'827180198974850251346040503571630354028730739922630510403035951101462416102879095946916346129451320835818082364031105294538912887323728938476873101851833737413786521262214018610449140256542050',
'2865430381483951091255841095588526906365189841362911471962468099978754940312113575370206008218813680853702687426582082358088967617347354442485247427131933159293410581400802374349541124302792208',
'4393388794641484612404556656201951750018875542314653050681564288682862917685485474600354107130276092191337167099778352855941683043559055397149807460007156857721366402688665082205361502063413028',
'1117978028869578699665886606762720293830152902128799581196422555012244281112256510139728157285337656504464395605506548009672540817333393931468535730837516253650525174631121338727756951104015406',
'482516272690720961835331688194762653200090700466965040460466572294595333161844662938530955253514382682461711223035488653670353863019200042470138825655699872140147621454450306370684646214649701',
);

my $cryptpack;

sub new {
	my ($class, $io) = @_;
	my ($self, $resp, $platpack);
#
#	need to set endian properly ???
#
	($self, $resp, $platpack, $cryptpack) = DBD::Teradata::WeakCrypt::new(@_, "\5\1\1\0\1\0\0\0\1\0\1");
#	($self, $resp, $platpack, $cryptpack) = DBD::Teradata::WeakCrypt::new(@_, "\5\1\1\0\1\0\0\0\0\0\1");
	return unless $self;
#
#	got SSOResp
#
	my $len = unpack('S', substr($$resp, TDAT_HDRSZ+2, 2));
#
#	generate DH key
#
	my $generator = unpack('C', substr($$resp, TDAT_HDRSZ + 4 + 21, 1));

	print STDERR "Bogus generator!!!\n" and
	return undef
		unless ($generator >= 0) && ($generator <= 6);

#	print "\n *** Generator is $generator\n";
#
#	if capturing for verification, open a file
#	File format is:
#		<opcode>: 1 byte
#			0 => blowfish encrypt
#			1 => blowfish decrypt
#			2 => AES encrypt
#			3 => AES decrypt
#		<generator number>: 1 byte
#		<publickey modpow len> 2 bytes (always 80), network order
#		<publickey> 80 bytes
#		<keyresponse len> 2 bytes (always 80), network order
#		<keyresp> 80 bytes
#		<encryptkey len> 2 bytes (always 16) , network order
#		<encryptkey> 16 bytes
#		<cleartext request len> 2 bytes, network order
#		<cleartext request msg> N bytes
#		<encrypted request len> 2 bytes, network order
#		<encrypted request msg> N bytes
#		<encrypted response len> 2 bytes, network order
#		<encrypted response msg> N bytes
#		<cleartext response len> 2 bytes, network order
#		<cleartext response msg> N bytes
#
#
#	my $encfh;
#	if ($ENV{TDAT_DBD_ENC_CHECK}) {
#		open($encfh, ">$ENV{TDAT_DBD_ENC_CHECK}")
#			or die "Can't open $ENV{TDAT_DBD_ENC_CHECK}: $!\n";
#		binmode $encfh;
#		my $oldfh = select($encfh);
#		$| = 1;
#		select $oldfh;
#
#		$self->{_encfh} = $encfh;
#		print $encfh "\0", pack('C', $generator);
#	}
#
#	copy public value into tail of auth data
#
	my $authdata = "\0" x 80;
	if ($ENV{TDAT_DBD_SLOWNC}) {
		my $pv = $self->_getBigIntBytes($modpow_seeds[$generator]);

		my $offset = length($authdata) - length($pv);
		substr($authdata, $offset, length($pv), $pv);
		print "\n *** NONZERO OFFSET\n" if $offset;
	}

    my $reqlen = 4 + 4 + 80;
	my $reqmsg = $io->io_buildtdhdr(COPKINDSSOREQ, $reqlen);
	substr($reqmsg, TDAT_HDRSZ, $reqlen) =
		pack('SS C C S a80',
			PclSSOREQ, $reqlen,
	   		8, # encryption method code
	   		2, # trip count
	   		80, # keylen LSB: need to check if Java uses this
	   		$authdata);
#
#	send our key and get the response
#
	return undef
		unless $io->io_tdsend($reqmsg);

	$resp = $io->io_gettdresp;
	return undef unless $resp;

	return undef
		unless (unpack('S', substr($$resp, TDAT_HDRSZ, 2)) == PclSSORESP);

	my $shared_secret;
	unless ($ENV{TDAT_DBD_SLOWNC}) {
		substr($$self, 1, 16, "\0" x 16);
	}
	else {
		$authdata = '0x' . unpack('H160', substr($$resp, TDAT_HDRSZ + 10, 80));

#print "Authdata is $authdata\n";

		eval {
			my $gb = Math::BigInt->new($authdata);
			my $seed = Math::BigInt->new($seeds[$generator]);
			my $prime = Math::BigInt->new($primes[$generator]);
			my $t = $gb->bmodpow($seed, $prime);
			$shared_secret = $t->as_hex();
		};
		$shared_secret = substr($shared_secret, 2);	# skip '0x'
#		print "\n ** secret len is ", length($shared_secret), "\n";
		$shared_secret = '0' . $shared_secret
			if (length($shared_secret) & 1);

		substr($$self, 1, 16, pack("H32", substr($shared_secret, 0, 32)));
	}

	return $self;
}

sub encrypt_request {
#
#	encrypt $req and then send it
#	(currently only encrypt connection msgs,
#	but eventually we might support full encryption)
#
	return undef
		unless (unpack('C', substr($_[2], 2, 1)) == COPKINDCONNECT);

	my $bf = (substr(${$_[0]}, 0, 1) eq 'A') ?
		Crypt::Blowfish->new(substr(${$_[0]}, 1)) :
		Crypt::Blowfish_PP->new(substr(${$_[0]}, 1));
#
#	pad request to 8 byte chunks
#	encrypted part is msg body + last 28 bytes of header
#
	my $msglen = (unpack('n', substr($_[2], 3, 2)) << 16) + unpack('n', substr($_[2], 8, 2));

	my $enclen = (($msglen + 35) >> 3) * 8;
	my $enclen2 = $enclen - 28;
#
#	pad with zeros as needed
#
	my $pad = $enclen2 + 4 - $msglen;
	$_[2] .= "\0" x $pad;

#	print "\n *** msglen $msglen enclen $enclen enclen2 $enclen2 pad $pad k $k\n";

	my $lcnt = ($enclen >> 2);
	substr($_[2], 24, $enclen) =
		pack("$cryptpack$lcnt", unpack("L$lcnt", substr($_[2], 24, $enclen)));
#
#	###### encrypt the data ######
#
	my $cipher = substr($_[2], 0, 24);
	my $i = 24;
	$cipher .= $bf->encrypt(substr($_[2], $i, 8)),
	$i += 8
		while ($i < $enclen + 24);
#
#	then unswizzle
#
	substr($cipher, 24, $enclen) =
		pack("$cryptpack$lcnt", unpack("L$lcnt", substr($cipher, 24, $enclen)));
#
#	why are we doing this ? shouldn't this be included in the pad above ?
#
	$cipher .= pack('L', 0);	# add pad count
#
#	updates the tail of the msg with the pad delta
#
	$pad = $enclen2 - $msglen;
	$msglen = $enclen2 + 4;
	substr($cipher, $msglen + 52 - 1, 1) = pack('C', $pad);
#
#	updates the msg header with the new length
#
	substr($cipher, 1, 1) = pack('C', 0x81);

	substr($cipher, 3, 2) = pack('n', (($msglen >> 16) & 0xFFFF));
	substr($cipher, 8, 2) = pack('n', ($msglen & 0xFFFF));
	return $cipher;
}
#
#	decrypt LAN msg
#
sub decrypt_response {
	return $_[2]
		unless (unpack('C', substr($_[2], 1, 1)) & 0x80);

	my $bf = (substr(${$_[0]}, 0, 1) eq 'A') ?
		Crypt::Blowfish->new(substr(${$_[0]}, 1)) :
		Crypt::Blowfish_PP->new(substr(${$_[0]}, 1));

	my $msglen = length($_[2]) - 52;
	my $pad = unpack('C', substr($_[2], length($_[2]) - 1, 1));
	my $size = length($_[2]) - 24 - 4;

	$_[1]->io_set_error("Msg not a multiple of 8 bytes long.")
		if ($size & 7);

	$size = (($size + 7) >> 3) * 8;
	my $cnt  = $size/4;
#
#	byte swizzle
#
	substr($_[2], 24, $size) =
		pack("$cryptpack$cnt", unpack("L$cnt", substr($_[2], 24, $size)));
#
#	extract padding length and trim the excess
#
	my $cleartext = substr($_[2], 0, 24) . ('\0' x ($cnt * 4));
	my $offset = 24;

	substr($cleartext, $offset, 8) = $bf->decrypt(substr($_[2], $offset, 8)),
	$offset += 8
		while ($offset < $size + 24);

	substr($cleartext, 24, $size) =
		pack("$cryptpack$cnt", unpack("L$cnt", substr($cleartext, 24, $size)));
#
#	truncate the padding (if any)
#
	$msglen -= ($pad + 4);
	$cleartext = substr($cleartext, 0, ($msglen + 52));

	substr($cleartext, 1, 1) = 2;
	substr($cleartext, 3, 2) = pack('n', ($msglen >> 16) & 0xFFFF);
	substr($cleartext, 8, 2) = pack('n', ($msglen & 0xFFFF));
	return $cleartext;
}

1;
}

package DBD::Teradata::TD2Crypt;
#use Math::BigInt lib => 'GMP';
use Math::BigInt;

use DBD::Teradata qw(
	:tdat_parcels
	:tdat_msgkinds
	:tdat_misc_codes
	:tdat_platforms
	:tdat_impl_codes
	);

use DBD::Teradata::Diagnostic qw(io_hexdump io_bytedump);

use strict;
use warnings;
use bytes;

our $fixp = '4076653813596228070915447416944725510807416354818957791449753629083622523532685785748661925083661511663809188009153909841098212825371782955390163815581154783689562226720304611253908799093111623';
our $fixg = '89110168312933500364085382923833814939320869282198436144124853865220218109544480205193609596042410151926607608859265767786888764089364023403372291400824495864296770983598924806306136567316480';
our $fixr =
#'4055193897162306822636888276474014626220385290416370010815746044697128926996784678793993184570783113310088942756002690594004081319511968278612631561736401986587853063475846728101417744932126462';
#'3449515697031036470451344993524589247656444221976371816844030832290038268979580033248405230030149119924021788885082409867596397955425512345178430265446747550984030732632278497809886594809861220';
'58274';
our $fixpv =
#'38a65be182806d9db0bccbd04601f8636e382471742b275db491cea7f429d784d8e7ded7085373a1e13e77a5f60d8aaca4761f11f448d88974f209c479489e3dce8002acba3575ec6087250ecd3fc43b';
#'8f4999eb53ed2ceed0ba78276efcf218f79ef5aac9e8548c0c44eb6a754a7477650d6b04d70604280b06d7adec2bc992d6fbb484b888dd550540c2c08f43f0143d9ca3ffdda191b7506c2d6394e996c5';
'573130b99bbb1b92b828fd07061d686e7934dc24b3834bfa1ebcbde0b5ea92800e24dc87f7cbe72b8aa4420b1b75e34ea0a621b9f730495e886385d2725521290dfad2671e7d7726485fbb569096b5be';

sub _bigRandom {
#	my $prime = shift;
#
#	iterate until we get enough bits OR
#	we exceed the input prime value
#
# force the first byte so we meet the < prime - 1 rule
#	assumes the input only has leading zeros if it is zero
#
	my $bi;
	srand(time());
	unless ($_[0]) {
#
#	if its zero (never should be, but what the heck),
#	return zero
#
		eval {
			$bi = Math::BigInt->new(0);
		};
		return $bi;
	}

	$_[0] = '0' . $_[0] if length($_[0]) & 1;

	my $cnt = $ENV{TDAT_DBD_SLOWNC} ? length($_[0]) >> 1 : 2;
	my $firstbyte = hex(substr($_[0], 0, 2));
#
#	generate a number guaranteed to be less than the prime,
#	with all nonzero bytes
#
	my @random = ( $firstbyte - 1, map { $_ = int(rand(254)) + 1; } 2..$cnt );

	eval {
		$bi = Math::BigInt->new(
			'0x' . unpack('H' . ($cnt << 1), pack("C$cnt", @random)));
	};
	return $bi;
}

sub new {
	my ($class, $io, $inplatform, $p, $g, $gb, $verify) = @_;

	eval "require Crypt::Rijndael;";

	return $io->io_set_error(
"Unable to locate Crypt::Rijndael(required for encryption support). Check your installation.")
		if $@;
#
#	now compute keys
#
	my ($bi, $bi1, $keyout);
	my $self = "\0" x 32;

	eval {
		my ($pvk, $bi1);
		if ($ENV{TDAT_DBD_SLOWNC} || ($g->bstr() ne $fixg) || ($p->bstr() ne $fixp)) {
#
#	random must be less than prime...but our random guarantees that
#
			$pvk = _bigRandom(substr($p->as_hex(), 2));

#			print "My random is $pvk\n";

			$bi1 = $g->bmodpow($pvk, $p);
#
#	we may need this..but in byte form
#
#			substr($self, 16, 16, $pvk);

			my $bistr = substr($bi1->as_hex(), 2);
			$bistr = '0' . $bistr
				if (length($bistr) & 1);	# make sure its byte aligned

			$keyout = (length($bistr) == 160) ? $bistr :
#
#	if key too long, take the 80 least significant bytes of the key
#
				(length($bistr) > 160) ? substr($bistr, length($bistr) - 160) :
#
#	else prefix with zeroes
#
				(('0' x (160 - length($bistr))) . $bistr);
#
#	finally, convert to bytes
#
#				print "keyout is 0x$keyout\n";
			$keyout = pack('H160', $keyout);
		}
		else {
#
#	use fixed values for speed
#
			$keyout = pack('H160', $fixpv);
			$pvk = Math::BigInt->new($fixr);
		}
#
#	now compute the key (and make sure its byte aligned)
#
		my $key = substr($gb->bmodpow($pvk, $p)->as_hex(), 2);
		$key = '0' . $key if (length($key) & 1);
#
#	trim if key too long
#
		if ((length($key) > 160) || (substr($key, 0, 2) ne '00')) {
			$key = (length($key) > 160) ?
				substr($key, length($key) - 160, 32) :
				substr($key, 0, 32);
		}
		else {
#
#	leading zeros, move them to the back
#
			$key=~s/^(00)+(.+)$/$2$1/;
		}

		substr($self, 0, 16, pack('H32', $key));
	};	# end eval

	return $io->io_set_error("Key gen failed: $@")
		if $@;

#
#	this request uses network order for the length field ?!?!
#	now send to server, and wait for (brief) response
#
#	print io_bytedump("\nMy privatekey:\n", $keyout);

#	print io_bytedump("\nMy encrypt key:\n", substr($self, 0, 16));

	my $ssoreq2 = (($inplatform == COPFORMATATT_3B2) ?
		"\0\2\0\140\1\1\2\0\0\0\0\120\0\0\0\0\0\0\0\0" :
		"\0\2\140\0\1\1\2\0\0\0\0\120\0\0\0\0\0\0\0\0") . $keyout;
#		"\0\2\0\140\1\1\2\0\0\0\0\120\0\0\0\0\2\0\0\0" :
#		"\0\2\140\0\1\1\2\0\0\0\0\120\0\0\0\0\2\0\0\0") . $keyout;

#	$self = bless \$self, $class;
	if ($verify) {
#
#	if verify requested, we have some other stuff to do
#
		my $maxSequenceNumber = 3;
		my $bf = DBD::Teradata::TD2Crypt::Engine->new(substr($self, 0, 16), 'Rijndael');
		my $abyte8 = $bf->encrypt($self, 0, length($self), 0, 1);

		$ssoreq2 .= $abyte8;
		print "\n *** AES Verification requested!!!\n";
	}
	my $reqmsg = $io->io_buildtdhdr(COPKINDSSOREQ, length($ssoreq2) + 4);
	substr($reqmsg, TDAT_HDRSZ, 4 + length($ssoreq2)) = pack('SS a*',
		PclSSOREQ,
		4 + length($ssoreq2),
		$ssoreq2);

	return undef unless $io->io_tdsend($reqmsg);

	my $resp = $io->io_gettdresp;
	return undef unless $resp;

	return undef
		unless (unpack('C', substr($$resp, 2, 1)) == COPKINDSSOREQ)
			&& (unpack('S', substr($$resp, TDAT_HDRSZ, 2)) == PclSSORESP);

	return bless \$self, $class;
}

sub encrypt_request {
	my $self = $_[0];
#
#	for debug, do a hex dump of the input msg
#

#	my ($self, $io, $req) = @_;
#
#	encrypt $req and then send it
#	(currently only encrypt connection msgs,
#	but eventually we might support full encryption)
#
	return undef
		unless (unpack('C', substr($_[2], 2, 1)) == COPKINDCONNECT);

	my $msglen = (unpack('n', substr($_[2], 3, 2)) << 16) +
		unpack('n', substr($_[2], 8, 2));

#	print io_bytedump("\nRequest msg:\n", $_[2]), "\n";

	my $bf = DBD::Teradata::TD2Crypt::Engine->new(substr($$self, 0, 16), 'Rijndael');
	my $cipher = substr($_[2], 0, 24) . $bf->encrypt(substr($_[2], 24, $msglen + 28))
		or return $_[1]->io_set_error($bf->errstring);
#
#	now add the trailer
#
	$cipher .= "\1\3\2\0" . pack('N', length($cipher) - 24) .
		"\0\0\0\0\4\0\0\0";	# presumably, QOP (4 bytes, net order) and privacy flag

	$msglen = length($cipher) - TDAT_HDRSZ;
#
#	updates the msg header with the new length
#
	substr($cipher, 1, 1) = pack('C', 0x81);

	substr($cipher, 3, 2) = pack('n', (($msglen >> 16) & 0xFFFF));
	substr($cipher, 8, 2) = pack('n', ($msglen & 0xFFFF));

#	print io_bytedump("\nEncrypted Request msg:\n", $cipher), "\n";

	return $cipher;
}
#
#	decrypt LAN msg
#
sub decrypt_response {
	my $self = $_[0];
#	my ($self, $io, $resp) = @_;

	return $_[2]
		unless (ord(substr($_[2], 1, 1)) > 128);

	my $msginfo = substr($_[2], -16);

	return $_[1]->io_set_error('Invalid message info.')
		unless (substr($msginfo, 0, 4) eq "\1\3\2\1");

	my $msglen = (unpack('n', substr($_[2], 3, 2)) << 16) + unpack('n', substr($_[2], 8, 2));
	my $bf = DBD::Teradata::TD2Crypt::Engine->new(substr($$self, 0, 16), 'Rijndael');

	my $cleartext = substr($_[2], 0, 24) .
		$bf->decrypt(substr($_[2], 24, $msglen + 28 - 16))
		or return $_[1]->io_set_error($bf->errstring);
#
#	because our decrypt isn't correct, we must walk the parcel list to trim
#
	my $pos = TDAT_HDRSZ;
	while ($pos < length($cleartext)) {
		my ($flavor, $len) = unpack('SS', substr($cleartext, $pos, 4));
		$pos += $len;
		last unless ($flavor == PclSUCCESS) || ($flavor == PclLSN);
	}
	$cleartext = substr($cleartext, 0, $pos);
	$msglen = length($cleartext) - TDAT_HDRSZ;

	substr($cleartext, 1, 1) = 2;
	substr($cleartext, 3, 2) = pack('n', ($msglen >> 16) & 0xFFFF);
	substr($cleartext, 8, 2) = pack('n', ($msglen & 0xFFFF));
	return $cleartext;
}

1;

package DBD::Teradata::TD2Crypt::Engine;

use Digest::SHA1 qw(sha1);
use Crypt::ECB;
use base ('Crypt::ECB');

use strict;
use warnings;
use bytes;

#
# applies the encrypt/decrypt (depnding on the mode)
#	note that the input is assumed to be properly padded
#	to the blocksize multiple
#
sub crypt {
    my $crypt = shift;
    my $data  = $crypt->{buffer} . (shift || $_ || '');

    my @blocks = map substr($data, $_ << 4, 16), 0..(length($data) >> 4) - 1;
#
#	pecadillo of the base class requires us to keep this...but is it the right
#	value ???
#
    $crypt->{buffer} = pop @blocks;
#	$crypt->{buffer} = '';

    my $cipher = $crypt->_getcipher;
    my $text = '';

    my $skey = $crypt->{Iv};

	$skey  = $cipher->encrypt($skey),
	$text .= $_ ^ $skey
    	foreach (@blocks);

    return $text;
}
#
#	encrypt: add SHA1 digest, padd via PKCS5,
#	then apply encryption
#
sub encrypt {
#
# set the initialVector if provided
#
	$_[0]{Iv} = ($#_ > 1) ? pop : "\0" x 16;
#
#	add our digest first, then add the padding
#
	my $data = pop;

	$data .= sha1($data);
	my $pad = 16 - (length($data) % 16);
	$data .= chr($pad) x $pad;
#
#	add 16 bytes of crap cuz Crypt::ECB does stupid things
#
	$data .= "\0" x 16;

	$_[0]->start('encrypt')
		or die $_[0]->errstring;

	my $t = $_[0]->crypt($data) . $_[0]->finish;
	return substr($t, 0, length($t) - 16);
}

#
#	decrypt: decrypt, rmeove PKCS5 pad, then validate and remove
#	add SHA1 digest
#
sub decrypt {
	my $self = shift;
#
# set the initialVector if provided
#
	$self->{Iv} = $#_ ? pop : "\0" x 16;

	$self->start('decrypt')
		or die $self->errstring;

	my $clear = $self->crypt($_[0]) . $self->finish;
	if (1 == 0) {
#
#	we don't care about this stuff, and Crypt::ECB is screwing up
#	our decryption, so just trim to what we know we need
#
#	remove padding, validate and remove digest
#
	my $pad = ord(substr($clear, -1, 1));
	$clear = substr($clear, 0, length($clear) - $pad);
	my $digest = sha1(substr($clear, 0, length($clear) - 20));
	$self->{ErrString} = 'Invalid digest.',
	return undef
		unless ($digest eq substr($clear, length($clear) - 20));
	}

	return substr($clear, 0, length($clear) - 20);
}

1;