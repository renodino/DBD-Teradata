#
#    DBD::Teradata = Perl DBI Driver for Teradata
#    Copyright (C) 2001-2009, Presicient Corp., USA
#    email: info@presicient.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; Refer to the included LICENSE file, or the LICENSE section 
#    of the DBD::Teradata documentation, for details. If neither is available,
#    write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
package DBD::Teradata::PMAPI;

use DBI qw(:sql_types);
use Exporter;
use base ('Exporter');
@EXPORT = qw(get_pmapi_template);

use strict;
use warnings;

#
#	some backfill for MONITOR sessions:
#
my @pmbackfill = (
# abortsess
	{
	Activity => 'Abort Session',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT,
		SQL_INTEGER, SQL_CHAR, SQL_CHAR, SQL_CHAR, SQL_CHAR ],
	InputPrec => [ 2, 2, 4, 30, 1, 1, 1 ],
	Packstring => 'S S L a30 a a a',
	OutCols => [ 'HostId', 'UserName', 'SessionNo', 'AbortStatus' ],
	OutType => [ SQL_SMALLINT, SQL_CHAR, SQL_INTEGER, SQL_CHAR ],
	OutPrec => [ 2, 30, 4, 1 ],
	UnpackStrings => [ 'S a30 L a1' ],
	StmtCnt => 2,
	StartsAt => [ 0, undef ],
	EndsAt => [ 3, undef ]
	},

# identsess
	{
	Activity => 'Identify Session',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT, SQL_INTEGER ],
	InputPrec => [ 2, 2, 4 ],
	Packstring => 'S S L',
	OutCols => [ 'Name' ],
	OutType => [ SQL_CHAR ],
	OutPrec => [ 30 ],
	UnpackStrings => [ 'a30' ],
	StmtCnt => 1,
	StartsAt => [ 0 ],
	EndsAt => [ 0 ]
	},

# identuser, dbase, or table
	{
	Activity => 'Identify',
	InputType => [ SQL_SMALLINT, SQL_INTEGER ],
	InputPrec => [ 2, 4],
	Packstring => 'S L',
	OutCols => [ 'Name' ],
	OutType => [ SQL_CHAR ],
	OutPrec => [ 30 ],
	UnpackStrings => [ 'a30' ],
	StmtCnt => 1,
	StartsAt => [ 0 ],
	EndsAt => [ 0 ]
	},

# monphyscfg
	{
	Activity => 'Monitor Physical Config',
	InputType => [ SQL_SMALLINT ],
	InputPrec => [ 2 ],
	Packstring => 'S',
	OutCols => [ 'NetAUp', 'NetBUp', 'SystemType', 'ProcId', 'Status',
		'CPUType', 'CPUCount' ],
	OutType => [ SQL_CHAR, SQL_CHAR, SQL_CHAR, SQL_SMALLINT, SQL_CHAR,
		SQL_CHAR , SQL_SMALLINT ],
	OutPrec => [ 1, 1, 7, 2, 1, 7, 2 ],
	UnpackStrings => [ 'a1 a1 a7', undef, undef, 'S a1 a7 S' ],
	StmtCnt => 2,
	StartsAt => [ 0, 3 ],
	EndsAt => [ 2, 6 ]
	},

# monphysrsc
	{
	Activity => 'Monitor Physical Resource',
	InputType => [ SQL_SMALLINT ],
	InputPrec => [ 2 ],
	Packstring => 'S',
	OutCols => [ 'NetAUp', 'NetBUp', 'SampleSec',
		'ProcId', 'AMPCount', 'PECount', 'CPUUse', 'PercntKernel',
		'PercntService', 'PercntUser', 'Status', 'NetAUse',
		'NetBUse', 'DiskUse', 'CICUse', 'DiskReads',
		'DiskWrites', 'DiskOutReqAvg', 'HostBlockReads',
		'HostBlockWrites', 'SwapReads', 'SwapWrites',
		'SwapDrops', 'MemAllocates', 'MemAllocateKB',
		'MemFailures', 'MemAgings', 'NetReads', 'NetWrites',
		'NVMemAgings', 'NVMemAllocate', 'NVMemAllocSegs' ],
	OutType => [ SQL_CHAR, SQL_CHAR, SQL_SMALLINT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_CHAR, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT ],
	OutPrec => [ 1, 1, 2, 2, 2, 2, 8, 8, 8, 8, 1, 8, 8, 8, 8, 8, 8, 8, 8, 8,
		8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8 ],
	UnpackStrings => [ 'a1 a1 S', undef, undef,
		'S S S d d d d a1 ddddddddddddddddddddd' ],
	StmtCnt => 2,
	StartsAt => [ 0, 3 ],
	EndsAt => [ 2, 31 ]
	},

#	monphyssum
	{
	Activity => 'Monitor Physical Summary',
	InputType => [ SQL_SMALLINT ],
	InputPrec => [ 2 ],
	Packstring => 'S',
	OutCols => [ 'AvgCPU', 'AvgDisk', 'AvgDiskIO','HighCPUUse',
		'HighCPUProcId', 'LowCPUUse', 'LowCPUProcId', 'HighDisk',
		'HighDiskProcId', 'LowDisk', 'LowDiskProcId', 'HighDiskIO',
		'HighDiskIOProcId', 'LowDiskIO', 'LowDiskIOProcId', 'NetUse',
		'NetAUp','NetBUp', 'ResLogging', 'ResMonitor','Release', 'Version' ],
	OutType => [ SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_SMALLINT,
		SQL_FLOAT, SQL_SMALLINT, SQL_FLOAT, SQL_SMALLINT, SQL_FLOAT,
		SQL_SMALLINT, SQL_FLOAT, SQL_SMALLINT, SQL_FLOAT, SQL_SMALLINT,
		SQL_FLOAT, SQL_CHAR, SQL_CHAR, SQL_SMALLINT, SQL_SMALLINT,
		SQL_CHAR, SQL_CHAR ],
	OutPrec => [ 8, 8, 8, 8, 2, 8, 2, 8, 2, 8, 2, 8, 2, 8, 2, 8, 1, 1, 2, 2,
		29, 32 ],
	UnpackStrings => [ 'd d d d S d S d S d S d S d S d a1 a1 S S a29 a32' ],
	StmtCnt => 1,
	StartsAt => [ 0 ],
	EndsAt => [ 21 ]
	},

# monses
	{
	Activity => 'Monitor Session',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT,
		 SQL_INTEGER, SQL_CHAR ],
	InputPrec => [ 2, 2, 4, 30 ],
	Packstring => 'S S L a30',
	OutCols => [ 'SampleSec', 'HostId','LogonPENo','RunVProcNo',
		'SessionNo','UserName','UserAccount','UserId', 'LSN','LogonTime',
		'LogonDate','PartName', 'Priority','PEState','PECPUSec','XactCount',
		'ReqCount','ReqCacheHits','AMPState', 'AMPCPUSec','AMPIO',
		'Delta_AMPSpool', 'Blk_1_HostId', 'Blk_1_SessNo', 'Blk_1_UserID',
		'Blk_1_LMode','Blk_1_OType','Blk_1_ObjDBId', 'Blk_1_ObjTId',
		'Blk_1_Status','Blk_2_HostId', 'Blk_2_SessNo','Blk_2_UserID',
		'Blk_2_LMode','Blk_2_OType','Blk_2_ObjDBId','Blk_2_ObjTId',
		'Blk_2_Status','Blk_3_HostId','Blk_3_SessNo', 'Blk_3_UserID',
		'Blk_3_LMode','Blk_3_OType', 'Blk_3_ObjDBId','Blk_3_ObjTId',
		'Blk_3_Status','MoreBlockers','LogonSource' ],
	OutType => [SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_INTEGER, SQL_CHAR, SQL_CHAR, SQL_INTEGER, SQL_INTEGER, SQL_FLOAT,
		SQL_DATE, SQL_CHAR, SQL_CHAR, SQL_CHAR, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT, SQL_CHAR, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_SMALLINT, SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_CHAR,
		SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_SMALLINT, SQL_INTEGER,
		SQL_INTEGER, SQL_CHAR, SQL_CHAR, SQL_INTEGER, SQL_INTEGER, SQL_CHAR,
		SQL_SMALLINT, SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_CHAR,
		SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_CHAR, SQL_VARCHAR ],
	OutPrec => [ 2, 2, 2, 2, 4, 30, 30, 4, 4, 8, 4, 16, 2, 18, 8, 8, 8, 8,
		18, 8, 8, 8, 2, 4, 4, 1, 1, 4, 4,
		1, 2, 4, 4, 1, 1, 4, 4, 1, 2, 4,4,1, 1, 4,4,1, 1, 128 ],
	UnpackStrings => [ 'S',
		'S S S l a30 a30 l l d l a16 a2 a18 d d d d a18 d d d S l l a1 a1 l l a1 S l l a1 a1 l l a1 S l l a1 a1 l l a1 a1 S/a' ],
	StmtCnt => 2,
	StartsAt => [ 0, 1 ],
	EndsAt => [ 0, 47 ]
	},

# monvirtcfg
	{
	Activity => 'Monitor Virtual Config',
	InputType => [ SQL_SMALLINT ],
	InputPrec => [ 2 ],
	Packstring => 'S',
	OutCols => [ 'NetAUp', 'NetBUp', 'SystemType', 'ProcId', 'VProcNo',
		'VProcType', 'HostId', 'Status','DiskSlice' ],
	OutType => [ SQL_CHAR, SQL_CHAR, SQL_CHAR, SQL_SMALLINT, SQL_SMALLINT,
		SQL_CHAR, SQL_SMALLINT, SQL_CHAR, SQL_SMALLINT ],
	OutPrec => [ 1, 1, 7, 2, 2, 3, 2, 1, 2 ],
	UnpackStrings => [ 'a1 a1 a7', undef, undef, 'S S a3 S a1 S' ],
	StmtCnt => 2,
	StartsAt => [ 0, 3 ],
	EndsAt => [ 2, 8 ]
	},

#	monvirtrsc
	{
	Activity => 'Monitor Virtual Resource',
	InputType => [ SQL_SMALLINT ],
	InputPrec => [ 2 ],
	Packstring => 'S',
	OutCols => [ 'NetAUp', 'NetBUp', 'SampleSec', 'VProcType', 'ProcId',
		'VProcNo', 'HostId_ClusterNo', 'CPUUse', 'Status', 'SessLogCount',
		'SessRunCount', 'DiskUse', 'CICUse', 'DiskReads', 'DiskWrites',
		'DiskOutReqAvg', 'HostBlockReads', 'HostBlockWrites',
		'MemAllocates', 'MemAllocateKB', 'PercntService', 'PercntAMPWT',
		'PercntParser', 'PercntDispatcher', 'NetReads', 'NetWrites',
		'NVMemAllocSegs' ],
	OutType => [SQL_CHAR, SQL_CHAR, SQL_SMALLINT, SQL_CHAR, SQL_SMALLINT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_FLOAT, SQL_CHAR, SQL_SMALLINT,
		SQL_SMALLINT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT ],
	OutPrec => [ 1, 1, 2, 3, 2, 2, 2, 8, 1, 2, 2, 8, 8, 8, 8, 8, 8, 8, 8, 8,
		8, 8, 8, 8, 8, 8, 8 ],
	UnpackStrings => [ 'a1 a1 S', undef, undef,
		'a3 S S S d a1 S S dddddddddddddddd' ],
	StmtCnt => 2,
	StartsAt => [ 0, 3 ],
	EndsAt => [ 2, 26 ]
	},

# monvirtsum
	{
	Activity => 'Monitor Virtual Config',
	InputType => [ SQL_SMALLINT ],
	InputPrec => [ 2 ],
	Packstring => 'S',
	OutCols => [ 'AMPAvgCPU', 'AMPAvgDisk', 'AMPAvgDiskIO',
		'HiCPUAMPUse', 'HiCPUAMPNo', 'HiCPUAMPProc',
		'LoCPUAMPUse', 'LoCPUAMPNo', 'LoCPUAMPProc',
		'HiDiskAMP', 'HiDiskAMPNo', 'HiDiskAMPProc',
		'LoDiskAMP', 'LoDiskAMPNo', 'LoDiskAMPProc',
		'HiDiskIOAMP', 'HiDiskIOAMPNo', 'HiDiskIOAMPProc',
		'LoDiskIOAMP', 'LoDiskIOAMPNo', 'LoDiskIOAMPProc',
		'PEAvgCPU', 'HiCPUPEUse', 'HiCPUPENo', 'HiCPUPEProc',
		'LoCPUPEUse', 'LoCPUPENo', 'LoCPUPEProc',
		'SessionCount', 'SesMonitorSys', 'SesMonitorLoc',
		'VProcLogging', 'VProcMonitor', 'Release', 'Version' ],
	OutType => [ SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_SMALLINT,
		SQL_SMALLINT, SQL_FLOAT, SQL_SMALLINT, SQL_SMALLINT, SQL_FLOAT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_FLOAT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_FLOAT, SQL_SMALLINT, SQL_SMALLINT, SQL_FLOAT, SQL_SMALLINT,
		SQL_SMALLINT, SQL_FLOAT, SQL_FLOAT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_FLOAT, SQL_SMALLINT, SQL_SMALLINT, SQL_FLOAT, SQL_SMALLINT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_CHAR, SQL_CHAR ],
	OutPrec => [ 8, 8, 8, 8, 2, 2, 8, 2, 2, 8, 2, 2, 8, 2, 2, 8, 2, 2, 8, 2,
		2, 8, 8, 2, 2, 8, 2, 2, 8, 2, 2, 2, 2, 29, 32 ],
	UnpackStrings => [ 'ddddSSdSSdSSdSSdSSdSSddSSdSSdSSSS a29 a32' ],
	StmtCnt => 1,
	StartsAt => [ 0 ],
	EndsAt => [ 34 ]
	},

#	setrscrate
	{
	Activity => 'Set Resource Rate',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT, SQL_CHAR, SQL_CHAR ],
	InputPrec => [ 2, 2, 1, 1 ],
	Packstring => 'S S a a',
	OutCols => undef,
	StmtCnt => 1,
	StartsAt => [ undef ],
	EndsAt => [ undef ]
	},

# setsesacct
	{
	Activity => 'Set Session Account',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT, SQL_INTEGER, SQL_CHAR,
		SQL_CHAR ],
	InputPrec => [ 2, 2, 4, 30, 1 ],
	Packstring => 'S S L a30 a',
	OutCols => [ 'OldAccount', 'ErrorCode' ],
	OutType => [ SQL_CHAR, SQL_SMALLINT ],
	OutPrec => [ 30, 2 ],
	UnpackStrings => [ 'a30 S' ],
	StmtCnt => 1,
	StartsAt => [ 0 ],
	EndsAt => [ 1 ]
	},

#	setsesrate
	{
	Activity => 'Set Session Rate',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT, SQL_CHAR ],
	InputPrec => [ 2, 2, 1 ],
	Packstring => 'S s a',
	OutCols => undef,
	StmtCnt => 1,
	StartsAt => [ undef ],
	EndsAt => [ undef ]
	},
# monses for V2R4.1
	{
	Activity => 'Monitor Session',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT,
		 SQL_INTEGER, SQL_CHAR ],
	InputPrec => [ 2, 2, 4, 30 ],
	Packstring => 'S s L a30',
	OutCols => [ 'SampleSec', 'HostId','LogonPENo','RunVProcNo',
		'SessionNo','UserName','UserAccount','UserId', 'LSN','LogonTime',
		'LogonDate','PartName', 'Priority','PEState','PECPUSec','XactCount',
		'ReqCount','ReqCacheHits','AMPState', 'AMPCPUSec','AMPIO',
		'Delta_AMPSpool','Blk_1_HostId', 'Blk_1_SessNo', 'Blk_1_UserID',
		'Blk_1_LMode','Blk_1_OType','Blk_1_ObjDBId','Blk_1_ObjTId',
		'Blk_1_Status','Blk_2_HostId','Blk_2_SessNo','Blk_2_UserID',
		'Blk_2_LMode','Blk_2_OType','Blk_2_ObjDBId','Blk_2_ObjTId',
		'Blk_2_Status','Blk_3_HostId','Blk_3_SessNo','Blk_3_UserID',
		'Blk_3_LMode','Blk_3_OType','Blk_3_ObjDBId','Blk_3_ObjTId',
		'Blk_3_Status','MoreBlockers','LogonSource',
#
#	new fields:
#	NOTE: Tempspace seems to actually exist in the 4.0 version,
#	but is not documented. And the field type definition is not
#	listed in either 4.0 or 4.1...
		'TempSpace', 'HotAmp1CPU', 'HotAmp2CPU', 'HotAmp3CPU','HotAmp1IO',
		'HotAmp2IO', 'HotAmp3IO','HotAmp1CPUId', 'HotAmp2CPUId',
		'HotAmp3CPUId','HotAmp1IOId', 'HotAmp2IOId', 'HotAmp3IOId',
		'LowAmp1CPU', 'LowAmp2CPU', 'LowAmp3CPU', 'LowAmp1IO', 'LowAmp2IO',
		'LowAmp3IO','LowAmp1CPUId', 'LowAmp2CPUId', 'LowAmp3CPUId',
		'LowAmp1IOId', 'LowAmp2IOId', 'LowAmp3IOId',
		'UpAmpCount', # doc says its INT, DATAINFO says SMALLINT !!!
		'AvgAmpCPUSec', 'AvgAmpIOCnt'
		],
	OutType => [SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_INTEGER, SQL_CHAR, SQL_CHAR, SQL_INTEGER, SQL_INTEGER,
		SQL_FLOAT, SQL_DATE, SQL_CHAR, SQL_CHAR, SQL_CHAR, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_CHAR, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT, SQL_SMALLINT, SQL_INTEGER, SQL_INTEGER, SQL_CHAR,
		SQL_CHAR, SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_SMALLINT,
		SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_CHAR, SQL_INTEGER,
		SQL_INTEGER, SQL_CHAR, SQL_SMALLINT, SQL_INTEGER, SQL_INTEGER,
		SQL_CHAR, SQL_CHAR, SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_CHAR,
		SQL_VARCHAR, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_SMALLINT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_FLOAT, SQL_FLOAT ],
	OutPrec => [ 2, 2, 2, 2, 4, 30, 30, 4, 4, 8, 4, 16, 2, 18, 8, 8, 8, 8,
		18, 8, 8, 8, 2, 4, 4, 1, 1, 4, 4, 1, 2, 4, 4, 1, 1, 4, 4, 1, 2, 4,
		4,1, 1, 4,4,1, 1, 128, 8, 8, 8, 8, 8, 8, 8, 2, 2, 2, 2, 2, 2, 8, 8,
		8, 8, 8, 8, 2, 2, 2, 2, 2, 2, 2, 8, 8 ],
	UnpackStrings => [ 'S',
'S S S l a30 a30 l l d l a16 a2 a18 d d d d a18 d d d S l l a1 a1 l l a1 S l l a1 a1 l l a1 S l l a1 a1 l l a1 a1 S/a d d d d d d d S S S S S S d d d d d d S S S S S S S d d'],
	StmtCnt => 2,
	StartsAt => [ 0, 1 ],
	EndsAt => [ 0, 75 ]
	},
#
#	monitor version
	{
	Activity => 'Monitor Version',
	InputType => [ SQL_SMALLINT ],
	InputPrec => [ 2 ],
	Packstring => 'S',
	OutCols => [ 'FunctionBitmap' ],
	OutType => [ SQL_BINARY ],
	OutPrec => [ 2 ],
	UnpackStrings => [ 'a2' ],
	StmtCnt => 1,
	StartsAt => [ 0 ],
	EndsAt => [ 0 ]
	},
#
#	monitor sql
	{
	Activity => 'Monitor SQL',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT, SQL_INTEGER ],
	InputPrec => [ 2, 2, 4 ],
	Packstring => 'S S L',
	OutCols => [ 'RequestText', 'NumOfSteps','CurStepBegNum','CurStepEndNum',
		'StepNum','StepText' ],
	OutType => [SQL_VARCHAR, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_SMALLINT, SQL_VARCHAR ],
	OutPrec => [ 16344, 2, 2, 2, 2, 16344 ],
	UnpackStrings => [ 'S/a', 'S S S', undef, undef, 'S S/a'],
	StmtCnt => 3,
	StartsAt => [ 0, 1, 4 ],
	EndsAt => [ 0, 3, 5 ]
	},
#
#	V2R5 changes
#
# monses for V2R5.0
	{
	Activity => 'Monitor Session',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT, SQL_INTEGER, SQL_CHAR ],
	InputPrec => [ 2, 2, 4, 30 ],
	Packstring => 'S S L a30',
	OutCols => [ 'SampleSec', 'HostId','LogonPENo','RunVProcNo', 'SessionNo',
		'UserName','UserAccount','UserId','LSN','LogonTime','LogonDate',
		'PartName','Priority','PEState','PECPUSec','XactCount','ReqCount',
		'ReqCacheHits','AMPState','AMPCPUSec','AMPIO','Delta_AMPSpool',
		'Blk_1_HostId', 'Blk_1_SessNo', 'Blk_1_UserID', 'Blk_1_LMode',
		'Blk_1_OType','Blk_1_ObjDBId', 'Blk_1_ObjTId','Blk_1_Status',
		'Blk_2_HostId','Blk_2_SessNo','Blk_2_UserID','Blk_2_LMode',
		'Blk_2_OType','Blk_2_ObjDBId','Blk_2_ObjTId','Blk_2_Status',
		'Blk_3_HostId','Blk_3_SessNo','Blk_3_UserID','Blk_3_LMode',
		'Blk_3_OType','Blk_3_ObjDBId','Blk_3_ObjTId','Blk_3_Status',
		'MoreBlockers','LogonSource','TempSpace','HotAmp1CPU','HotAmp2CPU',
		'HotAmp3CPU','HotAmp1IO', 'HotAmp2IO', 'HotAmp3IO','HotAmp1CPUId',
		'HotAmp2CPUId', 'HotAmp3CPUId','HotAmp1IOId', 'HotAmp2IOId',
		'HotAmp3IOId', 'LowAmp1CPU', 'LowAmp2CPU', 'LowAmp3CPU',
		'LowAmp1IO', 'LowAmp2IO', 'LowAmp3IO', 'LowAmp1CPUId',
		'LowAmp2CPUId', 'LowAmp3CPUId', 'LowAmp1IOId', 'LowAmp2IOId',
		'LowAmp3IOId', 'UpAmpCount', # doc says its INT, DATAINFO says SMALLINT !!!
		'AvgAmpCPUSec', 'AvgAmpIOCnt',
#	new V2R5 fields
		'RequestStartTime', 'RequestStartDate', 'RequestAmpCPU', 'RequestAmpIO'
		],
	OutType => [SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_INTEGER,SQL_CHAR, SQL_CHAR, SQL_INTEGER, SQL_INTEGER, SQL_FLOAT,
		SQL_DATE,SQL_CHAR, SQL_CHAR, SQL_CHAR, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT,SQL_FLOAT, SQL_CHAR, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_SMALLINT,SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_CHAR,
		SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_SMALLINT, SQL_INTEGER,
		SQL_INTEGER, SQL_CHAR, SQL_CHAR, SQL_INTEGER, SQL_INTEGER, SQL_CHAR,
		SQL_SMALLINT, SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_CHAR,
		SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_CHAR, SQL_VARCHAR, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_SMALLINT,SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT,SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT, SQL_DATE, SQL_FLOAT, SQL_FLOAT
		],
	OutPrec => [ 2, 2, 2, 2, 4, 30, 30, 4, 4, 8, 4, 16, 2, 18, 8, 8, 8, 8,
		18, 8, 8, 8, 2, 4, 4, 1, 1, 4, 4, 1, 2, 4, 4, 1, 1, 4, 4, 1, 2, 4,
		4,1, 1, 4,4,1, 1, 128, 8, 8, 8, 8, 8, 8, 8, 2, 2, 2, 2, 2, 2, 8, 8,
		8, 8, 8, 8, 2, 2, 2, 2, 2, 2, 2, 8, 8, 8, 4, 8, 8 ],
	UnpackStrings => [ 'S',
'S S S l a30 a30 l l d l a16 a2 a18 d d d d a18 d d d S l l a1 a1 l l a1 S l l a1 a1 l l a1 S l l a1 a1 l l a1 a1 S/a d d d d d d d S S S S S S d d d d d d S S S S S S S d d d l d d'],
	StmtCnt => 2,
	StartsAt => [ 0, 1 ],
	EndsAt => [ 0, 79 ]
	},
#
#	monitor sql
	{
	Activity => 'Monitor SQL',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT, SQL_INTEGER ],
	InputPrec => [ 2, 2, 4 ],
	Packstring => 'S S L',
	OutCols => [ 'RequestText',
#	names changed for V2R5
		'NumOfSteps','CurLev1StepNum','CurLev2StepNum','StepNum',
		'Confidence', 'EstRowCount', 'ActRowCount', 'EstElapTime',
		'ActElapTime','StepText',],
	OutType => [SQL_VARCHAR, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT,SQL_VARCHAR ],
	OutPrec => [ 16344, 2, 2, 2, 2, 2, 8, 8, 8, 8, 16344 ],
	UnpackStrings => [ 'S/a', 'S S S', undef, undef, 'S S d d d d S/a'],
	StmtCnt => 3,
	StartsAt => [ 0, 1, 4 ],
	EndsAt => [ 0, 3, 10 ]
	},
#
#	V2R6 changes
#
# monses for V2R6.0
	{
	Activity => 'Monitor Session',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT, SQL_INTEGER, SQL_CHAR ],
	InputPrec => [ 2, 2, 4, 30 ],
	Packstring => 'S S L a30',
	OutCols => [ 'SampleSec', 'HostId','LogonPENo','RunVProcNo', 'SessionNo',
		'UserName','UserAccount','UserId','LSN','LogonTime','LogonDate',
		'PartName','Priority','PEState','PECPUSec','XactCount','ReqCount',
		'ReqCacheHits','AMPState','AMPCPUSec','AMPIO','Delta_AMPSpool',
		'Blk_1_HostId', 'Blk_1_SessNo', 'Blk_1_UserID', 'Blk_1_LMode',
		'Blk_1_OType','Blk_1_ObjDBId', 'Blk_1_ObjTId','Blk_1_Status',
		'Blk_2_HostId','Blk_2_SessNo','Blk_2_UserID','Blk_2_LMode',
		'Blk_2_OType','Blk_2_ObjDBId','Blk_2_ObjTId','Blk_2_Status',
		'Blk_3_HostId','Blk_3_SessNo','Blk_3_UserID','Blk_3_LMode',
		'Blk_3_OType','Blk_3_ObjDBId','Blk_3_ObjTId','Blk_3_Status',
		'MoreBlockers','LogonSource','TempSpace','HotAmp1CPU','HotAmp2CPU',
		'HotAmp3CPU','HotAmp1IO', 'HotAmp2IO', 'HotAmp3IO','HotAmp1CPUId',
		'HotAmp2CPUId', 'HotAmp3CPUId','HotAmp1IOId', 'HotAmp2IOId',
		'HotAmp3IOId', 'LowAmp1CPU', 'LowAmp2CPU', 'LowAmp3CPU',
		'LowAmp1IO', 'LowAmp2IO', 'LowAmp3IO', 'LowAmp1CPUId',
		'LowAmp2CPUId', 'LowAmp3CPUId', 'LowAmp1IOId', 'LowAmp2IOId',
		'LowAmp3IOId', 'UpAmpCount', # doc says its INT, DATAINFO says SMALLINT !!!
		'AvgAmpCPUSec', 'AvgAmpIOCnt',
#	new V2R5 fields
		'RequestStartTime', 'RequestStartDate', 'RequestAmpCPU', 'RequestAmpIO',
#	new V2R6 fields
		'RequestNumber', 'WDID', 'ClassificationMode',
		],
	OutType => [SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_INTEGER,SQL_CHAR, SQL_CHAR, SQL_INTEGER, SQL_INTEGER, SQL_FLOAT,
		SQL_DATE,SQL_CHAR, SQL_CHAR, SQL_CHAR, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT,SQL_FLOAT, SQL_CHAR, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_SMALLINT,SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_CHAR,
		SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_SMALLINT, SQL_INTEGER,
		SQL_INTEGER, SQL_CHAR, SQL_CHAR, SQL_INTEGER, SQL_INTEGER, SQL_CHAR,
		SQL_SMALLINT, SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_CHAR,
		SQL_INTEGER, SQL_INTEGER, SQL_CHAR, SQL_CHAR, SQL_VARCHAR, SQL_FLOAT,
		SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_SMALLINT,SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT,SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT, SQL_DATE, SQL_FLOAT, SQL_FLOAT,
		SQL_INTEGER, SQL_INTEGER, SQL_SMALLINT
		],
	OutPrec => [ 2, 2, 2, 2, 4, 30, 30, 4, 4, 8, 4, 16, 2, 18, 8, 8, 8, 8,
		18, 8, 8, 8, 2, 4, 4, 1, 1, 4, 4, 1, 2, 4, 4, 1, 1, 4, 4, 1, 2, 4,
		4,1, 1, 4,4,1, 1, 128, 8, 8, 8, 8, 8, 8, 8, 2, 2, 2, 2, 2, 2, 8, 8,
		8, 8, 8, 8, 2, 2, 2, 2, 2, 2, 2, 8, 8, 8, 4, 8, 8, 4, 4, 2 ],
	UnpackStrings => [ 'S',
'S S S l a30 a30 l l d l a16 a2 a18 d d d d a18 d d d S l l a1 a1 l l a1 S l l a1 a1 l l a1 S l l a1 a1 l l a1 a1 S/a d d d d d d d S S S S S S d d d d d d S S S S S S S d d d l d d l l s'],
	StmtCnt => 2,
	StartsAt => [ 0, 1 ],
	EndsAt => [ 0, 82 ]
	},
#
#	V2R6 monitor sql: adds RunPEVProcNo; requires monitor version 5+
#
	{
	Activity => 'Monitor SQL',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT, SQL_INTEGER, SQL_SMALLINT ],
	InputPrec => [ 2, 2, 4, 2 ],
	Packstring => 'S S L S',
	OutCols => [ 'RequestText',
#	names changed for V2R5
		'NumOfSteps','CurLev1StepNum','CurLev2StepNum','StepNum',
		'Confidence', 'EstRowCount', 'ActRowCount', 'EstElapTime',
		'ActElapTime','StepText',],
	OutType => [SQL_VARCHAR, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_SMALLINT, SQL_SMALLINT, SQL_FLOAT, SQL_FLOAT, SQL_FLOAT,
		SQL_FLOAT,SQL_VARCHAR ],
	OutPrec => [ 16344, 2, 2, 2, 2, 2, 8, 8, 8, 8, 16344 ],
	UnpackStrings => [ 'S/a', 'S S S', undef, undef, 'S S d d d d S/a'],
	StmtCnt => 3,
	StartsAt => [ 0, 1, 4 ],
	EndsAt => [ 0, 3, 10 ]
	},

# TDWM Delay Request Change
	{
	Activity => 'TDWM Delay Request Change',
	InputType => [ SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT, SQL_SMALLINT,
		SQL_INTEGER, SQL_INTEGER, SQL_INTEGER ],
	InputPrec => [ 2, 2, 2, 2, 4, 4, 4 ],
	Packstring => 'S S S S L L L',
	OutCols => undef,
	StmtCnt => 1,
	StartsAt => [ undef ],
	EndsAt => [ undef ]
	},

);

my %pmbfhash = (
'ABORT SESSION', $pmbackfill[0],
'IDENTIFY SESSION', $pmbackfill[1],
'IDENTIFY USER', $pmbackfill[2],
'IDENTIFY DATABASE', $pmbackfill[2],
'IDENTIFY TABLE', $pmbackfill[2],
'MONITOR PHYSICAL CONFIG', $pmbackfill[3],
'MONITOR PHYSICAL RESOURCE', $pmbackfill[4],
'MONITOR PHYSICAL SUMMARY', $pmbackfill[5],
# 'MONITOR SESSION', $pmbackfill[6], always use v2r4.1 version from now on
# 'MONITOR SESSION', $pmbackfill[13], always use v2r5 version from now on
# 'MONITOR SESSION', $pmbackfill[16], always use v2r6 version from now on
'MONITOR SESSION', $pmbackfill[18],
'MONITOR VIRTUAL CONFIG', $pmbackfill[7],
'MONITOR VIRTUAL RESOURCE', $pmbackfill[8],
'MONITOR VIRTUAL SUMMARY', $pmbackfill[9],
'SET RESOURCE RATE', $pmbackfill[10],
'SET SESSION ACCOUNT', $pmbackfill[11],
'SET SESSION RATE', $pmbackfill[12],
'MONITOR VERSION', $pmbackfill[14],
#'MONITOR SQL', $pmbackfill[15]	always use v2r5 version, we'll have to adjust if we get other
'MONITOR SQL', $pmbackfill[17],
'TDWM DELAY REQUEST CHANGE', $pmbackfill[20]
);

sub get_pmapi_template { return $pmbfhash{$_[0]}; }

#
#	hack to support old monses format
sub get_old_monses {
	return $pmbackfill[6]{UnpackStrings}[1];
}

1;