#
#    DBD::Teradata = Perl DBI Driver for Teradata
#    Copyright (C) 2001-2009, Presicient Corp., USA
#    email: info@presicient.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; Refer to the included LICENSE file, or the LICENSE section 
#    of the DBD::Teradata documentation, for details. If neither is available,
#    write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
require 5.008;
use DBI;
DBI->require_version(1.39);
use DBI::DBD;
use Encode;

use bytes;

our $phdfltsz = 16;	# default placeholder VARCHAR size
our $maxbufsz = 64256;	# default request buffer size
our $maxbigbufsz = 2097100;	# for V2R5 utilities
our $platform;
our $copyright = "Copyright(c) 2001-2008, Presicient Corporation, USA";
our $hostchars;
our $debug;
our $inited;
our $use_arm;
our $can_weaken;
our $has_bigint;

package DBD::Teradata;

#pragma begin_clear_text

use Config;
use Exporter;
#pragma begin_redact
use DBI qw(:sql_types);
#pragma end_redact
use Time::Local;

BEGIN {
our @ISA = qw(Exporter);
#pragma begin_redact
#
#	default charset
#
use constant tdat_ASCII =>  127;
use constant tdat_EBCDIC =>  64;
use constant tdat_UTF8 =>  63;
use constant TD_CS_ASCII => 0;
use constant TD_CS_LATIN => 1;
use constant TD_CS_UNICODE => 2;
use constant TD_CS_KANJISJIS => 3;
use constant TD_CS_GRAPHIC => 4;
use constant TD_CS_KANJI1 => 5;
#
#	Datatype codes
#
use constant tdat_BLOB =>  400;
use constant tdat_BLOB_DEFERRED =>  404;
use constant tdat_BLOB_LOCATOR =>  408;
use constant tdat_CLOB =>  416;
use constant tdat_CLOB_DEFERRED =>  420;
use constant tdat_CLOB_LOCATOR =>  424;
use constant tdat_VARCHAR =>  448 ;
use constant tdat_CHAR =>  452 ;
use constant tdat_LONG_VARCHAR =>  456;
use constant tdat_VARGRAPHIC =>  464 ;
use constant tdat_GRAPHIC =>  468 ;
use constant tdat_LONG_VARGRAPHIC =>  472;
use constant tdat_FLOAT =>  480;
use constant tdat_DECIMAL =>  484;
use constant tdat_INTEGER =>  496;
use constant tdat_SMALLINT =>  500;
use constant tdat_BIGINT =>  600;
use constant tdat_VARBYTE =>  688;
use constant tdat_BYTE =>  692;
use constant tdat_LONG_VARBYTE =>  696;
use constant tdat_DATE =>  752;
use constant tdat_BYTEINT =>  756;
use constant tdat_TIMESTAMP =>  760;
use constant tdat_TIME =>  764;

#
#	parcel definitions
#
use constant PclREQUEST => 1;
use constant PclDATA => 3;
use constant PclRESP => 4;
use constant PclKEEPRESP => 5;
use constant PclABORT => 6;
use constant PclCANCEL => 7;
use constant PclSUCCESS => 8;
use constant PclFAILURE => 9;
use constant PclRECORD => 10;
use constant PclENDSTATEMENT => 11;
use constant PclENDREQUEST => 12;
use constant PclFMREQ => 13;
use constant PclOK => 17;
use constant PclFIELD => 18;
use constant PclNULLFIELD => 19;
use constant PclTITLESTART => 20;
use constant PclTITLEEND => 21;
use constant PclFORMATSTART => 22;
use constant PclFORMATEND => 23;
use constant PclSIZESTART => 24;
use constant PclSIZEEND => 25;
use constant PclSIZE => 26;
use constant PclRECSTART => 27;
use constant PclRECEND => 28;
use constant PclPROMPT => 29;
use constant PclREWIND => 31;
use constant PclNOP => 32;
use constant PclWITH => 33;
use constant PclPOSITION => 34;
use constant PclENDWITH => 35;
use constant PclLOGON => 36;
use constant PclLOGOFF => 37;
use constant PclCONFIG => 42;
use constant PclCONFIGRESP => 43;
use constant PclPOSSTART => 46;
use constant PclPOSEND => 47;
use constant PclERROR => 49;
use constant PclINDICDATA => 68;
use constant PclINDICREQ => 69;
use constant PclDATAINFO => 71;
use constant PclOPTIONS => 85;
use constant PclPREPINFO => 86;
use constant PclCONNECT => 88;
use constant PclLSN => 89;
use constant PclASSIGN => 100;
use constant PclASSIGNRSP => 101;
use constant PclMLOADCTRL => 102;
use constant PclMLOADREC => 104;
use constant PclERRORCNT => 105;
use constant PclSESSOPT => 114;
use constant PclCURSORHOST => 120;
use constant PclCURSORDBC => 121;
use constant PclPREPINFOX	=>	125;
use constant PclMULTITSR => 128;
use constant PclSPOPTIONS => 129;
use constant PclSSOAUTHREQ => 130;
use constant PclSSOAUTHRESP => 131;
use constant PclSSOREQ => 132;
use constant PclSSORESP => 134;
use constant PclMULTIPARTREC	=> 144;
use constant PclENDMULTIPARTREC	=> 145;
use constant PclDATAINFOX	=> 146;
use constant PclMULTIPARTREQ	=> 148;
use constant PclBIGRESP => 153;
use constant PclBIGKEEPRESP => 154;
use constant PclGTWCONFIG => 165;
use constant PclCLIENTCONFIG => 166;
use constant PclAUTHMECH => 167;
#
#	msg kinds
#
use constant COPKINDASSIGN    =>  1;
use constant COPKINDREASSIGN =>     2;
use constant COPKINDCONNECT =>   3;
use constant COPKINDRECONNECT =>   4;
use constant COPKINDSTART  =>  5;
use constant COPKINDCONTINUE =>     6;
use constant COPKINDABORT    =>  7;
use constant COPKINDLOGOFF   =>  8;
use constant COPKINDTEST    =>  9;
use constant COPKINDCONFIG  =>  10;
use constant COPKINDAUTHMETHOD => 11;
use constant COPKINDSSOREQ => 12;
use constant COPKINDELICITDATA => 13;
use constant COPKINDELICITFILE => 13;
use constant COPKINDDIRECT  =>  255;
#
#	Platform format codes
#
use constant COPFORMATIBM =>   3;
use constant COPFORMATHONEYWELL  =>  4;
use constant COPFORMATATT_3B2  =>  7;
use constant COPFORMATINTEL8086  =>  8;
use constant COPFORMATVAX  =>  9;
use constant COPFORMATUTS  =>  10;
#
#	implementation object attribute indexes
#
use constant TD_IMPL_REQ2STH => 1;
use constant TD_IMPL_CURREQ => 2;
use constant TD_IMPL_SESRSPSZ => 3;
use constant TD_IMPL_SESAUTH => 4;
use constant TD_IMPL_SESAUTHX => 5;
use constant TD_IMPL_PROMPTED => 6;
use constant TD_IMPL_COMMITSQL => 7;
use constant TD_IMPL_ABORTSQL => 8;
use constant TD_IMPL_CHARSET => 9;
use constant TD_IMPL_SESMODE => 10;
use constant TD_IMPL_SESINXACT => 11;
use constant TD_IMPL_C2S_REQMAP => 12;
use constant TD_IMPL_SESSNO => 13;
use constant TD_IMPL_TWOBUF => 14;
use constant TD_IMPL_CONNFD => 15;
use constant TD_IMPL_SESBUFF => 16;
use constant TD_IMPL_LASTEMSG => 17;
use constant TD_IMPL_LASTERR => 18;
use constant TD_IMPL_SESPART => 19;
use constant TD_IMPL_SESLSN => 20;
use constant TD_IMPL_MAJOR_VER => 21;
use constant TD_IMPL_S2C_REQMAP => 22;
use constant TD_IMPL_ACTIVE => 23;
use constant TD_IMPL_LASTSTATE => 24;
use constant TD_IMPL_UTF8 => 25;
use constant TD_IMPL_BIGREQ => 26;
use constant TD_IMPL_MINOR_VER => 27;
use constant TD_IMPL_VERSION => 28;
use constant TD_IMPL_HOSTID => 29;
use constant TD_IMPL_S2C_SESAUTHX => 30;
use constant TD_IMPL_DBH => 31;
use constant TD_IMPL_SESBUFPOS => 32;
use constant TD_IMPL_S2C_SESAUTH => 33;
use constant TD_IMPL_CRYPT => 34;
use constant TD_IMPL_PASSTHRU => 35;
use constant TD_IMPL_MAINT_VER => 36;
use constant TD_IMPL_EMERG_VER => 37;
use constant TD_IMPL_REQFAC => 38;
use constant TD_IMPL_SESREQSZ => 39;
use constant TD_IMPL_VERSNUM => 40;
use constant TD_IMPL_DECPREC => 41;
#
#	private sth object attribute indexes
#
use constant TD_STH_COLARY => 1;
use constant TD_STH_PTYPES => 2;
use constant TD_STH_STSARY => 3;
use constant TD_STH_CALLPARMS => 4;
use constant TD_STH_PARAMS => 5;
use constant TD_STH_ROWID => 6;
use constant TD_STH_FM_RECORD => 7;
use constant TD_STH_PARMMAP => 8;
use constant TD_STH_PLENS => 9;
use constant TD_STH_UNPACKSTRS => 10;
use constant TD_STH_IOBJ => 11;
use constant TD_STH_REQNO => 12;
use constant TD_STH_ROWS => 13;
use constant TD_STH_MAXCOLARY => 14;
use constant TD_STH_USEPHS => 15;
use constant TD_STH_UPDATABLE => 16;
use constant TD_STH_RSPBUF => 17;
use constant TD_STH_PACKSTR => 18;
use constant TD_STH_RSPBUFPOS => 19;
use constant TD_STH_MONSES => 20;
use constant TD_STH_USENAMES => 21;
use constant TD_STH_LASTRESP => 22;
use constant TD_STH_DBH => 23;
use constant TD_STH_FAILED => 24;
use constant TD_STH_IGNORE_PCLS => 25;
use constant TD_STH_ACTIVE => 26;
use constant TD_STH_MULTIPART => 27;

#
#	echo test constants (used in ByteVar)
#
use constant COPDISCARDTEST =>  0 ;
use constant COPECHOTEST =>  1 ;
#
#	bytevar for start/continue response
#	indicates xaction state
#
use constant COPNOTINTRANS  => 0;
use constant COPINTRANS     => 1;
#
#	offset to frequently ref'd header fields
#
use constant TDAT_SESSNO_OFF => 20;
use constant TDAT_AUTH_OFF => 24;
use constant TDAT_REQNO_OFF => 32;
use constant TDAT_LENHI_OFF => 3;
use constant TDAT_LENLO_OFF => 8;

use constant TDAT_LAN_VERSION => 3;
use constant TDAT_ACT_CALL => 105;
use constant TDAT_ACT_CREATE_PROC => 104;
use constant TDAT_ACT_DROP_PROC => 103;
use constant TDAT_SP_IND => 500;
use constant TDAT_SP_OUT_MASK => 3;

use constant TDAT_HDRSZ => 52;

use constant tdat_NULL_MASK => 0xfffe;
use constant MAX_PARM_TUPLES => 2147483648;
#
#	partition codes
#
use constant TD_PART_SQL => 1;
use constant TD_PART_MONITOR => 2;
use constant TD_PART_EXPORT => 3;
use constant TD_PART_MLOAD => 4;
use constant TD_PART_FASTLOAD => 5;
use constant TD_PART_DBCCONS => 6;
#pragma end_redact

# Make some utility functions available if asked for
our @EXPORT    = ();		    # we export nothing by default
our @EXPORT_OK = qw(
	%td_type_code2str
	%td_type_str2baseprec
	%td_type_str2basescale
	%td_lob_scale
	@td_decszs
	%td_type_str2dbi
	%td_type_str2size
	%td_type_str2pack
	%td_type_str2stringtypes
	%td_type_str2binarytypes
	%td_type_dbi2stringtypes
	%td_type_dbi2str
	%td_activity_types
	%td_sqlstates
	@td_indicbits
	@td_indicmasks
	%td_type_dbi2preconly
	%td_type_dbi2hasprec
	%td_type_dbi2hasscale
	%td_type_dbi2pack
	%td_type_code2dbi
	%td_type_dbi2code
	%td_type_dbi2size
	%td_type_str2ddcodes
	%td_type_ddcode2str
	%td_part_map
);

#
#	another Perl screwup...we can't use empty export tag lists
#	(at least on AS Win32 5.8.6...)
#	so we'll create a set of dummys, and change obfu to
#	insert a dummy
#
our %EXPORT_TAGS = (
	tdat_dummycodes => [
#pragma begin_redact qw/tdat_BLOB/
		qw/tdat_BLOB/
#pragma end_redact
	],

	tdat_charsets => [
#pragma begin_redact
		qw/tdat_ASCII tdat_EBCDIC tdat_UTF8 TD_CS_ASCII TD_CS_LATIN TD_CS_UNICODE
		TD_CS_KANJISJIS TD_CS_GRAPHIC TD_CS_KANJI1/
#pragma end_redact
	],

	tdat_msgkinds => [
#pragma begin_redact
	qw/COPKINDASSIGN COPKINDREASSIGN COPKINDCONNECT COPKINDRECONNECT
		COPKINDSTART COPKINDCONTINUE COPKINDABORT COPKINDLOGOFF
		COPKINDTEST COPKINDCONFIG COPKINDAUTHMETHOD COPKINDSSOREQ
		COPKINDELICITDATA COPKINDELICITFILE COPKINDDIRECT/
#pragma end_redact
	],

	tdat_platforms => [
#pragma begin_redact
	qw/COPFORMATIBM COPFORMATHONEYWELL COPFORMATATT_3B2
	COPFORMATINTEL8086 COPFORMATVAX COPFORMATUTS/
#pragma end_redact
	],

	tdat_typecodes => [
#pragma begin_redact
	qw/tdat_BLOB tdat_BLOB_DEFERRED tdat_BLOB_LOCATOR tdat_CLOB
	tdat_CLOB_DEFERRED tdat_CLOB_LOCATOR tdat_VARCHAR tdat_CHAR
	tdat_LONG_VARCHAR tdat_VARGRAPHIC tdat_GRAPHIC
	tdat_LONG_VARGRAPHIC tdat_FLOAT tdat_DECIMAL tdat_INTEGER
	tdat_SMALLINT tdat_BIGINT tdat_VARBYTE tdat_BYTE
	tdat_LONG_VARBYTE tdat_DATE tdat_BYTEINT tdat_TIMESTAMP
	tdat_TIME/
#pragma end_redact
	],

	tdat_parcels => [
#pragma begin_redact
	qw/PclREQUEST PclDATA PclRESP PclKEEPRESP PclABORT PclCANCEL PclSUCCESS
	PclFAILURE PclRECORD PclENDSTATEMENT PclENDREQUEST PclFMREQ PclOK
	PclFIELD PclNULLFIELD PclTITLESTART PclTITLEEND PclFORMATSTART
	PclFORMATEND PclSIZESTART PclSIZEEND PclSIZE PclRECSTART PclRECEND
	PclPROMPT PclREWIND PclNOP PclWITH PclPOSITION PclENDWITH PclLOGON
	PclLOGOFF PclCONFIG PclCONFIGRESP PclPOSSTART PclPOSEND PclERROR
	PclINDICDATA PclINDICREQ PclDATAINFO PclOPTIONS PclPREPINFO PclCONNECT
	PclLSN PclASSIGN PclASSIGNRSP PclMLOADCTRL PclMLOADREC PclERRORCNT
	PclSESSOPT PclCURSORHOST PclCURSORDBC PclMULTITSR PclSPOPTIONS
	PclSSOAUTHREQ PclSSOAUTHRESP PclSSOREQ PclSSORESP PclBIGRESP
	PclBIGKEEPRESP PclGTWCONFIG PclCLIENTCONFIG PclAUTHMECH PclMULTIPARTREQ
	PclPREPINFOX PclMULTIPARTREC PclENDMULTIPARTREC PclDATAINFOX/
#pragma end_redact
	],

	tdat_impl_codes => [
#pragma begin_redact
	qw/TD_IMPL_REQ2STH TD_IMPL_CURREQ TD_IMPL_SESRSPSZ TD_IMPL_SESAUTH
	TD_IMPL_SESAUTHX TD_IMPL_PROMPTED TD_IMPL_COMMITSQL TD_IMPL_ABORTSQL
	TD_IMPL_CHARSET TD_IMPL_SESMODE TD_IMPL_SESINXACT TD_IMPL_C2S_REQMAP
	TD_IMPL_SESSNO TD_IMPL_TWOBUF TD_IMPL_CONNFD TD_IMPL_SESBUFF
	TD_IMPL_LASTEMSG TD_IMPL_LASTERR TD_IMPL_SESPART TD_IMPL_SESLSN
	TD_IMPL_MAJOR_VER TD_IMPL_S2C_REQMAP TD_IMPL_ACTIVE TD_IMPL_LASTSTATE
	TD_IMPL_UTF8 TD_IMPL_BIGREQ TD_IMPL_MINOR_VER TD_IMPL_VERSION TD_IMPL_HOSTID
	TD_IMPL_S2C_SESAUTHX TD_IMPL_DBH TD_IMPL_SESBUFPOS TD_IMPL_S2C_SESAUTH
	TD_IMPL_CRYPT TD_IMPL_PASSTHRU TD_IMPL_MAINT_VER TD_IMPL_EMERG_VER
	TD_IMPL_REQFAC TD_IMPL_SESREQSZ TD_IMPL_VERSNUM TD_IMPL_DECPREC/
#pragma end_redact
	],

	tdat_sth_codes => [
#pragma begin_redact
	qw/TD_STH_COLARY TD_STH_PTYPES TD_STH_STSARY TD_STH_CALLPARMS TD_STH_PARAMS
	TD_STH_ROWID TD_STH_FM_RECORD TD_STH_PARMMAP TD_STH_PLENS TD_STH_UNPACKSTRS
	TD_STH_IOBJ TD_STH_REQNO TD_STH_ROWS TD_STH_MAXCOLARY TD_STH_USEPHS
	TD_STH_UPDATABLE TD_STH_RSPBUF TD_STH_PACKSTR TD_STH_RSPBUFPOS TD_STH_MONSES
	TD_STH_USENAMES TD_STH_LASTRESP TD_STH_DBH TD_STH_FAILED TD_STH_IGNORE_PCLS
	TD_STH_ACTIVE TD_STH_MULTIPART/
#pragma end_redact
	],

	tdat_misc_codes => [
#pragma begin_redact
	qw/COPDISCARDTEST COPECHOTEST COPNOTINTRANS COPINTRANS
	TDAT_ACT_CALL TDAT_ACT_CREATE_PROC TDAT_ACT_DROP_PROC TDAT_SP_IND
	TDAT_SP_OUT_MASK TDAT_HDRSZ tdat_NULL_MASK TDAT_LAN_VERSION
	TDAT_SESSNO_OFF TDAT_REQNO_OFF TDAT_AUTH_OFF TDAT_LENHI_OFF
	TDAT_LENLO_OFF MAX_PARM_TUPLES/
#pragma end_redact
	],

	tdat_part_codes => [
#pragma begin_redact
	qw/TD_PART_SQL TD_PART_MONITOR TD_PART_EXPORT TD_PART_MLOAD TD_PART_FASTLOAD TD_PART_DBCCONS/
#pragma end_redact
	],

);
#
#	add the tags
#
Exporter::export_tags(keys %EXPORT_TAGS);
#
#	some platform-specific constants
#
	$platform = $ENV{TDAT_PLATFORM_CODE};
	$hostchars = (ord('A') != 65) ? tdat_EBCDIC : tdat_ASCII;

	my $netval = unpack('n', pack('s', 1234));
	$platform = ($hostchars == tdat_EBCDIC) ? COPFORMATIBM :
		(($netval == 1234) ? COPFORMATATT_3B2 : COPFORMATINTEL8086)
		unless $platform && ($platform=~/^\d+$/);
	$hostchars |= 128 if ($netval == 1234);

	my $phsz = $ENV{TDAT_PH_SIZE};
	$phdfltsz = $phsz
		if defined($phsz) && ($phsz=~/^\d+$/) && ($phsz > 0) && ($phsz < 1024);

	$debug = $ENV{TDAT_DBD_DEBUG} || 0;

	$use_arm = ($Config{archname}=~/^arm-linux/i);
#
#	check if we can weaken
#	NOTE: borrowed from DBI 1.50; we should
#	consider switching to ChildHandles
#	attribute at some future release
#
	$can_weaken = eval {
	    require Scalar::Util;
# this will croak() if this Scalar::Util doesn't have a working weaken().
	    Scalar::Util::weaken(my $test = \"foo");
	    1;
	};

	$has_bigint = eval {
		require Math::BigInt;
		1;
	};
};

our %td_type_code2str = (
tdat_BLOB, 'BLOB',
tdat_BLOB_DEFERRED, 'DEFERRED BLOB',
tdat_BLOB_LOCATOR, 'BLOB LOCATOR',
tdat_CLOB, 'CLOB',
tdat_CLOB_DEFERRED, 'DEFERRED CLOB',
tdat_CLOB_LOCATOR, 'CLOB LOCATOR',
tdat_VARCHAR, 'VARCHAR',
tdat_CHAR, 'CHAR',
tdat_LONG_VARCHAR, 'LONG VARCHAR',
tdat_VARGRAPHIC, 'VARGRAPHIC',
tdat_GRAPHIC, 'GRAPHIC',
tdat_LONG_VARGRAPHIC, 'LONG VARGRAPHIC',
tdat_FLOAT, 'FLOAT',
tdat_DECIMAL, 'DECIMAL',
tdat_INTEGER, 'INTEGER',
tdat_SMALLINT, 'SMALLINT',
tdat_BIGINT, 'BIGINT',
tdat_VARBYTE, 'VARBYTE',
tdat_BYTE, 'BYTE',
tdat_LONG_VARBYTE, 'LONG VARBYTE',
tdat_DATE, 'DATE',
tdat_BYTEINT, 'BYTEINT',
tdat_TIMESTAMP, 'TIMESTAMP',
tdat_TIME, 'TIME',
);

our %td_type_code2dbi = (
tdat_BLOB, SQL_BLOB,
tdat_BLOB_DEFERRED, SQL_BLOB,
tdat_BLOB_LOCATOR, SQL_BLOB_LOCATOR,
tdat_CLOB, SQL_CLOB,
tdat_CLOB_DEFERRED, SQL_CLOB,
tdat_CLOB_LOCATOR, SQL_CLOB_LOCATOR,
tdat_VARCHAR, SQL_VARCHAR,
tdat_CHAR, SQL_CHAR,
# tdat_LONG_VARCHAR, SQL_LONGVARCHAR, cheat and use regular varchar
tdat_LONG_VARCHAR, SQL_VARCHAR,
tdat_VARGRAPHIC, SQL_WVARCHAR,
tdat_GRAPHIC, SQL_WCHAR,
tdat_LONG_VARGRAPHIC, SQL_WLONGVARCHAR,
tdat_FLOAT, SQL_FLOAT,
tdat_DECIMAL, SQL_DECIMAL,
tdat_INTEGER, SQL_INTEGER,
tdat_SMALLINT, SQL_SMALLINT,
tdat_BIGINT, SQL_BIGINT,
tdat_VARBYTE, SQL_VARBINARY,
tdat_BYTE, SQL_BINARY,
#tdat_LONG_VARBYTE, SQL_LONGVARBINARY,	cheat and use regular varbinary
tdat_LONG_VARBYTE, SQL_VARBINARY,
tdat_DATE, SQL_DATE,
tdat_BYTEINT, SQL_TINYINT,
tdat_TIMESTAMP, SQL_TIMESTAMP,
tdat_TIME, SQL_TIME,
);

#
#	for USING parse
#
our %td_type_str2baseprec = (
'DEC', 5, 'DECIMAL', 5, 'CHAR', 1, 'VARCHAR', 1, 'BYTE', 1,
'VARBYTE', 1, 'GRAPHIC', 1, 'VARGRAPHIC', 1,
'TIMESTAMP', 6, 'TIME', 6, 'GRAPHIC', 1, 'YEAR', 2,
'MONTH', 2, 'DAY', 2, 'HOUR', 2,
'SECOND', 2, 'BLOB', 2147483648, 'CLOB', 2147483648);

our %td_type_str2basescale = (
'DEC', 0, 'DECIMAL', 0, 'YEAR', 6, 'DAY', 6, 'HOUR', 6, 'SECOND', 6);

our %td_lob_scale = (
'K', 1024,
'M', 1048576,
'G', 1073741824);

our @td_decszs = (
	1, 1, 1,		# 0 to 2
	2, 2, 			# 3 to 4
	4, 4, 4, 4, 4,	# 5 to 9
	((8) x 9),		# 10 to 18
	((16) x 20));	# 19 to 38

our %td_type_str2dbi = (
'CHAR', SQL_CHAR,
'VARCHAR', SQL_VARCHAR,
'BYTE', SQL_BINARY,
'VARBYTE', SQL_VARBINARY,
'INT', SQL_INTEGER,
'INTEGER', SQL_INTEGER,
'SMALLINT', SQL_SMALLINT,
'BYTEINT', SQL_TINYINT,
'FLOAT', SQL_FLOAT,
'DEC', SQL_DECIMAL,
'DECIMAL', SQL_DECIMAL,
'DATE', SQL_DATE,
'TIMESTAMP', SQL_TIMESTAMP,
'INTERVAL', SQL_TIMESTAMP,
'GRAPHIC', SQL_BINARY,
'VARGRAPHIC', SQL_VARBINARY,
'TIME', SQL_TIME,
'INTERVAL DAY', SQL_INTERVAL_DAY,
'INTERVAL DAY TO HOUR', SQL_INTERVAL_DAY_TO_HOUR,
'INTERVAL DAY TO MINUTE', SQL_INTERVAL_DAY_TO_MINUTE,
'INTERVAL DAY TO SECOND', SQL_INTERVAL_DAY_TO_SECOND,
'INTERVAL HOUR', SQL_INTERVAL_HOUR,
'INTERVAL HOUR TO MINUTE', SQL_INTERVAL_HOUR_TO_MINUTE,
'INTERVAL HOUR TO SECOND', SQL_INTERVAL_HOUR_TO_SECOND,
'INTERVAL MINUTE', SQL_INTERVAL_MINUTE,
'INTERVAL MINUTE TO SECOND', SQL_INTERVAL_MINUTE_TO_SECOND,
'INTERVAL MONTH', SQL_INTERVAL_MONTH,
'INTERVAL SECOND', SQL_INTERVAL_SECOND,
'INTERVAL YEAR', SQL_INTERVAL_YEAR,
'INTERVAL YEAR TO MONTH', SQL_INTERVAL_YEAR_TO_MONTH,
'TIMESTAMP WITH TIME ZONE', SQL_TYPE_TIMESTAMP_WITH_TIMEZONE,
'TIME WITH TIME ZONE', SQL_TYPE_TIME_WITH_TIMEZONE,
'CLOB', SQL_CLOB,	# need LOCATOR/DEFERRED versions of these
'BLOB', SQL_BLOB,
'BIGINT', SQL_BIGINT,
);

our %td_type_str2size = (
'CHAR', 1,
'VARCHAR', 32000,
'BYTE', 1,
'VARBYTE', 32000,
'INT', 4,
'INTEGER', 4,
'SMALLINT', 2,
'BYTEINT', 1,
'FLOAT', 8,
'DEC', 4,
'DECIMAL', 4,
'DATE', 4,
'GRAPHIC', 2,
'VARGRAPHIC', 32000,
'TIME', 8,
'TIMESTAMP', 19,
'TIMESTAMP WITH TIME ZONE', 25,
'TIME WITH TIME ZONE', 14,
'INTERVAL DAY', 1,
'INTERVAL DAY TO HOUR', 4,
'INTERVAL DAY TO MINUTE', 7,
'INTERVAL DAY TO SECOND', 10,
'INTERVAL HOUR', 1,
'INTERVAL HOUR TO MINUTE', 4,
'INTERVAL HOUR TO SECOND', 7,
'INTERVAL MINUTE', 1,
'INTERVAL MINUTE TO SECOND', 4,
'INTERVAL MONTH', 1,
'INTERVAL SECOND', 1,
'INTERVAL YEAR', 1,
'INTERVAL YEAR TO MONTH', 4,
'BIGINT', 8
);

our %td_type_str2pack = (
	'VARCHAR', 'S/a*',
	'CHAR', 'A',
	'FLOAT', 'd',
	'DEC', 'a',
	'DECIMAL', 'a',
	'INT', 'l',
	'INTEGER', 'l',
	'SMALLINT', 's',
	'BYTEINT', 'c',
	'VARBYTE', 'S/a*',
	'BYTE', 'a',
	'DATE', 'l',
	'TIMESTAMP', 'A',
	'TIME', 'A',
	'INTERVAL DAY', 'A',
	'INTERVAL DAY TO HOUR', 'A',
	'INTERVAL DAY TO MINUTE', 'A',
	'INTERVAL DAY TO SECOND', 'A',
	'INTERVAL HOUR', 'A',
	'INTERVAL HOUR TO MINUTE', 'A',
	'INTERVAL HOUR TO SECOND', 'A',
	'INTERVAL MINUTE', 'A',
	'INTERVAL MINUTE TO SECOND', 'A',
	'INTERVAL MONTH', 'A',
	'INTERVAL SECOND', 'A',
	'INTERVAL YEAR', 'A',
	'INTERVAL YEAR TO MONTH', 'A',
	'TIMESTAMP WITH TIME ZONE', 'A',
	'TIME WITH TIME ZONE', 'A',
	'CLOB', 'Q/a*',
	'BLOB', 'Q/a*',
	'BIGINT', 'a8',
	);

our %td_type_str2stringtypes = (
	'VARCHAR', 1,
	'CHAR', 1,
	'TIMESTAMP', 1,
	'TIME', 1,
	'INTERVAL DAY', 1,
	'INTERVAL DAY TO HOUR', 1,
	'INTERVAL DAY TO MINUTE', 1,
	'INTERVAL DAY TO SECOND', 1,
	'INTERVAL HOUR', 1,
	'INTERVAL HOUR TO MINUTE', 1,
	'INTERVAL HOUR TO SECOND', 1,
	'INTERVAL MINUTE', 1,
	'INTERVAL MINUTE TO SECOND', 1,
	'INTERVAL MONTH', 1,
	'INTERVAL SECOND', 1,
	'INTERVAL YEAR', 1,
	'INTERVAL YEAR TO MONTH', 1,
	'TIMESTAMP WITH TIME ZONE', 1,
	'TIME WITH TIME ZONE', 1,
	'CLOB', 1,
	);

our %td_type_str2binarytypes = (
	'VARGRAPHIC', 1,
	'GRAPHIC', 1,
	'VARBYTE', 1,
	'BYTE', 1,
	'BLOB', 1,
	);

our %td_type_dbi2str = (
	SQL_VARCHAR, 'VARCHAR',
	SQL_CHAR, 'CHAR',
	SQL_FLOAT, 'FLOAT',
	SQL_DECIMAL, 'DECIMAL',
	SQL_INTEGER, 'INTEGER',
	SQL_SMALLINT, 'SMALLINT',
	SQL_TINYINT, 'BYTEINT',
	SQL_VARBINARY, 'VARBYTE',
	SQL_BINARY, 'BYTE',
#	SQL_LONGVARBINARY, 'LONG VARBINARY',
	SQL_DATE, 'DATE',
	SQL_TIMESTAMP, 'TIMESTAMP',
	SQL_TIME, 'TIME',
	SQL_INTERVAL_DAY, 'INTERVAL DAY',
	SQL_INTERVAL_DAY_TO_HOUR, 'INTERVAL DAY TO HOUR',
	SQL_INTERVAL_DAY_TO_MINUTE, 'INTERVAL DAY TO MINUTE',
	SQL_INTERVAL_DAY_TO_SECOND, 'INTERVAL DAY TO SECOND',
	SQL_INTERVAL_HOUR, 'INTERVAL HOUR',
	SQL_INTERVAL_HOUR_TO_MINUTE, 'INTERVAL HOUR TO MINUTE',
	SQL_INTERVAL_HOUR_TO_SECOND, 'INTERVAL HOUR TO SECOND',
	SQL_INTERVAL_MINUTE, 'INTERVAL MINUTE',
	SQL_INTERVAL_MINUTE_TO_SECOND, 'INTERVAL MINUTE TO SECOND',
	SQL_INTERVAL_MONTH, 'INTERVAL MONTH',
	SQL_INTERVAL_SECOND, 'INTERVAL SECOND',
	SQL_INTERVAL_YEAR, 'INTERVAL YEAR',
	SQL_INTERVAL_YEAR_TO_MONTH, 'INTERVAL YEAR TO MONTH',
	SQL_TYPE_TIMESTAMP_WITH_TIMEZONE, 'TIMESTAMP WITH TIME ZONE',
	SQL_TYPE_TIME_WITH_TIMEZONE, 'TIME WITH TIME ZONE',
	SQL_BIGINT, 'BIGINT',
	);

#
#	activity type map
#
our %td_activity_types = (
0, 'Unknown',
1, 'Select',
2, 'Insert',
3, 'Update',
4, 'Update..RETRIEVING',
5, 'Delete',
6, 'Create Table',
7, 'Modify Table',
8, 'Create View',
9, 'Create Macro',
10, 'Drop Table',
11, 'Drop View',
12, 'Drop Macro',
13, 'Drop Index',
14, 'Rename Table',
15, 'Rename View',
16, 'Rename Macro',
17, 'Create Index',
18, 'Create Database',
19, 'Create User',
20, 'Grant',
21, 'Revoke',
22, 'Give',
23, 'Drop Database',
24, 'Modify Database',
25, 'Database',
26, 'Begin Transaction',
27, 'End Transaction',
28, 'Abort',
29, 'Null',
30, 'Execute',
31, 'Comment Set',
32, 'Comment Returning',
33, 'Echo',
34, 'Replace View',
35, 'Replace Macro',
36, 'Checkpoint',
37, 'Delete Journal',
38, 'Rollback',
39, 'Release Lock',
40, 'HUT Config',
41, 'Verify Checkpoint',
42, 'Dump Journal',
43, 'Dump',
44, 'Restore',
45, 'RollForward',
46, 'Delete Database',
47, 'internal use only',
48, 'internal use only',
49, 'Show',
50, 'Help',
51, 'Begin Loading',
52, 'Checkpoint Load',
53, 'End Loading',
54, 'Insert',
64, 'Exec MLOAD',
76, '2PC Vote Request',
77, '2PC Vote and Terminate',
78, '2PC Commit',
79, '2PC Abort',
80, '2PC Yes Vote',
83, 'Set Session Rate',
84, 'Monitor Session',
85, 'Identify Session',
86, 'Abort Session',
87, 'Set Resource Rate',
89, 'ANSI Commit Work',
91, 'Monitor Virtual Config',
92, 'Monitor Physical Config',
93, 'Monitor Virtual Summary',
94, 'Monitor Physical Summary',
95, 'Monitor Virtual Resource',
96, 'Monitor Physical Resource',
97, 'Create Trigger',
98, 'Drop Trigger',
99, 'Rename Trigger',
100, 'Replace Trigger',
101, 'Alter Trigger',
103, 'Drop Procedure',
104, 'Create Procedure',
105, 'Call',
106, 'Rename Procedure',
107, 'Replace Procedure',
108, 'Set Session Account',
110, 'Monitor SQL',
111, 'Monitor Version',
112, 'Begin DBQL',
113, 'End DBQL',
114, 'Create Role',
115, 'Drop Role',
116, 'Grant Role',
117, 'Revoke Role',
118, 'Create Profile',
119, 'Modify Profile',
120, 'Drop Profile',
121, 'Set Role',
122, 'UDF',
123, 'UDF',
124, 'UDF',
125, 'UDF',
126, 'UDF',
127, 'Merge Mixed',
128, 'Merge Update',
129, 'Merge Insert',
130, 'Alter Procedure',
131, 'TDQM Enable',
132, 'TDQM Stats',
133, 'TDQM Performance Group',
);
#
#	SQLSTATE map
#
our %td_sqlstates = qw(
2147 53018 2149 53018 2161 22012 2162 22012 2163 22012 2164 22003 2165 22003 2166 22003
2232 22003 2233 22003 2239 22003 2240 22003 2450 40001 2603 53015 2604 53015 2605 53015
2606 53015 2607 53015 2608 53015 2614 22003 2615 22003 2616 22003 2617 22003 2618 22012
2619 22012 2620 22021 2621 22012 2622 53015 2623 53015 2631 40001 2661 22000 2662 22011
2663 22011 2664 54001 2665 22007 2666 22007 2674 22003 2675 22003 2676 22012 2679 22012
2680 22023 2682 22003 2683 22003 2684 22012 2687 22012 2689 23502 2700 23700 2726 23726
2727 23727 2728 23728 2801 23505 2802 23505 2803 23505 2805 57014 2827 58004 2828 58004
2843 57014 2892 01003 2893 22001 2894 24894 2895 24895 2896 24896 2938 00000 2980 23505
3002 46000 3003 46000 3004 46000 3006 08T06 3007 08T07 3014 46000 3015 46000 3016 46000
3023 46000 3025 08T25 3026 46000 3110 00000 3111 40502 3120 58004 3121 02000 3130 57014
3504 53003 3509 54001 3513 00000 3514 00000 3515 52011 3517 52011 3518 54008 3519 52010
3520 22003 3523 42000 3524 42000 3526 52004 3527 42015 3528 22012 3529 42015 3530 42015
3532 53021 3534 52010 3535 22018 3539 52004 3540 54001 3541 57014 3543 57014 3545 42502
3554 53003 3556 54011 3560 52011 3564 44000 3568 42507 3569 56003 3574 56003 3577 57014
3580 53015 3581 53015 3582 42582 3597 54001 3604 23502 3606 52001 3609 54001 3617 42015
3622 53019 3627 42507 3628 42507 3629 54001 3637 53005 3638 57014 3639 53018 3640 53018
3641 53021 3642 53021 3643 53019 3644 53019 3653 53026 3654 53025 3656 52004 3659 53008
3660 53015 3661 57014 3662 53015 3663 53015 3669 21000 3702 54001 3704 42506 3705 54001
3710 54001 3712 54001 3714 54001 3721 24721 3731 42501 3732 0A732 3733 42514 3735 01004
3737 01004 3738 01004 3741 54001 3744 52010 3747 01003 3751 42504 3752 22003 3753 22003
3754 22003 3755 22003 3756 22012 3757 22003 3758 22003 3759 42504 3760 42503 3775 42506
3789 42514 3801 52010 3802 52004 3803 52010 3804 52010 3805 52010 3807 42000 3809 52002
3810 52003 3811 23502 3812 42000 3813 42000 3816 42505 3817 42505 3818 42505 3819 53015
3820 42505 3821 42505 3822 52002 3823 53007 3824 52004 3827 56021 3829 56021 3833 56021
3848 52006 3850 54001 3851 54001 3856 42501 3857 53015 3858 42501 3865 42501 3866 42501
3867 54001 3872 56003 3880 42501 3881 42501 3883 53003 3885 52001 3889 42514 3896 54001
3897 58004 3919 54011 3968 42968 3969 42969 3970 42970 3971 42971 3973 42973 3974 42974
3975 42975 3976 53030 3977 42977 3978 42978 3979 42979 3980 42980 3981 42981 3982 42982
3989 01001 3990 42990 3991 42991 3992 42992 3993 42993 3994 42994 3995 42995 3996 22001
3997 22019 3998 22025 3999 01999 5300 42J00 5301 42J01 5302 52009 5303 42J03 5304 42J04
5305 42J05 5306 42J06 5307 42J07 5308 42J08 5309 42J09 5310 42J10 5311 42J11 5312 42J12
5313 42J13 5314 42J14 5315 42J15 5316 44000 5317 23000 5800 01800 5801 01801 5802 01802
5803 01803 5804 01804 5805 01805 5806 01806 5807 01807 5808 01808 5809 01809 5810 01810
5811 01811 5812 01812 5813 01813 5814 01814 5815 01815 5816 01816 5817 01817 5818 01818
5819 01819 5820 01820 5821 01821 5822 01822 5823 01823 5824 01824 5825 01825 5826 01826
5827 01827 5828 01828 5829 01829 5830 01830 5831 01831 5832 01832 5833 01833 5834 01834
5835 01835 5836 01836 5837 01837 5838 01838 5839 01839 5840 01840 5841 01841 7601 20000
7610 24502 7627 21000 7631 24501 7632 02000 );
#
#	indicator bit masks
our @td_indicbits = (128, 64, 32, 16, 8, 4, 2, 1);
our @td_indicmasks = (0x7F, 0xBF, 0xDF, 0xEF, 0xF7, 0xFB, 0xFD, 0xFE);
#
#	decimal pack map, indexed by precision

our %td_type_dbi2preconly = (
	SQL_CHAR, 1,
	SQL_VARCHAR, 1,
	SQL_LONGVARCHAR, 1,
	SQL_BINARY, 1,
	SQL_VARBINARY, 1,
	SQL_DECIMAL, 2);

our %td_type_dbi2hasprec = (
SQL_CHAR, 1,
SQL_VARCHAR, 1,
SQL_BINARY, 1,
SQL_VARBINARY, 1,
SQL_DECIMAL, 1,
SQL_TIME, 1,
SQL_TIMESTAMP, 1);

our %td_type_dbi2hasscale = (
SQL_DECIMAL, 1,
SQL_INTERVAL_DAY_TO_SECOND, 1,
SQL_INTERVAL_HOUR_TO_SECOND, 1,
SQL_INTERVAL_MINUTE_TO_SECOND, 1,
);

our %td_type_dbi2pack = (
	SQL_VARCHAR, 'S/a*',
	SQL_CHAR, 'A',
	SQL_FLOAT, 'd',
	SQL_DECIMAL, 'a',
	SQL_INTEGER, 'l',
	SQL_SMALLINT, 's',
	SQL_TINYINT, 'c',
	SQL_VARBINARY, 'S/a*',
	SQL_BINARY, 'a',
	SQL_LONGVARBINARY, 'S/a*',
	SQL_DATE, 'l',
	SQL_TIMESTAMP, 'A',
	SQL_TIME, 'A',
	SQL_INTERVAL_DAY, 'A',
	SQL_INTERVAL_DAY_TO_HOUR, 'A',
	SQL_INTERVAL_DAY_TO_MINUTE, 'A',
	SQL_INTERVAL_DAY_TO_SECOND, 'A',
	SQL_INTERVAL_HOUR, 'A',
	SQL_INTERVAL_HOUR_TO_MINUTE, 'A',
	SQL_INTERVAL_HOUR_TO_SECOND, 'A',
	SQL_INTERVAL_MINUTE, 'A',
	SQL_INTERVAL_MINUTE_TO_SECOND, 'A',
	SQL_INTERVAL_MONTH, 'A',
	SQL_INTERVAL_SECOND, 'A',
	SQL_INTERVAL_YEAR, 'A',
	SQL_INTERVAL_YEAR_TO_MONTH, 'A',
	SQL_TYPE_TIMESTAMP_WITH_TIMEZONE, 'A',
	SQL_TYPE_TIME_WITH_TIMEZONE, 'A',
	SQL_BIGINT, 'a8',
	);

our %td_type_dbi2size = (
	SQL_VARCHAR, 32000,
	SQL_CHAR, 32000,
	SQL_FLOAT, 8,
	SQL_DECIMAL, 8,
	SQL_INTEGER, 4,
	SQL_SMALLINT, 2,
	SQL_TINYINT, 1,
	SQL_VARBINARY, 32000,
	SQL_BINARY, 32000,
	SQL_LONGVARBINARY, 32000,
	SQL_DATE, 4,
	SQL_TIMESTAMP, 26,
	SQL_TIME, 17,
	SQL_BIGINT, 8,
	);

our %td_type_dbi2code = (
SQL_BLOB, tdat_BLOB,
#SQL_BLOB, tdat_BLOB_DEFERRED,
SQL_BLOB_LOCATOR, tdat_BLOB_LOCATOR,
SQL_CLOB, tdat_CLOB,
#  SQL_CLOB, tdat_CLOB_DEFERRED,
SQL_CLOB_LOCATOR, tdat_CLOB_LOCATOR,
SQL_VARCHAR, tdat_VARCHAR,
SQL_CHAR, tdat_CHAR,
SQL_LONGVARCHAR, tdat_LONG_VARCHAR,
SQL_WVARCHAR, tdat_VARGRAPHIC,
SQL_WCHAR, tdat_GRAPHIC,
SQL_WLONGVARCHAR, tdat_LONG_VARGRAPHIC,
SQL_FLOAT, tdat_FLOAT,
SQL_DECIMAL, tdat_DECIMAL,
SQL_INTEGER, tdat_INTEGER,
SQL_SMALLINT,tdat_SMALLINT,
SQL_BIGINT, tdat_BIGINT,
SQL_VARBINARY, tdat_VARBYTE,
SQL_BINARY, tdat_BYTE,
SQL_LONGVARBINARY, tdat_LONG_VARBYTE,
SQL_DATE, tdat_DATE,
SQL_TINYINT, tdat_BYTEINT,
SQL_TIMESTAMP,tdat_TIMESTAMP,
SQL_TIME, tdat_TIME,
);

our %td_type_dbi2stringtypes = (
SQL_TIMESTAMP, 1,
SQL_TIME, 1,
SQL_INTERVAL_DAY, 1,
SQL_INTERVAL_DAY_TO_HOUR, 1,
SQL_INTERVAL_DAY_TO_MINUTE, 1,
SQL_INTERVAL_DAY_TO_SECOND, 1,
SQL_INTERVAL_HOUR, 1,
SQL_INTERVAL_HOUR_TO_MINUTE, 1,
SQL_INTERVAL_HOUR_TO_SECOND, 1,
SQL_INTERVAL_MINUTE, 1,
SQL_INTERVAL_MINUTE_TO_SECOND, 1,
SQL_INTERVAL_MONTH, 1,
SQL_INTERVAL_SECOND, 1,
SQL_INTERVAL_YEAR, 1,
SQL_INTERVAL_YEAR_TO_MONTH, 1,
SQL_TYPE_TIMESTAMP_WITH_TIMEZONE, 1,
SQL_TYPE_TIME_WITH_TIMEZONE,  1
);

our %td_type_ddcode2str = (
#pragma begin_full
'I ', 'INTEGER',
'I2', 'SMALLINT',
'I1', 'BYTEINT',
'CF', 'CHAR',
'CV', 'VARCHAR',
'F ',	'FLOAT',
'D ',	'DEC',
'DA',	'DATE',
'AT',	'TIME',
'TS',	'TIMESTAMP',
'BV',	'VARBYTE',
'BF',	'BYTE',
'SZ',	'TIMESTAMP WITH TIME ZONE',
'TZ',	'TIME WITH TIME ZONE',
'YR',	'INTERVAL YEAR',
'YM',	'INTERVAL YEAR TO MONTH',
'MO',	'INTERVAL MONTH',
'DY',	'INTERVAL DAY',
'DH',	'INTERVAL DAY TO HOUR',
'DM',	'INTERVAL DAY TO MINUTE',
'DS',	'INTERVAL DAY TO SECOND',
'HR',	'INTERVAL HOUR',
'HM',	'INTERVAL HOUR TO MINUTE',
'HS',	'INTERVAL HOUR TO SECOND',
'MI',	'INTERVAL MINUTE',
'MS',	'INTERVAL MINUTE TO SECOND',
'SC',	'INTERVAL SECOND',
'I8', 'BIGINT',
#pragma end_full
);

our %td_type_str2ddcodes = (
#pragma begin_full
'INTEGER', 'I ',
'INT', 'I ',
'SMALLINT', 'I2',
'BYTEINT', 'I1',
'CHAR', 'CF',
'CHARACTER', 'CF',
'VARCHAR', 'CV',
'FLOAT', 'F ',
'DEC', 'D ',
'DECIMAL', 'D ',
'NUMERIC', 'D ',
'DATE', 'DA',
'TIME', 'AT',
'TIMESTAMP', 'TS',
'VARBYTE', 'BV',
'BYTE', 'BF',
'TIMESTAMP WITH TIME ZONE', 'SZ',
'TIME WITH TIME ZONE', 'TZ',
'INTERVAL YEAR', 'YR',
'INTERVAL YEAR TO MONTH', 'YM',
'INTERVAL MONTH', 'MO',
'INTERVAL DAY', 'DY',
'INTERVAL DAY TO HOUR', 'DH',
'INTERVAL DAY TO MINUTE', 'DM',
'INTERVAL DAY TO SECOND', 'DS',
'INTERVAL HOUR', 'HR',
'INTERVAL HOUR TO MINUTE', 'HM',
'INTERVAL HOUR TO SECOND', 'HS',
'INTERVAL MINUTE', 'MI',
'INTERVAL MINUTE TO SECOND', 'MS',
'INTERVAL SECOND', 'SC',
'BIGINT', 'I8',
#pragma end_full
);

our %td_part_map = qw(
	DBC/SQL  1
	MONITOR  2
	EXPORT   3
	MLOAD    4
	FASTLOAD 5
	DBCCONS  6
);

#pragma end_clear_text

#
#	NOTE:
#		ByteVar is used to track xaction status
#			Bit 0 indicates in a xaction
#			Bit 1 indicates xaction has a commitable operation
#

#
#	Package attributes:
#	From base class:
#		Name :		Driver name ('Teradata')
#		Version :	Current module version
#		Err :		Last error code
#		Errstr :	Last error string
#		State :		SQLSTATE value
#		Attribution : Creator name
#	Public:	None
#	Private: None
#
use vars qw($VERSION $drh);

our $VERSION = '12.001';
$drh = undef;
my $installed = undef;

use strict;
use warnings;

sub driver {
#
#	if we've already been init'd, don't do it again
#
	return $drh if $drh;
	my ($class, $attr) = @_;
	$class .= '::dr';

	$drh = DBI::_new_drh($class,
		{
			Name => 'Teradata',
			Version => $VERSION,
			Attribution => 'DBD::Teradata by Presicient Corp.'
		});
#
#	install our private methods
#
#pragma begin_clear_text
	unless ($DBD::Teradata::installed) {
		DBD::Teradata::dr->install_method('tdat_FirstAvailable');
		DBD::Teradata::dr->install_method('tdat_FirstAvailList');

#pragma begin_full
		DBD::Teradata::db->install_method('tdat_CharSet');
		DBD::Teradata::db->install_method('tdat_SetDebug');
		DBD::Teradata::db->install_method('tdat_Do');
		DBD::Teradata::db->install_method('tdat_ServerSideVartext');
		DBD::Teradata::st->install_method('tdat_Unpack');

		eval {
			require DBD::Teradata::Utility;
		};
		DBD::Teradata::db->install_method('tdat_UtilitySetup')
			unless $@;

		eval {
			require DBD::Teradata::PassThru;
		};
		unless ($@) {
			DBD::Teradata::db->install_method('tdat_SendPassThru');
			DBD::Teradata::db->install_method('tdat_RecvPassThru');
			DBD::Teradata::db->install_method('tdat_GetSocket');
			DBD::Teradata::db->install_method('tdat_GetPlatform');
			DBD::Teradata::db->install_method('tdat_MakeEndRequest');
			DBD::Teradata::db->install_method('tdat_MakeTestResponse');
			DBD::Teradata::db->install_method('tdat_CloseXact');
			DBD::Teradata::db->install_method('tdat_ProcessPrepare');
			DBD::Teradata::st->install_method('tdat_SetStmtNum');
			DBD::Teradata::st->install_method('tdat_SetSummary');

			DBD::Teradata::st->install_method('tdat_CopyResp');
		}
#pragma end_full

		DBD::Teradata::st->install_method('tdat_BindColArray');
		DBD::Teradata::st->install_method('tdat_BindParmArray');
		DBD::Teradata::st->install_method('tdat_Rewind');
		DBD::Teradata::st->install_method('tdat_Realize');
		DBD::Teradata::st->install_method('tdat_CharSet');

		$DBD::Teradata::installed = 1;
	}
#pragma end_clear_text
	DBI->trace_msg("DBD::Teradata v.$VERSION loaded on $^O\n", 1);
	$drh->{_connections} = {};
	return $drh;
}

sub CLONE {
	DBD::Teradata::impl->io_clone;
	undef $drh;
}

1;

package DBD::Teradata::dr;

use DBD::Teradata qw(:tdat_impl_codes :tdat_part_codes  %td_part_map);
#
#	Driver attributes:
#	From base class:
#	Public:
#	Private:
#
#pragma begin_clear_text
$DBD::Teradata::dr::imp_data_size = 0;

our %dsnattrs = qw(
ACCOUNT tdat_account
CHARSET tdat_charset
CLI tdat_nocli
COMPATIBLE tdat_compatible
DATABASE tdat_database
MODE tdat_mode
REQBUFSZ tdat_reqsize
RSPBUFSZ tdat_respsize
);

#pragma end_clear_text

sub connect {
	my ($drh, $dsn, $user, $auth, $attr) = @_;

	my $host = '';
	my $port = 1025;

	$attr = {}
		unless defined $attr;

	$attr->{tdat_utility} = 'DBC/SQL'
		unless exists $attr->{tdat_utility};
	$attr->{tdat_mode} = 'DEFAULT'
		unless exists $attr->{tdat_mode};
#
#	extract any connection attributes from DSN:
#
	my $origdsn = $dsn;
	my @connopts = split ';', $dsn;
	$dsn = shift @connopts;
#
#	extract hostname and optional port from DSN
#
	if ($dsn=~/^(\w[^:]*)(:(\d+))?$/) {
		($host, $port) = ($1, $3);
	}
	elsif ($dsn=~/^(\d+\.\d+\.\d+\.\d+)(:(\d+))?$/) {
		($host, $port) = ($1, $3);
	}
	else {
		return $drh->DBI::set_err(-1, "Malformed dsn $origdsn", '08001');
	}

	$port = 1025 unless $port;
#
#	process dsn attributes
#
	foreach (@connopts) {
		my ($opt, $val) = split '=';
		$opt = uc $opt;
		return $drh->DBI::set_err(-1, "Invalid DSN attribute specification.", 'S1000')
			unless defined $val;

		return $drh->DBI::set_err(-1, "Unknown DSN attribute $opt.", 'S1000')
			unless exists $dsnattrs{$opt};

		$attr->{$dsnattrs{$opt}} = $val,
		next
			unless ($opt eq 'CLI');

		$attr->{tdat_no_cli} = (!$val) || (uc $val eq 'NO');
	}
#
#	check on attributes
#
	my $use_cli = (($DBD::Teradata::VERSION > 8) &&
		($ENV{TDAT_DBD_NO_CLI} || ($attr && $attr->{tdat_no_cli}))) ?
		undef : 1;

	my ($key, $val);
	while (($key, $val) = each %$attr) {
		if ($key eq 'tdat_mode') {
			return $drh->DBI::set_err(-1, "Unknown session mode $val specified.", 'S1000')
				unless ($val=~/^(ANSI|TERADATA|DEFAULT)$/i);
		}
		elsif ($key eq 'tdat_bufsize') {
#
# deprecated; used for both request an response size, but
#	ignore for either if explicit req/resp sizes provided
#	also silently upgrade/downgrade if needed
#
			return $drh->DBI::set_err(-1, "Bad buffer size $val. Value must be between 64000 and 2097151.", 'S1000')
				unless ($val=~/^\d+$/);

			$attr->{tdat_bufsize} = 64000 unless ($val >= 64000);
			$attr->{tdat_bufsize} = 2097151 unless ($val < 2097152);
		}
		elsif ($key eq 'tdat_respsize') {
#
#	silently upgrade/downgrade if needed
#
			return $drh->DBI::set_err(-1, "Bad reponse buffer size $val. Value must be between 64000 and 2097151.", 'S1000')
				unless ($val=~/^\d+$/);

			$attr->{tdat_respsize} = 64000 unless ($val >= 64000);
			$attr->{tdat_respsize} = 2097151 unless ($val < 2097152);
		}
		elsif ($key eq 'tdat_reqsize') {
#
#	silently upgrade/downgrade if needed
#
			return $drh->DBI::set_err(-1, "Bad request buffer size $val. Value must be between 256 and 2097151.", 'S1000')
				unless ($val=~/^\d+$/);
			$attr->{tdat_reqsize} = 256 unless ($val >= 256);
			$attr->{tdat_reqsize} = 2097151 unless ($val < 2097152);
		}
		elsif ($key eq 'tdat_charset') {
			return $drh->DBI::set_err(-1, 'Bad character set.', 'S1000')
				unless ($val=~/^ASCII|UTF8|EBCDIC$/i);
		}
		elsif ($attr->{tdat_reconnect}) {
#
#	make sure its possible
#
			return $drh->DBI::set_err(-1, 'Cannot reconnect non-SQL sessions.', 'S1000')
				if ($attr->{tdat_utility} ne 'DBC/SQL');
			return $drh->DBI::set_err(-1, 'Invalid tdat_reconnect value: must be scalar or coderef.', 'S1000')
				if (ref $attr->{tdat_reconnect} ne 'CODE');
		}
#pragma begin_full
		elsif ($key eq 'tdat_lsn') {
			return $drh->DBI::set_err(-1, 'Non-numeric LSN specified.', 'S1000')
				unless ($val=~/^\d+$/);
		}
		elsif ($key eq 'tdat_utility') {
			return $drh->DBI::set_err(-1, 'Unsupported partition specified.', 'S1000')
				unless ($val=~/^(DBC\/SQL|FASTLOAD|EXPORT|MONITOR|MLOAD|DBCCONS)$/i);
		}
#pragma end_full
	}

	return $drh->DBI::set_err(-1, 'Username required for connect.', 'S1000')
		unless defined($user) || $attr->{tdat_passthru};
#
#	change default resp/req sizes if requested/required
#
	$attr->{tdat_bufsize} = $maxbufsz
		unless exists $attr->{tdat_bufsize};

	$attr->{tdat_reqsize} = $attr->{tdat_bufsize}
		unless exists $attr->{tdat_reqsize};

	$attr->{tdat_respsize} = $attr->{tdat_bufsize}
		unless exists $attr->{tdat_respsize};
#
#	if ACCOUNT was specified, replace any current account string
#
	$auth = join(',', (split(',', $auth))[0], "'" . $attr->{tdat_account} . "'")
		if $attr->{tdat_account};
#
#	now connect to the DBMS
#
	my ($iobj, $err, $errstr, $state) =
		DBD::Teradata::impl->new($host, $port, $user, $auth, $attr, $use_cli);

	return $drh->set_err($err, $errstr, $state)
		unless $iobj;

	$attr->{tdat_reqsize} = $iobj->io_set_reqsz($maxbufsz)
		if $attr->{tdat_reqsize} &&
			($attr->{tdat_reqsize} > $maxbufsz) &&
			(($iobj->[TD_IMPL_VERSNUM] < 5000000) ||
			(($attr->{tdat_utility} ne 'DBC/SQL') && ($iobj->[TD_IMPL_VERSNUM] < 6000000)));

	$attr->{tdat_respsize} = $iobj->io_set_respsz($maxbufsz)
		if $attr->{tdat_respsize} &&
			($attr->{tdat_respsize} > $maxbufsz) &&
			($iobj->[TD_IMPL_VERSNUM] < 6000000);
#
#	create a new connection handle for a connection
#
	my ($outer, $dbh) = DBI::_new_dbh($drh,{
		Name 			=> $dsn,
		USER 			=> $user,
		CURRENT_USER	=> $user,
		tdat_utility	=> $attr->{tdat_utility},
		tdat_sessno 	=> $iobj->[TD_IMPL_SESSNO],
		tdat_hostid 	=> $iobj->[TD_IMPL_HOSTID],
		tdat_mode 		=> $attr->{tdat_mode},
		tdat_version	=> $iobj->[TD_IMPL_VERSION],
		tdat_compatible => (defined($attr->{tdat_compatible}) ? $attr->{tdat_compatible} : 99.0),
		tdat_reconnect	=> $attr->{tdat_reconnect},
		tdat_charset	=> $attr->{tdat_charset},
		tdat_reqsize	=> $attr->{tdat_reqsize},
		tdat_respsize	=> $attr->{tdat_respsize},
		tdat_no_bigint  => $attr->{tdat_no_bigint},
		tdat_max_dec_prec  => $attr->{tdat_max_dec_prec},
	});

	$dbh->{tdat_password} = $auth
		if $attr->{tdat_lsn};
#pragma begin_full
#
#	create a passthru object if requested
#
	$dbh->{tdat_passthru} = $attr->{tdat_passthru},
	$iobj->io_create_passthru()
		if $attr->{tdat_passthru};
#pragma end_full

	$iobj->[TD_IMPL_DBH] = $dbh;

	$dbh->{tdat_uses_cli} = (ref $iobj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli');
	$dbh->{tdat_versnum} = $iobj->[TD_IMPL_VERSNUM];
	$dbh->{tdat_has_bignum} = ($dbh->{tdat_versnum} >= 6020000);

	$dbh->{_iobj} = $iobj;
	$dbh->{_nextcursor} = 0;
	$dbh->{_cursors} = { };
	$dbh->{_debug} = $ENV{TDAT_DBD_DEBUG};
	$dbh->{_utf8} = ($attr->{tdat_charset} eq 'UTF8');
	$dbh->{Active} = 1;
#
#	save the handle for future reference
#
	$drh->{_connections}{($dsn . '_' . $dbh->{tdat_sessno})} = $dbh
		unless $attr->{tdat_passthru};	# we'll need to assign later
	return $outer;
}
#
#	sorry, no way to know...
#
sub data_sources {}

sub DESTROY {
	$_[0]->disconnect_all();
}

sub disconnect_all {
	foreach (values %{$_[0]{_connections}}) {
		$_->disconnect if defined($_);
	}
	$_[0]{_connections} = { };
}
#
#	wait for first idle session
#
sub FirstAvailable {
	my ($drh, $dbhlist, $timeout) = @_;
	my @sesslist = ();
#
#	default timeout is wait forever
#
	my %seshash;
	foreach my $dbh (@$dbhlist) {
#
#	convert to list of IO objects
#	note that we allow undefined entries in the input
#	handle array, to make life easier for the application
#
		push(@sesslist, defined($dbh) ?
			(ref $dbh) ? $dbh->{_iobj} : $dbh : undef);
	}
	my @outlist = DBD::Teradata::impl::io_FirstAvailList(\@sesslist, $timeout);
	return (@outlist ? $outlist[0] : undef);
}
*tdat_FirstAvailable = \&FirstAvailable;
#
#	wait for first idle session
#
sub FirstAvailList {
	my ($drh, $dbhlist, $timeout) = @_;
	my @sesslist = ();
#
#	default timeout is wait forever
#
	foreach my $dbh (@$dbhlist) {
#
#	note that we allow undefined entries in the input
#	handle array, to make life easier for the application
#	we also permit GLOB ref's
#
		push(@sesslist, defined($dbh) ?
			(ref $dbh) ? $dbh->{_iobj} : $dbh : undef);
	}
	return DBD::Teradata::impl::io_FirstAvailList(\@sesslist, $timeout);
}
*tdat_FirstAvailList = \&FirstAvailList;

1;

package DBD::Teradata::db;

use DBI qw(:sql_types);
use DBD::Teradata qw(
	:tdat_impl_codes
	:tdat_sth_codes
	:tdat_misc_codes
	:tdat_part_codes
	%td_part_map);

#
#	Connection attributes:
#	From base class:
#		Name : DSN used for conneciton
#		USER :	Logon userid
#		CURRENT_USER : Logon userid
#	Public:
#		tdat_nowait : 1 => use async requests
#		tdat_lsn    : undef => no LSN, 0 => get LSN, >0 => associated LSN
#		tdat_utility : partition string (DBC/SQL, MONITOR, EXPORT, FASTLOAD, MLOAD)
#		tdat_host	: DBMS hostname
#		tdat_sessno :	connection's session number
#		tdat_mode	:   connection mode (ANSI, TERADATA, or DEFAULT)
#	Private:
#		_iobj	: our I/O object
#		_stmts	:	hash of cursorname => statement objects prepared on this connection
#		_cursors :	hash of cursorname => open statement objects
#		_nextcursor :	monotonic incr. integer for cursor name generation
#		tdat_password : password if session allocs an LSN
#
#pragma begin_clear_text
$DBD::Teradata::db::imp_data_size = 0;
#pragma end_clear_text

our %readonly_attrs = qw(
	tdat_utility 1
	tdat_sessno  1
	tdat_hostid  1
	tdat_mode	 1
	tdat_version 1
	tdat_compatible 1
	tdat_charset 1
);

our %valid_attrs = (
'ChopBlanks', 1,		# not handled by DBI anymore ?
'tdat_sp_print', 1,		# SP PRINT flag
'tdat_sp_save', 1,		# SP Save Source flag
'tdat_compatible', 1,	# set driver compatibility level
'tdat_formatted', 1,	# enable Field (formatted output) mode
'tdat_keepresp', 1,		# use KEEPRESP
'tdat_nowait', 1,		# run asynch
'tdat_raw_in', 1,		# use raw input
'tdat_raw_out', 1,		# use raw output
'tdat_raw', 1,			# use raw input and output
'tdat_clone', 1,		# provide sth to be cloned for EXPORT
'tdat_mload', 1,		# provides MLOAD control flags for each query
'tdat_mlseq', 1,		# provide MLOAD [ sequence number, offset, jobcount ]
'tdat_mlmask', 1,		# provides job control bitmask
'tdat_vartext_out', 1,	# use VARTEXT output
'tdat_vartext_in', 1,	# use VARTEXT input
'tdat_charset', 1,		# specify connection characterset; write on connect(), otherwise readonly
'tdat_passthru', 1,		# use passthru mode; specifies ref to recv fail/error response msg
'tdat_no_bigint', 1,	# don't use Math::BigInt to convert DECIMALs; default false
);

our %terminator = qw(
UNION 1
INTERSECT 1
EXCEPT 1
MINUS 1
SAMPLE 1
WHERE 1
ORDER 1
HAVING 1
GROUP 1
QUALIFY 1
);

our %cvttypes = (
'DA',	'(VARCHAR(10))',	# DATE
'AT',	'(VARCHAR(20))',	#TIME
'TS',	'(VARCHAR(30))',	#TIMESTAMP
'SZ',	'(VARCHAR(36))',	#TIMESTAMP WITH TIME ZONE
'TZ',	'(VARCHAR(24))',	#TIME WITH TIME ZONE
'YR',	'(VARCHAR(6))',		#INTERVAL YEAR
'YM',	'(VARCHAR(10))',	#INTERVAL YEAR TO MONTH
'MO',	'(VARCHAR(10))',	#INTERVAL MONTH
'DY',	'(VARCHAR(10))',	#INTERVAL DAY
'DH',	'(VARCHAR(16))',	#INTERVAL DAY TO HOUR
'DM',	'(VARCHAR(20))',	#INTERVAL DAY TO MINUTE
'DS',	'(VARCHAR(30))',	#INTERVAL DAY TO SECOND
'HR',	'(VARCHAR(10))',	#INTERVAL HOUR
'HM',	'(VARCHAR(16))',	#INTERVAL HOUR TO MINUTE
'HS',	'(VARCHAR(24))',	#INTERVAL HOUR TO SECOND
'MI',	'(VARCHAR(10))',	#INTERVAL MINUTE
'MS',	'(VARCHAR(20))',	#INTERVAL MINUTE TO SECOND
'SC',	'(VARCHAR(24))',	#INTERVAL SECOND
);

my $pmapi_loaded;
#
#	PREPARE a 'SELECT * FROM $table' and return the
#	table info
#
sub table_info {
	my $dbh = shift;
	return undef
		unless ($dbh->{tdat_utility} eq 'DBC/SQL');
	my $sth = $dbh->prepare(
"SELECT NULL AS TABLE_CAT,
	DATABASENAME AS TABLE_SCHEM,
	TABLENAME AS TABLE_NAME,
	CASE TABLEKIND WHEN 'I' THEN 'JOIN INDEX'
	WHEN 'V' THEN 'VIEW'
	ELSE
	(CASE WHEN UPPER(REQUESTTEXT) LIKE '% GLOBAL TEMP% TABLE %'
		THEN 'GLOBAL TEMPORARY'
		ELSE (CASE WHEN UPPER(REQUESTTEXT) LIKE '% VOLATILE TABLE %'
			THEN 'VOLATILE'
		ELSE 'TABLE' END)
		END)
	END AS TABLE_TYPE,
	COMMENTSTRING (VARCHAR(256)) AS REMARKS
	FROM DBC.TABLESX
	WHERE TABLEKIND IN ('I', 'T', 'V')
	ORDER BY 2,3");
	$sth->execute;
	return $sth;
}

sub tables {
	my $dbh = shift;
	return undef
		unless ($dbh->{tdat_utility} eq 'DBC/SQL');
	my $sth = $dbh->prepare(
"SELECT TRIM(DATABASENAME) || '.' || TRIM(TABLENAME) AS ALL_TABLES
	FROM DBC.TABLESX
	WHERE TABLEKIND IN ('I', 'T', 'V')");
	$sth->execute;
	my @tbls = ();
	my $row;
	push(@tbls, $$row[0])
		while ($row = $sth->fetchrow_arrayref);
	return @tbls;
}

sub ping {
	return $_[0]{_iobj}->io_ping ||
		$_[0]->set_err($_[0]{_iobj}->io_get_error());
}

sub prepare {
	my ($dbh, $stmt, $attribs) = @_;

	$attribs = {}
		unless $attribs;
#
#	new raw_in/raw_out replaces old raw;
#	for compatibility, raw = (raw_in + raw_out)
#
	$attribs->{tdat_raw_in} = $attribs->{tdat_raw},
	$attribs->{tdat_raw_out} = $attribs->{tdat_raw}
		if $attribs->{tdat_raw};
#
#	can't use raw mode with formatted
#
	return $dbh->set_err(-1, 'Raw output mode not compatible with formatted mode.', 'S1000')
		if $attribs->{tdat_raw_out} && $attribs->{tdat_formatted};

	return $dbh->set_err(-1, 'Raw input mode incompatible with vartext input mode.', 'S1000')
		if $attribs->{tdat_raw_in} && $attribs->{tdat_vartext_in};

	return $dbh->set_err(-1, 'Raw output mode incompatible with vartext output mode.', 'S1000')
		if $attribs->{tdat_raw_out} && $attribs->{tdat_vartext_out};

	my $passthru = $attribs->{tdat_passthru};

	my $csth = $attribs->{tdat_clone};
	return $dbh->set_err(-1, 'tdat_clone value must be DBI statement handle.', 'S1000')
		unless (! defined($csth)) ||
			(((ref $csth eq 'DBI::st') || (ref $csth eq "DBD::Teradata::Utility::st")));

	foreach (keys %$attribs) {
		return $dbh->set_err(-1, "Unknown statement attribute \"$_\"." , 'S1000')
			unless (substr($_, 0, 5) ne 'tdat_') || $valid_attrs{$_};

		return $dbh->set_err(-1, 'Invalid raw mode value.', 'S1000')
			if (($_ eq 'tdat_raw_in') || ($_ eq 'tdat_raw_out')) &&
				($attribs->{$_} ne 'RecordMode') &&
				($attribs->{$_} ne 'IndicatorMode');
	}

	my $sessno = $dbh->{tdat_sessno};
	my $iobj = $dbh->{_iobj};
	my $partition = $td_part_map{$dbh->{tdat_utility}};
#
#	Teradata doesn't like newlines, so replace them with carriage-returns
#
	$stmt =~s/\n/\r/g;
	$stmt=~s/^\s+//;
	$stmt=~s/\s+$//;
#
#	can't open cursors except in ANSI mode
#
	if ($stmt=~/\s+FOR\s+CURSOR\s*;?$/i) {
		return $dbh->set_err(-1, 'Cursor syntax only supported in ANSI mode.' , 'S1000')
			if ($dbh->{tdat_mode} ne 'ANSI');
		return $dbh->set_err(-1, 'Updatable cursors deprecated for Teradata versions >= R6.2.', 'S1000')
			if ($dbh->{tdat_versnum} >= 6200000);
	}

	my $compatible = $attribs->{tdat_compatible} || $dbh->{tdat_compatible};

#pragma begin_full
	if ($partition == TD_PART_EXPORT) {
#
#	exports use binary statements, so don't prepare
#	check for an inherited statement handle, and copy its
#	attributes
#
		my $csth = $attribs->{tdat_clone};
		return $dbh->set_err(-1, 'prepare() for EXPORT requires tdat_clone attribute.', 'S1000')
			unless $csth && ref $attribs->{tdat_clone};

		my $cstmthash = $csth->{tdat_stmt_info}[1];

		my $sthargs =
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{ %$cstmthash },
			],

			NUM_OF_FIELDS => $csth->{NUM_OF_FIELDS},
			NAME => [ @{$csth->{NAME}} ],
			TYPE => [ @{$csth->{TYPE}} ],
			PRECISION => [ @{$csth->{PRECISION}} ],
			SCALE => [ @{$csth->{SCALE}} ],
			NULLABLE => [ @{$csth->{NULLABLE}} ],
			tdat_TYPESTR => [ @{$csth->{tdat_TYPESTR}} ],
			tdat_TITLE => [ @{$csth->{tdat_TITLE}} ],
			tdat_FORMAT => [ @{$csth->{tdat_FORMAT}} ],
			_unpackstr => [ @{$csth->{_private}[TD_STH_UNPACKSTRS]} ],

			NUM_OF_PARAMS => 2,
			_ptypes => [ SQL_INTEGER, SQL_INTEGER ],
			_plens => [ 4, 4 ],
			_usenames => $csth->{_private}[TD_STH_USENAMES] ?
				[ @{$csth->{_private}[TD_STH_USENAMES]} ] : undef,
			_packstr => 'LL'
			};
		$sthargs->{tdat_CHARSET} = [ @{$csth->{tdat_CHARSET}} ],
		$sthargs->{tdat_CASESPECIFIC} = [ @{$csth->{tdat_CASESPECIFIC}} ]
			if exists $csth->{tdat_CHARSET};
		return _make_sth($dbh, $sthargs);
	}
#
#	console sessions always return a single 32K VARCHAR
	elsif ($partition == TD_PART_DBCCONS) {
		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{
					ActivityType => 'Remote Console',
					ActivityCount => 1,
					StartsAt => 0,
					EndsAt => 0,
				}
			],

			NUM_OF_FIELDS => 1,
			NAME => [ 'CONSOLE_RESPONSE' ],
			TYPE => [ SQL_VARCHAR ],
			PRECISION => [ 32000 ],
			SCALE => [ undef ],
			NULLABLE => [ undef ],
			tdat_TYPESTR => [ 'VARCHAR(32000)' ],
			tdat_TITLE => [ 'Console Response' ],
			tdat_FORMAT => [ 'X(32000)' ],
			_unpackstr => [ 'S/A*' ],

			NUM_OF_PARAMS => 1,
			_ptypes => [ SQL_VARCHAR ],
			_plens => [ 32000 ],
			_packstr => 'S/A*',
			}
		);
	}
#
#	bypass prepare on these, let execute deal with them
#
	elsif (($partition == TD_PART_FASTLOAD) || ($partition == TD_PART_MLOAD)) {
#
#	we support partial prepare on the input statement
#	to allow non-rawmode parameters
#
		$iobj->[TD_IMPL_SESBUFPOS] = TDAT_HDRSZ;
		$iobj->[TD_IMPL_SESBUFF] = (chr(0) x (TDAT_HDRSZ + $iobj->[TD_IMPL_SESREQSZ]));
		return $iobj->io_prepare($dbh, \&_make_sth, $stmt, undef, $attribs, $compatible) ||
			$dbh->set_err($iobj->io_get_error())
			unless $attribs->{tdat_raw_in} || ($stmt eq ';');
#
#	rawmode, synthesize the statement
#

#print "\n *** FASTLOAD stmt is ", ($stmt || 'undef'), "\n";
		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{
					ActivityType => $dbh->{tdat_utility},
					ActivityCount => 0,
					MloadCnts => [ 0,0,0,0 ]
				}
			],
			NUM_OF_FIELDS => 0,
			NUM_OF_PARAMS => 1,
			_ptypes => [ SQL_VARBINARY ],
			_plens => $iobj->[TD_IMPL_SESREQSZ],
			_packstr => 'a*'
			}
		);
	}
 	elsif ($stmt=~/^(BEGIN|CHECKPOINT|END)\s+LOADING/i) {
		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{
					ActivityType => "$1 Loading",
					ActivityCount => 0,
				}
			],
			NUM_OF_FIELDS => 0,
			NUM_OF_PARAMS => 0,
			}
		);
	}
#
#	MLOAD stmts
#
	elsif ($stmt=~/^BEGIN\s+MLOAD\s+/i) {
		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{
					ActivityType => 'Begin MLOAD',
					ActivityCount => 0,
				}
			],
			NUM_OF_FIELDS => 0,
			NUM_OF_PARAMS => 0,
			}
		);
	}
	elsif ($stmt=~/^MLOAD\s+/i) {
		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{
					ActivityType => 'MLOAD Declaration',
					ActivityCount => 0,
				}
			],
			NUM_OF_FIELDS => 0,
			NUM_OF_PARAMS => 0,
			}
		);
	}
	elsif ($stmt=~/^RELEASE\s+MLOAD\s+/i) {
		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{
					ActivityType => 'Release MLOAD',
					ActivityCount => 0,
				}
			],
			NUM_OF_FIELDS => 0,
			NUM_OF_PARAMS => 0,
			}
		);
	}
	elsif ($stmt=~/^EXEC(?:UTE)?\s+MLOAD/i) {
#
#	since this may be multistmt, find the number of line terminators
#
		my $stmtcnt = ($stmt=~tr/;//);
		$stmtcnt++;
		my $mlstart = 0;
		my @stmtinfo = (undef);
		my @unpackstrs = ();
		push(@stmtinfo,
			{
				ActivityType => 'Exec MLOAD',
				ActivityCount => 0,
				StartsAt => $mlstart,
				EndsAt => $mlstart + 2,
			}),
		$unpackstrs[$mlstart] = 'LLL',
		$mlstart += 3
			foreach (1..$stmtcnt);

		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => \@stmtinfo,

			NUM_OF_FIELDS => (3 * $stmtcnt),
			NAME => [ ('TOTAL_INSERTS', 'TOTAL_UPDATES', 'TOTAL_DELETES' ) x $stmtcnt ],
			TYPE => [ (SQL_INTEGER, SQL_INTEGER, SQL_INTEGER) x $stmtcnt ],
			PRECISION => [ (0,0,0) x $stmtcnt ],
			SCALE => [ (undef,undef,undef) x $stmtcnt ],
			NULLABLE => [ (undef,undef,undef)  x $stmtcnt ],
			tdat_TYPESTR => [ ('INTEGER','INTEGER','INTEGER') x $stmtcnt ],
			tdat_TITLE => [ ('Total Inserts', 'Total Updates', 'Total Deletes') x $stmtcnt ],
			tdat_FORMAT => [ ('Z(9)','Z(9)','Z(9)') x $stmtcnt ],
			_unpackstr => \@unpackstrs,
			NUM_OF_PARAMS => 0,
			}
		);
	}
	elsif ($stmt=~/^(BEGIN|END)\s+FASTEXPORT/i) {
		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{
					ActivityType => "$1 Export",
					ActivityCount => 0,
				}
			],
			NUM_OF_FIELDS => 0,
			NUM_OF_PARAMS => 0,
			}
		);
	}
	elsif ($partition == TD_PART_MONITOR) {
		unless ($pmapi_loaded) {
			eval {
				require DBD::Teradata::PMAPI;
				import DBD::Teradata::PMAPI qw(get_pmapi_template);
			};
			return $dbh->set_err(-1, "Unable to support MONITOR session: $@", 'S1000')
				if $@;
			$pmapi_loaded = 1;
		}
#
#	provide backfill for these
#	all MONITOR input/output is LATIN1
#
		$stmt=~s/\s*;\s*$//g;
		$stmt=~s/^\s+//g;
		$stmt=~s/\s+/ /g;

		my $bf = get_pmapi_template(uc $stmt);

		return $dbh->set_err(-1, 'Unrecognized statement for MONITOR session.', 'S1000')
			unless $bf;

		my %args = (
			NUM_OF_PARAMS => scalar @{$bf->{InputType}},
			_ptypes => [ @{$bf->{InputType}} ],
			_plens => [ @{$bf->{InputPrec}} ],
			_packstr => $bf->{Packstring},
		);
		$args{NUM_OF_FIELDS} = scalar @{$bf->{OutCols}},
		$args{NAME} = [ @{$bf->{OutCols}} ],
		$args{TYPE} = [ @{$bf->{OutType}} ],
		$args{PRECISION} = [ @{$bf->{OutPrec}} ],
		$args{_unpackstr} = [ @{$bf->{UnpackStrings}} ]
			if $bf->{OutCols};

		my $stmtcnt = $bf->{StmtCnt} - 1;
		my @stmtinfo = ( undef );
		map {
			push @stmtinfo, {
				ActivityType => $bf->{Activity},
				ActivityCount => 0,
				StartsAt => $bf->{StartsAt}[$_],
				EndsAt => $bf->{EndsAt}[$_],
			};
		} 0..$stmtcnt;

		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			%args,
			tdat_stmt_info => \@stmtinfo,
			_usephs => 0,
			}
		);

	}
#pragma end_full
#
#	HELP VOLATILE returns empty PREPINFO if no volatile tables exist
#	(DUUUH!!!), so we have to mock it up here
#
	if ($stmt=~/^\s*HELP\s+VOLATILE\s+TABLE\s*$/i) {
		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{
					ActivityType => 'Help',
					ActivityCount => 1,
					StartsAt => 0,
					EndsAt => 1,
				}
			],

			NUM_OF_FIELDS => 2,
			NAME => [ 'Table Name', 'Table Id' ],
			TYPE => [ SQL_CHAR, SQL_CHAR ],
			PRECISION => [ 30, 12 ],
			SCALE => [ undef, undef ],
			NULLABLE => [ undef, undef ],
			tdat_TYPESTR => [ 'CHAR(30)', 'CHAR(30)' ],
			tdat_TITLE => [ 'Table Name', 'Table Id' ],
			tdat_FORMAT =>  [ 'X(30)', 'X(12)' ],
			_unpackstr => [ 'A30 A12' ],
			NUM_OF_PARAMS => 0,
			}
		);
	}
	elsif ($stmt=~/^ECHO\b/i) {
		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{
					ActivityType => 'Echo',
					ActivityCount => 0,
					StartsAt => 0,
					EndsAt => 0,
				}
			],
			NUM_OF_FIELDS => 1,
			NAME => [ 'ECHOTEXT' ],
			TYPE => [ SQL_VARCHAR ],
			PRECISION => [ 255 ],
			SCALE => [ undef ],
			NULLABLE => [ 0 ],
			tdat_TYPESTR => [ 'VARCHAR(255)' ],
			tdat_TITLE => [ 'Echo Text' ],
			tdat_FORMAT => [ 'X(255)' ],
			_unpackstr => [ 'A*' ],
			NUM_OF_PARAMS => 0,
			}
		);
	}
	elsif ($stmt=~/^EXPLAIN\b/i) {
		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{
					ActivityType => 'Explain',
					ActivityCount => 0,
					StartsAt => 0,
					EndsAt => 0,
				}
			],
			NUM_OF_FIELDS => 1,
			NAME => [ 'Explanation' ],
			TYPE => [ SQL_VARCHAR ],
			PRECISION => [ 80 ],
			SCALE => [ undef ],
			NULLABLE => [ 0 ],
			tdat_TYPESTR => [ 'VARCHAR(80)' ],
			tdat_TITLE => [ 'Explanation' ],
			tdat_FORMAT => [ 'X(80)' ],
			_unpackstr => [ 'A*' ],
			NUM_OF_PARAMS => 0,
			}
		);
	}
#
#	for CREATE PROCEDURE, bypass this (we should do this for
#	the other CREATE/REPLACE/DROP too!
#
	elsif ($stmt=~/^(?:CREATE|REPLACE)\s+PROCEDURE\s+/i) {
		return _make_sth($dbh,
			{
			%$attribs,
			Statement => $stmt,
			tdat_stmt_info => [
				undef,
				{
					ActivityType => 'Create Procedure',
					ActivityCount => 0,
					StartsAt => 0,
					EndsAt => 0,
				},
			],
			NUM_OF_FIELDS => 1,
			NAME => [ 'COMPILE_ERROR' ],
			TYPE => [ SQL_VARCHAR ],
			PRECISION => [ 255 ],
			SCALE => [ undef ],
			NULLABLE => [ 0 ],
			tdat_TYPESTR => [ 'VARCHAR(255)' ],
			tdat_TITLE => [ 'Compile Error' ],
			tdat_FORMAT => [ 'X(255)' ],
			_unpackstr => [ 'S/A' ],
			NUM_OF_PARAMS => 0,
			}
		);
	}
#
#	if the cursor has been executed, we have to provide a rowid
#	for the prepare of positioned updates
#
	my $rowid = undef;
	$rowid = $dbh->{_cursors}{uc $1}{_rowid}
		if ($stmt=~/\s+WHERE\s+CURRENT\s+OF\s+([^\s;]+)\s*;?\s*$/i);

	my $sth = $iobj->io_prepare($dbh, \&_make_sth, $stmt, $rowid, $attribs, $compatible, $passthru);
#	print "!!! where the hell is my unpackstr ?\n"
#		unless $sth->{_private}[TD_STH_UNPACKSTRS] &&
#			$sth->{_private}[TD_STH_UNPACKSTRS][0];
	return $sth || $dbh->set_err($iobj->io_get_error);
}

sub _make_sth {
	my ($dbh, $args) = @_;
	delete $args->{tdat_clone};
	delete $args->{tdat_passthru};
#
#	every stmt gets a name
#
	$args->{CursorName} = 'CURS' . $dbh->{_nextcursor};
	$dbh->{_nextcursor}++;

	$args->{tdat_stmt_num} = 0;
	$args->{tdat_sessno} = $dbh->{tdat_sessno};
	$args->{tdat_compatible} = '999.0'
		unless $args->{tdat_compatible};
#
#	inherit some attributes
#
	$args->{tdat_no_bigint} = $dbh->{tdat_no_bigint}
		unless exists $args->{tdat_no_bigint};
	$args->{tdat_has_bignum} = $dbh->{tdat_has_bignum};

	$args->{ParamValues} = {}
		unless $args->{ParamValues};

	$args->{ParamArrays} = {}
		unless $args->{ParamArrays};

	my @sthpriv = ();

	$sthpriv[TD_STH_ROWS] = -1;
	$sthpriv[TD_STH_PACKSTR] = delete $args->{_packstr};
	$sthpriv[TD_STH_UNPACKSTRS] = delete $args->{_unpackstr};
	$sthpriv[TD_STH_IOBJ] = $dbh->{_iobj};
#
#	save input parameter values, types, and lengths
#
	$sthpriv[TD_STH_PARAMS] = [];
	$sthpriv[TD_STH_PTYPES] = delete $args->{_ptypes};
	$sthpriv[TD_STH_PLENS] = delete $args->{_plens};
	$sthpriv[TD_STH_USEPHS] = delete $args->{_usephs}; # 0 => USING clause, else PH's
	$sthpriv[TD_STH_USENAMES] = delete $args->{_usenames};
	$sthpriv[TD_STH_LASTRESP] = 1;
	$sthpriv[TD_STH_FAILED] = 0;
	$sthpriv[TD_STH_CALLPARMS] = delete $args->{_parmdesc};
	$sthpriv[TD_STH_PARMMAP] = delete $args->{_parmmap};
	$sthpriv[TD_STH_DBH] = $dbh;

	$args->{_private} = \@sthpriv;
#
#	verify param types for vartext mode:
#	NOTE: as of 2.2.10, force the non-VARCHAR to VARCHARs
#	instead of reporting an error
#
	if ($args->{tdat_vartext_in}) {
		my ($ptypes, $plens) = ($sthpriv[TD_STH_PTYPES], $sthpriv[TD_STH_PLENS]);
		foreach (0..$#$ptypes) {
#
#	warn if vartext input w/ non-string USING params
#
			$dbh->set_err(0,
				'Using VARTEXT input with other than VARCHAR USING parameter.', '00000'),
			$ptypes->[$_] = SQL_VARCHAR,
			$plens->[$_] = 16
				unless ($ptypes->[$_] == SQL_VARCHAR);
		}
		$sthpriv[TD_STH_PACKSTR] = ('S/a*' x (scalar @$ptypes));
	}
#
#	extract driver-specific args; they can be supplied to the
#	handle constructor
#
	my $stmtinfo = $args->{tdat_stmt_info};
#
#	Because of legacy DBI behavior, we can't rely on DBI/sth attributes
#	being properly applied to the sth in the constructor (sigh), so we have to apply
#	them after the handle is created
#
	my ($outer, $sth) = DBI::_new_sth($dbh,
		{
			Statement => $args->{Statement},
			CursorName => $args->{CursorName}
		})
		or return $dbh->set_err(-1, 'Unable to create statement handle.', 'S1000');

#print join(', ', $outer, $sth, $outer->{NUM_OF_FIELDS}, $outer->{NUM_OF_FIELDS}), "\n";
#
#	don't ask me why...another DBI screwup...
#
	$sth->STORE('NUM_OF_PARAMS', delete $args->{NUM_OF_PARAMS});
	$sth->STORE('NUM_OF_FIELDS', delete $args->{NUM_OF_FIELDS});

	my ($key, $val);
	$sth->{$key} = $val
		while (($key, $val) = each %$args);
#
#	keep a link
#
#	if its a real cursor, record the statement in the dbh
#
	$dbh->{_cursors}{$sth->{CursorName}} = $sth,
	$sth->{tdat_keepresp} = 1,
	$sthpriv[TD_STH_UPDATABLE] = 1
		if ($#$stmtinfo == 1) &&
			$stmtinfo->[1]{ActivityType} &&
			($stmtinfo->[1]{ActivityType} eq 'Select') &&
			($sth->{Statement}=~/\s+FOR\s+CURSOR\s*;?\s*$/i);
#
#	make sure utility sessions use same wait semantic as statement
#
	$dbh->{tdat_nowait} = $sth->{tdat_nowait}
		if ($dbh->{tdat_utility} ne 'DBC/SQL');

	$sth->{tdat_TYPESTR} = DBD::Teradata::st::map_type2str($sth)
		unless defined $sth->{tdat_TYPESTR} || (! $sth->{NUM_OF_FIELDS});

	return wantarray ? ($outer, $sth) : $outer;
}

sub DESTROY {
	my $dbh = shift;
	return 1
		unless (defined($dbh->{tdat_sessno}) &&
			defined($dbh->{Name}) &&
			defined($dbh->{Driver}));

	my $host = $dbh->{Name} . '_' . $dbh->{tdat_sessno};
	return 1
		unless defined($dbh->{Driver}{_connections}{$host});

	$dbh->disconnect;
}

sub disconnect {
	my $dbh = shift;
	my $i;

	$dbh->{Active} = undef;
#	$dbh->{_stmts} = $dbh->{_cursors} = undef;
#
#	remove the handle from the internal handle list
#
	return 1
		unless defined($dbh->{tdat_sessno}) &&
			defined($dbh->{Name}) && defined($dbh->{Driver});

	my $sessno = $dbh->{tdat_sessno};
	my $host;
	my $drh = $dbh->{Driver};
	if ($sessno) {
		$host = $dbh->{Name} . '_' . $sessno;
		$dbh->set_err(0, 'Session not found'),
		return 1
			unless defined($drh->{_connections}{$host});
	}
#
#	cancel any pending queries
#
#	map { delete $dbh->{_stmts}{$_}; } keys %{$dbh->{_stmts}};

	my $iobj = $dbh->{_iobj};
	$iobj->io_disconnect unless $dbh->{_ignore_destroy};
	$dbh->{_iobj} = undef;
	delete $drh->{_connections}{$host}
		if defined($host);
	1;
}

sub commit {
	my $dbh = shift;
	my $xactmode = $dbh->{AutoCommit};

	if (defined($xactmode) && ($xactmode == 1)) {
		warn('Commit ineffective while AutoCommit is on')
			if $dbh->{Warn};
		return 1;
	}
#
#	explicit transaction mode, so send ET
#
	my $iobj = $dbh->{_iobj};

#pragma begin_full

	if ((($dbh->{tdat_utility} eq 'FASTLOAD') ||
		($dbh->{tdat_utility} eq 'MLOAD')) &&
		($iobj->[TD_IMPL_SESBUFPOS] > 0)) {
#
#	transfer accumulated buffer to DBMS
#
		my $stmtno = 0;
		my @stmtinfo = ( undef, { });
		$iobj->[TD_IMPL_SESINXACT] = 0;
#
#	use sth from stmt list (should only be 1!)
		my $sth = $dbh->{ChildHandles}[0];
#print "\n *** fastload commit stmt is ", ($sth->{Statement} || 'undef'), "\n";

		my $rowcnt = $iobj->io_execute($sth, undef, undef);
		return $dbh->set_err($iobj->io_get_error())
			unless defined($rowcnt);

		$iobj->[TD_IMPL_SESBUFPOS] = TDAT_HDRSZ;
		return 1;
	}
#pragma end_full

	return 1 unless $iobj->[TD_IMPL_SESINXACT];
#
#	close all open requests except keepresp for non-updatables
	my @sths = @{$dbh->{ChildHandles}};
	foreach (@sths) {
		next unless $_;	# contents may be empty
		$_->finish
			unless $_->{_private}[TD_STH_LASTRESP] ||
				($_->{tdat_keepresp} &&
					(! $_->{_private}[TD_STH_UPDATABLE]));
	}
	$iobj->io_commit;
	$dbh->set_err($iobj->io_get_error());
	$iobj->[TD_IMPL_SESINXACT] = 0;
	return 1;
}

sub rollback {
	my $dbh = shift;
	my $xactmode = $dbh->{AutoCommit};

	if (defined($xactmode) && ($xactmode == 1)) {
		warn('Rollback ineffective while AutoCommit is on')
			if $dbh->{Warn};
		return 1;
	}
#
#	purge accumulated buffer
#
	my $iobj = $dbh->{_iobj};

#pragma begin_full

	$iobj->[TD_IMPL_SESBUFPOS] = TDAT_HDRSZ,
	$iobj->[TD_IMPL_SESINXACT] = 0,
	return 1
		if (($dbh->{tdat_utility} eq 'FASTLOAD') || ($dbh->{tdat_utility} eq 'MLOAD'));
#pragma end_full
#
#	explicit transaction mode, so send ABORT;
#
	return 1 unless $iobj->[TD_IMPL_SESINXACT];
#
#	close all open requests
#
	my @sths = @{$dbh->{ChildHandles}};
	foreach (@sths) {
		next unless $_;	# contents may be empty
		$_->finish
			unless $_->{_private}[TD_STH_LASTRESP] ||
				($_->{tdat_keepresp} &&
					(! $_->{_private}[TD_STH_UPDATABLE]));
	}
	$iobj->io_rollback;
	$dbh->set_err($iobj->io_get_error());
	$iobj->[TD_IMPL_SESINXACT] = 0;
	return 1;
}
#
#	need better error checking here!!!
sub STORE {
	my ($dbh, $attr, $val) = @_;
	return $dbh->SUPER::STORE($attr, $val)
		unless ($attr eq 'AutoCommit') || ($attr =~ /^(tdat_|_)/);
#	return 1 if $readonly_attr{$attr};
	$dbh->{$attr} = $val;
	$dbh->{tdat_respsize} = $dbh->{_iobj}->io_set_respsz($val)
		if ($attr eq 'tdat_respsize');
	$dbh->{tdat_reqsize} = $dbh->{_iobj}->io_set_reqsz($val)
		if ($attr eq 'tdat_reqsize');
	$dbh->{_iobj}->io_update_version($val),
	$dbh->{tdat_versnum} = $val
		if ($attr eq 'tdat_versnum') && ($val < 6020000);
	return 1;
}

sub FETCH {
	my ($dbh, $attr) = @_;
	return $dbh->{_iobj}[TD_IMPL_ACTIVE] if ($attr eq 'tdat_active');
	return $dbh->{_iobj}[TD_IMPL_SESINXACT] if ($attr eq 'tdat_inxact');
	return $dbh->{$attr}
		if ($attr eq 'AutoCommit') || ($attr =~ /^(tdat_|_)/);
	return $dbh->SUPER::FETCH($attr);
}
#pragma begin_full
#
#	tdat_UtilitySetup connects several utility sessions using the current
#	sessions's user, password, and LSN, and prepares statement handles
#	on each of them using the input statement and attributes
#
sub tdat_UtilitySetup {
	my ($dbh, $attrs) = @_;
#
#	use require here so that we only load when needed
#
	require DBD::Teradata::Utility;

	my $utility = DBD::Teradata::Utility->new($dbh, $attrs);
	return $dbh->set_err($dbh->{_iobj}->io_get_error())
		unless $utility;

	my $rc = $utility->run;
	return defined($rc) ? $rc :
		$dbh->set_err($dbh->{_iobj}->io_get_error());
}

sub tdat_ServerSideVartext {
	my ($dbh, $sql, $sep) = @_;

	$sql=~s/[\s;\r]+$//;
	$sql .= "\r";	# incase last piece is ANSI comment

	return $dbh->set_err(-1, 'VARTEXT SQL must be a SELECT statement without LOCK, USING, or WITH qualifiers.', 'S1000')
		unless ($sql=~s/^(\s*sel(?:ect)?)\b//i);
	my $origsql = $1;
	$origsql .= $1
		if ($sql=~s/^(\s*(distinct|all))\b//i);
	$origsql .= $1
		if ($sql=~s/^(\s*top\s+\d+(\.\d+)?(\s+percent)?(\s+with\s+ties)?)\b//i);
	pos($sql) = 0;
	my @cols = ();
	my $spos = 0;
	my $finished = 0;
	my $pcnt = 0;
	my $bcnt = 0;
	my $dcnt = 0;
	while ($sql=~/\G.*?(['",\(\)\[\]\{\}]|--|\/\*|\bFROM\b)/igcs) {
		if ($1 eq '"') {
			return $dbh->set_err(-1,
				'Invalid SQL: Unclosed double quote.', 'S1000')
				unless ($sql=~/\G.*?"/gcs);
		}
		elsif ($1 eq "'") {
			return $dbh->set_err(-1,
				'Invalid SQL: Unclosed single quote.', 'S1000')
				unless ($sql=~/\G.*?'/gcs);
		}
		elsif ($1 eq '(') {
			$pcnt++;
		}
		elsif ($1 eq ')') {
			$pcnt--;
			return $dbh->set_err(-1,
				'Invalid SQL: close parenthesis without matching open parenthesis.', 'S1000')
				if ($pcnt < 0);
		}
		elsif ($1 eq '{') {
			$bcnt++;
		}
		elsif ($1 eq '}') {
			$bcnt--;
			return $dbh->set_err(-1,
				'Invalid SQL: close bracket without matching open bracket.', 'S1000')
				if ($bcnt < 0);
		}
		elsif ($1 eq '[') {
			$dcnt++;
		}
		elsif ($1 eq ']') {
			$dcnt--;
			return $dbh->set_err(-1,
				'Invalid SQL: close brace without matching open brace.', 'S1000')
				if ($dcnt < 0);
		}
		elsif ($1 eq ',') {
			push(@cols, substr($sql, $spos, $+[1] - $spos)),
			$spos = $+[1]
				unless $pcnt || $dcnt || $bcnt;
		}
		elsif ($1 eq '--') {
#
#	ANSI comment, scan to end of line and remove it
#
			my $tpos = pos($sql);
			$sql=~/\G.*?\n/gcs;
			substr($sql, $tpos, $+[0], ' ');
			pos($sql) = $tpos;
		}
		elsif ($1 eq '/*') {
#
#	C comment, scan to end of comment (note we don't worry about
#	endof comment inside string) and remove it
#
			my $tpos = pos($sql);
			return $dbh->set_err(-1,
				'Invalid SQL: "/*" comment without matching "*/".', 'S1000')
				unless ($sql=~/\G.*?\*\//gcs);
			substr($sql, $tpos, $+[0], ' ');
			pos($sql) = $tpos;
		}
		else { # FROM, end of list (hopefully there's no bare expression with FROM key in it)
			push(@cols, substr($sql, $spos, $-[1] - $spos)),
			$finished = $+[0],
			last
				unless $pcnt || $dcnt || $bcnt;
		}
	}	# end while scanning

	return $dbh->set_err(-1,
		'Invalid SQL: open parenthesis without matching close parenthesis.', 'S1000')
		if $pcnt;

	return $dbh->set_err(-1,
		'Invalid SQL: open bracket without matching close bracket.', 'S1000')
		if $bcnt;

	return $dbh->set_err(-1,
		'Invalid SQL: open brace without matching close brace.', 'S1000')
		if $dcnt;

	return $dbh->set_err(-1,
		'Invalid SQL: Missing FROM clause.', 'S1000')
		unless $finished;
#
#	check for wildcards; must always be first or last
#
	my $wild;
	foreach (0..$#cols) {
		$cols[$_]=~s/^\s+//;
		$cols[$_]=~s/\s+$//;
		$cols[$_]=~s/\s*,$//;
		return $dbh->set_err(-1,
			'Invalid SQL: Empty column list entry.', 'S1000')
			if ($cols[$_] eq '');

		if ($cols[$_]=~/^(?:([\$\#\@\w]+|"[^"]+")\.)?\*$/) {
			return $dbh->set_err(-1,
				'Invalid SQL: Wildcards must occur as first, last, or only column list entry.', 'S1000')
				if $_ && ($_ < $#cols);
			$wild = $_;
		}
	}
#
#	now scan over the FROM clause so we can trim everything after
#	(HELP COLUMN is finicky about that sort of thing)
#
	my $frompos = $finished;
	pos($sql) = $finished;
	$spos = pos($sql);
	$finished = 0;
	while ($sql=~/\G.*?(['"\(\)\[\]\{\}]|--|\/\*|\b(\w+)\b)/igcs) {
		if ($1 eq '"') {
			return $dbh->set_err(-1,
				'Invalid SQL: Unclosed double quote.', 'S1000')
				unless ($sql=~/\G.*?"/gcs);
		}
		elsif ($1 eq "'") {
			return $dbh->set_err(-1,
				'Invalid SQL: Unclosed single quote.', 'S1000')
				unless ($sql=~/\G.*?'/gcs);
		}
		elsif ($1 eq '(') {
			$pcnt++;
		}
		elsif ($1 eq ')') {
			$pcnt--;
			return $dbh->set_err(-1,
				'Invalid SQL: close parenthesis without matching open parenthesis.', 'S1000')
				if ($pcnt < 0);
		}
		elsif ($1 eq '{') {
			$bcnt++;
		}
		elsif ($1 eq '}') {
			$bcnt--;
			return $dbh->set_err(-1,
				'Invalid SQL: close bracket without matching open bracket.', 'S1000')
				if ($bcnt < 0);
		}
		elsif ($1 eq '[') {
			$dcnt++;
		}
		elsif ($1 eq ']') {
			$dcnt--;
			return $dbh->set_err(-1,
				'Invalid SQL: close brace without matching open brace.', 'S1000')
				if ($dcnt < 0);
		}
		elsif ($1 eq '--') {
#
#	ANSI comment, scan to end of line and remove it
#
			my $tpos = pos($sql);
			$sql=~/\G.*?\n/gcs;
			substr($sql, $tpos, $+[0], ' ');
			pos($sql) = $tpos;
		}
		elsif ($1 eq '/*') {
#
#	C comment, scan to end of comment (note we don't worry about
#	endof comment inside string) and remove it
#
			my $tpos = pos($sql);
			return $dbh->set_err(-1,
				'Invalid SQL: "/*" comment without matching "*/".', 'S1000')
				unless ($sql=~/\G.*?\*\//gcs);
			substr($sql, $tpos, $+[0], ' ');
			pos($sql) = $tpos;
		}
		else { # FROM clause terminator keyword
			next unless exists $terminator{uc $2};
			$finished = $-[1],
			last
				unless $pcnt || $dcnt || $bcnt;
		}
	}	# end while scanning
#
#	remove trailing carriage return
#
	$sql=~s/\r$//;
	$finished = length($sql) unless $finished;
#
#	now send HELP COLUMNS to get true type info
#
#print "\n****select current_time, current_date\n";
#	my $rows = $dbh->selectall_arrayref('select current_time, current_date',
#		{ Slice => [0, 1], ChopBlanks => 1 });
#	
#print "\n****'HELP COLUMN ", substr($sql, 0, $finished), "\n";
	my $rows = $dbh->selectall_arrayref('HELP COLUMN ' . substr($sql, 0, $finished),
		{ Slice => [0, 1, 2], ChopBlanks => 1 })
		or return undef;
#
#	replace wild card with proper names
#
	if (defined($wild)) {
		if ($wild) {
			pop @cols;
			push @cols, map $rows->[$_][0], $wild..$#$rows;
		}
		else {
			shift @cols;
			$wild = (scalar @$rows) - (scalar @cols) - 1;
			unshift @cols, map $rows->[$_][0], 0..$wild;
		}
	}

	my @finalcols = ();
#
#	convert to concat'able form:
#		if not nullable, just cast and trim as needed,
#		else apply coalesce
#
	foreach (0..$#$rows) {
		push @finalcols,
			($rows->[$_][2] eq 'Y') ?
				(exists $cvttypes{$rows->[$_][1]} ?
					"COALESCE(($cols[$_]$cvttypes{$rows->[$_][1]}), '')" :
					"COALESCE($cols[$_], '')") :
				(exists $cvttypes{$rows->[$_][1]} ?
					"($cols[$_]$cvttypes{$rows->[$_][1]})" :
					"$cols[$_]");

		$finalcols[-1] = "TRIM(LEADING FROM $finalcols[-1])"
			unless ($rows->[$_][1] eq 'CV') || ($rows->[$_][1] eq 'CF');
	}

	$sep ||= '|';
	my $newlist = '(' . join(" || '$sep' ||\n", @finalcols) . "\n) as vartext";
#	print "\n*** Server side query is \n$origsql $newlist FROM ", substr($sql, $frompos), "\n";
	return "$origsql $newlist FROM " . substr($sql, $frompos);
}

sub tdat_Do {
	return $_[0]{_iobj}->io_tddo($_[1]);
}

sub tdat_CharSet {
	return $_[0]{tdat_charset};
}

sub tdat_GetSocket {
	return $_[0]{_iobj}[TD_IMPL_CONNFD];
}

sub tdat_GetPlatform {
	return $platform;
}

sub tdat_SendPassThru {
#
#	process msg based on msg kind
#
	return $_[0][TD_IMPL_PASSTHRU]->pt_send_passthru($_[1]) ||
		$_[0]->set_err($_[0]{_iobj}->io_get_error());
}

sub tdat_RecvPassThru {
#
#	process msg based on msg kind
#
	my $resp = $_[0][TD_IMPL_PASSTHRU]->pt_recv_passthru();
	return $resp ||
		$_[0]->set_err($_[0]{_iobj}->io_get_error());
}

sub tdat_MakeEndRequest {
#
#	process msg based on msg kind
#
	return $_[0]{_iobj}[TD_IMPL_PASSTHRU]->io_make_endreq($_[1]);
}

sub tdat_MakeTestResponse {
#	my ($dbh, $msg) = @_;
#
#	process msg based on msg kind
#
	return $_[0]{_iobj}[TD_IMPL_PASSTHRU]->io_make_testresp();
}
#
#	close any notion of xaction we might have
#
sub tdat_CloseXact {
	$_[0]{_iobj}[TD_IMPL_SESINXACT] = undef;
	return $_[0];
}
#
#	enable/disable debug on assoc. CLI object
#
#pragma end_full
sub tdat_SetDebug {
#pragma begin_full
	return $_[0]
		unless (ref $_[0]{_iobj}[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli');
#pragma end_full
	$_[0]{_iobj}[TD_IMPL_CONNFD]->cli_set_debug($_[1]);
	return $_[0];
}
#
#	process a PREPINFO
#
sub tdat_ProcessPrepare {
	my ($dbh, $flavor, $len, $parcel, $stmtno, $activity, $is_a_call) = @_;

	my %sthargs = (
		tdat_stmt_info => [ undef ],
		NAME => [],
		TYPE => [],
		tdat_TYPESTR => [],
		PRECISION => [],
		SCALE => [],
		NULLABLE => [],
		tdat_TITLE => [],
		tdat_FORMAT => [],
		tdat_CHARSET => [],
		_unpackstr => [],
		_parmdesc => [ ],
		_parmap => { },
		_parmnum => 0,
		_phnum => 0
	);

	my $nextcol = $dbh->{_iobj}->io_proc_prepinfo(
		$parcel, $stmtno, \%sthargs, $activity, $is_a_call, 0, $flavor);

	return $dbh->set_err($dbh->{_iobj}->io_get_error())
		unless defined($nextcol);

	return $nextcol ?
		{
			Names => $sthargs{NAME},
			Types => $sthargs{tdat_TYPESTR},
			Titles => $sthargs{tdat_TITLE},
			Formats => $sthargs{tdat_FORMAT},
		} :
		{};
}
#pragma end_full

sub get_info {
	my($dbh, $info_type) = @_;
	eval {
		require DBD::Teradata::GetInfo;
	};
	return undef if $@;
	my $v = $DBD::Teradata::GetInfo::info{int($info_type)};
	$v = $v->($dbh) if ref $v eq 'CODE';
	return $v;
}

sub type_info_all
{
	my ($dbh) = @_;
	eval {
		require DBD::Teradata::TypeInfo;
	};
	return undef if $@;
	return $DBD::Teradata::TypeInfo::type_info_all;
}

1;

package DBD::Teradata::st;
use DBI qw(:sql_types);
use Config;
#pragma begin_clear_text

use DBD::Teradata::BigNum qw(
	cvt_dec2flt 
	cvt_dec2bigint 
	cvt_int64tobigint 
	cvt_flt2dec
	cvt_bigint2int64
	cvt_bigint2dec);

use DBD::Teradata qw(
	:tdat_platforms
	:tdat_parcels
	:tdat_impl_codes
	:tdat_sth_codes
	:tdat_misc_codes
	:tdat_part_codes
	@td_decszs
	%td_type_dbi2stringtypes
	%td_type_dbi2pack
	%td_type_dbi2code
	@td_indicmasks
	@td_indicbits
	%td_type_dbi2str
	%td_type_dbi2hasprec
	%td_type_dbi2hasscale
	%td_type_dbi2size
	%td_part_map
);
#pragma end_clear_text

#
#	Statement attributes:
#	From base class:
#		NAME : array of returned column names
#		NAME_lc	: array of lowercase column names
#		NAME_uc	: array of uppercase column names
#		TYPE 	: array of column types
#		PRECISION	: array of column precision
#		SCALE	: array of column scales
#		NULLABLE	: array of column nullable indicators
#		CursorName : assoc. cursor name
#		NUM_OF_PARAMS : number of parameters
#		NUM_OF_FIELDS : number of output columns
#		ChopBlanks	: defined && != 0 => trim trailing whitespace
#
#	Public:
#		tdat_nowait :	1 => async request
#		tdat_raw	:	RecordMode or IndicatorMode
#		tdat_keepresp	:	1 => use keepresp
#		tdat_clone	:	statement handle to be cloned
#		tdat_TITLE	: array of column titles
#		tdat_FORMAT	: array of column formats
#		tdat_sessno : assoc. session num
#		tdat_stmt_num : current result stmt number
#		tdat_stmt_info : array of stmt info hashes
#		tdat_sp_save : 1 => enable save of stored proc text
#		tdat_sp_print : 1 => enable console PRINT of stored proc
#		tdat_mload : assoc. MLOAD statement number
#		tdat_mlseq : MLOAD record sequence number/offset
#					as a 2 element arrayref
#
#	Private:
#		_iobj :	I/O object
#		_colary : array of bound column arrays
#		_maxcolary : length of larget bound column array
#		_usephs : number of PH's in stmt
#		_params : array of bound parameters
#		_ptypes : array of parameter types
#		_plens : array of parameter precisions
#		_packstr : pack() string for params
#		_rows : number of rows affected by statement
#		_unpackstrs : array of fields unpack() strings
#		_rspbuf : current response buffer
#		_rspbufpos : current position in response buffer
#		_rowid : current cursor rowid
#		_reqno : request no. of current response.
#		_lastresp : 1 => curr. response buffer has ENDREQUEST
#		_failed : 1 => at least one FAILURE in response
#		_callparms : arrayref of CALL parameter descriptors
#		_parmmap : arrayref to map CALL param number to output column position
#		_dbh : our connection
#		_updatable : 1 => its a FOR CURSOR stmt
#		_active : 1 => query is active
#
#pragma begin_clear_text
$DBD::Teradata::st::imp_data_size = 0;

#pragma end_clear_text

#
#	null param fillers: note that fixed length CHAR
#	types (including temporals) get backfilled from the
#	packstring
#
our %nullvals = (
	SQL_VARCHAR, '',
	SQL_CHAR, '',
	SQL_FLOAT, 0.0,
	SQL_DECIMAL,'',
	SQL_INTEGER, 0,
	SQL_SMALLINT, 0,
	SQL_TINYINT, 0,
	SQL_VARBINARY, '',
	SQL_BINARY, '',
	SQL_LONGVARBINARY, '',
	SQL_DATE, 0,
	SQL_TIMESTAMP, '',
	SQL_TIME, '',
	SQL_INTERVAL_DAY, '',
	SQL_INTERVAL_DAY_TO_HOUR, '',
	SQL_INTERVAL_DAY_TO_MINUTE, '',
	SQL_INTERVAL_DAY_TO_SECOND, '',
	SQL_INTERVAL_HOUR, '',
	SQL_INTERVAL_HOUR_TO_MINUTE, '',
	SQL_INTERVAL_HOUR_TO_SECOND, '',
	SQL_INTERVAL_MINUTE, '',
	SQL_INTERVAL_MINUTE_TO_SECOND, '',
	SQL_INTERVAL_MONTH, '',
	SQL_INTERVAL_SECOND, '',
	SQL_INTERVAL_YEAR, '',
	SQL_INTERVAL_YEAR_TO_MONTH, '',
	SQL_TYPE_TIMESTAMP_WITH_TIMEZONE, '',
	SQL_TYPE_TIME_WITH_TIMEZONE, ''
	);

sub BindColArray {
	my ($sth, $pNum, $ary, $maxlen) = @_;

	return $sth->set_err(-1, 'BindColArray() requires arrayref parameter.', 'S1000')
		if (ref $ary ne 'ARRAY');

	return $sth->set_err(-1, 'Invalid column number.', 'S1000')
		if ($pNum <= 0);

	my $sthpriv = $sth->{_private};
	my $c = $sthpriv->[TD_STH_COLARY];
	$sthpriv->[TD_STH_COLARY] = [ ],
	$c = $sthpriv->[TD_STH_COLARY]
		unless $c;

	$$c[$pNum - 1] = $ary;
	if (defined($maxlen)) {
		my $ml = $sthpriv->[TD_STH_MAXCOLARY];
		$sthpriv->[TD_STH_MAXCOLARY] = $maxlen
			unless defined($ml) && ($ml >= $maxlen);
	}
	1;
}
*tdat_BindColArray = \&BindColArray;
*bind_col_array = \&BindColArray;

sub _split_vartext {
	my ($sth, $val) = @_;
	my $params = $sth->{_private}[TD_STH_PARAMS];
	my $numparms = $sth->{NUM_OF_PARAMS};
	my $p = (ref $val eq 'ARRAY') ? $val : (ref $val) ? [ $$val ] : [ $val ];
#
#	force array binding
#
	map { $params->[$_] = []; } 0..$numparms-1;
#
#	for external version
#
#	my @spare = ();
#	map { $spare[$_] = []; } 0..$numparms-1;

	my $plens = $sth->{_private}[TD_STH_PLENS];
	@$plens = (0) x $numparms;
	my @ps;
	my $notbar = ($sth->{tdat_vartext_in} eq '|') ? undef : $sth->{tdat_vartext_in};
	foreach my $v (@$p) {
		@ps = $notbar ? split($notbar, $v) : split('\|', $v);
		foreach (0..$numparms-1) {
			push (@{$params->[$_]}, $ps[$_]);
#			push (@{$spare[$_]}, $ps[$_]);
#
#	2.2.10: ignore oversize args in USING mode; let dbms report that error
#
			$plens->[$_] = length($ps[$_])
				if defined($ps[$_]) && ($plens->[$_] < length($ps[$_]));
		}
	}
#
#	we have to coerce these here; be sure to doc this!!!
#	Note they both point to same arrayrefs, so we only have to populate one
#
	map { $sth->{ParamArrays}{$_} = $sth->{ParamValues}{$_} = $params->[$_-1]; } 1..$numparms;
	return $params;
}

sub bind_param {
	my ($sth, $pNum, $val, $attr) = @_;
#
#	to permit apps to filter input sources and supply applicable job numbers,
#	a bitmask is supplied via
#	bind_param[_array](<paramcnt> + 1, [$bitmask | arrayref of bitmasks])
#	default is all jobs
#	if a bit is set, then the correspnding job will get a copy of the
#	parameter tuple
#
#	check if we're using named params
#
	my $sthpriv = $sth->{_private};
	my $pname = $pNum;
	unless ($pNum=~/^\d+$/) {
		return $sth->set_err(-1, 'Invalid parameter name.', 'S1000')
			unless (substr($pNum, 0, 1) eq ':') && $sthpriv->[TD_STH_USENAMES];

		my $i = 0;
		$pNum = uc substr($pNum, 1);
		$i++
			while (($i <= $#{$sthpriv->[TD_STH_USENAMES]}) &&
				($sthpriv->[TD_STH_USENAMES][$i] ne $pNum));

		return $sth->set_err(-1, 'Invalid parameter name.', 'S1000')
			if ($i > $#{$sthpriv->[TD_STH_USENAMES]});
		$pNum = $i + 1;
	}

#pragma begin_full
	if (ref $val eq 'ARRAY') {
		my $iobj = $sthpriv->[TD_STH_IOBJ];
		return $sth->set_err(-1, 'bind_param_array() valid only for DBC/SQL, FASTLOAD, and MLOAD sessions.', 'S1000')
			unless ($iobj->[TD_IMPL_SESPART] == TD_PART_SQL) ||
				($iobj->[TD_IMPL_SESPART] == TD_PART_FASTLOAD) ||
				($iobj->[TD_IMPL_SESPART] == TD_PART_MLOAD);
	}
#pragma end_full
#
#	make sure raw or vartext mode only allows param 1
#
	return $sth->set_err(-1, 'Only parameter number 1 valid for raw or vartext mode.', 'S1000')
		if ($pNum != 1) && ($sth->{tdat_raw_in} || $sth->{tdat_vartext_in});
#
#	default data type for placeholders is VARCHAR or default size
#	NOTE: we ignore type attrs for USING clauses
#
	my $type = SQL_VARCHAR;
	my $tlen = $phdfltsz;
	my $usephs = $sthpriv->[TD_STH_USEPHS];
	if ($usephs && defined($attr)) {
		if (ref $attr) {
			$type = $attr->{TYPE} || SQL_VARCHAR;
			if ($type == SQL_DECIMAL) {
				my $prec = $attr->{PRECISION} || 5;
				return $sth->set_err(-1, 'DECIMAL precision > 18 not supported when tdat_no_bigint enabled.', 'S1000')
					if ($prec > 18) && $sth->{tdat_no_bigint};

				return $sth->set_err(-1, 'Server does not support DECIMAL precision > 18.', 'S1000')
					unless ($prec <= 18) || $sth->{tdat_has_bignum};
				$tlen = ($prec  << 8) | ($attr->{SCALE} || 0);
			}
			elsif (($type == SQL_BIGINT) && (!$sth->{tdat_has_bignum})) {
				return $sth->set_err(-1, 'Server does not support BIGINT.', 'S1000');
			}
			else {
				$tlen = ($td_type_dbi2hasprec{$type} && exists $attr->{PRECISION}) 
					? $attr->{PRECISION} 
					: $td_type_dbi2size{$type};
			}
		}
		else {
			$type = $attr;
			return $sth->set_err(-1, 'Server does not support BIGINT.', 'S1000')
				unless ($type != SQL_BIGINT) || $sth->{tdat_has_bignum};
			$tlen = ($type == SQL_DECIMAL) ? (5 << 8) : $td_type_dbi2size{$type};
		}
	}
#
#	NOTE: these assignments are required!!!
#	DBI does NOT route these attributes thru our FETCH!!!
#
	$sth->{ParamValues}{$pNum} = $val;
	$sth->{ParamArrays}{$pNum} = $val;
#
#	preprocess vartext
#
	_split_vartext($sth, $val),
	return 1
		if $sth->{tdat_vartext_in};

	$sthpriv->[TD_STH_PARAMS][$pNum-1] = $val;
	return 1
		unless $usephs;

	my $ptypes = $sthpriv->[TD_STH_PTYPES];
	$ptypes->[$pNum-1] = $td_type_dbi2stringtypes{$type} ? SQL_VARCHAR : $type;
	my $plens = $sthpriv->[TD_STH_PLENS];
	$plens->[$pNum-1] =
		(($type == SQL_VARCHAR) ||
		($type == SQL_LONGVARCHAR) ||
		($type == SQL_LONGVARBINARY) ||
		($type == SQL_VARBINARY)) ?
			(($attr && $attr->{PRECISION}) ? $tlen :
				(defined($val) && (! ref $val)) ? length($val) : $phdfltsz) :
			$tlen;
	1;
}
*BindParamArray = \&bind_param;
*tdat_BindParamArray = \&bind_param;
*bind_param_array = \&bind_param;

sub bind_param_status {
	return $_[0]->set_err(-1, 'Status argument must be arrayref.', 'S1000')
		unless (ref $_[1] eq 'ARRAY');

	$_[0]{_private}[TD_STH_STSARY] = $_[1];
	return 1;
}

sub cvt_arm_flt {
	my ($lo, $hi) = unpack('LL', pack('d', $_[0]));
	return unpack('d', pack('LL', $hi, $lo));
}

sub bind_param_inout {
	my ($sth, $pNum, $val, $maxlen, $attr) = @_;
#
#	what do I need maxlen for ???
#
#	check if we're using named params
#
	my $sthpriv = $sth->{_private};
	unless ($pNum=~/^\d+$/) {
		return $sth->set_err(-1, 'Invalid parameter name.', 'S1000')
			unless (substr($pNum, 0, 1) eq ':') && $sthpriv->[TD_STH_USENAMES];

		my $i = 0;
		$pNum = uc substr($pNum, 1);
		$i++
			while (($i <= $#{$sthpriv->[TD_STH_USENAMES]}) &&
				($sthpriv->[TD_STH_USENAMES][$i] ne $pNum));

		return $sth->set_err(-1, 'Invalid parameter name.', 'S1000')
			if ($i > $#{$sthpriv->[TD_STH_USENAMES]});
		$pNum = $i + 1;
	}
#
#	if its a real INOUT CALL param, bind the $val as a column
#
	$sth->bind_col($sthpriv->[TD_STH_PARMMAP]{$pNum}+1, $val)
		if $sthpriv->[TD_STH_PARMMAP] &&
			defined($sthpriv->[TD_STH_PARMMAP]{$pNum});

	return bind_param($sth, $pNum, $val, $attr)
}

#
#	wrapper to handle regular execute vs. execute_for_fetch()
#
sub execute {
	return _execute_any({}, @_);
}

sub _execute_any {
	my ($attrs, $sth, @bind_values) = @_;
#
#	make sure we're not active
#
	my $sthpriv = $sth->{_private};
	my $iobj = $sthpriv->[TD_STH_IOBJ];

	$sth->finish
		if $sthpriv->[TD_STH_ACTIVE];

	delete $iobj->[TD_IMPL_REQ2STH]{$sthpriv->[TD_STH_REQNO]}
		if defined($sthpriv->[TD_STH_REQNO]);

	my $params = $attrs->{_fetch_sub} ? undef :
		(scalar @bind_values) ? \@bind_values :
		$sthpriv->[TD_STH_PARAMS];
#
#	if there were leftovers from prior array binding:
#
	$params = delete $attrs->{_residual}
		if $attrs->{_residual};

	my $numParam = $sth->{NUM_OF_PARAMS} || 0;
#
#	if vartext input mode, and params supplied here,
#	use bind_param to fix things up
#
	if ($sth->{tdat_vartext_in} && ($#bind_values >= 0)) {
		return undef
			unless $sth->bind_param(1, $bind_values[0]);
		$params = $sthpriv->[TD_STH_PARAMS];
	}

	my ($ptypes, $plens, $usephs) =
		($sthpriv->[TD_STH_PTYPES], $sthpriv->[TD_STH_PLENS], $sthpriv->[TD_STH_USEPHS]);

	my ($sessno, $dbh, $partition) =
		($sth->{tdat_sessno}, $sthpriv->[TD_STH_DBH], $iobj->[TD_IMPL_SESPART]);
	my $loading = (($partition == TD_PART_FASTLOAD) || ($partition == TD_PART_MLOAD));
#
#	check for positioned update/delete
#
	my ($use_cursor, $cursnm, $cursth) = (0, '', undef);
	if ($sth->{Statement}=~/\s+WHERE\s+CURRENT\s+OF\s+([^\s;]+)\s*;?$/i) {
		$cursnm = uc $1;
		$cursth = $dbh->{_cursors}{$cursnm};
		return $sth->set_err(-1, 'Specified cursor not defined or not updatable.', 'S1000')
			unless $cursth;

		return $sth->set_err(-1, 'Specified cursor not positioned on a valid row.', 'S1000')
			unless $cursth->{_private}[TD_STH_ROWID];
		$use_cursor = 1;
	}
	$iobj->[TD_IMPL_TWOBUF] = 0,
	$sth->{tdat_keepresp} = 1
		if ($sth->{Statement}=~/\s+FOR\s+CURSOR\s*;?$/i);
#
#	if params provided directly, then force
#	VARCHAR type
#	NO! Prepare should already have established defaults, and
#	if they've been overridden, DBI std. says keep prior metadata
#	NOTE this may create problems for default length...
#
#	@$ptypes = (SQL_VARCHAR) x $numParam,
#	@$plens = ($phdfltsz) x $numParam
#		if (scalar @bind_values) && $usephs && (! $sth->{tdat_vartext_in});
#
#	check for rawmode, in which case, we expect only
#	a single parameter binding, which is the raw binary
#	row buffer
#
	my $rawmode = $sth->{tdat_raw_in};
	my $modepcl =
		($partition == TD_PART_MLOAD) ? PclMLOADREC :
		(($partition == TD_PART_DBCCONS) ||
			($rawmode && ($rawmode eq 'RecordMode'))) ? PclDATA :
				PclINDICDATA;

#pragma begin_full
	$numParam = 1
		if (($partition != TD_PART_EXPORT) && $numParam && defined($rawmode));
#pragma end_full

	$numParam = 0
		unless ($partition != TD_PART_DBCCONS) ||
			($iobj->[TD_IMPL_PROMPTED] && ($#$params >= 0));

	return $sth->set_err(-1, 'Too many parameters provided.', 'S1000')
		if defined($params) && (@$params > $numParam);

	return $sth->set_err(-1, 'No parameters provided for parameterized statement.', 'S1000')
		unless ($numParam == 0) || defined($params) || defined($dbh->{tdat_loading}) ||
			$attrs->{_fetch_sub};

	my $stmtno = 0;
#
#	need to check for array or in/out params, and adjust as needed
#
	my $maxparmlen = 1;
	if ($attrs->{_fetch_sub}) {
		$maxparmlen = MAX_PARM_TUPLES;
	}
	else {
		foreach (0..$numParam-1) {
			$maxparmlen = scalar(@{$$params[$_]})
				if (ref $$params[$_] eq 'ARRAY') &&
					(scalar(@{$$params[$_]}) > $maxparmlen);
		}
	}
#pragma begin_full
#
#	clean up for prior monses case
	$sthpriv->[TD_STH_MONSES] = undef;
#
#	handle EXPORT case
#
	if ($partition == TD_PART_EXPORT) {
		my $stmt = pack('a8', '');

		foreach (0..1) {
			return $sth->set_err(-1, 'EXPORT session requires 2 non-NULL INTEGER parameters.', 'S1000')
				if ($$ptypes[$_] != SQL_INTEGER);
			my $p = $$params[$_];

			return $sth->set_err(-1, 'EXPORT session requires 2 non-NULL INTEGER parameters.', 'S1000')
				unless defined($p);

			$p = $$p[0],
			return $sth->set_err(-1, 'Parameter arrays not supported for EXPORT sessions.', 'S1000')
				if (ref $p eq 'ARRAY');

			$p = $$p if (ref $p eq 'SCALAR');
			last unless $p;
			substr($stmt, ($_ * 4), 4) = pack('L', $p);
		}
		return 0 if ($stmt eq '');
		$sth->{Statement} = $stmt;
		my $rowcnt = $iobj->io_execute($sth, '', '');

		return $sth->set_err($iobj->io_get_error())
			unless defined($rowcnt);
		return ($rowcnt == 0) ? '0E0' : $rowcnt;
	}
#
#	check for fastload/mload control BEGIN LOADING
#
	if (($partition == TD_PART_SQL) && defined($dbh->{tdat_lsn}) &&
		(($sth->{Statement}=~/^BEGIN\s+LOADING/i) ||
		($sth->{Statement}=~/^BEGIN\s+(DELETE\s+)?MLOAD/i) ||
		($sth->{Statement}=~/^MLOAD\s+/i))) {
		my $rowcnt = $iobj->io_execute($sth, '', '');

		$dbh->{tdat_loading} = 1;
		$sth->set_err($iobj->io_get_error()),
		$dbh->{tdat_loading} = 0
			unless defined($rowcnt);
		$sth->{tdat_stmt_num} = $stmtno;

		return undef unless defined($rowcnt);
		return ($rowcnt == 0) ? '0E0' : $rowcnt;
	}
#pragma end_full

	my $fldcnt = $numParam;
	my ($tuples, $datainfo, $indicdata) = (0, '', '');
	my $pos = 0;
	if (($params && scalar @$params) || $attrs->{_fetch_sub}) {
		($tuples, $datainfo, $indicdata) =
#pragma begin_full
			(($partition == TD_PART_MLOAD) || ($partition == TD_PART_FASTLOAD)) ?
				_process_util_params($sth, $fldcnt, $rawmode, $modepcl, $maxparmlen, $params) :
#pragma end_full
			$usephs ? _process_ph_params($sth, $fldcnt, $params, $attrs) :
				_process_using_params($sth, $fldcnt, $rawmode, $modepcl, $params, $attrs);
		return undef
			unless $tuples;
	}

#pragma begin_full
	$sth->{tdat_stmt_num} = 1,
	return $tuples
		if ($partition == TD_PART_FASTLOAD) ||
			($partition == TD_PART_MLOAD);
#
#	not fastload, just send it
#
#pragma end_full
#
#	open transaction if needed
#
	$iobj->io_tddo('BT'),
	$iobj->[TD_IMPL_SESINXACT] = 1
		if ($partition == TD_PART_SQL) && (!$dbh->{AutoCommit}) &&
			($dbh->{tdat_mode} ne 'ANSI') && ($iobj->[TD_IMPL_SESINXACT] == 0);

	$iobj->[TD_IMPL_SESINXACT] = 1
		if ($partition == TD_PART_SQL) && ($dbh->{tdat_mode} eq 'ANSI');
#
#	exit now if array bound
#
	return $tuples
		if $attrs->{_fetch_sub};
#
#	execute for non-array binding case
#
	my $rowcnt = $iobj->io_execute($sth, $datainfo, $indicdata,
		($use_cursor ? $cursth->{_private}[TD_STH_ROWID] : undef));

	$sthpriv->[TD_STH_ROWS] = $rowcnt;

	return $sth->set_err($iobj->io_get_error())
		unless defined($rowcnt);
#
#	make us 'Active' if we're data returning
#
	$sth->{Active} = (defined($sth->{NUM_OF_FIELDS}) && ($sth->{NUM_OF_FIELDS} != 0));
#
#	void rowid if positioned DELETE
#
	undef $sthpriv->[TD_STH_ROWID]
		if ($sth->{Statement}=~/^DELETE\s+.+\s+WHERE\s+CURRENT\s+OF\s+\w+$/i);
	$sthpriv->[TD_STH_ACTIVE] = 1
		unless $sthpriv->[TD_STH_LASTRESP] && (! $sth->{tdat_keepresp});
#
#	for 1.12 compatibility
#
	return ($rowcnt == 0) ? -1 : $rowcnt
		if ($sth->{tdat_compatible} lt '2.0');

	return ($rowcnt == 0) ? '0E0' : $rowcnt;
}

sub _gen_datainfo {
	my ($fldcnt, $ptypes, $plens, $maxszs) = @_;
#
#	DATAINFO parcel only needed for statements with placeholders,
#	not for USING clauses *AND* we don't permit array binding
#	(mload/fastload requires USING, and SQL uses execute_for_fetch)
#
	my $packstr = '';
	my $datainfo = pack('S', $fldcnt) . ("\0" x ($fldcnt * 4));
	my $prec;
	my $j = 2;
	foreach (0..$fldcnt-1) {

		my $tdtype = $td_type_dbi2code{$ptypes->[$_]}+1;

		$plens->[$_] ||= 0
			if ($ptypes->[$_] == SQL_VARCHAR) || ($ptypes->[$_] == SQL_VARBINARY);

		$prec = ($ptypes->[$_] == SQL_DECIMAL) ?
			$td_decszs[($plens->[$_] >> 8) & 255] : $plens->[$_];

		$packstr .=
			(($ptypes->[$_] == SQL_BINARY) ||
			($ptypes->[$_] == SQL_DECIMAL))		? "a$prec " :
			($ptypes->[$_] == SQL_CHAR) 		? "A$prec " :
				$td_type_dbi2pack{$ptypes->[$_]} . ' ';

		$prec = 2 + $plens->[$_]
			if ($maxszs->[$_] < $plens->[$_]) &&
				(($ptypes->[$_] == SQL_VARCHAR) ||
					($ptypes->[$_] == SQL_VARBINARY));

		substr($datainfo, $j, 4) = pack('SS', $tdtype, $plens->[$_]);

		$maxszs->[$_] = $prec;
		$j += 4;
	} # end for each field
	return ($datainfo, $packstr);
}

sub _process_ph_params {
	my ($sth, $fldcnt, $params, $attrs) = @_;

	my $indicdata;
	my $pos = 0;
	my $maxsz = 100;
	my ($i, $p);
	my $sthpriv = $sth->{_private};
	my $iobj = $sthpriv->[TD_STH_IOBJ];
	my $ptypes = $sthpriv->[TD_STH_PTYPES];
	my $plens = $sthpriv->[TD_STH_PLENS];
	my $fetch_sub = $attrs->{_fetch_sub};
#
#	build up the current packstring for the params
#	NOTE: we do not support the implicit conversion of temporal types to
#	fixed-length CHAR for placeholders, so the app is responsible for
#	appropriate casting...which is required anyway, since Teradata
#	doesn't understand its own temproal type codes in DATAINFO (sigh)
#
	my @maxszs = ((0) x $fldcnt);
	my ($datainfo, $packstr) = _gen_datainfo($fldcnt, $ptypes, $plens, \@maxszs);
#
#	iterate for param array mode or execute_for_fetch
#
	my @tmpary = ();
	my $ttype = SQL_VARCHAR;
	my @indicvec = ();
	my $tuples = 0;
#
#	our DECIMAL conversion depends on Math::BigInt, and it has
#	issues on Linux
#
	my $deccvt =
		($sth->{tdat_no_bigint} || (!$has_bigint)) ? \&cvt_flt2dec : \&cvt_bigint2dec;
#
#	prime the pump if needed
#
	$params = &$fetch_sub()
		unless $params && scalar @$params;

	while ($params) {
#
#	build INDICDATA parcel
#
		@indicvec = (0xFF) x (($fldcnt & 7) ?
			($fldcnt>>3) + 1 : $fldcnt>>3);
		$pos = 0;
		@tmpary = ();
		foreach (0..$fldcnt-1) {

			$ttype = $ptypes->[$_];
			$p = $params->[$_];
			$p = (ref $p eq 'ARRAY') ? $p->[0] : $$p
				if defined($p) && (ref $p);
#
#	skip over NULLs (we already set them to zeros above)
#
			push(@tmpary, $nullvals{$ttype}),
			$pos +=
				(($ttype == SQL_VARCHAR) ||
				($ttype == SQL_VARBINARY))	? 2 : $maxszs[$_],
			next
				unless defined($p);
#
#	else load the data
#
			$indicvec[$_>>3] &= $td_indicmasks[$_ & 7],
			push(@tmpary,
				($ttype == SQL_DECIMAL)				? $deccvt->($p, $plens->[$_]) :
				(($ttype == SQL_FLOAT) && $use_arm) ? cvt_arm_flt($p) :
				($ttype == SQL_BIGINT)				? cvt_bigint2int64($p) :
				$p);

			$sth->set_err(-1, $@, 'S1000'),
			return (undef, undef, undef)
				if ($ttype == SQL_DECIMAL) && !defined($tmpary[-1]);

			$pos +=
				(($ttype == SQL_VARCHAR) ||
				($ttype == SQL_VARBINARY))	? 2 + length($p) :
				($ttype == SQL_DECIMAL)		? $td_decszs[(($plens->[$_]>>8) & 255)] :
				$plens->[$_];
#
#	update max length for VARCHAR/VARBINARY if bigger
#	NOTE: we cheat the datainfo position by omitting the
#	2 byte fldcount, since we'd just have to backtrack for
#	the length field anyway
#
			$maxszs[$_] = length($p) + 2,
			substr($datainfo, 4 + ($_ << 2), 2, pack('S', $maxszs[$_]))
				if (($ttype == SQL_VARCHAR) || ($ttype == SQL_VARBINARY)) &&
					($maxszs[$_] < length($p) + 2);
		}	# end for
		map { $maxsz += $_; } @maxszs;
		$maxsz += scalar @indicvec;
		$indicdata = "\0" x $maxsz;
		substr($indicdata, 0, scalar(@indicvec), pack('C*', @indicvec));
#		if (index($packstr, 'c') >= 0) {
#			if (($tmpary[2] < -128) || ($tmpary[2] > 127)) {
#				print "\nTrying to pack $tmpary[2] as a char\n";
#			}
#		}
		substr($indicdata, scalar(@indicvec), $pos, pack($packstr, @tmpary));
		$pos += scalar(@indicvec);

		return (++$tuples, $datainfo, substr($indicdata, 0, $pos))
#pragma begin_full
			unless $fetch_sub;
#pragma end_full
			;
#pragma begin_full
		$attrs->{_residual} = $params,
		substr($iobj->[TD_IMPL_SESBUFF], $attrs->{_datainfop}, length($datainfo), $datainfo),
		return ($tuples, undef, undef)
			if (($iobj->[TD_IMPL_SESBUFPOS] + $pos + 8 + 12) > $iobj->[TD_IMPL_SESREQSZ]);

		$tuples++;
		$iobj->[TD_IMPL_SESBUFPOS] =
			$iobj->[TD_IMPL_REQFAC]->floadPackRow(
				$iobj->[TD_IMPL_SESBUFPOS], $pos, PclINDICDATA,
					$iobj->[TD_IMPL_SESBUFF], $indicdata);
#
#	get next tuple
#
		$params = &$fetch_sub();
#pragma end_full

	}	# end while forever

	substr($iobj->[TD_IMPL_SESBUFF], $attrs->{_datainfop}, length($datainfo), $datainfo);
	return ($tuples, undef, undef);
}

sub _process_using_params {
	my ($sth, $fldcnt, $rawmode, $modepcl, $params, $attrs) = @_;

	my $indicdata;
	my $pos = 0;
	my $maxsz = (($fldcnt & 7) ? ($fldcnt>>3) + 1 : $fldcnt>>3);
	my ($i, $k, $p);
	my $sthpriv = $sth->{_private};
	my $iobj = $sthpriv->[TD_STH_IOBJ];
	my $ptypes = $sthpriv->[TD_STH_PTYPES];
	my $plens = $sthpriv->[TD_STH_PLENS];
	my $packstr = $sthpriv->[TD_STH_PACKSTR];
	my $fetch_sub = $attrs->{_fetch_sub};
	my $tuples = 0;
#
#	our DECIMAL conversion depends on Math::BigInt, and it has
#	issues on Linux
#
	my $deccvt =
		($sth->{tdat_no_bigint} || (!$has_bigint)) ? \&cvt_flt2dec : \&cvt_bigint2dec;

	unless ($rawmode) {	# for USING clauses
		for ($i = 0; $i < $fldcnt; $i++) {
			$maxsz += (($ptypes->[$i] == SQL_DECIMAL) ?
				$td_decszs[(($plens->[$i]>>8) & 255)] : $plens->[$i]);
			$maxsz += 2
				if ($ptypes->[$i] == SQL_VARCHAR) || ($ptypes->[$i] == SQL_VARBINARY);
		}
	}
#
#	iterate for param array mode or execute_for_fetch
#
	my @tmpary = ();
	my $ttype = SQL_VARCHAR;
	my @indicvec = ();
#
#	prime the pump if needed
#
	$params = &$fetch_sub()
		unless $params && scalar @$params;

	my $is_vartext = $sth->{tdat_vartext_in};
	my @ps;
	my $notbar = ($is_vartext && ($is_vartext eq '|')) ? undef : $is_vartext;

	while ($params) {
#
#	if vartext input and only one param column, split it
#
		if ($is_vartext && (scalar @$params == 1)) {

			@$params = $notbar ? split($notbar, $params->[0]) : split('\|', $params->[0]);
			foreach (0..$fldcnt-1) {
				$plens->[$_] = length($params->[$_])
					if defined($params->[$_]) && ($plens->[$_] < length($params->[$_]));
			}
		}

		unless ($rawmode) {
#
#	build INDICDATA parcel
#
			@indicvec = (0xFF) x (($fldcnt & 7) ? ($fldcnt>>3) + 1 : $fldcnt>>3);
			$pos = 0;
			@tmpary = ();
			foreach (0..$fldcnt-1) {

				$ttype = $ptypes->[$_];
				$p = $params->[$_];
#
#	vartext gets bound as arrayrefs, so we need to check
#
				$p = (ref $p eq 'ARRAY') ? $p->[0] : $$p
					if defined($p) && (ref $p);
#
#	skip over NULLs (we already set them to zeros above)
#
				push(@tmpary, $nullvals{$ttype}),
				$pos +=
					(($ttype == SQL_VARCHAR) ||
					($ttype == SQL_VARBINARY))	? 2 :
					($ttype == SQL_DECIMAL)		? $td_decszs[($plens->[$_] >> 8) & 255] :
						$plens->[$_],
				next
					unless defined($p);
#
#	else load the data
#
				$indicvec[$_>>3] &= $td_indicmasks[$_ & 7],
				push(@tmpary,
					($ttype == SQL_DECIMAL)				? $deccvt->($p, $plens->[$_]) :
					($ttype == SQL_BIGINT)				? cvt_bigint2int64($p) :
					(($ttype == SQL_FLOAT) && $use_arm) ? cvt_arm_flt($p) :
					$p);

				$sth->set_err(-1, $@, 'S1000'),
				return (undef, undef, undef)
					if ($ttype == SQL_DECIMAL) && !defined($tmpary[-1]);

				$pos +=
					(($ttype == SQL_VARCHAR) ||
					($ttype == SQL_VARBINARY))	? 2 + length($p) :
					($ttype == SQL_DECIMAL)		? $td_decszs[(($plens->[$_]>>8) & 255)] :
					$plens->[$_];
					$ttype = $ptypes->[$i];
					$p = $params->[$i];
					$p = $$p
						if defined($p) && (ref $p);
			}	# end for

			$indicdata = "\0" x $maxsz;
			substr($indicdata, 0, scalar(@indicvec), pack('C*', @indicvec));
			substr($indicdata, scalar(@indicvec), $pos, pack($packstr, @tmpary));
			$pos += scalar(@indicvec);
		} # end if not raw mode
		else {
#
#	rawmode: trim the length prefix and newline suffix
#	adjust for param arrays
#
			$p = $params->[0];
			$p = (ref $p eq 'ARRAY') ? $p->[0] : $$p
				if defined($p) && (ref $p);

			$pos = length($p) - 3;
			$indicdata = substr($p, 2, $pos);
		}

		return (++$tuples, undef, substr($indicdata, 0, $pos))
#pragma begin_full
			unless $fetch_sub
#pragma end_full
		;

#pragma begin_full
		$attrs->{_residual} = $params,
		return ($tuples, undef, undef)
			if (($iobj->[TD_IMPL_SESBUFPOS] + $pos + 8 + 12) > $iobj->[TD_IMPL_SESREQSZ]);

		$tuples++;
		$iobj->[TD_IMPL_SESBUFPOS] =
			$iobj->[TD_IMPL_REQFAC]->floadPackRow(
				$iobj->[TD_IMPL_SESBUFPOS], $pos, $modepcl, $iobj->[TD_IMPL_SESBUFF], $indicdata);
#
#	get next tuple
#
		$params = &$fetch_sub();
#pragma end_full
	} # end for param tuple
	return ($tuples, undef, undef);
} # end if params

#pragma begin_full
#
#	only for utility sessions (mload/fastload)
#
sub _process_util_params {
	my ($sth, $fldcnt, $rawmode, $modepcl, $maxparmlen, $params) = @_;

	my $indicdata;
	my $pos = 0;
	my $mlrowcnt = 0;
	my $maxsz = 100;
	my ($i, $k, $p);
	my $sthpriv = $sth->{_private};
	my $iobj = $sthpriv->[TD_STH_IOBJ];
	my $ptypes = $sthpriv->[TD_STH_PTYPES];
	my $plens = $sthpriv->[TD_STH_PLENS];
	my $packstr = $sthpriv->[TD_STH_PACKSTR];
	my $part = $iobj->[TD_IMPL_SESPART];
#
#	compute number of parameters in array
#
	unless ($rawmode) {	# for USING clauses
		for ($i = 0; $i < $fldcnt; $i++) {
			$maxsz += (($ptypes->[$i] == SQL_DECIMAL) ?
				$td_decszs[(($plens->[$i]>>8) & 255)] : $plens->[$i]);
			$maxsz += 2
				if ($ptypes->[$i] == SQL_VARCHAR) || ($ptypes->[$i] == SQL_VARBINARY);
		}
	}
#
#	process MLOAD masks
#
	my @mlmask = ();
	if ($part == TD_PART_MLOAD) {
		$p = $sth->{tdat_mlmask} || 127;
		@mlmask = ((ref $p) ? @$p :
			($p) x $maxparmlen);

		push @mlmask, (0) x ($maxparmlen - $#mlmask)
			if ($maxparmlen > scalar @mlmask);
	}
#
#	iterate for param array mode
#
	my @tmpary = ();
	my $ttype = SQL_VARCHAR;
	my @indicvec = ();
	my $pad = ($modepcl == PclMLOADREC) ? 18 : 8;
#
#	our DECIMAL conversion depends on Math::BigInt, and it has
#	issues on Linux
#
	my $deccvt =
		($sth->{tdat_no_bigint} || (!$has_bigint)) ? \&cvt_flt2dec : \&cvt_bigint2dec;
#
#	force all param arrays to max length
#
	$maxparmlen--;
	foreach (@$params) {
		$#$_ = $maxparmlen
			if (ref $_ eq 'ARRAY') && ($maxparmlen > $#$_);
	}
	$maxparmlen++;

	for ($k = 0; $k < $maxparmlen; $k++) {
		unless ($rawmode) {
			$indicdata = (chr(0) x $maxsz);
#
#	build INDICDATA parcel
#
			@indicvec = (0xFF) x (($fldcnt & 7) ?
				($fldcnt>>3) + 1 : $fldcnt>>3);
			$pos = 0;
			@tmpary = ();
			for ($i = 0; $i < $fldcnt; $i++) {
#
#	adjust for param arrays
#
				$ttype = $$ptypes[$i];
				$p = $$params[$i];
				$p = (ref $p eq 'ARRAY') ? $$p[$k] : $$p
					if defined($p) && (ref $p);
#
#	skip over NULLs (we already set them to zeros above)
#
				push(@tmpary, $nullvals{$ttype}),
				$pos +=
					(($ttype == SQL_VARCHAR) || ($ttype == SQL_VARBINARY)) ? 2 :
					($ttype != SQL_DECIMAL)                                ? $$plens[$i] :
						$td_decszs[(($$plens[$i]>>8) & 255)],
				next
					unless defined($p);
#
#	else load the data
#
				$indicvec[$i>>3] &= $td_indicmasks[$i & 7],
				push(@tmpary,
					($ttype == SQL_DECIMAL) ? $deccvt->($p, $$plens[$i]) :
					($ttype == SQL_BIGINT) ? cvt_bigint2int64($p) :
					(($ttype == SQL_FLOAT) && $use_arm) ? cvt_arm_flt($p) :
					$p);

				$sth->set_err(-1, $@, 'S1000'),
				return (undef, undef, undef)
					if ($ttype == SQL_DECIMAL) && !defined($tmpary[-1]);

				$pos +=
					(($ttype == SQL_VARCHAR) ||
					($ttype == SQL_VARBINARY)) ? 2 + length($p) :
					($ttype != SQL_DECIMAL)    ? $$plens[$i] :
						$td_decszs[(($$plens[$i]>>8) & 255)];

			}	# end for

			substr($indicdata, 0, scalar(@indicvec)) = pack('C*', @indicvec);
			substr($indicdata, scalar(@indicvec), $pos) = pack($packstr, @tmpary);
			$pos += scalar(@indicvec);
		} # end if not raw mode
		else {
#
#	rawmode: trim the length prefix and newline suffix
#	adjust for param arrays
#
			$p = $$params[0];
			$p = ((ref $p eq 'ARRAY') ? $$p[$k] : $$p)
				if defined($p) && (ref $p);

#			print "Got null p\n"
#				unless defined($p);

			$pos = length($p) - 3;
			$indicdata = substr($p, 2, $pos);
		}
#
#	fastload/mload uses deferred transfer
#
		if (($iobj->[TD_IMPL_SESBUFPOS] + $pos + $pad + 12) > $iobj->[TD_IMPL_SESREQSZ]) {
			$sth->set_err(-1,
				($maxparmlen > 1) ?
		"Message buffer overflow at row $k; reduce parameter array size(s) and resubmit." :
		'Message buffer overflow; commit, then resubmit.',
					'S1000');
			$iobj->[TD_IMPL_SESINXACT] = 0,
			$iobj->[TD_IMPL_SESBUFPOS] = TDAT_HDRSZ
				if ($maxparmlen > 1);
			return undef;
		}
		$iobj->[TD_IMPL_SESINXACT] = 1;
		if ($modepcl == PclMLOADREC) {
#
#	add the control header; consists of a sequence
#	number within a ML job, and 6 bytes:
#	(1, job number, 1, job number,RFU,RFU
#	to compute sequence number
#	used solely for identifying UV or error records
#	from the source (otherwise, maintaining sequence
#	in parallel would be impossible!)
#	tdat_mlseq is arrayref of (sequence number, session offset, jobcount)
#
			foreach my $mljob (0..$sth->{tdat_mlseq}[2]-1) {
				$iobj->[TD_IMPL_SESBUFPOS] =
					$iobj->[TD_IMPL_REQFAC]->mloadPackRow(
						$iobj->[TD_IMPL_SESBUFPOS], $pos, $modepcl, $sth->{tdat_mlseq}[0],
						$mljob, $iobj->[TD_IMPL_SESBUFF], $indicdata),
				$mlrowcnt++
					if (1 & ($mlmask[$k] >> $mljob));
			}
			$sth->{tdat_mlseq}[0] += $sth->{tdat_mlseq}[1];
		}
		else {
			$iobj->[TD_IMPL_SESBUFPOS] =
				$iobj->[TD_IMPL_REQFAC]->floadPackRow(
					$iobj->[TD_IMPL_SESBUFPOS], $pos, $modepcl,
						$iobj->[TD_IMPL_SESBUFF], $indicdata);
		}
	} # end for each param array element
	return ((($part == TD_PART_MLOAD) ? $mlrowcnt : $maxparmlen), undef, undef);
} # end if params
#
#	new array binding i/f:
#	execute_array() is cut/paste from DBI 1.52, w/ minor
#	mods to handle raw/vartext
#
sub execute_array {
	my $sth = shift;

	my ($attr, @array_of_arrays) = @_;
	my $NUM_OF_PARAMS = $sth->FETCH('NUM_OF_PARAMS'); # may be undef at this point
#
# get tuple status array or hash attribute
#
	my $tuple_sts = $attr->{ArrayTupleStatus};
	return $sth->set_err(1, "ArrayTupleStatus attribute must be an arrayref")
		if $tuple_sts and ref $tuple_sts ne 'ARRAY';
#
# bind all supplied arrays
#
	if (@array_of_arrays) {
		$sth->{ParamArrays} = { };	# clear out old params
		return $sth->set_err(1,
			@array_of_arrays." bind values supplied but $NUM_OF_PARAMS expected")
			if defined ($NUM_OF_PARAMS) &&
				(! $sth->{tdat_vartext_in}) &&
				(! $sth->{tdat_raw_in}) &&
				(@array_of_arrays != $NUM_OF_PARAMS);
		$sth->bind_param_array($_, $array_of_arrays[$_-1])
			or return
			foreach (1..@array_of_arrays);
	}

	my $fetch_tuple_sub;
	my $tuple_idx = 0;
	my ($tupleincr, $report) = $attr->{tdat_progress} ? @{$attr->{tdat_progress}} : ();

	if ($fetch_tuple_sub = $attr->{ArrayTupleFetch}) {	# fetch on demand

		return $sth->set_err(1,
			"Can't use both ArrayTupleFetch and explicit bind values")
			if @array_of_arrays; # previous bind_param_array calls will simply be ignored

		if (UNIVERSAL::isa($fetch_tuple_sub,'DBI::st')) {
			my $fetch_sth = $fetch_tuple_sub;
			return $sth->set_err(1,
				"ArrayTupleFetch sth is not Active, need to execute() it first")
				unless $fetch_sth->{Active};
#
# check column count match to give more friendly message
#
			my $NUM_OF_FIELDS = $fetch_sth->{NUM_OF_FIELDS};
			return $sth->set_err(1,
				"$NUM_OF_FIELDS columns from ArrayTupleFetch sth but $NUM_OF_PARAMS expected")
				if defined($NUM_OF_FIELDS) && defined($NUM_OF_PARAMS) && ($NUM_OF_FIELDS != $NUM_OF_PARAMS);
			$fetch_tuple_sub = $tupleincr ?
				sub {
				 	$tuple_idx++;
				 	&$report("$tuple_idx") unless $tuple_idx % $tupleincr;
					$fetch_sth->fetchrow_arrayref;
				} :
				sub { $fetch_sth->fetchrow_arrayref; };
	    }
	    elsif (!UNIVERSAL::isa($fetch_tuple_sub,'CODE')) {
			return $sth->set_err(1,
				"ArrayTupleFetch '$fetch_tuple_sub' is not a code ref or statement handle");
		}
	}
	else {
		my $NUM_OF_PARAMS_given = keys %{ $sth->{ParamArrays} || {} };
		return $sth->set_err(1,
			"$NUM_OF_PARAMS_given bind values supplied but $NUM_OF_PARAMS expected")
			if defined($NUM_OF_PARAMS) &&
				(! $sth->{tdat_vartext_in}) &&
				(! $sth->{tdat_raw_in}) &&
				($NUM_OF_PARAMS != $NUM_OF_PARAMS_given);
#
# get the length of a bound array
#
		my $maxlen;
		my %hash_of_arrays = %{$sth->{ParamArrays}};
		my %isa_ref = map { ($_, (ref $hash_of_arrays{$_} eq 'ARRAY')); } keys %hash_of_arrays;
		foreach (values %hash_of_arrays) {
			$maxlen = @$_
				if (ref $_ eq 'ARRAY') && (!$maxlen || (@$_ > $maxlen));
		}
#
# if there are no arrays then execute scalars once
#
		$maxlen = 1 unless defined $maxlen;
		my @bind_ids = 1..keys(%hash_of_arrays);

		$fetch_tuple_sub = sub {
			return undef
				if $tuple_idx >= $maxlen;
			my @tuple = map {
				$isa_ref{$_} ? $hash_of_arrays{$_}[$tuple_idx] : $hash_of_arrays{$_};
			} @bind_ids;
			++$tuple_idx;
		 	&$report("$tuple_idx")
		 		unless (! $tupleincr) || ($tuple_idx % $tupleincr);
			return \@tuple;
		};
	}

	return $sth->execute_for_fetch($fetch_tuple_sub, $tuple_sts);
}

#
#	NOTE: we've included the patch to return both
#	tuple counts AND affected rowcounts if called in array context
#
sub execute_for_fetch {
	my ($sth, $fetch_tuple_sub, $tuple_status) = @_;
#
#	array params only valid on FASTLOAD, MLOAD, and SQL w/ R6.0+
#
	my $sthpriv = $sth->{_private};
	my $iobj = $sthpriv->[TD_STH_IOBJ];
	$tuple_status = [] unless defined $tuple_status;
#
#	only support SQL sessions
#
	return $sth->set_err(-1,
		'Array binding valid only for DBC/SQL connection; use tdat_UtilitySetup instead.', 'S1000')
		if ($iobj->[TD_IMPL_SESPART] == TD_PART_FASTLOAD) || ($iobj->[TD_IMPL_SESPART] == TD_PART_MLOAD);

	return $sth->set_err(-1, 'Array binding valid only for DBC/SQL sessions.', 'S1000')
		unless ($iobj->[TD_IMPL_SESPART] == TD_PART_SQL);
#
#	only support non-data returning statements
#
	return $sth->set_err(-1,
		'Array binding not valid for data returning queries.', 'S1000')
		if $sth->{NUM_OF_FIELDS};
#
#	only support parameterized statements
#
	return $sth->set_err(-1, 'No parameters defined for query.', 'S1000')
		unless $sth->{NUM_OF_PARAMS};
#
#	only support single statement requests (verify this for R6.1+)
#
	return $sth->set_err(-1,
		'Array binding not valid for multistatement requests.', 'S1000')
		if (scalar @{$sth->{tdat_stmt_info}} > 2);
#
#	check configured max request size; if query + datainfo + resppcl too big, throw error
#
	my $base = 8 + length($_[2]) +
		($sthpriv->[TD_STH_USEPHS] ? 8 + 2 + ($sth->{NUM_OF_PARAMS} * 4) : 0) + 12;
	return $sth->set_err(-1,
		'Insufficient request buffer size $iobj->[TD_IMPL_SESREQSZ]; increase tdat_reqsize connection attribute.', 'S1000')
		if ($base > $iobj->[TD_IMPL_SESREQSZ]);
#
#	if release < R6+, then just execute each stmt
#
	my $rc = 0;
	my $rowcount = 0;
	$#$tuple_status = -1;
	my $failed;
	my $dbh = $sth->{Database};
	my $errfail = ($dbh->{tdat_mode} eq 'TERADATA') && (!$dbh->{AutoCommit});
	if ($iobj->[TD_IMPL_VERSNUM] < 6000000) {
		my $tuple;
		while ($tuple = $fetch_tuple_sub->()) {
			$rc = _execute_any({}, $sth, @$tuple);
			if (defined($rc)) {
				push @$tuple_status, $rc;
				$rowcount += $rc;
			}
			else {
#
#	if AutoCommit OFF and transaction mode is TERADATA,
#	we must clear all prior tuple status entries, and return immediately
#
				if ($errfail) {
					map { $tuple_status->[$_] = -2; } 0..$#$tuple_status;
				}

				push @$tuple_status, [ $sth->err, $sth->errstr, $sth->state ];
#
#	if internal error, exit now; else save as status
#
				return undef
					if $errfail || ($sth->err < 0);
				$failed = 1;
			}
		}
		$rowcount = '0E0' unless $rowcount;
		return $failed ? undef :
			wantarray ? (scalar @$tuple_status, $rowcount) :
				scalar @$tuple_status;
	}
#
#	init a buffer if we don't have one, and save the
#	position at the end of the fixed part (SQL, OPTIONS, and DATAINFO)
#	so we can just reposition there for each iteration
#
	my $dip = $iobj->io_init_bulk_request($sth->{NUM_OF_PARAMS}, $sthpriv->[TD_STH_USEPHS], $sth->{Statement});
	my $endfixed = $iobj->[TD_IMPL_SESBUFPOS];
#
#	use _execute_any() to load and send;
#	force fully blocking behavior
#
	$sth->{tdat_nowait} = undef;
#
#	need explicit variable cuz we're going to store our residual here
#
	my $attrs = {_fetch_sub => $fetch_tuple_sub, _datainfop => $dip };
	my $curtuple = 0;
	my $tuples = 0;
	while ($tuples = _execute_any($attrs, $sth)) {
#
#	send the accumulated buffer
#	if there's a hard failure, then tuple status is not updated, so
#	we just return the status for anything sent prior
#
		return $sth->set_err($iobj->io_get_error())
			unless $iobj->io_array_execute($sth);
#
#	reset request
#
		$iobj->[TD_IMPL_SESBUFPOS] = $endfixed;
#
#	suck in whatever's waiting
#
		my $rspmsg = $iobj->io_gettdresp($sth);
		return undef unless $rspmsg;
#
#	startup the next continue if not end of request
#
		$iobj->io_tdcontinue($sth, 1)
			if (! $sth->{_private}[TD_STH_LASTRESP]) && $iobj->[TD_IMPL_TWOBUF];
#
#	iterate over the response, collecting status
#
		$rc = $iobj->io_array_fetch($sth, $tuple_status);
#
#	if AutoCommit OFF and transaction mode is TERADATA,
#	we must clear all prior tuple status entries, and return immediately
#
		unless (defined($rc)) {
			$failed = 1;
			if ($errfail) {
				map { $tuple_status->[$_] = -2; } 0..$#$tuple_status-1;
				return wantarray ? () : undef;
			}
#
#	just mark the entries up to and after the failure as ineffective
#
			my $i = $tuples - (scalar @$tuple_status - $curtuple);
#			print "\n *** sent $tuples error at $curtuple returned ", (scalar @$tuple_status), " with $i remaining\n";
#			map { $tuple_status->[$_] = -2; } $curtuple..$#$tuple_status-1;
			push @$tuple_status, ((-2) x $i);
		}
		$curtuple = scalar @$tuple_status;
		$rowcount += $rc if $rc;
	}
	$rowcount = '0E0' unless $rowcount;
#	print "\n*** execute_for_fetch failed!\n" if $failed;
	return wantarray ?
		($failed ? () : (scalar @$tuple_status, $rowcount)) :
		($failed ? undef : scalar @$tuple_status);
}
#pragma end_full

sub Realize {
	return defined($_[0]{_private}[TD_STH_IOBJ]->io_Realize($_[0])) ?
		1 : $_[0]->set_err($_[0]{_private}[TD_STH_IOBJ]->io_get_error());
}
*tdat_Realize = \&Realize;
#
#	rewind the current resultset (if possible)
#
sub tdat_Rewind {
	return $_[0]{_private}[TD_STH_IOBJ]->io_rewind($_[0]);
}
#
#	we've broken this up to permit TeraCeptor to use our
#	unpack function
#
sub fetch {
	return $_[0]->tdat_Unpack();
}

#pragma begin_full
sub tdat_SetStmtNum {
	$_[0]{tdat_stmt_num} = $_[1];
	return 1;
}

sub tdat_SetSummary {
	my ($sth, $tdflavor, $value) = @_;
	$sth->{tdat_stmt_info}[$sth->{tdat_stmt_num}]{IsSummary} = undef,
	return
		unless $tdflavor;

	my $stmthash = $sth->{tdat_stmt_info}[$sth->{tdat_stmt_num}];
	if ($tdflavor == PclWITH) {
		$stmthash->{IsSummary} = $value;
		$stmthash->{SummaryPosition} = [ ],
		$stmthash->{SummaryPosStart} = [ ]
			unless defined($stmthash->{SummaryPosition});
	}
	elsif ($tdflavor == PclENDWITH) {
		delete $stmthash->{IsSummary};
	}
	elsif ($tdflavor == PclPOSSTART) {
		my $sumpos = $stmthash->{SummaryPosition};
		my $sumstart = $stmthash->{SummaryPosStart};
		push(@$sumstart, scalar(@$sumpos));
	}
	elsif ($tdflavor == PclPOSITION) {
		my $sumpos = $stmthash->{SummaryPosition};
		push @$sumpos, $value;
	}

	return 1;
}
#pragma end_full
#
#	externally callable unpacker
#	$rec is the "raw" record
#	$recmode, if present, indicates the record
#		lacks indicators
#	if $rec no provided, the I/O fetch is performed
#
sub tdat_Unpack {
	my ($sth, $rec, $recmode) = @_;

	my $sthpriv = $sth->{_private};
	my $sessno = $sth->{tdat_sessno};
	my $stmtno = $sth->{tdat_stmt_num};
	my $nowait = $sth->{tdat_nowait};
	my $stmtinfo = $sth->{tdat_stmt_info};
	my $rawmode = $sth->{tdat_raw_out};
	my $vartext = $sth->{tdat_vartext_out};
	my $colary = $sthpriv->[TD_STH_COLARY];
	my $maxlen = $sthpriv->[TD_STH_MAXCOLARY];
	my $iobj = $sthpriv->[TD_STH_IOBJ];
	my $data = '';
	my @tmpary = ();
	my $ary = (defined($colary) ? ($rawmode ? $$colary[0] : \@tmpary) : undef);
	my $rc;

	if (defined($rec)) {
		$data = $rec;
		$rc = 1;
	}
	else {
		$rc = $iobj->io_fetch($sth, $ary, \$data);
		return $sth->set_err($iobj->io_get_error())
			unless defined($rc);
		$sth->{Active} = undef
			unless ($rc > 0) || $sth->{tdat_more_results};
		return $rc if ($rc <= 0);
	}

	my $loopcnt = $rc;
	my $ftypes = $sth->{TYPE};
	my $fprec = $sth->{PRECISION};
	my $fscale = $sth->{SCALE};
	my $stmthash = $$stmtinfo[$stmtno];
	my $isCall = ($stmthash->{ActivityType} eq 'Call');
	my $actends = $stmthash->{EndsAt};
	my $actstarts = $stmthash->{StartsAt};
	my $actsumstarts = $stmthash->{SummaryStarts};
	my $actsumends = $stmthash->{SummaryEnds};
	my $issum = $stmthash->{IsSummary};
	my $numflds = defined($issum) ?
		$$actsumends[$issum] - $$actsumstarts[$issum] + 1 :
		$actends - $actstarts + 1;
#
#	lookup our unpack string
#
	my $unpackstr = $sthpriv->[TD_STH_UNPACKSTRS][
		(defined($issum) ? $$actsumstarts[$issum] : $actstarts)];
#pragma begin_full
#
#	hack to support old mon ses format
#	$unpackstr = $sthpriv->[TD_STH_DBH]->get_old_monses,
	if ($sthpriv->[TD_STH_MONSES] && ($sthpriv->[TD_STH_MONSES] != $numflds)) {
		$numflds = $sthpriv->[TD_STH_MONSES];
#
#	if its MONITOR SQL, check for v2r5 vs pre-v2r5
#
		if (($sth->{Statement}=~/^MONITOR\s+SQL/i) && ($numflds == 2)) {
			$sth->{NAME}[5] = 'StepText';
#			$sth->{NAME_lc}[5] = 'steptext';
#			$sth->{NAME_uc}[5] = 'STEPTEXT';
			$sth->{TYPE}[5] = SQL_VARCHAR;
			$sth->{PRECISION}[5] = 32768;
			$unpackstr = 'S S/a';
		}
	}
#pragma end_full

	my $ibytes = ((($numflds & 7) !=  0) ? ($numflds>>3) + 1 : $numflds>>3);
#
#	to fake out DBI, we need to backfill the extra row fields with undef
#
	my @row;
	$#row = $sth->{NUM_OF_FIELDS} - 1;
	if ($rawmode) {
		return $sth->_set_fbav(\@row) if defined($colary);
		$data = substr($data, $ibytes) if ($rawmode eq 'RecordMode');
#
#	add length prefix and newline suffix
#
		$row[0] = pack('S a* c', length($data), $data, 10);
		return $sth->_set_fbav(\@row);
	}

	my $fpos = 0;
	$tmpary[0] = $data unless defined($colary);

	my $ibit = 0;
	my $pos = 0;
	my $prec = 0;
	my $ftype = 0;
	my $indstr ='';
	my @indics = ();
	my $lastpos = 0;
	my $chopem = $sth->{ChopBlanks};
	my $no_indics = (($stmthash->{ActivityType} eq 'Exec MLOAD') ||
		$recmode || ($stmthash->{ActivityType} eq 'Echo') ||
		($iobj->[TD_IMPL_SESPART] == TD_PART_DBCCONS));
#
#	init everthing to NULLs
#
	if (defined($colary)) {
		map { @$_ = (undef) x $loopcnt; } @$colary;
	}
	my $use_arm = ($Config{archname}=~/^arm-linux/i) ? 1 : undef;
	my $i;
	my $deccvt =
		($sth->{tdat_no_bigint} || (!$has_bigint)) ? \&cvt_dec2flt : \&cvt_dec2bigint;

	my $utf8 = $sthpriv->[TD_STH_DBH]{_utf8};
	my $vtext = $sth->{tdat_vartext_out};
	for (my $k = 0; $k < $loopcnt; $k++) {
		@row = $vtext ? (('') x $sth->{NUM_OF_FIELDS}) :
			((undef) x $sth->{NUM_OF_FIELDS});
#
#	if using formatted mode, then we've already got the array
#
		if (ref $data) {
			$data = $tmpary[$k] if defined($colary);

			$fpos = defined($issum) ? $$actsumstarts[$issum] : $actstarts;
			$lastpos = $fpos + $numflds - 1;
			@row[$fpos..$lastpos] = @$data;
			$#row = $numflds - 1
				if ($#row < $numflds-1);

			foreach $i ($fpos..$lastpos) {
				next unless defined($row[$i]);
#
#	if UTF8, let Perl know
#	NOTE: this can fail, we should wrap w/ eval()
#
				if ($utf8) {
					$row[$i] = Encode::decode_utf8($row[$i]);
#
#	DAA 12/17/05
#	if UTF8 and CHAR(), then trim down or pad up to true precision
#	(only pad up if not ChopBlanks)
#
					if ($$ftypes[$i] == SQL_CHAR) {
						no bytes;
						if ((length($row[$i]) < $$fprec[$i]) && (!$chopem)) {
							$row[$i] .= ' ' x ($$fprec[$i] - length($row[$i]));
						}
						else {
							$row[$i] = substr($row[$i], 0, $$fprec[$i]);
						}
						use bytes;
					}
				}
				$row[$i] =~ s/\s+$//
					if $chopem;
#
#	should we trim leading spaces from numerics ? not for now
#
				next if defined($vtext);
				$colary->[$i][$k] = $row[$i] if $$colary[$i];
			}
			if (defined($vtext)) {
#
#	vartext output, join the columns into single item
#
				$row[0] = join($vtext, @row);
				$numflds--;
				@row[1..$numflds] = (undef) x $numflds;
				$numflds++;

				$colary->[0][$k] = $row[0] if $$colary[0];
			}
			next;
		}
#
#	trim off extra we add for rawmode in array output mode
#
		$data = substr($tmpary[$k], 2, length($tmpary[$k]) - 3)
			if defined($colary);
		$pos = 0;

		$indstr = substr($data, 0, $ibytes),
		$data = substr($data, $ibytes)
			unless $no_indics;
		@indics = $no_indics ? (0) : unpack('C*', $indstr);
#
#	unpack the data into our row array
#
#print "!!!!! unpackstr is empty!!!\n" unless $unpackstr;
		$unpackstr=~tr/A/a/;
		$fpos = defined($issum) ? $$actsumstarts[$issum] : $actstarts;
		splice(@row, $fpos, $numflds, unpack($unpackstr, $data));
		$#row = $sth->{NUM_OF_FIELDS} - 1
			if ($#row < $sth->{NUM_OF_FIELDS}-1);
#
#	then cleanup the NULL fields, trim blanks,
#	and move to output arrays if needed
#
		for (my $i = 0; $i < $numflds; $i++, $fpos++) {
			my $ftype = $$ftypes[$fpos];

			$row[$fpos] = ($vtext ? '' : undef), next
				if defined($indics[$i>>3]) &&
					($indics[$i>>3] & $td_indicbits[($i & 7)]);

#			next unless defined($row[$fpos]);

			if ($ftype == SQL_DECIMAL) {
				return $sth->set_err(-1, 'DECIMAL precision > 18 not supported when tdat_no_bigint enabled.', 'S1000')
					if ($$fprec[$fpos] > 18) && $sth->{tdat_no_bigint};
				$row[$fpos] =
					$deccvt->($row[$fpos], $$fprec[$fpos], $$fscale[$fpos]);
			}
			elsif ($ftype == SQL_BIGINT) {
				return $sth->set_err(-1, 'BIGINT not supported when tdat_no_bigint enabled.', 'S1000')
					if $sth->{tdat_no_bigint};
				$row[$fpos] = cvt_int64tobigint($row[$fpos]);
			}
			elsif ($ftype == SQL_FLOAT) {
				$row[$fpos] = cvt_arm_flt($row[$fpos])
					if $use_arm;
			}
			elsif (($ftype == SQL_CHAR) || ($ftype == SQL_VARCHAR)) {

				if ($utf8) {
					$row[$fpos] = Encode::decode_utf8($row[$fpos]);
#
#	DAA 12/17/05
#	if UTF8 and CHAR(), then trim down or pad up to true precision
#	(only pad up if not ChopBlanks)
#
					if ($ftype == SQL_CHAR) {
						no bytes;
						if ((length($row[$fpos]) < $$fprec[$fpos]) && (!$chopem)) {
							$row[$fpos] .= ' ' x ($$fprec[$fpos] - length($row[$fpos]));
						}
						else {
							$row[$fpos] = substr($row[$fpos], 0, $$fprec[$fpos]);
						}
						use bytes;
					}
				}

				$row[$fpos] =~ s/\s+$//
					if $chopem;
			}

			next if defined($vtext);
			$colary->[$i][$k] = $row[$fpos] if $$colary[$i];
		}

		if (defined($vtext)) {
#
#	vartext output, join the columns into single item
#
			$row[0] = join($vtext, @row);
			$numflds--;
			@row[1..$numflds] = (undef) x $numflds;
			$numflds++;
			$colary->[0][$k] = $row[0] if $$colary[0];
		}
	}
#
#	if called with a record, just return the arrayref
#
	return $rec ? \@row : $sth->_set_fbav(\@row);
}
*fetchrow_arrayref = \&fetch;

sub STORE {
	my ($sth, $attr, $val) = @_;

	return $sth->SUPER::STORE($attr, $val)
		unless ($attr=~/^(tdat)?_/) ;
	$sth->{$attr} = $val;
	return 1;
}
#
#	return current parameter values
#
sub tdat_get_param_values {
	my $sth = shift;

#print "we got to get_param_values\n";

	return undef unless $sth->{NUM_OF_PARAMS};

	my $sthpriv = $sth->{_private};
	my $numParam = $sth->{NUM_OF_PARAMS};
	my $params = $sthpriv->[TD_STH_PARAMS];

	my %pval = ();
	if ($sthpriv->[TD_STH_USEPHS]) {
		map { $pval{$_+1} = $params->[$_]; } (0..$numParam-1);
		return \%pval;
	}
#
# must be named params (ie, USING clause)
#
	my $pnames = $sthpriv->[TD_STH_USENAMES];
	map { $pval{$pnames->[$_]} = $params->[$_]; } (0..$#$pnames);
	return \%pval;
}
#
#	return current parameter types
#
sub tdat_get_param_types {
	my $sth = shift;

	return undef unless $sth->{NUM_OF_PARAMS};

	my $sthpriv = $sth->{_private};
	my $numParam = $sth->{NUM_OF_PARAMS};
	my $ptypes = $sthpriv->[TD_STH_PTYPES];
	my $plens = $sthpriv->[TD_STH_PLENS];
	my $pnames = $sthpriv->[TD_STH_USENAMES];
	my $callparms = $sthpriv->[TD_STH_CALLPARMS];
	my $name;
	my %pval = ();
	foreach (0..$numParam-1) {
		$name = $pnames ? $pnames->[$_] : $_+1;
		$pval{$name} = {
			TYPE => ($ptypes->[$_] || SQL_UNKNOWN_TYPE),
		};
#
#	check if a SP CALL
#	1 => PH
#	2 => IN
#	4 => OUT
#
		$pval{$name}{IN_OR_OUT} = ($callparms->[$_] & 2) ?
			(($callparms->[$_] & 4) ? 'INOUT' : 'IN') : 'OUT'
			if $callparms;

		next unless $ptypes->[$_] && $td_type_dbi2hasprec{$ptypes->[$_]};

		$pval{$name}{PRECISION} = $plens->[$_];

		$pval{$name}{PRECISION} = ($plens->[$_] >> 8) && 0xFF,
		$pval{$name}{SCALE} = $plens->[$_] & 0xFF
			if ($ptypes->[$_] == SQL_DECIMAL);
	}
	return \%pval;
}

sub FETCH {
	my ($sth, $attr) = @_;

	return ($attr eq 'tdat_param_types') ?
		tdat_get_param_types($sth) : $sth->{$attr}
		if ($attr =~ /^(tdat)?_/);

	return ($attr eq 'ParamTypes') ? tdat_get_param_types($sth) :
		$sth->SUPER::FETCH($attr);
}

sub rows {
	$_[0]{_private}[TD_STH_ROWS];
}

sub finish { return tdat_finish($_[0]); }

sub tdat_finish {
	my $sthpriv = $_[0]{_private};
	my $iobj = $sthpriv->[TD_STH_IOBJ];
	$iobj->io_finish($_[0]) if $iobj;
	$sthpriv->[TD_STH_LASTRESP] = 1;
	$_[0]{Active} = $sthpriv->[TD_STH_ACTIVE] = undef;
	$sthpriv->[TD_STH_RSPBUF] = undef;
	$sthpriv->[TD_STH_RSPBUFPOS] = -1;
	delete $_[0]{tdat_more_results};
#	$_[0]->SUPER::finish;
	1;
}

sub cancel {
	my $sthpriv = $_[0]{_private};
	my $iobj = $sthpriv->[TD_STH_IOBJ];
	$iobj->io_cancel($_[0]) if $iobj;
#
#	do we need to purge our buffers here ?
#
	unless ($_[0]{tdat_nowait}) {
		$sthpriv->[TD_STH_LASTRESP] = 1;
		$_[0]{Active} = $sthpriv->[TD_STH_ACTIVE] = undef;
		$sthpriv->[TD_STH_RSPBUF] = undef;
		$sthpriv->[TD_STH_RSPBUFPOS] = -1;
	}
	1;
}

sub DESTROY {
	my ($sth) = @_;
	my $sthpriv = $sth->{_private};
	my $dbh = $sthpriv->[TD_STH_DBH];
	tdat_finish($sth)
		if $sthpriv->[TD_STH_ACTIVE] && (! $dbh->{_ignore_destroy});

	$sthpriv->[TD_STH_DBH] = undef;
	$sthpriv->[TD_STH_IOBJ] = undef;
	$sthpriv->[TD_STH_RSPBUF] = undef;
	delete $sth->{_private};
#
#	due to bug in DBI, I have to re-establish error info here
#
	$dbh->set_err($dbh->{_iobj}->io_get_error)
		if $dbh->{_iobj} && $dbh->{_iobj}->[TD_IMPL_LASTERR];
}

sub tdat_CharSet {
	return $_[0]{_private}[TD_STH_DBH]{tdat_charset};
}

sub map_type2str {
	my ($sth) = @_;

	return undef unless $sth->{NUM_OF_FIELDS};
	my $types = $sth->{TYPE};
	my $precs = $sth->{PRECISION};
	my $scales = $sth->{SCALE};
	my @typestrs = ();
	foreach my $i (0..$#$types) {
		my $type = $td_type_dbi2str{$types->[$i]};

		push (@typestrs, "DECIMAL($$precs[$i],$$scales[$i])"), next
			if ($types->[$i] == SQL_DECIMAL);
#
#	we don't actually ever see this since Tdat always returns
#	intervals as CHARs
#
		$type=~s/\(\)([^\(]+)\(\)/\($$precs[$i]\)$1\($$scales[$i]\)/,
		push (@typestrs, $type),
		next
			if $td_type_dbi2hasscale{$types->[$i]};

		$type .= "($$precs[$i])"
			if $td_type_dbi2hasprec{$types->[$i]};

		push (@typestrs, $type);
	}
	return \@typestrs;
}

#pragma begin_full
#
#	return unpack info for specified stmt
#
sub tdat_GetUnpackStr {
#	my ($sth, $rec, $stmtnum, $mode, $issum) = @_;
	return $_[0]->tdat_Unpack($_[1]);
}

sub tdat_CopyResp {
	return $_[0]{_private}[TD_STH_IOBJ][TD_IMPL_PASSTHRU]->pt_copy_passthru(@_);
}
#pragma end_full

1;

package DBD::Teradata::ReqFactory;
#
#	provides basic request message factory
#	(no APH, regular response)
#
use DBD::Teradata qw(:tdat_parcels :tdat_msgkinds :tdat_misc_codes :tdat_impl_codes);

use strict;
use warnings;

sub new {
	my $class = shift;
	my $self = {
		noprepopts => pack('SSa10', PclOPTIONS, 14, 'RS'),
		prepopts => pack('SSa10', PclOPTIONS, 14, 'RP'),
		multinoprepopts => pack('SSa10', PclOPTIONS, 14, 'MS'),
		multiprepopts => pack('SSa10', PclOPTIONS, 14, 'MP'),

		execopts => pack('SSa10', PclOPTIONS, 14, 'RE'),
		indicopts => pack('SSa10', PclOPTIONS, 14, 'IE'),
		tsrpcl => pack('SSSC', PclMULTITSR, 7, 1, 1)
	};
	return bless $self, $class;
}
#pragma begin_full
#
#	pack an mload row
sub mloadPackRow {
	my ($self, $pos, $len, $modepcl, $seqno, $job) = @_;
#
#	$_[6] is now the target buffer, $_[7] is src buffer
#
	substr($_[6], $pos, $len+14) =
		pack('SSLCCCCCCa*', $modepcl, $len+14, $seqno, 1, $job+1, 1,
			$job+1, 0, 0, substr($_[7], 0, $len));
	return $pos + (4 + $len + 10);
}

sub floadPackRow {
	my ($self, $pos, $len, $modepcl) = @_;
#
#	$_[4] is now the target buffer, $_[5] is src buffer
#
	substr($_[4], $pos, $len+4) =
		pack('SSa*', $modepcl, $len+4, substr($_[5], 0, $len));
	return $pos + (4 + $len);
}
#pragma end_full

sub simpleRequest {
	my ($self, $iobj, $respsz) = @_;
#
#	this is only used internally for small reqs, so
#	we don't need to handle big V2R5 reqs
#
	my $reqlen = 4 + length($_[3]) + 6;
	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, $reqlen) =
		pack('SSA* SSS',
			PclREQUEST,
			4 + length($_[3]),	# pclsize
			$_[3],	# request
			PclRESP,
			6,
			$respsz);

	return $reqmsg;
}

sub continueReq {
	my ($self, $iobj, $reqno, $keepresp, $respsz) = @_;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDCONTINUE, 6, $reqno);
	return undef unless $reqmsg;
	substr($reqmsg, TDAT_HDRSZ, 6) =
		pack('SSS', ($keepresp ? PclKEEPRESP : PclRESP), 6, $respsz);
	return $reqmsg;
}

sub prepareRequest {
	my ($self, $iobj, $rowid, $usingvars, $respsz) = @_;
#
#	$dbreq in $_[5]
#	but we don't copy out since dbreq may be very big
#
	my $reqlen = 14 + 4 + length($_[5]) + 6;

	$reqlen += 4 + length($rowid)
		if $rowid;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	my $reqpos = TDAT_HDRSZ;

	substr($reqmsg, $reqpos, 14) = $usingvars ?
		$self->{prepopts} : $self->{noprepopts};

	$reqpos += 14;

	substr($reqmsg, $reqpos, 4+length($_[5])) =
		pack('SSa*', PclREQUEST, 4 + length($_[5]), $_[5]);

	$reqpos += 4 + length($_[5]);
	substr($reqmsg, $reqpos, 4 + length($rowid)) =
		pack('SSA*', PclCURSORHOST, 4 + length($rowid), $rowid),
	$reqpos += 4 + length($rowid)
		if $rowid;

	substr($reqmsg, $reqpos, 6) = pack('SSS', PclRESP, 6, $respsz);
	return $reqmsg;
}

sub spRequest {
	my ($self, $iobj, $sz, $pos, $segment, $sth, $respsz) = @_;

	my $reqlen = 7 + 4 + $sz + 6;	# we only expect small responses
# room for OPTION & SPOPT for first TSR
	$reqlen += 6 + 14
		unless $pos;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;
	my $bufpos = TDAT_HDRSZ;

	substr($reqmsg, $bufpos, 7) =
		pack('SSSC', PclMULTITSR, 7, $segment, (($pos + $sz < length($sth->{Statement})) ? 0 : 1));
	$bufpos += 7;
#
#	only for 1st TSR
#
	substr($reqmsg, $bufpos, 14) = pack('SSa10', PclOPTIONS, 14, 'IE'),
	$bufpos += 14
		unless $pos;

	substr($reqmsg, $bufpos, 4 + $sz) =
		pack('SSA*', PclINDICREQ, 4 + $sz, substr($sth->{Statement}, $pos, $sz));
	$bufpos += 4 + $sz;
#
#	only for 1st TSR
#
	substr($reqmsg, $bufpos, 6) = pack('SSAA',
		PclSPOPTIONS, 6,
		($sth->{tdat_sp_print} ? 'Y' : 'N'),
		($sth->{tdat_sp_save} ? 'Y' : 'N')),
	$bufpos += 6
		unless $pos;

	substr($reqmsg, $bufpos, 6) = pack('SSS', PclRESP, 6, $respsz);

	$pos += $sz;

	return ($reqmsg, $pos);
}

#pragma begin_full
sub exportRequest {
	my ($self, $iobj, $respsz) = @_;
	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, 18);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, 18) =
		pack('SSa* SSS', PclREQUEST, 12, substr($_[3], 0, 8),
			PclRESP, 6, $respsz);
	return $reqmsg;
}

sub loadRequest {
	my ($self, $iobj, $respsz) = @_;
#
#	use buffer as is
#
	substr($iobj->[TD_IMPL_SESBUFF], $iobj->[TD_IMPL_SESBUFPOS], 6, pack('SSS', PclRESP, 6, $respsz));
	$iobj->[TD_IMPL_SESBUFPOS] += 6;
	$iobj->io_buildtdhdr(COPKINDSTART, $iobj->[TD_IMPL_SESBUFPOS] - TDAT_HDRSZ, undef, $iobj->[TD_IMPL_SESBUFF]);
	return $iobj;
}

sub monitorRequest {
	my ($self, $iobj, $modepcl, $keepresp, $respsz, $stmt, $indicdata) = @_;

	my $rsppcl = $keepresp ? PclKEEPRESP : PclRESP;

	my $reqlen =
		4 + length($stmt) + (length($indicdata) ? 4 + length($indicdata) : 0) + 6;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, $reqlen) = ($indicdata ne '') ?
		pack('SSA* SSa* SSS',
			PclINDICREQ,
			4 + length($stmt),	# pclsize
			$stmt,	# request

			$modepcl,
			4 + length($indicdata),
			$indicdata,

			$rsppcl,
			6,
			$respsz)
		: pack('SSA* SSS',
			PclINDICREQ,
			4 + length($stmt),
			$stmt,

			$rsppcl,
			6,
			$respsz);

	return $reqmsg;
}

sub consoleRequest {
	my ($self, $iobj, $respsz, $stmt, $datainfo, $indicdata) = @_;

	my $bufpos = TDAT_HDRSZ;

	my $reqlen = ($stmt eq ';') ?
		6 + 4 + length($indicdata) :
		4 + length($stmt) + 6 +
			(length($datainfo) ? 4 + length($datainfo) : 0) +
			(length($indicdata) ? 4 + length($indicdata) : 0);

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	substr($reqmsg, $bufpos, 4 + length($stmt)) = pack('SSA*',
		PclREQUEST, 4 + length($stmt),	$stmt),
	$bufpos += (4 + length($stmt))
		if ($stmt ne ';');

	$respsz = 64000 if ($respsz > 64000);
	substr($reqmsg, $bufpos, 6) = pack('SSS', PclRESP, 6, $respsz);
	$bufpos += 6;

	substr($reqmsg, $bufpos, 3 + length($indicdata)) =
		pack('SSa*', PclDATA, length($indicdata) + 4, substr($indicdata, 1)),
	$bufpos += (3 + length($indicdata)),
	$iobj->io_clearPrompt()
		if ($stmt eq ';') && ($indicdata ne '');

	substr($reqmsg, $bufpos, 4) = pack('SS', PclDATA, 4)
		if ($stmt eq ';') && ($indicdata eq '');

	return $reqmsg;
}

sub bulkRequest {
	my ($self, $numparams, $usephs) = @_;
#
#	$buf is $_[3], SQL stmt is $_[4]
#
	my $bufpos = TDAT_HDRSZ;
	substr($_[3], $bufpos, 14 + 4 + length($_[4])) =
		pack('SSa* SSA*',
			PclOPTIONS, 14, "IE\0\0Y\0\0\0\0\0",
			PclINDICREQ, 4 + length($_[4]), $_[4]);
	$bufpos += 14 + 4 + length($_[4]);

	return ($bufpos, undef)
		unless $usephs;

	my $dipos = $bufpos + 4;	# point to body of parcel
	my $dilen = 4 + ($numparams * 4) + 2;
	substr($_[3], $bufpos, $dilen) = pack('SSS', PclDATAINFO, $dilen, $numparams);

	return ($bufpos + $dilen, $dipos);
}

sub closeBulkRequest {
	my ($self, $pos, $respsz) = @_;
#
#	$buf is $_[3]
#
	substr($_[3], $pos, 6) = pack('SSS', PclRESP, 6, $respsz);
	$pos += 6;
	return $pos;
}
#pragma end_full

sub sqlRequest {
	my ($self, $iobj, $sth, $forCursor, $rowid, $modepcl, $keepresp, $respsz) = @_;
#
#	SQL stmt is $_[8], $datainfo is $_[9], $indicdata is $_[10]
#
	my $reqlen = 4 + length($_[8]) + 6 +
		((defined($_[9]) && length($_[9])) ? 4 + length($_[9]) : 0) +
		((defined($_[10]) && length($_[10])) ? 4 + length($_[10]) : 0) +
		($forCursor ? 4 + length($rowid) : 0) +
		($sth->{tdat_mload} ? 14 : 0);
#
#	if we exceeded the max limit, throw error
#
	return $iobj->io_set_error('Request length exceeds 64K limit.')
		if ($reqlen > 64256);

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	my $bufpos = TDAT_HDRSZ;

	my $reqpcl =
		$sth->{tdat_formatted} ? PclFMREQ :
		$sth->{tdat_mload} ? PclREQUEST :
		PclINDICREQ;

	substr($reqmsg, $bufpos, 4 + length($_[8])) =
		pack('SSA*', $reqpcl, 4 + length($_[8]), $_[8]);
	$bufpos += 4 + length($_[8]);

#pragma begin_full
	substr($reqmsg, $bufpos, 14) =
		pack('SSa10', PclMLOADCTRL, 14, $sth->{tdat_mload}),
	$bufpos += 14
		if ($sth->{tdat_mload});
#pragma end_full

	substr($reqmsg, $bufpos, 4 + length($_[9])) =
		pack('SSa*', PclDATAINFO, length($_[9]) + 4, $_[9]),
	$bufpos += (4 + length($_[9]))
		if defined($_[9]) && ($_[9] ne '');

	substr($reqmsg, $bufpos, 4 + length($_[10])) =
		pack('SSa*', $modepcl, length($_[10]) + 4, $_[10]),
	$bufpos += (4 + length($_[10]))
		if defined($_[10]) && ($_[10] ne '');

	substr($reqmsg, $bufpos, 4 + length($rowid)) =
		pack('SSa*', PclCURSORHOST, length($rowid) + 4, $rowid) ,
	$bufpos += (4 + length($rowid))
		if $rowid;

	substr($reqmsg, $bufpos, 6) =
		pack('SSS', (($keepresp) ? PclKEEPRESP : PclRESP), 6,
			($sth->{tdat_mload} ? 4096 : $respsz));

	return $reqmsg;
}

sub finishRequest {
	my ($self, $iobj, $kind, $reqno) = @_;
	my $reqmsg = $iobj->io_buildtdhdr($kind, 4, $reqno);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, 4) =
		pack('SS', ($kind == COPKINDABORT) ? PclABORT : PclCANCEL, 4);
	return $reqmsg;
}

sub rewindRequest {
	my ($self, $iobj, $reqno, $respsz) = @_;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDCONTINUE, 10, $reqno);
	return undef unless $reqmsg;

	$respsz = 64256 if ($respsz > 64256);
	substr($reqmsg, TDAT_HDRSZ, 10) =
		pack('SS SSS',
			PclREWIND,
			4,	# pclsize
			PclKEEPRESP,
			6,	# pclsize
			$respsz);
	return $reqmsg;
}

1;

package DBD::Teradata::APHReqFactory;
#
#	Provides request message processing for Alt. Parcel Header
#	but wo/ large responses
#
use DBD::Teradata qw(:tdat_parcels :tdat_msgkinds :tdat_misc_codes :tdat_impl_codes);
use base('DBD::Teradata::ReqFactory');

use strict;
use warnings;

sub new {
	my $class = shift;
	my $self = {
		noprepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, 'RS'),
		prepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, 'RP'),
#		multinoprepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, 'MS'),
#		multiprepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, 'MP'),
#		multinoprepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "MS\0\0Y"),
#		multiprepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "MP\0\0Y"),
		multinoprepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "MS"),
		multiprepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "MP"),

		execopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, 'RE'),
		indicopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, 'IE'),
		tsrpcl => pack('SSLSC', PclMULTITSR | 0x8000, 0, 11, 1, 1),

		multiexecopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "ME\0\0Y"),
		multiindicopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "ME\0\0Y"),
	};
	return bless $self, $class;
}
#
#	use nase utility packrow, large utility packs not supported until
#	large responses
#
#	build simple SQL request (for internal SQL stmts only)
#
sub simpleRequest {
	my ($self, $iobj, $respsz) = @_;
#
#	this is only used internally for small reqs, so
#	we don't need to handle big V2R5 reqs
#
	my $reqlen = 8 + length($_[3]) + 10;
	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, $reqlen) =
		pack('SSLA* SSLS',
			PclREQUEST | 0x8000,
			0,
			8 + length($_[3]),	# pclsize
			$_[3],	# request
			PclRESP | 0x8000,
			0,
			10,
			$respsz);

	return $reqmsg;
}
#
#	pre-bigresp continue
#
sub continueReq {
	my ($self, $iobj, $reqno, $keepresp, $respsz) = @_;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDCONTINUE, 10, $reqno);
	return undef unless $reqmsg;
	substr($reqmsg, TDAT_HDRSZ, 10) =
		pack('SSLS', ($keepresp ? PclKEEPRESP : PclRESP) | 0x8000, 0, 10, $respsz);
	return $reqmsg;
}

sub prepareRequest {
	my ($self, $iobj, $rowid, $usingvars, $respsz) = @_;
#
#	$dbreq in $_[5]
#	but we don't copy out since dbreq may be very big
#
	my $reqlen = 18 + 8 + length($_[5]) + 10;

	$reqlen += 8 + length($rowid)
		if $rowid;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	my $reqpos = TDAT_HDRSZ;
#
#	only support prepinfox for 5.1+
#
	my ($pcl, $opts) =
		(($iobj->[TD_IMPL_VERSNUM] >= 5010000) && $iobj->[TD_IMPL_UTF8]) ?
			(PclMULTIPARTREQ, ($usingvars ? $self->{multiprepopts} : $self->{multinoprepopts})) :
			(PclREQUEST, ($usingvars ? $self->{prepopts} : $self->{noprepopts}));
	substr($reqmsg, $reqpos, 18) = $opts;

	$reqpos += 18;
	substr($reqmsg, $reqpos, 8+length($_[5])) =
		pack('SSLa*', $pcl | 0x8000, 0, 8 + length($_[5]), $_[5]);
#		pack('SSLa*', PclREQUEST | 0x8000, 0, 8 + length($_[5]), $_[5]);

	$reqpos += 8 + length($_[5]);
	substr($reqmsg, $reqpos, 8 + length($rowid)) =
		pack('SSLA*', PclCURSORHOST | 0x8000, 0, 8 + length($rowid), $rowid),
	$reqpos += 8 + length($rowid)
		if $rowid;
#
#	if we support a bigger respbuf, should we use it ?
#	we'll have to experiment
#
	substr($reqmsg, $reqpos, 10) =
		pack('SSLS', PclRESP | 0x8000, 0, 10, $respsz);
	return $reqmsg;
}

#pragma begin_full
sub aphExportRequest {
	my ($self, $iobj, $respsz) = @_;
	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, 22);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, 22) =
		pack('SSLa* SSLS',
			PclREQUEST | 0x8000, 0, 12, substr($_[3], 0, 8),
			PclRESP | 0x8000, 0, 10, $respsz);
	return $reqmsg;
}

sub loadRequest {
	my ($self, $iobj, $respsz) = @_;
#
#	use buffer as is
#
	substr($iobj->[TD_IMPL_SESBUFF], $iobj->[TD_IMPL_SESBUFPOS], 10,
		pack('SSLS', PclRESP | 0x8000, 0, 10, $respsz));
	$iobj->[TD_IMPL_SESBUFPOS] += 10;
	$iobj->io_buildtdhdr(COPKINDSTART, $iobj->[TD_IMPL_SESBUFPOS] - TDAT_HDRSZ, undef, $iobj->[TD_IMPL_SESBUFF]);
	return $iobj;
}

sub aphMonitorRequest {
	my ($obj, $modepcl, $keepresp, $respsz, $stmt, $indicdata ) = @_;

	my $rsppcl = 0x8000 | ($keepresp ? PclKEEPRESP : PclRESP);

	my $reqlen =
		8 + length($stmt) + (length($indicdata) ? 8 + length($indicdata) : 0) + 10;

	my $reqmsg = $obj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, $reqlen) = ($indicdata ne '') ?
		pack('SSLA* SSLa* SSLS',
			PclINDICREQ | 0x8000,
			0,
			8 + length($stmt),	# pclsize
			$stmt,	# request

			$modepcl | 0x8000,
			0,
			8 + length($indicdata),
			$indicdata,

			$rsppcl,
			0,
			10,
			$respsz)
		: pack('SSLA* SSLS',
			PclINDICREQ | 0x8000,
			0,
			8 + length($stmt),
			$stmt,

			$rsppcl,
			0,
			10,
			$respsz);

	return $reqmsg;
}

sub bulkRequest {
	my ($self, $numparams, $usephs) = @_;
#
#	$buf is $_[3], SQL stmt is $_[4]
#
	my $bufpos = TDAT_HDRSZ;
	substr($_[3], $bufpos, 18 + 8 + length($_[4])) =
		pack('SSLA* SSLA*',
		PclOPTIONS | 0x8000, 0, 18, "IE\0\0Y\0\0\0\0\0",
		PclINDICREQ | 0x8000, 0, 8 + length($_[4]), $_[4]);
	$bufpos += 18 + 8 + length($_[4]);

	return ($bufpos, undef)
		unless $usephs;

	my $dipos = $bufpos + 8; # point to body of parcel
	my $dilen = 8 + ($numparams * 4) + 2;
	substr($_[3], $bufpos, $dilen) =
		pack('SSLS', PclDATAINFO | 0x8000, 0, $dilen, $numparams);

	return ($bufpos + $dilen, $dipos);
}

sub closeBulkRequest {
	my ($self, $pos, $respsz) = @_;
#
#	$buf is $_[3]
#
	substr($_[3], $pos, 10) = pack('SSLS', PclRESP | 0x8000, 0, 10, $respsz);
	$pos += 10;
	return $pos;
}
#pragma end_full

sub sqlRequest {
	my ($self, $iobj, $sth, $forCursor, $rowid, $modepcl, $keepresp, $respsz) = @_;
#
#	SQL stmt is $_[8], $datainfo is $_[9], $indicdata is $_[10]
#
	my $reqlen = 8 + length($_[8]) + 10 +
		((defined($_[9]) && length($_[9])) ? 8 + length($_[9]) : 0) +
		((defined($_[10]) && length($_[10])) ? 8 + length($_[10]) : 0) +
		($forCursor ? 8 + length($rowid) : 0) +
		($sth->{tdat_mload} ? 18 : 0);
#
#	DISABLE FOR NOW
#
#	add room for options
#
#	$reqlen += 18
#		if (!$sth->{tdat_mload}) && $iobj->[TD_IMPL_UTF8] &&
#			($iobj->[TD_IMPL_VERSNUM] >= 5010000);

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	my $bufpos = TDAT_HDRSZ;

	my $reqpcl =
		$sth->{tdat_formatted} ? PclFMREQ :
		$sth->{tdat_mload} ? PclREQUEST :
		PclINDICREQ;

#
#	DISABLE FOR NOW
#
#	if ((!$sth->{tdat_mload}) && $iobj->[TD_IMPL_UTF8] && ($iobj->[TD_IMPL_VERSNUM] >= 5010000)) {
#		substr($reqmsg, $bufpos, 18) = $self->{multiindicopts};
#		$bufpos += 18;
#	}

	substr($reqmsg, $bufpos, 8 + length($_[8])) =
		pack('SSLA*', $reqpcl | 0x8000, 0, 8 + length($_[8]), $_[8]);
	$bufpos += 8 + length($_[8]);

#pragma begin_full
	substr($reqmsg, $bufpos, 18) =
		pack('SSLa10', PclMLOADCTRL | 0x8000, 0, 18, $sth->{tdat_mload}),
	$bufpos += 18
		if $sth->{tdat_mload};
#pragma end_full

	substr($reqmsg, $bufpos, 8 + length($_[9])) =
		pack('SSLa*', PclDATAINFO | 0x8000, 0, length($_[9]) + 8, $_[9]),
	$bufpos += (8 + length($_[9]))
		if defined($_[9]) && ($_[9] ne '');

	substr($reqmsg, $bufpos, 8 + length($_[10])) =
		pack('SSLa*', $modepcl | 0x8000, 0, length($_[10]) + 8, $_[10]),
	$bufpos += (8 + length($_[10]))
		if defined($_[10]) && ($_[10] ne '');

	substr($reqmsg, $bufpos, 8 + length($rowid)) =
		pack('SSLa*', PclCURSORHOST | 0x8000, 0, length($rowid) + 8, $rowid) ,
	$bufpos += (8 + length($rowid))
		if $rowid;

	substr($reqmsg, $bufpos, 10) =
		pack('SSLS', (($keepresp) ? PclKEEPRESP : PclRESP) | 0x8000, 0, 10,
		($sth->{tdat_mload} ? 4096 : $respsz));

	return $reqmsg;
}

sub finishRequest {
	my ($self, $iobj, $kind, $reqno) = @_;
	my $reqmsg = $iobj->io_buildtdhdr($kind, 8, $reqno);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, 8) =
		pack('SSL', 0x8000 | (($kind == COPKINDABORT) ? PclABORT : PclCANCEL), 0, 8);
	return $reqmsg;
}

sub rewindRequest {
	my ($self, $iobj, $reqno, $respsz) = @_;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDCONTINUE, 18, $reqno);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, 18) =
		pack('SSL SSLS',
			PclREWIND | 0x8000,
			0,
			8,
			PclKEEPRESP | 0x8000,
			0,
			10,
			$respsz);
	return $reqmsg;
}

1;

package DBD::Teradata::BigAPHReqFactory;
#
#	same as APHReqFactory, but with big response support
#
use DBD::Teradata qw(:tdat_parcels :tdat_msgkinds :tdat_misc_codes :tdat_impl_codes);
use base('DBD::Teradata::APHReqFactory');

use strict;
use warnings;

#
#	use base constructor
#
#pragma begin_full
#	utility's only support large request *if* it supports large resp
#
sub mloadPackRow {
	my ($self, $pos, $len, $modepcl, $seqno, $job) = @_;
#
#	$_[6] is now the target buffer, $_[7] is src buffer
#
	substr($_[6], $pos, $len+18) =
		pack('SSLLCCCCCCa*', $modepcl | 0x8000, 0, $len+18, $seqno, 1, $job+1, 1,
			$job+1, 0, 0, substr($_[7], 0, $len));
	return $pos + (8 + $len + 10);
}

sub floadPackRow {
	my ($self, $pos, $len, $modepcl) = @_;
#
#	$_[4] is now the target buffer, $_[5] is src buffer
#
	substr($_[4], $pos, $len+8) =
		pack('SSLa*', $modepcl | 0x8000, 0, $len+8, substr($_[5], 0, $len));
	return $pos + (8 + $len);
}
#pragma end_full
#
#	use base simple request, we don't need big responses for this
#
sub continueReq {
	my ($self, $iobj, $reqno, $keepresp, $respsz) = @_;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDCONTINUE, 12, $reqno);
	return undef unless $reqmsg;
	substr($reqmsg, TDAT_HDRSZ, 12) =
		pack('SSLL', ($keepresp ? PclBIGKEEPRESP : PclBIGRESP) | 0x8000, 0, 12, $respsz);
	return $reqmsg;
}

sub prepareRequest {
	my ($self, $iobj, $rowid, $usingvars, $respsz) = @_;
#
#	$dbreq in $_[5]
#	but we don't copy out since dbreq may be very big
#
	my $reqlen = 18 + 8 + length($_[5]) + 12;

	$reqlen += 8 + length($rowid)
		if $rowid;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	my $reqpos = TDAT_HDRSZ;

#
#	only support prepinfox for 5.1+
#
	my ($pcl, $opts) =
		(($iobj->[TD_IMPL_VERSNUM] >= 5010000) && $iobj->[TD_IMPL_UTF8]) ?
			(PclMULTIPARTREQ, ($usingvars ? $self->{multiprepopts} : $self->{multinoprepopts})) :
			(PclREQUEST, ($usingvars ? $self->{prepopts} : $self->{noprepopts}));
	substr($reqmsg, $reqpos, 18) = $opts;

	$reqpos += 18;
	substr($reqmsg, $reqpos, 8+length($_[5])) =
		pack('SSLa*', $pcl | 0x8000, 0, 8 + length($_[5]), $_[5]);
#		pack('SSLa*', PclREQUEST | 0x8000, 0, 8 + length($_[5]), $_[5]);

	$reqpos += 8 + length($_[5]);
	substr($reqmsg, $reqpos, 8 + length($rowid)) =
		pack('SSLA*', PclCURSORHOST | 0x8000, 0, 8 + length($rowid), $rowid),
	$reqpos += 8 + length($rowid)
		if $rowid;
#
#	if we support a bigger respbuf, should we use it ?
#	we'll have to experiment
#
	substr($reqmsg, $reqpos, 12) =
		pack('SSLL', PclBIGRESP | 0x8000, 0, 12, $respsz);
	return $reqmsg;
}
#pragma begin_full

sub bigExportRequest {
	my ($self, $iobj, $respsz) = @_;
	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, 24);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, 24) =
		pack('SSLa* SSLL',
			PclREQUEST | 0x8000, 0, 12, substr($_[3], 0, 8),
			PclBIGRESP | 0x8000, 0, 12, $respsz);
	return $reqmsg;
}
#
#	we may need a special bigreq to keep CLI happy
#
sub loadRequest {
	my ($self, $iobj, $respsz) = @_;
#
#	use buffer as is
#
	substr($iobj->[TD_IMPL_SESBUFF], $iobj->[TD_IMPL_SESBUFPOS], 12,
		pack('SSLL', PclBIGRESP | 0x8000, 0, 12, $respsz));
	$iobj->[TD_IMPL_SESBUFPOS] += 12;
	$iobj->io_buildtdhdr(COPKINDSTART, $iobj->[TD_IMPL_SESBUFPOS] - TDAT_HDRSZ, undef, $iobj->[TD_IMPL_SESBUFF]);
	return $iobj;
}

sub bigMonitorRequest {
	my ($self, $iobj, $modepcl, $keepresp, $respsz, $stmt, $indicdata ) = @_;

	my $rsppcl = 0x8000 | ($keepresp ? PclBIGKEEPRESP : PclBIGRESP);

	my $reqlen =
		8 + length($stmt) + (length($indicdata) ? 8 + length($indicdata) : 0) + 12;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, $reqlen) = ($indicdata ne '') ?
		pack('SSLA* SSLa* SSLL',
			PclINDICREQ | 0x8000,
			0,
			8 + length($stmt),	# pclsize
			$stmt,	# request

			$modepcl | 0x8000,
			0,
			8 + length($indicdata),
			$indicdata,

			$rsppcl,
			0,
			12,
			$respsz)
		: pack('SSLA* SSLL',
			PclINDICREQ | 0x8000,
			0,
			8 + length($stmt),
			$stmt,

			$rsppcl,
			0,
			12,
			$respsz);

	return $reqmsg;
}

sub closeBulkRequest {
	my ($self, $pos, $respsz) = @_;
#
#	$buf is $_[3]
#
	substr($_[3], $pos, 12) = pack('SSLL', PclBIGRESP | 0x8000, 0, 12, $respsz);
	$pos += 12;
	return $pos;
}
#pragma end_full

sub sqlRequest {
	my ($self, $iobj, $sth, $forCursor, $rowid, $modepcl, $keepresp, $respsz) = @_;
#
#	SQL stmt is $_[8], $datainfo is $_[9], $indicdata is $_[10]
#
	my $reqlen = 8 + length($_[8]) + 12 +
		((defined($_[9]) && length($_[9])) ? 8 + length($_[9]) : 0) +
		((defined($_[10]) && length($_[10])) ? 8 + length($_[10]) : 0) +
		($forCursor ? 8 + length($rowid) : 0) +
		($sth->{tdat_mload} ? 18 : 0);
#
#	add room for options for newer releases
#
	$reqlen += 18
		if (!$sth->{tdat_mload}) && $iobj->[TD_IMPL_UTF8] &&
			($iobj->[TD_IMPL_VERSNUM] >= 6020000);

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDSTART, $reqlen);
	return undef unless $reqmsg;

	my $bufpos = TDAT_HDRSZ;

	my $reqpcl =
		$sth->{tdat_formatted} ? PclFMREQ :
		$sth->{tdat_mload} ? PclREQUEST :
		PclINDICREQ;

	substr($reqmsg, $bufpos, 18) = $self->{multiindicopts},
	$bufpos += 18
		if (!$sth->{tdat_mload}) && 
			$iobj->[TD_IMPL_UTF8] && 
			($iobj->[TD_IMPL_VERSNUM] >= 6020000);

	substr($reqmsg, $bufpos, 8 + length($_[8])) =
		pack('SSLA*', $reqpcl | 0x8000, 0, 8 + length($_[8]), $_[8]);
	$bufpos += 8 + length($_[8]);

#pragma begin_full
	substr($reqmsg, $bufpos, 18) =
		pack('SSLa10', PclMLOADCTRL | 0x8000, 0, 18, $sth->{tdat_mload}),
	$bufpos += 18
		if $sth->{tdat_mload};
#pragma end_full

	substr($reqmsg, $bufpos, 8 + length($_[9])) =
		pack('SSLa*', PclDATAINFO | 0x8000, 0, length($_[9]) + 8, $_[9]),
	$bufpos += (8 + length($_[9]))
		if defined($_[9]) && ($_[9] ne '');

	substr($reqmsg, $bufpos, 8 + length($_[10])) =
		pack('SSLa*', $modepcl | 0x8000, 0, length($_[10]) + 8, $_[10]),
	$bufpos += (8 + length($_[10]))
		if defined($_[10]) && ($_[10] ne '');

	substr($reqmsg, $bufpos, 8 + length($rowid)) =
		pack('SSLa*', PclCURSORHOST | 0x8000, 0, length($rowid) + 8, $rowid) ,
	$bufpos += (8 + length($rowid))
		if $rowid;

	substr($reqmsg, $bufpos, 12) =
		pack('SSLL', (($keepresp) ? PclBIGKEEPRESP : PclBIGRESP) | 0x8000, 0, 12,
		($sth->{tdat_mload} ? 4096 : $respsz));

	return $reqmsg;
}

#
#	use base finishRequest
#
sub rewindRequest {
	my ($self, $iobj, $reqno, $respsz) = @_;

	my $reqmsg = $iobj->io_buildtdhdr(COPKINDCONTINUE, 20, $reqno);
	return undef unless $reqmsg;

	substr($reqmsg, TDAT_HDRSZ, 20) =
		pack('SSL SSLL',
			PclREWIND | 0x8000,
			0,
			8,
			PclBIGKEEPRESP | 0x8000,
			0,
			12,
			$respsz);
	return $reqmsg;
}

1;

package DBD::Teradata::TW12ReqFactory;
#
#	same as BigAPHReqFactory, but supporting DEC(38)
#
use DBD::Teradata qw(:tdat_parcels :tdat_msgkinds :tdat_misc_codes :tdat_impl_codes);
use base('DBD::Teradata::BigAPHReqFactory');

use strict;
use warnings;

sub new {
	my ($class, $decprec) = @_;
#
#	need to include Identity column retrieval value
#	need to include Dynamic results value
#	need to include SP-ReturnResult
#
	$decprec = chr($decprec);
	my $self = {
		noprepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "RS\0\0\0\0\0$decprec"),
		prepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "RP\0\0\0\0\0$decprec"),
		multinoprepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "MS\0\0\0\0\0$decprec"),
		multiprepopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "MP\0\0\0\0\0$decprec"),

		execopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "RE\0\0\0\0\0$decprec"),
		indicopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "IE\0\0\0\0\0$decprec"),
		tsrpcl => pack('SSLSC', PclMULTITSR | 0x8000, 0, 11, 1, 1),

		multiexecopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "ME\0\0Y\0\0$decprec"),
		multiindicopts => pack('SSLa10', PclOPTIONS | 0x8000, 0, 18, "ME\0\0Y\0\0$decprec"),
		
		decprec => $decprec,
	};
	return bless $self, $class;
}

sub bulkRequest {
	my ($self, $numparams, $usephs) = @_;
#
#	$buf is $_[3], SQL stmt is $_[4]
#
	my $bufpos = TDAT_HDRSZ;
	substr($_[3], $bufpos, 18 + 8 + length($_[4])) =
		pack('SSLa10 SSLA*',
		PclOPTIONS | 0x8000, 0, 18, "IE\0\0Y\0\0" . $self->{decprec},
		PclINDICREQ | 0x8000, 0, 8 + length($_[4]), $_[4]);
	$bufpos += 18 + 8 + length($_[4]);

	return ($bufpos, undef)
		unless $usephs;

	my $dipos = $bufpos + 8; # point to body of parcel
	my $dilen = 8 + ($numparams * 4) + 2;
	substr($_[3], $bufpos, $dilen) =
		pack('SSLS', PclDATAINFO | 0x8000, 0, $dilen, $numparams);

	return ($bufpos + $dilen, $dipos);
}

1;

package DBD::Teradata::impl;

use IO::Socket;

use Socket;
use DBI qw(:sql_types);
use Time::HiRes qw(time sleep);
#use DBD::Teradata::Crypt;
#
#	I/O attributes:
#	No base class
#	Public:
#		_lasterr :	errorcode from last operation
#		_lastemsg :	error msg from last operation
#		_laststate : SQLSTATE from last operation
#		_sesinxact : 1 => session in transaction
#		_sesbuff : current FLOAD/MLOAD output buffer
#		_sesbufpos : current pos in FLOAD/MLOAD output buffer
#
#	Private:
#		_sessno : DBMS session number
#		_curreq : next reqno
#		_sesauth : authenticator
#		_sesauthx : add'l authenticator
#		_seslsn : session LSN
#		_sesrspsz : max size we tell DBMS
#		_active : current session state
#				0 => idle (no outstanding request)
#				>0 => active (current request in progress)
#		_connfd : socket fd for the session
#		_req2sth : hash of reqno's to sth's
#		_twobuf : 0 => no auto-continue
#
#	define lots of symbols for our various protocol constants
#
#pragma begin_clear_text

use DBD::Teradata qw(
	:tdat_charsets
	:tdat_msgkinds
	:tdat_platforms
	:tdat_typecodes
	:tdat_parcels
	:tdat_impl_codes
	:tdat_sth_codes
	:tdat_misc_codes
	:tdat_part_codes
	%td_type_code2str
	%td_type_str2baseprec
	%td_type_str2basescale
	%td_lob_scale
	@td_decszs
	%td_type_str2dbi
	%td_type_str2size
	%td_type_str2pack
	%td_type_str2stringtypes
	%td_type_str2binarytypes
	%td_type_dbi2stringtypes
	%td_type_dbi2str
	%td_activity_types
	%td_sqlstates
	@td_indicbits
	@td_indicmasks
	%td_type_dbi2preconly
	%td_type_dbi2hasprec
	%td_type_dbi2hasscale
	%td_type_dbi2pack
	%td_type_code2dbi
	%td_type_dbi2code
	%td_part_map
);

#pragma end_clear_text

use strict;
use warnings;

#
#	ClientConfig parcels:
#
#pragma begin_redact
use constant TD_PCLS_CLIENTCFG => 0;
use constant TD_PCLS_CONFIG => 1;
use constant TD_PCLS_ASSIGN => 2;
use constant TD_PCLS_TD2SSOREQ => 3;
#pragma end_redact

our @negotiate_pcls = (
#
#	little endian first
#
	[
		"\246\0\020\0\1\0\0\0\2\0\4\0\6\0\0\1", 			# CLIENTCFG
		"\052\0\4\0",										# CONFIG
		"\144\0\044\0" . (' ' x 32),						# ASSIGN
		"\204\0\167\0"										# TD2SSOREQ
			. "\0\0\157\0"	# authtype, trip, and content length
			. "\1\1\1\0\0\0\0\100\0\0\0\0\0\0\0\0\6\1\0\16"	# some stuff ???
			. ("\0" x 60)									# pad
			. "\6\015\053\6\1\4\1\201\077\1\207\164\1\1\11" # TD2OID
			. "\106\10\0\2\201\0\4\4\4\0\1\0\0\0\037\1",	# params and other stuff
	],
#
#	then big endian
#
	[
        "\0\246\0\020\0\0\0\1\0\2\0\4\6\0\0\1",				# CLIENTCFG
        "\0\052\0\4",										# CONFIG
        "\0\144\0\044" . (' ' x 32),						# ASSIGN
		"\0\204\0\167"										# TD2SSOREQ
			. "\0\0\0\157"	# authtype, trip, and content length
			. "\1\1\1\0\0\0\0\100\0\0\0\0\0\0\0\0\6\1\0\16"	# some stuff ???
			. ("\0" x 60)									# pad
			. "\6\015\053\6\1\4\1\201\077\1\207\164\1\1\11" # TD2OID
			. "\106\10\0\2\201\0\4\4\4\0\1\0\0\0\037\1",	# params and other stuff
	]
);

our %authoids = (
"\053\6\1\4\1\201\077\1\207\164\1\1\11", 'TD2',
#"\053\6\1\4\1\201\077\1\207\164\1\1\10", 'TD1',
#"\053\6\1\4\1\201\077\1\207\164\1\1\4", 'NTLM',
#"\053\6\1\4\1\201\077\1\207\164\1\24", 'LDAP',
# "1.2.840.113554.1.2.2", 'KRB5', 	need to convert this
);

#
#	abbreviated form of some stmts
our %stmt_abbrv = (
'INS', 'Insert',
'UPD','Update',
'DEL','Delete',
'CT','Create Table',
'CV','Create View',
'CD','Create Database'
);
#
#	column charset names
#
our @colcharsets = qw(
ASCII
LATIN
UNICODE
KANJISJIS
GRAPHIC
KANJI1
);
#
#	mask to remove the 'nullable' bit
#

our %rawmodes = ( 'RecordMode', PclDATA, 'IndicatorMode', PclINDICDATA);

our $req_template = "\3\1" . ("\0" x 50);

#my $shazbat = 100581;
#my $mork = time();
my %hostcache = ();
my $has_cli;

#
#	make this thread safe so only first thread actually effects
#	the global operating params
#
sub new {
	my ($class, $host, $port, $user, $auth, $attrs, $use_cli) = @_;
#  $$err = -1, $$state = 'S1000', $$errstr = 'Evaluation period has expired.',
#  return undef if ($shazbat < int($mork/10000));
	my $obj = [ ];
    bless $obj, $class;

	$obj->io_init;
#pragma begin_full
	$use_cli = ($use_cli && $has_cli);
#pragma end_full
	$obj->[TD_IMPL_CHARSET] = $hostchars;

	$obj->[TD_IMPL_SESRSPSZ] = $attrs->{tdat_respsize};
	$obj->[TD_IMPL_SESREQSZ] = $attrs->{tdat_reqsize};

	return (undef,
		$obj->[TD_IMPL_LASTERR],
		$obj->[TD_IMPL_LASTEMSG],
		$obj->[TD_IMPL_LASTSTATE])
		unless $obj->io_connect($host, $port, $user, $auth, $attrs, $use_cli);

    return ($obj, 0, '', '');
}
#
#	private method for handling parcle headers
#
sub _getPclHeader {
#
#	buffer and position are in $_[0] and $_[1] now
#
	my ($tdflavor, $tdlen) = unpack('SS', substr($_[0], $_[1], 4));
	return ($tdflavor & 0x8000) ?
		($tdflavor & 0x00FF, unpack('L', substr($_[0], $_[1]+4, 4)), 8) :
		($tdflavor, $tdlen, 4);
}

sub io_init {
	my $obj = shift;

	$obj->[TD_IMPL_SESSNO] = 0;		# DBMS session number
	$obj->[TD_IMPL_CURREQ] = 0;		# current reqno
	$obj->[TD_IMPL_SESAUTH] = 0;		# authenticator
	$obj->[TD_IMPL_SESAUTHX] = 0;		# add'l authenticator
	$obj->[TD_IMPL_SESLSN] = undef;	# session LSN
	$obj->[TD_IMPL_SESRSPSZ] = undef;	# max size we tell DBMS
	$obj->[TD_IMPL_ACTIVE] = 0;		# current session state
#				0 => idle (no outstanding request)
#				>0 => reqno of active request
	$obj->[TD_IMPL_CONNFD] = undef;	# socket fd for the session
	$obj->[TD_IMPL_SESBUFF] = undef;	# current output buffer
	$obj->[TD_IMPL_SESBUFPOS] = undef;	# current position in output buffer
	$obj->[TD_IMPL_TWOBUF] = 1;	# default to auto-continue until we get a cursor
							# operation
	$obj->[TD_IMPL_REQ2STH] = { };	# start with empty reqno-to-sth map

	unless ($inited) {

		$debug = $ENV{TDAT_DBD_DEBUG} || 0;
		if ($debug) {
			eval {
				require DBD::Teradata::Diagnostic;
				import DBD::Teradata::Diagnostic qw(io_hexdump io_hdrdump io_pcldump);
			};
		}

		DBI->trace_msg(
"DBD::Teradata init: platform = $platform, debug = $debug,
charset = $hostchars, ph size = $phdfltsz, response buf size = $maxbufsz\n", 1);
#
#	update template request header
#
		substr($req_template, 5, 1) = pack('C', $platform);
		substr($req_template, 37, 1) = pack('C', $hostchars);
#
#	check if we have CLI available
#
		eval {
			require DBD::Teradata::Cli;
	 		$DBD::Teradata::Cli::debug = 1
				if $debug;
		};
		$has_cli = $@ ? undef : ($DBD::Teradata::Cli::VERSION >= 12.001);
		$inited = 1;
	}
#
#	seed our rand() for cop selection
#
	srand(time);
	return 1;
}

sub io_get_error {
	return ($_[0][TD_IMPL_LASTERR], $_[0][TD_IMPL_LASTEMSG], $_[0][TD_IMPL_LASTSTATE]);
}

sub io_set_error {
	my $obj = shift;

	($obj->[TD_IMPL_LASTERR], $obj->[TD_IMPL_LASTEMSG], $obj->[TD_IMPL_LASTSTATE]) =
		($#_ == 2) ? @_ :
		($#_ == 1) ? (@_,
			(($_[0] > 999) ?	# filter out driver or CLI errors
				(exists $td_sqlstates{$_[0]} ? $td_sqlstates{$_[0]} : "T$_[0]") : 'S1000')) :
		(-1, $_[0], 'S1000');
	return undef;
}

#
#	general purpose function for building msg headers
#
sub io_buildtdhdr {	# kind, len
	my ($obj, $kind, $len, $reqno) = @_;
#
#	we *may* have a buffer in $_[4]!!!
#
#	trap large requests here if we're not up to V2R5
#
	return $obj->io_set_error('Large requests not supported prior to V2R5.0')
		if (($kind == COPKINDSTART) || ($kind == COPKINDCONTINUE)) &&
			($len > 65535) && ($obj->[TD_IMPL_VERSNUM] < 5000000);

	my $charset = ($kind == COPKINDASSIGN) ? 0 : $obj->[TD_IMPL_CHARSET];
#
#	hmmm, looks like hostbyte can be left in the header ?
#
	my $hostbyte = ($kind == COPKINDTEST) ? COPECHOTEST : $platform;

	$obj->[TD_IMPL_CURREQ] = 0 if ($kind == COPKINDCONNECT);
	$obj->[TD_IMPL_CURREQ]++ if ($kind == COPKINDSTART) || ($kind == COPKINDLOGOFF);
	my $reqmsg;

	if ($_[4]) {
		$obj->io_init_reqbuf($reqno, $len, $kind, $hostbyte, $charset, $_[4]);
	}
	else {
		$len += TDAT_HDRSZ;

		$reqmsg = pack("a$len", '');
		$len -= TDAT_HDRSZ;
		$obj->io_init_reqbuf($reqno, $len, $kind, $hostbyte, $charset, $reqmsg);
	}

	return $reqmsg || 1
		if ($kind == COPKINDCONFIG) || ($kind == COPKINDTEST);

	if ($obj->[TD_IMPL_SESAUTH] == 0xffffffff) {
		$obj->[TD_IMPL_SESAUTHX]++;
		$obj->[TD_IMPL_SESAUTH] = 0;
	}
	else { $obj->[TD_IMPL_SESAUTH]++; }

	return $reqmsg || 1;
}

sub io_init_reqbuf {
	my ($obj, $reqno, $len, $kind, $hostbyte, $charset) = @_;
#
#	buffer is $_[6]
#
	substr($_[6], 0, TDAT_HDRSZ) = $req_template;
	substr($_[6], 2, 1) = pack('C', $kind);
#
#	new for v2r5: if its a big msg, include the 64K segment count
#
	if ($len > 65535) {
		substr($_[6], 4, 1) = pack('C', ($len >> 16) & 255);
		substr($_[6], 8, 2) = pack('n', $len & 65535);
	}
	else {
		substr($_[6], 8, 2) = pack('n', $len);
	}
	substr($_[6], 5, 1) = pack('C', $hostbyte);
#
#	 CONFIG is simple
#
	substr($_[6], 27, 11) = pack('CL N C C', 0, 1, 0, 0, 0),
	return 1
		if ($kind == COPKINDCONFIG);

	substr($_[6], 20, 4) = pack('N', $obj->[TD_IMPL_SESSNO]);
	substr($_[6], 27, 11) = pack('CN N C C',
		$obj->[TD_IMPL_SESAUTHX], $obj->[TD_IMPL_SESAUTH],
		((($kind == COPKINDCONTINUE) || ($kind == COPKINDABORT)) ? $reqno : $obj->[TD_IMPL_CURREQ]),
		0, $charset);
	return 1;
}

sub io_clisend {
	my $obj = shift;
#
#	buffer is in $_[4]
#
	my ($reqid, $err, $errstr) =
		$obj->[TD_IMPL_CONNFD]->cli_send_request(@_, $obj->[TD_IMPL_SESRSPSZ]);

	DBI->trace_msg("Can't send: $!\n", 1),
	$obj->[TD_IMPL_CONNFD]->cli_disconnect,
	return $obj->io_set_error($err, $errstr, '08C01')
		if $err;
#
#	set active request number
#
	$obj->[TD_IMPL_ACTIVE] = $reqid;

	return $_[0];
}

sub io_set_respsz {
	my $max = (($_[0][TD_IMPL_SESPART] == TD_PART_SQL) && ($_[0][TD_IMPL_VERSNUM] >= 6000000)) ?
		2097152 : 64256;
	$_[0][TD_IMPL_SESRSPSZ] = $_[1]
		if ($_[1]=~/^\d+$/) && ($_[1] >= 64000) && ($_[1] < $max);
	return $_[0][TD_IMPL_SESRSPSZ];
}

sub io_set_reqsz {
	my $max = (($_[0][TD_IMPL_VERSNUM] >= 6000000) ||
		(($_[0][TD_IMPL_SESPART] == TD_PART_SQL) && ($_[0][TD_IMPL_VERSNUM] >= 5000000))) ?
		2097152 : 64256;
	$_[0][TD_IMPL_SESREQSZ] = $_[1]
		if ($_[1]=~/^\d+$/) && ($_[1] >= 256) && ($_[1] < $max);
	return $_[0][TD_IMPL_SESREQSZ];
}

sub io_update_version {
	my ($obj, $versnum) = @_;
	$obj->[TD_IMPL_REQFAC] =
		($versnum < 5000000) ? DBD::Teradata::ReqFactory->new() :
		($versnum < 6010000) ? DBD::Teradata::APHReqFactory->new() :
#		DBD::Teradata::BigAPHReqFactory->new()
		($versnum < 6020000) ? DBD::Teradata::BigAPHReqFactory->new() :
		DBD::Teradata::TW12ReqFactory->new($obj->[TD_IMPL_DECPREC])
		if ($obj->[TD_IMPL_SESPART] == TD_PART_SQL);
	$obj->[TD_IMPL_VERSNUM] = $versnum;
	return $obj if ($versnum >= 6000000);
#
#	force buffer sizes down
#
	$obj->[TD_IMPL_SESREQSZ] = 64256
		if ($obj->[TD_IMPL_SESREQSZ] > 64256);
	$obj->[TD_IMPL_SESRSPSZ] = 64256
		if ($obj->[TD_IMPL_SESRSPSZ] > 64256);
}

sub io_tdsend {
	my ($obj, $msg, $encrypt, $keepresp) = @_;

	my $n = 0;

	if ($debug) {
		DBI->trace_msg("Sending request\n", 2);
#pragma begin_full
		DBI->trace_msg("<*** Using CLI ***>\n")
			if (ref $obj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli');
#pragma end_full
#
#	note we still build full lan headers; this will
#	will give us extra diagnostic info, even tho its
#	not used
#
		DBI->trace_msg(io_hdrdump('Request msg header',
			substr($msg, 0, TDAT_HDRSZ)), 1);
		DBI->trace_msg(io_pcldump(
			substr($msg, TDAT_HDRSZ), length($msg) - TDAT_HDRSZ), 1);
	}
#
#	check if encrypting
#
	if ($obj->[TD_IMPL_CRYPT] && $encrypt) {
		$msg = $obj->[TD_IMPL_CRYPT]->encrypt_request($obj, $msg);
		DBI->trace_msg(io_hdrdump('(Encrypted) Request msg header',
			substr($msg, 0, TDAT_HDRSZ)), 1),
		DBI->trace_msg(io_pcldump(
			substr($msg, TDAT_HDRSZ), length($msg) - TDAT_HDRSZ, 1), 1)
			if $debug;
	}
#
#	check if in active state and not aborting; if so, wait for response,
#	append to assoc. sth's buffer,
#	and revert to no-auto-continue
#
	my $kind = unpack('C', substr($msg, 2, 1));
	if (($kind != COPKINDABORT) && ($kind != COPKINDTEST) && $obj->[TD_IMPL_ACTIVE]) {
		my $rspmsg = '';
		return undef unless $obj->io_gettdresp;
		$obj->[TD_IMPL_TWOBUF] = 0;	# suspend auto-continues
	}

	return $obj->io_clisend(length($msg), $keepresp, $msg)
#pragma begin_full
		if (ref $obj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli')
#pragma end_full
		;

#pragma begin_full
#
#	need to check for reconnect support here
#
	$n = send($obj->[TD_IMPL_CONNFD], $msg, 0);
	DBI->trace_msg("Can't send: $!\n", 1),
	close($obj->[TD_IMPL_CONNFD]),
	return $obj->io_set_error(-1, 'System error: unable to send', '08C01')
		unless defined($n);

	DBI->trace_msg("Only sent $n bytes!\n", 1),
	close($obj->[TD_IMPL_CONNFD]),
	return $obj->io_set_error(-1, 'System error: unable to send', '08C01')
		if ($n != length($msg));

	DBI->trace_msg("Request sent\n", 2) if $debug;
	$obj->[TD_IMPL_ACTIVE] = unpack('N', substr($msg, TDAT_HDRSZ-20, 4));

	return length($msg);
#pragma end_full
}
#
#	a send that minimizes copying
#
sub io_quicksend {
	my $obj = shift;
	my ($len, $keepresp) = @_;
#
#	msgbuf in $_[2]
#
	my $n = 0;

	if ($debug) {
		DBI->trace_msg("Sending request\n", 2);
#pragma begin_full
		DBI->trace_msg("<*** Using CLI ***>\n")
			if (ref $obj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli');
#pragma end_full
#
#	note we still build full lan headers; this will
#	will give us extra diagnostic info, even tho its
#	not used
#
		DBI->trace_msg(io_hdrdump('Request msg header', substr($_[2], 0, TDAT_HDRSZ)), 1);
		DBI->trace_msg(io_pcldump(substr($_[2], TDAT_HDRSZ), $len - TDAT_HDRSZ), 1);
	}
#
#	check if in active state; if so, wait for response,
#	append to assoc. sth's buffer, and revert to no-auto-continue
#
	if ($obj->[TD_IMPL_ACTIVE]) {
		return undef unless $obj->io_gettdresp;
		$obj->[TD_IMPL_TWOBUF] = 0;	# suspend auto-continues
	}

	return $obj->io_clisend(@_)
#pragma begin_full
		if (ref $obj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli')
#pragma end_full
		;
#pragma begin_full
#
#	need to check for reconnect support here
#
	$n = send($obj->[TD_IMPL_CONNFD], substr($_[2], 0, $len), 0);
	DBI->trace_msg("Can't send: $!\n", 1),
	close($obj->[TD_IMPL_CONNFD]),
	return $obj->io_set_error(-1, 'System error: unable to send', '08C01')
		unless defined($n);

	DBI->trace_msg("Only sent $n bytes!\n", 1),
	close($obj->[TD_IMPL_CONNFD]),
	return $obj->io_set_error(-1, 'System error: unable to send', '08C01')
		if ($n != $len);

	DBI->trace_msg("Request sent\n", 2) if $debug;
	$obj->[TD_IMPL_ACTIVE] = unpack('N', substr($_[2], TDAT_HDRSZ-20, 4));

	return $len;
#pragma end_full
}

#
#	new function to handle response from CLI
#
sub io_getcliresp { #	returns buflen
	my ($obj, $sth) = @_;
	my $hdr = '';

	my $reqno = $obj->[TD_IMPL_ACTIVE];

	my ($err, $errstr) =
		$obj->[TD_IMPL_CONNFD]->cli_get_response(\$hdr, $reqno,
			(defined($sth) && $sth->{tdat_keepresp}), -1);

	DBI->trace_msg($errstr),
	$obj->[TD_IMPL_CONNFD]->cli_disconnect,
	return $obj->io_set_error($err, $errstr, '08S01')
		if $err;

	DBI->trace_msg("<*** CLI Response for Request $reqno ***>\n"),
	DBI->trace_msg(io_pcldump(
		substr($hdr, TDAT_HDRSZ), length($hdr) - TDAT_HDRSZ), 1)
		if $debug;

	$obj->[TD_IMPL_ACTIVE] = 0;

	return $obj->io_scan_response(\$hdr, $reqno);
}

sub io_gettdresp { #	returns buflen
	my ($obj, $sth) = @_;

	return $obj->io_getcliresp($sth)
#pragma begin_full
		if (ref $obj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli')
#pragma end_full
		;
#pragma begin_full
	my $rspmsg = '';
	my $hdrlen = 0;
	my $rsplen = 8192;
	my $hdr = '';
#
#	get fixed part first
#
	DBI->trace_msg("Rcving Header\n", 2) if $debug;

	while ($hdrlen < TDAT_HDRSZ) {
		unless (recv($obj->[TD_IMPL_CONNFD], $rspmsg, $rsplen, 0)) {
			DBI->trace_msg(
"System error: can\'t recv msg header $!;\n rcvd $hdrlen bytes.\n", 2),
			close($obj->[TD_IMPL_CONNFD]),
			return $obj->io_set_error(-1,
				"System error: can\'t recv msg header $!; closing connection.", '08S01')
				unless defined($rspmsg) && ($rspmsg ne '');
		}
		$hdr .= $rspmsg;
		$hdrlen += length($rspmsg);
	}
	DBI->trace_msg(io_hdrdump('Response msg header',
		substr($hdr, 0, TDAT_HDRSZ)), 1) if $debug;
	my ($tdver, $tdclass, $tdkind, $tdenc, $tdchksum, $tdbytevar,
		$tdwordvar, $tdmsglen) = unpack('C6Sn', $hdr);

	$tdmsglen += 65536 if ($tdmsglen < 0);	# to handle large resp msgs
#
#	for v2r5: use the checksum byte as the segment count, and adjust length
#	appropriately
#
	$tdmsglen = ($tdchksum * 65536) + $tdmsglen;
	DBI->trace_msg("Invalid response message header; closing connection.", 2),
	close($obj->[TD_IMPL_CONNFD]),
	return $obj->io_set_error(-1, 'Invalid response message header; closing connection.', '08S01')
		if ($tdver !=  3) || (($tdclass & 0x7F) !=  2);
#
#	get the rest
#
	$tdmsglen -= ($hdrlen - TDAT_HDRSZ) if ($hdrlen > TDAT_HDRSZ);
	$hdr .= (chr(0) x $tdmsglen) if ($tdmsglen > 0);
	my $lrspmsg = 0;
	while ($tdmsglen > 0) {
		unless (recv($obj->[TD_IMPL_CONNFD], $rspmsg, $tdmsglen, 0)) {

			$hdrlen = length($rspmsg),
			DBI->trace_msg(
		"System error: can\'t recv msg body$!;\n rcvd $hdrlen bytes.\n", 2),
			close($obj->[TD_IMPL_CONNFD]),
			return $obj->io_set_error(-1, "System error: can't recv() msg body; closing connection.", '08S01')
				unless defined($rspmsg) && length($rspmsg);
		}
		$lrspmsg = length($rspmsg);

		DBI->trace_msg("GOT $lrspmsg BYTES, NEEDED $tdmsglen\n",2)
			if $debug && ($lrspmsg < $tdmsglen);

		substr($hdr, $hdrlen, length($rspmsg)) = $rspmsg;
		$tdmsglen -= $lrspmsg;
		$hdrlen += $lrspmsg;
	}

	DBI->trace_msg(io_pcldump(
		substr($hdr, TDAT_HDRSZ), length($hdr) - TDAT_HDRSZ, ($tdclass & 0x80)), 1)
		if $debug;
#
#	exit now if test msg
#
	return \$hdr if ($tdkind == COPKINDTEST);
#
#	in assign state, leave header intact
#
	my $active_reqno = $obj->[TD_IMPL_ACTIVE];
	$obj->[TD_IMPL_ACTIVE] = 0;
	return \$hdr unless $obj->[TD_IMPL_SESSNO];
#
#	!!!ENCRYPT ISSUES HERE!!!
#	decrypt if its encrypted and we have a crypt
#
	if ($obj->[TD_IMPL_CRYPT] && ($tdclass & 0x80)) {
		$hdr = $obj->[TD_IMPL_CRYPT]->decrypt_response($obj, $hdr);
		DBI->trace_msg(io_pcldump(
			substr($hdr, TDAT_HDRSZ), length($hdr) - TDAT_HDRSZ), 1)
			if $debug;
	}

	my ($tdsess, $tdauth1, $tdauth2, $reqno) =
		unpack('N LL N', substr($hdr, 20));

	close($obj->[TD_IMPL_CONNFD]),
	return $obj->io_set_error("Message for unknown session $tdsess recv'd; closing connection.")
		if ($tdsess !=  $obj->[TD_IMPL_SESSNO]);

	close($obj->[TD_IMPL_CONNFD]),
	return $obj->io_set_error(-1,
		"Sent and recv'd request numbers do not match for session $tdsess; closing connection.",
		'08S01')
		if ($reqno != $active_reqno) && ($tdkind != COPKINDCONNECT);
#
#	set xaction state from response msg
#	BUT ONLY FOR SQL SESSIONS!
#
	$obj->[TD_IMPL_SESINXACT] = ($tdbytevar & 1)
		if ($obj->[TD_IMPL_SESPART] == TD_PART_SQL) &&
			(($tdkind eq COPKINDSTART) || ($tdkind eq COPKINDCONTINUE));

	return $obj->io_scan_response(\$hdr, $reqno);
#pragma end_full
}

sub io_scan_response {
	my ($obj, $hdr, $reqno) = @_;
#
#	append buffer to assoc. sth's response buffer
#
	my $sth = ($obj->[TD_IMPL_REQ2STH]) ?
		$obj->[TD_IMPL_REQ2STH]{$reqno} : undef;
	my $islastresp;
	my $isfailed;
#
#	scan response for ENDREQUEST
#
	my $pos = TDAT_HDRSZ;
	my $rsplen = length($$hdr);
	while ($pos < $rsplen) {
		my ($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$hdr, $pos);

		$pos += $tdlen;
		$islastresp = 1,
		last
			if ($tdflavor == PclENDREQUEST);
		$isfailed = 1
			if ($tdflavor == PclFAILURE);
	}

	if ($sth) {
		my $sthpriv = $sth->{_private};
		$sthpriv->[TD_STH_LASTRESP] = $islastresp ||
			($isfailed && ($obj->[TD_IMPL_SESMODE] ne 'ANSI'));
		$sthpriv->[TD_STH_FAILED] = $isfailed;
		if ((! $sthpriv->[TD_STH_RSPBUF]) ||
			($sthpriv->[TD_STH_RSPBUFPOS] >= length($sthpriv->[TD_STH_RSPBUF]))) {
			$sthpriv->[TD_STH_RSPBUF] = $hdr;
			$sthpriv->[TD_STH_RSPBUFPOS] = TDAT_HDRSZ;
		}
		else {
			${$sthpriv->[TD_STH_RSPBUF]} .= substr($$hdr, TDAT_HDRSZ);
		}
	}
#
#	check for failure/error parcel
#
	my ($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$hdr, TDAT_HDRSZ);

	if (($tdflavor == PclFAILURE) || ($tdflavor == PclERROR)) {
		my ($tderr, $tdelen) =
			unpack('SS', substr($$hdr, TDAT_HDRSZ+$pclhdrsz+4, 4));
		my $tdemsg = substr($$hdr, TDAT_HDRSZ+$pclhdrsz+8, $tdelen);
		DBI->trace_msg("ERROR\: $tdemsg\n", 2);
		$obj->io_set_error($tderr, $tdemsg);
		$islastresp = ($tdflavor == PclFAILURE);
	}
#
#	if using CLI, last buffer, and not keepresp, clean up CLI
#	note we've got a copy of the last response buffer to play with
#
	$obj->[TD_IMPL_CONNFD]->cli_end_request($reqno)
		if ($islastresp || $isfailed) &&
			(! $obj->[TD_IMPL_DBH]{tdat_keepresp})
#pragma begin_full
			&& (ref $obj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli')
#pragma end_full
			;
	return $hdr;
}
#
#	send test msg wo ECHO
#	note: this won't work with CLI...hmmm....
#	maybe send a nop parcel ?
#
sub io_ping {
	my $reqmsg = $req_template . "\0\0\0\0";
	substr($reqmsg, 2, 1) = pack('C', COPKINDTEST);
	substr($reqmsg, 9, 1) = "\4";
	substr($reqmsg, TDAT_HDRSZ, 4) = pack('SS', PclNOP, 4);
	return $_[0]->io_tdsend($reqmsg);
#
#	we don't echo
#	my $buf = $obj->io_gettdresp;
#	return undef unless $buf;
#	return 1;
}
#
#	simple internal 'do' function
#
sub io_tddo {
	my ($obj, $dbreq, $dbdata, $outrecs) = @_;

	$obj->[TD_IMPL_LASTERR] = undef;
#
#	this is only used internally for small reqs, so
#	we don't need to handle big V2R5 reqs
#
	my $reqmsg = $obj->[TD_IMPL_REQFAC]->simpleRequest($obj, $obj->[TD_IMPL_SESRSPSZ], $dbreq);
	return undef unless $reqmsg;
	$obj->io_tdsend($reqmsg) or return undef;

	my $rspmsg = $obj->io_gettdresp;

	return undef
		unless $rspmsg;
	my ($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, TDAT_HDRSZ);
	my $pos = TDAT_HDRSZ + $pclhdrsz;
#
#	rely on response msg to set xaction state
#
	$obj->[TD_IMPL_SESINXACT] = 0
		if ($obj->[TD_IMPL_SESMODE] ne 'ANSI') && ($tdflavor == PclFAILURE);

	return undef if ($tdflavor != PclSUCCESS) || ($tdlen < 4);

	my ($stmtno, $rowcnt, $warncode, $fldcount, $activity, $warnlen) =
		unpack('SLSSSS', substr($$rspmsg, $pos, 14));
	$obj->io_set_error($warncode, substr($$rspmsg, $pos + 14, $warnlen))
		if $warnlen;
#
#	collect any returned data, up to the end of the request buffer
#	(we don't continue)
#
	$pos += ($tdlen - $pclhdrsz);
	($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, $pos);

	push(@$outrecs, substr($$rspmsg, $pos+$pclhdrsz, $tdlen-$pclhdrsz)),
	$pos += $tdlen,
	($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, $pos)
		while (($tdflavor == PclRECORD) && $outrecs);

	DBI->trace_msg("Session $obj->[TD_IMPL_SESSNO] executed $dbreq\n", 1)
		if $debug;

	return $rowcnt;
}
#
#	get next batch of rows
#
sub io_tdcontinue {
	my ($obj, $sth, $nowait) = @_;
#pragma begin_full
#
#	special continue for CLI
#
	if (ref $obj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli') {
#pragma end_full
		my $rspmsg;
		my ($err, $errstr) =
			$obj->[TD_IMPL_CONNFD]->cli_get_response(\$rspmsg,
				$sth->{_private}[TD_STH_REQNO], $sth->{tdat_keepresp}, -1);
		return $obj->io_set_error($err, $errstr)
			if $err;
		return \$rspmsg;
#pragma begin_full
	}

	unless ($obj->[TD_IMPL_ACTIVE]) {
		my $reqmsg = $obj->[TD_IMPL_REQFAC]->continueReq(
			$obj,
			$sth->{_private}[TD_STH_REQNO],
			$sth->{tdat_keepresp},
			$obj->[TD_IMPL_SESRSPSZ]);
		return undef unless $reqmsg;

		$obj->io_tdsend($reqmsg) or return undef;

		DBI->trace_msg("Session $obj->[TD_IMPL_SESSNO] continued\n", 1)
			if $debug;
		return undef if $nowait;
	}

	my $rspmsg = $obj->io_gettdresp;
	return $rspmsg;
#pragma end_full
}

sub io_socket {
	my ($obj, $dbsys, $port) = @_;

	my ($inetaddr, $dest, $connfd);
	$inetaddr = inet_aton($dbsys);
#
#	If we fail and the hostname isn't XXXCOPN,
#	check for COPN versions
#
	return $obj->io_set_error(-1, 'Unable to get host address.', '08001')
		unless $inetaddr || ($dbsys!~/^\d+\.\d+\.\d+\.\d+$/) ||
			($dbsys!~/cop\d+$/i);
#
#	if we don't have this host cached, try to
#	find its maxcop
#
	unless ($inetaddr) {
		unless ($hostcache{uc $dbsys}) {
			my $maxcop;
			my $host;
			foreach $host (keys %ENV) {
				$maxcop = $ENV{$host}, last
					if (uc $host eq uc $dbsys);
			}
			$maxcop = 1 unless $maxcop;
			$hostcache{uc $dbsys} = $maxcop;
		}
#
#	select random cop from the set
#
		my $cop = ($hostcache{uc $dbsys} == 1) ? 1 :
			int(rand($hostcache{uc $dbsys})) + 1;
		$inetaddr = inet_aton($dbsys . 'COP' . $cop);
	}

	return $obj->io_set_error(-1, 'Unable to get host address.', '08001')
		unless $inetaddr;

	$dest = sockaddr_in($port, $inetaddr);
	return $obj->io_set_error(-1, 'Unable to get host address.', '08001')
		unless $dest;

	return $obj->io_set_error(-1, "Unable to allocate a socket: $!.", '08001')
		unless socket($connfd, PF_INET, SOCK_STREAM, getprotobyname('tcp'));

	close($connfd),
	return $obj->io_set_error(-1, "Unable to connect: $!", '08001')
		unless connect($connfd, $dest);
	setsockopt($connfd, SOL_SOCKET, SO_KEEPALIVE, pack('l', 1));
#	setsockopt($connfd, SOL_SOCKET, SO_LINGER, pack('ii', 1, 10));

	$obj->[TD_IMPL_CONNFD] = $connfd;
	return $connfd;
}
#
#	negotiate possible encryption (also handles ASSIGN
#
sub io_negotiate {
	my ($obj, $username, $attr, $use_cli) = @_;

	my $pcls = $negotiate_pcls[($platform == COPFORMATATT_3B2) ? 1 : 0];
	my $rspmsg;
	my $newenc;
	my ($tdflavor, $tdlen, $pclhdrsz, $tderr, $tdelen);
	if ($use_cli) {
#
#	get config info to determine available charsets
#
		my $cfgreq = $obj->io_buildtdhdr(COPKINDCONFIG, 0);
		return undef unless $obj->io_tdsend($cfgreq);
		$rspmsg = $obj->io_gettdresp;
		return undef unless $rspmsg;
		($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, TDAT_HDRSZ);

		($tderr, $tdelen) = unpack('SS', substr($$rspmsg, TDAT_HDRSZ + $pclhdrsz)),
		return $obj->io_set_error($tderr, substr($$rspmsg, TDAT_HDRSZ + 4 + $pclhdrsz, $tdelen))
			if (($tdflavor == PclERROR) || ($tdflavor == PclFAILURE));

		return $obj->io_set_error(-1, "Unexpected parcel flavor $tdflavor", '08C01')
			unless ($tdflavor == PclCONFIGRESP);
	}
#pragma begin_full
	else {
#
#	start by sending CLIENTCFG + CONFIG; if we get error, then fallback to
#	old way
#
		my $reqlen = length($pcls->[TD_PCLS_CLIENTCFG]) + length($pcls->[TD_PCLS_CONFIG]);
		my $reqmsg = $obj->io_buildtdhdr(COPKINDCONFIG, $reqlen);
		substr($reqmsg, TDAT_HDRSZ, $reqlen) =
			$pcls->[TD_PCLS_CLIENTCFG] . $pcls->[TD_PCLS_CONFIG];
		return undef unless $obj->io_tdsend($reqmsg, $reqlen);
		$rspmsg = $obj->io_gettdresp();
		return undef unless $rspmsg;
#
#	check for error, if so, go do old way
#	else, start AES negotiation
#
		($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, TDAT_HDRSZ);
		if (($tdflavor == PclERROR) || ($tdflavor == PclFAILURE)) {
			my $cfgreq = $obj->io_buildtdhdr(COPKINDCONFIG, 0);
			return undef unless $obj->io_tdsend($cfgreq);
			$rspmsg = $obj->io_gettdresp;
			return undef unless $rspmsg;
			($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, TDAT_HDRSZ);

			($tderr, $tdelen) = unpack('SS', substr($$rspmsg, TDAT_HDRSZ + $pclhdrsz)),
			return $obj->io_set_error($tderr,
				substr($$rspmsg, TDAT_HDRSZ + 4 + $pclhdrsz, $tdelen), '08C01')
				if (($tdflavor == PclERROR) || ($tdflavor == PclFAILURE));

			return $obj->io_set_error(-1, "Unexpected parcel flavor $tdflavor", '08C01')
				unless ($tdflavor == PclCONFIGRESP);
		}
	}
	my $pos = TDAT_HDRSZ + $pclhdrsz;
	my $maxdecprec = 15;
	while ($pos < length($$rspmsg)) {
		if ($tdflavor == PclCONFIGRESP) {
			my $cfgpos = $pos;
#
#	theres other stuff we could grab, but its not really worth the effort;
#	the version number pretty much tells us everything
#
			$cfgpos += 10;
			my $pe_cnt = unpack('S', substr($$rspmsg, $cfgpos, 2));
			$cfgpos += 2 + ($pe_cnt * 4);
			my $amp_cnt = unpack('S', substr($$rspmsg, $cfgpos, 2));
			$cfgpos += 2 + ($amp_cnt * 2);
			my $dflt_charset = unpack('C', substr($$rspmsg, $cfgpos, 1));
			$cfgpos += 2;
			my $charset_cnt = unpack('S', substr($$rspmsg, $cfgpos, 2));
			$cfgpos += 2 + ($charset_cnt * 32) + 6;
			my $defmode = unpack('A', substr($$rspmsg, $cfgpos, 1));
			$attr->{tdat_mode} = ($defmode eq 'A') ? 'ANSI' : 'TERADATA'
				unless $attr->{tdat_mode} && ($attr->{tdat_mode} ne 'DEFAULT');
#
#	verify that the session mode is compatible
#
			return $obj->io_set_error(-1,
				"Only DBC/SQL sessions allowed in ANSI mode; closing connection.",
				'08C01')
				if ($attr->{tdat_mode} eq 'ANSI') && ($attr->{tdat_utility} ne 'DBC/SQL');
#
#	apply our chosen charset now
#
			my $charset = $attr->{tdat_charset};
			$obj->[TD_IMPL_CHARSET] =
				(($obj->[TD_IMPL_CHARSET] & 128) ? 128 : 0) |
					(
					(! $charset)			? $dflt_charset :
					($charset eq 'UTF8')	? tdat_UTF8 :
					($charset eq 'ASCII')	? tdat_ASCII : tdat_EBCDIC);

			$attr->{tdat_charset} = $charset =
				(($dflt_charset & 127) == tdat_UTF8) ? 'UTF8' :
				(($dflt_charset & 127) == tdat_ASCII) ? 'ASCII' : 'EBCDIC'
				unless $charset;

			$obj->[TD_IMPL_UTF8] = (($obj->[TD_IMPL_CHARSET] & 127) == tdat_UTF8);
#
#	try to determine the max decimal precision
#
			$cfgpos++;
			while ($cfgpos - $pos < $tdlen - $pclhdrsz) {
				my ($extcode, $extlen) = unpack('SS', substr($$rspmsg, $cfgpos, 4));
				$cfgpos += 4;
				$maxdecprec = unpack('L', substr($$rspmsg, $cfgpos + 72, 4))
					if (($extcode == 7) && ($extlen > 116));
				$cfgpos += $extlen;
			}			
		}
		elsif ($tdflavor == PclGTWCONFIG) {
		# don't care...
		}
		elsif ($tdflavor == PclAUTHMECH) {
			my ($authcfgext, $oidlen) = unpack('L L', substr($$rspmsg, $pos, 8));
			my $oid = substr($$rspmsg, $pos + 8, $oidlen);
			$newenc = $has_bigint && (exists $authoids{$oid}) && ($authoids{$oid} eq 'TD2')
				unless $newenc;
#
#	theres some addl stuff, but we don't care
#
#			my ($ext1, $ext2) = unpack('SS', substr($$rspmsg, $pos + 8 + $oidlen, 4))
#				if $authcfgext;
		}
		$pos += ($tdlen - $pclhdrsz);
		($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, $pos)
			if ($pos < length($$rspmsg));
		$pos += $pclhdrsz;
	}
	$obj->[TD_IMPL_DECPREC] = $attr->{tdat_max_dec_prec} = $maxdecprec;
	return $obj
#pragma begin_full
		if $use_cli
#pragma end_full
		;
#pragma begin_full
#
#	send the ASSIGN, possibly with SSOREQ
#
	my $reqlen = length($pcls->[TD_PCLS_ASSIGN]) +
		($newenc ? length($pcls->[TD_PCLS_TD2SSOREQ]) : length($pcls->[TD_PCLS_CONFIG]));
	my $reqmsg = $obj->io_buildtdhdr(COPKINDASSIGN, $reqlen);
	substr($reqmsg, TDAT_HDRSZ, $reqlen) = $newenc ?
		$pcls->[TD_PCLS_ASSIGN] . $pcls->[TD_PCLS_TD2SSOREQ] :
		$pcls->[TD_PCLS_ASSIGN] . $pcls->[TD_PCLS_CONFIG];
	substr($reqmsg, TDAT_HDRSZ + 4, length($username), $username);

	return undef unless $obj->io_tdsend($reqmsg);

	$rspmsg = $obj->io_gettdresp;
	return undef unless $rspmsg;

	my ($sessno, $tdauth1, $tdauth2, $reqno) =
		unpack('N LL L', substr($$rspmsg, 20));

	($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, TDAT_HDRSZ);
	unless ($sessno) {
		($tderr, $tdelen) = unpack('SS', substr($$rspmsg, TDAT_HDRSZ+8));

		$obj->io_set_error($tderr, substr($$rspmsg, TDAT_HDRSZ+12, $tdelen), '08C01'),
		DBI->trace_msg("ERROR\: $obj->[TD_IMPL_LASTEMSG]\n", 2)
			if (($tdflavor == PclFAILURE) || ($tdflavor == PclERROR));

		return undef;
	}

	return $obj->io_set_error(-1,
"Unknown response parcel $tdflavor recv'd during ASSIGN; closing connection.",
		'08C01')
		unless ($tdflavor == PclASSIGNRSP);

	my ($pbkey, $sesaddr, $pubkeyn, $relary, $verary, $hostid) =
		unpack('A8A32A32A6A14S', substr($$rspmsg, TDAT_HDRSZ + $pclhdrsz));

	$pos = TDAT_HDRSZ + $tdlen;

	$obj->[TD_IMPL_SESSNO] = $sessno;
	$obj->[TD_IMPL_HOSTID] = $hostid & 0x03ff;

	$obj->[TD_IMPL_VERSION] = 
		($relary=~/^V(\d+)R/) ? "V$1R$verary" 
		: ($relary=~/^(\d+)\.(\d+)/) ? "TW$verary" 
		: "Unknown";
#
#	beta releases may have alphas in the major version part
#
	$obj->[TD_IMPL_MAJOR_VER] = $1,
	$obj->[TD_IMPL_MINOR_VER] = $2,
	$obj->[TD_IMPL_MAINT_VER] = $3,
	$obj->[TD_IMPL_EMERG_VER] = $4,
	$obj->[TD_IMPL_VERSNUM] = ($1 * 1000000) + ($2 * 10000) + ($3 * 100) + $4
		if ($verary=~/^(\d+)[A-Za-z]*\.(\d+)\.(\d+)\.(\d+)/);

	DBI->trace_msg("Session $sessno assigned for Rel $relary Vers $verary\n", 1)
		if $debug;

	my ($p, $g, $gb, $verify);
	if ($newenc) {
		my $pos = TDAT_HDRSZ + $tdlen;
		($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, $pos);
		return $obj->io_set_error(-1,
"Unknown response parcel $tdflavor recv'd during ASSIGN; closing connection.",
			'08C01')
			unless $tdflavor && ($tdflavor == PclSSORESP);
#
#	there's a verify flag in the header, grab a copy just in case
#
		$verify = unpack('L', substr($$rspmsg, $pos + $pclhdrsz + 6 + 32, 4));
		$p = Math::BigInt->new('0x' .
			unpack('H160', substr($$rspmsg, $pos + $pclhdrsz + 86, 80)));
		$g = Math::BigInt->new('0x' .
			unpack('H160', substr($$rspmsg, $pos + $pclhdrsz + 166, 80)));
		$gb = Math::BigInt->new('0x' .
			unpack('H160', substr($$rspmsg, $pos + $pclhdrsz + 246, 80)));

#		print "TD2 keyinfo:\n", join("\n", "P: $p", "G: $g", "GB: $gb", "verify: $verify"), "\n";
#			if $debug;
	}
#
#	check if we can encrypt
#
	if ($has_bigint && ($obj->[TD_IMPL_VERSNUM] >= 5010000)) {
		eval {
			require DBD::Teradata::Crypt;
		};
		$obj->[TD_IMPL_CRYPT] = $newenc ?
			DBD::Teradata::Crypt->new($obj->[TD_IMPL_VERSNUM], $obj, $platform, $p, $g, $gb, $verify) :
			DBD::Teradata::Crypt->new($obj->[TD_IMPL_VERSNUM], $obj, $platform)
			unless $@;
#
#	if we can't load the crypt class, or can't create an instance of it,
#	we'll try to use unencrypted logon
#
	}
	return $obj;
#pragma end_full
}
#
#	create a bigint from raw bytes
#
sub _bigIntFromBytes {
	my $cnt = length($_[0]);
	my $bi;
		$bi = Math::BigInt->new('0x' . unpack('H' . ($cnt * 2), $_[0]));
	return $bi;
}
#
#	connect to the DBMS
#
sub io_connect {
	my ($obj, $dbsys, $port, $username, $password, $attr, $use_cli) = @_;
#
#	validate parms
#
	$obj->[TD_IMPL_LASTERR] = -1;
	$obj->[TD_IMPL_LASTSTATE] = 'S1000';
	$obj->[TD_IMPL_LASTEMSG] = '';

#pragma begin_full
	return $obj->io_set_error('LSN required for utility sessions.')
		unless ($attr->{tdat_utility}=~/^DBC\/SQL|MONITOR|DBCCONS$/) ||
			$attr->{tdat_lsn};

	if ($attr->{tdat_utility} eq 'MONITOR') {
		eval {
			require DBD::Teradata::PMAPI;
		};
		return $obj->io_set_error("Unable to support MONITOR session: $@")
			if $@;
	}
#pragma end_full

	my $connfd = $obj->io_socket($dbsys, $port);
	return undef unless $connfd;

	my $authent = int(rand(time()));
	$obj->[TD_IMPL_SESAUTH] = $authent+1;
	$obj->[TD_IMPL_SESAUTHX] = 0;
#
#	for passthru
#
	$obj->[TD_IMPL_S2C_SESAUTH] = $authent+1;
	$obj->[TD_IMPL_S2C_SESAUTHX] = 0;
#
#	if in passthru mode, return here
#
	return 0 unless defined($username);
#
#	CLI bypass:
#	if using CLI, then get config
#	via our local socket to determine available
#	charsets
#
	$obj->[TD_IMPL_VERSNUM] = 0;
	$obj->[TD_IMPL_SESPART] = $td_part_map{$attr->{tdat_utility}};
	$obj->[TD_IMPL_SESLSN] = $attr->{tdat_lsn} || undef;
	my $rspmsg;

	close($connfd),
	return undef
		unless $obj->io_negotiate($username, $attr, $use_cli);

	$obj->[TD_IMPL_SESMODE] = $attr->{tdat_mode};
	($obj->[TD_IMPL_COMMITSQL], $obj->[TD_IMPL_ABORTSQL]) =
		($attr->{tdat_mode} eq 'ANSI') ?
			('COMMIT WORK', 'ROLLBACK WORK') :
			('ET', 'ABORT');
#
#	permit app to provide logonsource
#	(for passthru apps)
#
	my $lgnsrc = $attr->{tdat_logonsrc};
#
#	8.001:
#	use proper values for servername, username
#
	unless ($lgnsrc) {
		my $uid = getlogin || getpwuid $< || '????';
#	$uid=~s/\s+/_/g;	# so full name gets displayed
#		$dbsys=~s/COP\d+$//;
#		$dbsys .= ' ' x (9 - length($dbsys))
#			if (length($dbsys) < 9);
		my $app = $0;
#		my $app = 'PERL';
#
#	trim leading path
#
		$app=~s/^(.*[\/\\]+)?//;
		$app = substr($app, 0, 20)
			if (length($app) > 20);
		$app .= ' ' x (4 - length($app))
			if (length($app) < 4);
		$lgnsrc = sprintf '%s  %8d  %s  %s  01 LSS', $dbsys, $$, $uid, $app;
#		$lgnsrc = "$dbsys      $$  $uid  $app  01 LSS";
#		$lgnsrc .= ' ' x 33;	# test encrypt padding
	}
	$password = '' unless defined($password);
#
#	apply our chosen charset now
#
#	my $charset = $attr->{tdat_charset};
#	$obj->[TD_IMPL_CHARSET] =
#		(($obj->[TD_IMPL_CHARSET] & 128) ? 128 : 0) |
#			(
#			(! $charset)			? $dflt_charset :
#			($charset eq 'UTF8')	? tdat_UTF8 :
#			($charset eq 'ASCII')	? tdat_ASCII : tdat_EBCDIC);
#
#	$attr->{tdat_charset} = $charset =
#		(($dflt_charset & 127) == tdat_UTF8) ? 'UTF8' :
#		(($dflt_charset & 127) == tdat_ASCII) ? 'ASCII' : 'EBCDIC'
#		unless $charset;
#
#	$obj->[TD_IMPL_UTF8] = (($obj->[TD_IMPL_CHARSET] & 127) == tdat_UTF8);

	if ($use_cli) {
		close($connfd);
		eval {
			require DBD::Teradata::Cli;
		};
		return $obj->io_set_error(-1, "Unable to load DBD::Teradata::Cli: $@", '08C01')
			if $@;
#
#	create a Cli object, includes connection request/response
#	processing
#
		$dbsys=~s/COP\d+\s*$//i;	# just in case
		my $logonstr = "$dbsys/$username,$password";
		my $lsn;
		my ($hostid, $sessno, $version);
		($obj->[TD_IMPL_CONNFD], $obj->[TD_IMPL_LASTERR], $obj->[TD_IMPL_LASTEMSG],
			$hostid, $sessno, $version, $lsn) =
				DBD::Teradata::Cli->new(
					$logonstr, $attr->{tdat_mode}, $attr->{tdat_utility}, $attr->{tdat_lsn},
						$lgnsrc, $obj->[TD_IMPL_CHARSET], $debug);

		$obj->[TD_IMPL_LASTSTATE] = '08C01',
		return undef
			if defined($obj->[TD_IMPL_LASTERR]);

		$obj->[TD_IMPL_SESSNO] = $sessno;
		$obj->[TD_IMPL_HOSTID] = $hostid & 0x3FF;
#
#	get release info
#
		$obj->[TD_IMPL_VERSION] = "V2R$version";
		$obj->[TD_IMPL_MAJOR_VER] = $1,
		$obj->[TD_IMPL_MINOR_VER] = $2,
		$obj->[TD_IMPL_MAINT_VER] = $3,
		$obj->[TD_IMPL_EMERG_VER] = $4,
		$obj->[TD_IMPL_VERSNUM] = ($1 * 1000000) + ($2 * 10000) + ($3 * 100) + $4
			if ($version=~/^(\d+)[A-Za-z]*\.(\d+)\.(\d+)\.(\d+)/);
		DBI->trace_msg(
			"Session $$obj[TD_IMPL_SESSNO] connected via CLI for $$obj[TD_IMPL_VERSION]\n", 1)
			if $debug;
#
# 	we parallel CLI's request numbers for our sth mapping purposes
#
		$obj->[TD_IMPL_CURREQ] = 1;
#
#	dbl buffering is a challenge, so shut it off for now
#
		$obj->[TD_IMPL_TWOBUF] = 0;
		$obj->[TD_IMPL_SESLSN] = $lsn,
		$attr->{tdat_lsn} = $lsn
			if defined $attr->{tdat_lsn};
	}
#pragma begin_full
	else {
		my $reqlen = 4 + length($username) + length($password) + 1 + 14 +
			4 + 24 + length($lgnsrc) + 4;
#
#	for now we assume our userids must be ASCII
#
		my $reqmsg = $obj->io_buildtdhdr(COPKINDCONNECT, $reqlen);
		substr($reqmsg , TDAT_HDRSZ, $reqlen) =
			pack('SSA*A1A* SSA1A3C6 SSA16LSS SSA*',
			PclLOGON,
			4+length($username)+length($password)+1, # pclsize
			$username , ',' , $password,
			PclSESSOPT,
			14,
			(($attr->{tdat_mode} eq 'ANSI') ? 'A' : 'T'),
			'NND',0,0,0,0,0,0,

			PclCONNECT,
			4+24, $attr->{tdat_utility},
				(defined($attr->{tdat_lsn}) ? $attr->{tdat_lsn} : 0),
				(defined($attr->{tdat_lsn}) ? (($attr->{tdat_lsn} == 0) ? 1 : 2) : 0),
				0,
			PclDATA, length($lgnsrc)+4, $lgnsrc);
#
#	send connect request (possibly encrypted)
#
		return undef unless $obj->io_tdsend($reqmsg, 1);

		$rspmsg = $obj->io_gettdresp;
		return undef
			unless $rspmsg;	# er, shouldn't this be a failure ???

		my ($tdflavor, $tdlen) = unpack('SS', substr($$rspmsg, TDAT_HDRSZ));

		if (($tdflavor == PclFAILURE) || ($tdflavor == PclERROR)) {
			my ($tderr, $tdelen) =
				unpack('SS', substr($$rspmsg, TDAT_HDRSZ+8));
			$obj->io_set_error($tderr, substr($$rspmsg, TDAT_HDRSZ+12, $tdelen));
			DBI->trace_msg("ERROR\: $obj->[TD_IMPL_LASTEMSG]\n", 2);
			close($connfd);
			return undef;
		}
		close($connfd),
		return $obj->io_set_error(-1,"Unexpected parcel $tdflavor.",'08C01')
			if ($tdflavor != PclSUCCESS);

		if (defined($attr->{tdat_lsn}) && ($attr->{tdat_lsn} == 0)) {
			($tdflavor, $tdlen, $obj->[TD_IMPL_SESLSN]) =
				unpack('SSL', substr($$rspmsg, TDAT_HDRSZ+$tdlen));
			close($connfd),
			return $obj->io_set_error(-1, "Expected LSN parcel but got $tdflavor.", '08C01')
				if ($tdflavor != PclLSN);
			$attr->{tdat_lsn} = $obj->[TD_IMPL_SESLSN];
		}
	}
#pragma end_full
	DBI->trace_msg("Session $$obj[TD_IMPL_SESSNO] connected\n", 1) if $debug;
#
#	now determine which request factory to use
#	pre-5.1 OR utility OR a control session for a utility,
#	then use the basic factory
#	5.1 through 6.0, use the APH factory
#	6.1+, use the bigresp APH factory
#
#	!!!NOTE: APH *does* work for fastload w/ 5.1.1, but
#	does *not* work for mload (??? another quality product from
#	NCR!)
#
	$obj->[TD_IMPL_REQFAC] =
		(($obj->[TD_IMPL_VERSNUM] < 5000000) ||
			($obj->[TD_IMPL_SESPART] != TD_PART_SQL) ||
			defined($attr->{tdat_lsn})) ?
			DBD::Teradata::ReqFactory->new() :
		($obj->[TD_IMPL_VERSNUM] < 6010000) ?
			DBD::Teradata::APHReqFactory->new() :
		($obj->[TD_IMPL_VERSNUM] < 6020000) ? 
			DBD::Teradata::BigAPHReqFactory->new() :
		DBD::Teradata::TW12ReqFactory->new($obj->[TD_IMPL_DECPREC]);
#
#	maybe send run startup, but for now check for a dbname and set it if
#	needed
#
	$obj->[TD_IMPL_LASTERR] = 0;
	$obj->[TD_IMPL_LASTSTATE] = '00000';
	$obj->[TD_IMPL_SESINXACT] = 0;
#
#	execute DATABASE if needed
#	NOTE: should this be ignored for utility sessions ?
#	for now, assume yes...
#
	return $obj->[TD_IMPL_SESSNO]
		unless $attr->{tdat_database} &&
			($obj->[TD_IMPL_SESPART] == TD_PART_SQL);

	my ($err, $errmsg, $state);
	unless ($obj->io_tddo("DATABASE $attr->{tdat_database}")) {
		($err, $errmsg, $state) =
			($obj->[TD_IMPL_LASTERR], $obj->[TD_IMPL_LASTEMSG],	$obj->[TD_IMPL_LASTSTATE]);
		$obj->io_disconnect;
		($obj->[TD_IMPL_LASTERR], $obj->[TD_IMPL_LASTEMSG],	$obj->[TD_IMPL_LASTSTATE]) =
			($err, $errmsg, $state);
		return undef;
	}
#
#	have to explicitly commit in ANSI...
#
	return $obj->[TD_IMPL_SESSNO]
		if ($obj->[TD_IMPL_SESMODE] ne 'ANSI') || defined($obj->io_tddo('COMMIT WORK'));

	($err, $errmsg, $state) =
		($obj->[TD_IMPL_LASTERR], $obj->[TD_IMPL_LASTEMSG],	$obj->[TD_IMPL_LASTSTATE]);
	$obj->io_disconnect;
	($obj->[TD_IMPL_LASTERR], $obj->[TD_IMPL_LASTEMSG],	$obj->[TD_IMPL_LASTSTATE]) =
		($err, $errmsg, $state);
	return undef;
}
#
#	logoff a session
#
sub io_disconnect {
#
#	logoff session
#
	my ($obj) = @_;

	unless ($obj->[TD_IMPL_DBH]{_logged_off}) {

#pragma begin_full
		if (ref $obj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli') {
#pragma end_full
			$obj->[TD_IMPL_CONNFD]->cli_disconnect();
#pragma begin_full
		}
		else {
			my $reqmsg = $obj->io_buildtdhdr(COPKINDLOGOFF, 4);
			substr($reqmsg, TDAT_HDRSZ, 4) = pack('SS', PclLOGOFF, 4);

			$obj->io_tdsend($reqmsg);
#
#	wait for logoff reply, but only briefly
#
			if (defined($obj->[TD_IMPL_CONNFD]) && defined(fileno($obj->[TD_IMPL_CONNFD]))) {
				my $rmask = '';
				my $emask = '';
				my ($rout, $eout);
				vec($rmask, fileno($obj->[TD_IMPL_CONNFD]), 1) = 1;
				$emask = $rmask;
				my $n = select($rout=$rmask, undef, $eout=$emask, 0.5);

				$obj->io_gettdresp()
					if $n && vec($rout, fileno($obj->[TD_IMPL_CONNFD]), 1);
			}
			close($obj->[TD_IMPL_CONNFD]);
		}
#pragma end_full
		$obj->[TD_IMPL_CONNFD] = undef;
		DBI->trace_msg("Logged off session $obj->[TD_IMPL_SESSNO]\n", 1) if $debug;
	}
	$obj->[TD_IMPL_REQ2STH] = undef;
	$obj->[TD_IMPL_DBH] = undef;
	$obj->[TD_IMPL_LASTERR] = undef;
	$obj->[TD_IMPL_LASTEMSG] = undef;
	$obj->[TD_IMPL_LASTSTATE] = undef;
	return 1;
}
#
#	parse out any using clause
#
sub io_parse_using {
	my ($req, $typeary, $typelen, $ipackstr, $nameary) = @_;

	$$ipackstr = '';			# complete unpack string
#
#	variable declarations
#
# for convenience
	my $remnant = ', ' . uc $req;
	while ($remnant=~/^\s*,\s*(\w+)\s+((DOUBLE\s+PRECISION)|CHARACTER|TIMESTAMP|INTERVAL|VARCHAR|VARBYTE|GRAPHIC|DECIMAL|NUMERIC|SMALLINT|BYTEINT|INTEGER|BIGINT|FLOAT|CHAR|BYTE|REAL|DATE|TIME|INT|DEC)(.*)$/i) {
		my $name = uc $1;
		my $vtype = uc $2;
		$remnant = $4;

		$vtype = 'FLOAT' if ($vtype=~/^(DOUBLE\s+PRECISION)|REAL$/i);
		$vtype = 'DEC' if ($vtype=~/^DECIMAL|NUMERIC/i);
		$vtype = 'CHAR' if ($vtype eq 'CHARACTER');
		$vtype = 'INT' if ($vtype eq 'INTEGER');

		push @$nameary, $name;
		my ($prec, $scale);

		unless (($vtype eq 'INTERVAL') || ($vtype eq 'DEC') || ($vtype eq 'TIME') || ($vtype eq 'TIMESTAMP')) {
			push(@$typeary, $td_type_str2dbi{$vtype}),
			push(@$typelen, $td_type_str2size{$vtype}),
#	we don't care about attributes, so get rid of any
			$remnant=~s/^[^,]+(,.*)$/$1/,
			next
				unless defined($td_type_str2baseprec{$vtype});

			$prec = $td_type_str2baseprec{$vtype};
			if ($remnant && ($remnant=~/^\s*\(\s*(\d+)\s*\)(.*)$/)) {
				$prec = $1;
				$remnant = $2;
			}
			push @$typeary, $td_type_str2dbi{$vtype};
			push @$typelen, $prec;
#	we don't care about attributes, so get rid of any
			$remnant=~s/^[^,]+(,.*)$/$1/;
			next;
		}

		if (($vtype eq 'TIMESTAMP') || ($vtype eq 'TIME')) {
			$prec = $td_type_str2baseprec{$vtype};
			if ($remnant && ($remnant=~/^(\s*\(\s*(\d+)\s*\))?(\s+WITH\s+TIME\s+ZONE)?(.*)$/)) {
				$prec = $2 if defined($1);
				$vtype .= ' WITH TIME ZONE' if defined($3);
				$remnant = $4;
			}
			push @$typeary, $td_type_str2dbi{$vtype};
			push @$typelen, $td_type_str2size{$vtype} + ($prec ? 1 + $prec : 0);
#	we don't care about attributes, so get rid of any
			$remnant=~s/^[^,]+(,.*)$/$1/;
			next;
		}
		elsif ($vtype eq 'DEC') {
			$prec = $td_type_str2baseprec{DEC};
			$scale = $td_type_str2basescale{DEC};
			if ($remnant && ($remnant=~/^\s*\(\s*(\d+)\s*(,\s*(\d+)\s*)?\)(.*)$/)) {
				$prec = $1;
				$scale = $3 if defined($2);
				$remnant = $4;
			}
			push @$typelen, (($prec * 256) + $scale);
			push @$typeary, $td_type_str2dbi{'DEC'};
#	we don't care about attributes, so get rid of any
			$remnant=~s/^[^,]+(,.*)$/$1/;
			next;
		}
#
#	must be an interval
#
		return undef
			unless $remnant && ($remnant=~/^\s*(YEAR|MONTH|DAY|HOUR|MINUTE|SECOND)(.*)$/);
		my $intvl = $1;
		$remnant = $2;

		$prec = $td_type_str2baseprec{$intvl};
		$scale = 0;
		if ($intvl eq 'SECOND') {
			if ($remnant && ($remnant=~/^\s*\(\s*(\d+)(\s*,\s*(\d+))?\s*\)(.*)$/)) {
				$prec = $1;
				$scale = defined($3) ? $3 : 6;
				$remnant = $4;
			}
			push @$typelen,
			$td_type_str2size{"INTERVAL SECOND"} + $prec + ($scale ? $scale + 1 : 0);
			push @$typeary, $td_type_str2dbi{"INTERVAL SECOND"};
#	we don't care about attributes, so get rid of any
			$remnant=~s/^[^,]+(,.*)$/$1/;
			next;
		}

		push(@$typelen, $td_type_str2size{"INTERVAL $intvl"} + $prec),
		push(@$typeary, $td_type_str2dbi{"INTERVAL $intvl"}),
#	we don't care about attributes, so get rid of any
		$remnant=~s/^[^,]+(,.*)$/$1/,
		next
			unless $remnant &&
				($remnant=~/^(\s*\(\s*(\d+)\s*\))?(\s+TO\s+(MONTH|HOUR|MINUTE|(SECOND(\s*\(\s*(\d+)\s*\))?)))?(.*)$/) &&
				(defined($1) || defined($3));

		$vtype = defined($3) ? defined($5) ? "INTERVAL $intvl TO SECOND" :
			"INTERVAL $intvl TO $4" : "INTERVAL $intvl";
		$prec = $2 if defined($1);
		$scale = defined($5) ? (defined($7) ? $7 : $td_type_str2basescale{SECOND}) : 0;

		push @$typelen, $td_type_str2size{$vtype} + $prec + ($scale ? 1 + $scale : 0);
		push @$typeary, $td_type_str2dbi{$vtype};
#	we don't care about attributes, so get rid of any
		$remnant=~s/^[^,]+(,.*)$/$1/;
	} # end while
#
#	generate packstring for the input parameter list
#
	my $prec = 0;
	my $i = 0;
	my $pstr;
	$$ipackstr = '';
	while ($i < scalar(@$typeary)) {
		$prec = ($$typeary[$i] == SQL_DECIMAL) ?
			$td_decszs[($$typelen[$i] >> 8) & 255] : $$typelen[$i];
		$pstr = $td_type_dbi2pack{$$typeary[$i]};
		$$ipackstr .= " $pstr";
		$$ipackstr .= $prec if (uc $pstr eq 'A') || ($pstr eq 'U');
		$i++;
	}

	return scalar @$typeary;
}
#
#	parse CALL statement for param info
#
sub io_parse_call {
	my ($obj, $stmt, $parmdesc) = @_;
#
#	locate procedure name and param list (if any)
#
	return undef unless ($stmt=~/^\s*CALL\s+([^\s\(]+)\s*(\(.+\))?\s*;?$/i);
	my ($spname, $parmList) = (uc $1, $2);

	$parmList=~s/^\((.+)\)$/$1/;
	$parmList=~s/^\s+//;
	$parmList=~s/\s+$//;

	if ($parmList ne '') {
#
#	temporarily remove commas from expressions
#
		$parmList=~s/DEC(IMAL)?\s*\(\s*(\d+)\s*,\s*(\d+)\s*\)/DEC($2;$3)/ig;
		my $phCount = ($parmList=~tr/\?//);
		my @parmAry = split(',', $parmList);
#
#	restore the commas to DEC CAST's
#
		for (my $i = 0; $i <= $#parmAry; $i++) {
			$parmAry[$i]=~s/;/,/g;
			$parmAry[$i]=~s/^\s+//;
			$parmAry[$i]=~s/\s+$//;
#
#	if its a PH, flag as input and placeholder
#	1 => PH
#	2 => IN
#	4 => OUT
#
			$$parmdesc[$i] = 3, next
				if ($parmAry[$i]=~/(\?|\:\w+)/);
#
#	if its a name, flag as output
#
			$$parmdesc[$i] = 4, next
				if (uc $parmAry[$i] ne 'NULL') && ($parmAry[$i]=~/^[_A-Za-z]\w+/);
#
#	anything else must be a literal, which means its only input
#
			$$parmdesc[$i] = 2;
		}
	}
	1;
}
#
#	prepare a (possibly parameterized) statement
#
sub io_prepare {
#
#	$dbh	- connection handle
#	$make_sth - callback to DBD::Teradata::db::_make_sth
#	$dbreq	- text of the SQL statement
#	$rowid - cursor rowid
#	$attribs - statement attributes hash
#	$compatible - version compatibility
#	$passthru - scalar ref to recv any fail/error msg
#
	my ($obj, $dbh, $make_sth, $dbreq, $rowid, $attribs, $compatible, $passthru) = @_;
	$compatible = '999.0' unless defined $compatible;
#
#	setup to collect all the returned column info
#
	my @ptypes = ();	# parameter types
	my @plens = ();		# parameter lengths
	my $usephs = 0;		# if using '?', set to number of params
	my @usenames = ();	# for USING names if any
	my @stmtinfo = (undef);	# stmtinfo of each stmt in request

	my $ipackstr = '';	# pack() descriptor for input parameters
#
#	count the number of placeholders
#
	$obj->[TD_IMPL_LASTERR] = 0;
	$obj->[TD_IMPL_LASTSTATE] = '00000';
	$obj->[TD_IMPL_LASTEMSG] = '';

	my $tmpreq = $dbreq;
	$tmpreq=~s/\s+WHERE\s+CURRENT\s+OF\s+[^\s;]+(\s*;)?\s*$/ WHERE CURRENT/ig;
#
#	!!!NOTE: we need to improve the scanner here!!!
#
	$tmpreq =~s/'[^']+'/''/g;	# get rid of quoted stuff
	$tmpreq=~s/\-\-.*\r/ /g;	# get rid of ANSI comments
	$tmpreq=~s/\/\*.*?\\*\// /g; # and Tdat comments

	my $datainfo = '';
	$ipackstr = '';
	$usephs = ($tmpreq =~ tr/\?//);

	return $obj->io_set_error('Placeholders not supported for utility applications.')
		if $usephs && (($obj->[TD_IMPL_SESPART] != TD_PART_SQL) || $obj->[TD_IMPL_SESLSN]);
#
#	now check for a USING clause...note that PH's and USING are *not*
#	compatible, so if we get both in the same stmt, then error out
#	also note that we need to handle the alternate USING format
#
	my $usingvars = 0;
	$tmpreq = uc $2,
	$usingvars = io_parse_using($1, \@ptypes, \@plens, \$ipackstr, \@usenames)
		if ($tmpreq=~/^USING\s*\((.+)\)\s*((?:SELECT|EXECUTE|LOCKING|IGNORE|INSERT|UPDATE|DELETE|MERGE|ABORT|EXEC|LOCK|MARK|CALL|INS|UPD|SEL|DEL|DO)\s+.+)$/i);

	return $obj->io_set_error('Invalid USING clause.')
		if ($usingvars == -1);

	return $obj->io_set_error('Can\'t mix USING clause and placeholders in same statement.')
		if ($usephs > 0) && ($usingvars > 0);

#pragma begin_full
	return &$make_sth(
		$dbh,
		{
		%$attribs,
		Statement => $dbreq,
		tdat_stmt_info => [
			undef,
			{
				ActivityType => $dbh->{tdat_utility},
				ActivityCount => 0,
				MloadCnts => [ 0,0,0,0 ]
			}
		],
		NUM_OF_FIELDS => 0,
		NUM_OF_PARAMS => $usingvars,
		_ptypes => \@ptypes,
		_plens => \@plens,
		_packstr => $ipackstr,
		_usenames => \@usenames
		}
	)
		if ($obj->[TD_IMPL_SESPART] == TD_PART_FASTLOAD) ||
			($obj->[TD_IMPL_SESPART] == TD_PART_MLOAD);
#pragma end_full
#
#	if PHs, set default types
#
	if ($usephs) {
		@ptypes = ((SQL_VARCHAR) x $usephs);
		@plens = (($phdfltsz) x $usephs);
		$ipackstr = 'S/a* ' x $usephs;
	}
#
#	check if non-data returning request; if so, don't send PREPARE
#	just synth our own descriptive structures
#
	unless ($compatible lt '2.1') {
		$tmpreq = $1
			while ($tmpreq=~/^LOCK(?:ING)?(?:(?:(?:\s+TABLE|DATABASE|VIEW)?\s+\S+)|ROW|)(?:\s+(?:FOR|IN))?\s+\S+(?:\s+MODE)?(?:\s+NOWAIT)?\s+(.+)$/i);

		my @s = split(';', $tmpreq);
		my $i = 1;
		my $t;
		foreach $t (@s) {
			last unless ($t=~/^\s*(DATABASE|COLLECT|REPLACE|MODIFY|REVOKE|UPDATE|INSERT|DELETE|CREATE|MERGE|ALTER|GRANT|DROP|GIVE|UPD|INS|DEL|[CRD][TVMP]|SET)\s+(VOLATILE\s+|GLOBAL\s+TEMPORARY\s+)?(MULTISET\s+|SET\s+)?(\S+)(\s+(\S+))?/i);
			my ($keywd, $keytyp, $keyqual) = (uc $1, uc $4, uc $6);
			last
				if (($keywd eq 'REPLACE') || ($keywd eq 'CREATE')) &&
					(($keytyp eq 'MACRO') || ($keytyp eq 'PROCEDURE'));

			$stmtinfo[$i]{ActivityType} =
				(($keywd eq 'SET') || ($keytyp eq 'JOIN') || ($keytyp eq 'HASH')) ?
					join(' ', ucfirst (lc $keywd), ucfirst(lc $keytyp), ucfirst(lc $keyqual)) :

				((($keywd eq 'CREATE') || ($keywd eq 'DROP') || ($keywd eq 'RENAME') || ($keywd eq 'REPLACE') ||
					($keywd eq 'ALTER') || ($keywd eq 'COLLECT') || ($keywd eq 'MODIFY')) ||
					(($keywd eq 'DELETE') && (($keytyp eq 'DATABASE') || ($keytyp eq 'JOURNAL')))) ?
					ucfirst (lc $keywd) . ' ' . ucfirst(lc $keytyp) :

				exists $stmt_abbrv{$keywd} ?
					$stmt_abbrv{$keywd} :
					ucfirst(lc $keywd);

			$stmtinfo[$i]{ActivityCount} = 0;
			$i++;
		}

		return &$make_sth(
			$dbh,
			{
			%$attribs,
			Statement => $dbreq,
			tdat_stmt_info => \@stmtinfo,
			NUM_OF_FIELDS => 0,
			NUM_OF_PARAMS => (($usingvars > 0) ? $usingvars : $usephs),
			_ptypes => \@ptypes,
			_plens => \@plens,
			_packstr => $ipackstr,
			_usenames => \@usenames,
			_usephs => $usephs
			}
		)
			if ($i > $#s+1);	# only exit if its all non-data-returning
	}

	@stmtinfo = (undef);
#
#	parse params for CALL statement
#
	my @parmdesc = ();
	my ($parmnum, $phnum) = (0,0);
	my $is_a_call = undef;
	if ($tmpreq=~/^\s*CALL\s+/i) {
		return undef
			unless $obj->io_parse_call($tmpreq, \@parmdesc);
		$is_a_call = 1;
	}

	my $reqmsg = $obj->[TD_IMPL_REQFAC]->prepareRequest($obj, $rowid,
		$usingvars, $obj->[TD_IMPL_SESRSPSZ], $dbreq);

	return $obj->io_set_error('System error: can\'t send() PREPARE request.')
		unless $obj->io_quicksend(length($reqmsg), undef, $reqmsg);

	my $rspmsg = $obj->io_gettdresp;
	return undef unless $rspmsg ;
	my $rsplen = length($$rspmsg);
#
#	if passthru, keep a copy of the response
#	!!!NOTE: how might we handle multiple prepinfo's ?
#	e.g., if stmt 1 succeeds, but stmt 2 fails ?
#	of stmt2 requires another response msg ?
#
	$$passthru = $$rspmsg
		if defined($passthru);

	my ($tdflavor, $tdlen, $tderr, $tdelen);
	my ($stmtno, $rowcnt, $warncode, $fldcount, $activity, $warnlen, $pcl);
	my $nextcol = 0;
	my $curpos = TDAT_HDRSZ;
	my $pclhdrsz = 4;
	my %sthargs = (
		%$attribs,
		Statement => $dbreq,
		tdat_stmt_info => [ undef ],
		NAME => [],
		TYPE => [],
		PRECISION => [],
		SCALE => [],
		NULLABLE => [],
		tdat_TYPESTR => [],
		tdat_TITLE => [],
		tdat_FORMAT => [],
		tdat_CHARSET => [],
		_unpackstr => [],
		_ptypes => \@ptypes,
		_plens => \@plens,
		_packstr => $ipackstr,
		_usenames => \@usenames,
		_usephs => $usephs,
		_parmdesc => \@parmdesc,
		_parmmap => {},
		_parmnum => \$parmnum,
		_phnum => \$phnum,
	);

	my $stmtinfo = $sthargs{tdat_stmt_info};

	while ($curpos < $rsplen) {
		($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, $curpos);

		$curpos += $tdlen , next
			if (($tdflavor == PclENDSTATEMENT) || ($tdflavor == PclENDREQUEST));

		if ($tdflavor == PclSUCCESS) {
			($stmtno, $rowcnt, $warncode, $fldcount, $activity, $warnlen) =
				unpack('SLSSSS', substr($$rspmsg, $curpos+$pclhdrsz, 14));
			$stmtinfo->[$stmtno]{ActivityType} = ($td_activity_types{$activity}) ?
				$td_activity_types{$activity} : 'Unknown';
			$stmtinfo->[$stmtno]{ActivityCount} = $rowcnt;
			$sthargs{tdat_unpackstr}[$nextcol] = '';
			$stmtinfo->[$stmtno]{Warning} = "Warning $warncode: " .
				substr($rspmsg, 18, $warnlen)
				if $warnlen;
			$curpos += $tdlen;
			next;
		}

		if (($tdflavor == PclFAILURE) || ($tdflavor == PclERROR)) {
#
#	extract/save error code msg
#	NOTE: does a PREPARE failure abort an open transaction in Tdat mode?
#
			($stmtno, $rowcnt, $tderr, $tdelen) =
				unpack('SSSS', substr($$rspmsg, $curpos+$pclhdrsz, 8));
			my $tdemsg = substr($$rspmsg, $curpos+$pclhdrsz+8, $tdelen);
			DBI->trace_msg("ERROR $tderr\: $tdemsg\n", 2);
			$obj->io_set_error($tderr,
				(($tdflavor == PclFAILURE) ? 'Failure' : 'Error') .
					" $tderr\: $tdemsg on Statement $stmtno.");

			$stmtinfo->[$stmtno]{SummaryStarts} =
				$stmtinfo->[$stmtno]{SummaryEnds} =
				$stmtinfo->[$stmtno]{StartAt} =
				$stmtinfo->[$stmtno]{EndsAt} = undef;
			return undef;
		}

		return $obj->io_set_error("Invalid parcel stream: got $tdflavor when PREPINFO expected.")
			unless (($tdflavor == PclPREPINFO) || ($tdflavor == PclPREPINFOX));
#
#	now process it
#
		$nextcol = $obj->io_proc_prepinfo(
			substr($$rspmsg, $curpos+$pclhdrsz, $tdlen - $pclhdrsz),
			$stmtno, \%sthargs, $activity, $is_a_call, $nextcol, $tdflavor);

		return undef
			unless defined($nextcol);

		$curpos += $tdlen;
	}

	DBI->trace_msg("Session $obj->[TD_IMPL_SESSNO] PREPAREd $dbreq\n", 1)
		if $debug;
#
#	if we get here, everything was OK, so discard any passthru
#
	$$passthru = undef
		if defined($passthru);

	$sthargs{NUM_OF_FIELDS} = scalar @{$sthargs{NAME}};
	$sthargs{NUM_OF_PARAMS} = $usingvars || $usephs;

#print "!!! got ", $sthargs{_unpackstr}[0], "\n";
	return &$make_sth($dbh, \%sthargs);
}

sub io_proc_prepinfo {
	my ($obj, $pcl, $stmtno, $sthargs, $activity, $is_a_call, $nextcol, $prepflavor) = @_;

	my $curpos = 0;
	my ($sumcnt, $colcnt) = unpack('SS', substr($pcl, 8, 4));
	my $packstr = '';
	my $charset = 0;
	my $charsz = 1;
	my $charcase = 0;
#
#	non-data returning statement
#
	my $stmtinfo = $sthargs->{tdat_stmt_info}[$stmtno];
	$stmtinfo->{StartsAt} = $stmtinfo->{StartsAt} = undef,
	return $nextcol
		unless $colcnt || $sumcnt;

	my ($phnum, $parmnum, $parmdesc, $parmmap) =
		($sthargs->{_phnum}, $sthargs->{_parmnum}, $sthargs->{_parmdesc}, $sthargs->{_parmmap});
	my ($datatype, $datalen, $datascale, $cname, $cfmt, $ctitle);
	$curpos += 12;
	$stmtinfo->{StartsAt} = $nextcol;
	$stmtinfo->{EndsAt} = $nextcol + $colcnt - 1;
	my $nextsum = 0;
#
#	init for summary rows
#
	$stmtinfo->{SummaryStarts} = [ 0 ],
	$stmtinfo->{SummaryEnds} = [ 0 ]
		if $sumcnt;

	while (1) {
		for (my $i = 0; $i < $colcnt; $i++, $nextcol++) {
			if ($prepflavor == PclPREPINFO) {
				($datatype, $datalen, $cname, $cfmt, $ctitle) =
					unpack('SS S/a S/a S/a', substr($pcl, $curpos));
				$curpos += (4 +
					2 + length($cname) +
					2 + length($cfmt) +
					2 + length($ctitle));
				$datascale = 0;
			}
			else {	# PREPINFOX
				if ($platform == COPFORMATATT_3B2) {
					($datatype, $datalen, $datascale, $charset, $charcase, $cname, $cfmt, $ctitle) =
						unpack('SLL C C S/a S/a S/a', substr($pcl, $curpos));
				}
				else {
					($datatype, $datascale, $datalen, $charset, $charcase, $cname, $cfmt, $ctitle) =
						unpack('SLL C C S/a S/a S/a', substr($pcl, $curpos));
				}
				$curpos += (12 +
					2 + length($cname) +
					2 + length($cfmt) +
					2 + length($ctitle));
			}
#
#	check if for a CALL statement; if so, we need to adjust type
#	code, and check for an input param...only save INOUT and OUT's
#
#	It appears that the NULLABLE bit has been redefined: it indicates
#	an INOUT param if set ?
#	bit 0 set => INOUT
#	bit 1 set => OUT
#
#	also, need to accomodate macros calling SPs
#
			if ($activity == TDAT_ACT_CALL) {
				$datatype -= TDAT_SP_IND;
				if ($is_a_call) {
					$$phnum++
						if ($$parmdesc[$$parmnum] & 1);
					$$parmnum++,
					$nextcol--,
					next
						unless ($datatype & 3);

					$$parmdesc[$$parmnum] |= 4;
					$$parmmap{$$phnum} = $nextcol
						if ($$parmdesc[$$parmnum] & 1);
					$$parmnum++;
					$stmtinfo->{EndsAt} = $nextcol;
				}
				$datatype &= 0xfffc;
			}
#
#	associate column name w/ type
#
			$sthargs->{NAME}[$nextcol] = ($cname eq '') ?
				(($ctitle eq '') ? "COLUMN$nextcol" : $ctitle) :
				$cname;

			my $ttype = $sthargs->{TYPE}[$nextcol] =
				$td_type_code2dbi{$datatype & tdat_NULL_MASK};

			$sthargs->{NULLABLE}[$nextcol] = ($activity == TDAT_ACT_CALL) ?
				1 : $datatype & 1;

			$sthargs->{tdat_TITLE}[$nextcol] = $ctitle;

			$datalen = $datascale,
			$datascale = 0
				if ($prepflavor == PclPREPINFOX) && ($ttype != SQL_DECIMAL);
#
#	format specifiers seem to be specified in a somewhat
#	capricious manner, so try to normalize here
#
			$cfmt = uc $cfmt;
			$cfmt = '-(' . (length($1) + 1) . ")$2"
				if ($cfmt=~/^(-+)(.*)/);

			$sthargs->{tdat_FORMAT}[$nextcol] = $cfmt;
			$sthargs->{tdat_CHARSET}[$nextcol] =
				($charset <= $#colcharsets) ? $colcharsets[$charset] : 'Unknown';
			$sthargs->{tdat_CASESPECIFIC}[$nextcol] = ($charcase != 0);
#
#	adjust precision/scale for DECIMAL
#
			my $prec = $sthargs->{PRECISION}[$nextcol] =
				(($ttype == SQL_DECIMAL) && ($prepflavor == PclPREPINFO)) ?
				(($datalen >> 8) & 255) : $datalen;
#
#	adjust for UTF8...er maybe not...
#	DAA 12/17/05 er maybe yes
#	In UTF8 mode, LATIN returns 2 bytes/char
#	and UNICODE returns 3 bytes/char
#	seems to be a server bug with big strings;
#	if the byte size of the returned string exceeds
#	64000, then the bytelength returned is MOD 65536
#	possible fix:
#	$datalen |= 0x10000
#	if ($datalen % $charsize != 0) ||
#		($format=~/^x\(\d+\)/i) && ($datalen < $1);
#	--- but that ignores any special formatting...
#	worse, the actual returned string is *also* truncated
#	the same way...pretty stupid...so we'll do our best
#	to accomodate it...
#	Also note we don't support Kanji or Graphic
#
			if ($obj->[TD_IMPL_UTF8] && $charset &&
				(($ttype == SQL_CHAR) || ($ttype == SQL_VARCHAR))) {
				$charsz = ($charset == TD_CS_LATIN) ? 2 : 3;
#
#	Due to Teradata's screwed up UTF8 behavior, we'll just
#	throw an error here.
#	Note that Teradata's behavior wrt truncation IS NOT VALID
#	AND WILL LEAD TO BAD UTF8 ENCODED CHARACTERS!!!
#
				$datalen &= 0xFFFF;
				return $obj->io_set_error('Invalid server truncation of UNICODE field ' . $sthargs->{NAME}[$nextcol])
					if $datalen%$charsz;
				$prec /= $charsz;
				$sthargs->{PRECISION}[$nextcol] = $prec;
			}

			my $scale = $sthargs->{SCALE}[$nextcol] =
				(($ttype == SQL_DECIMAL) && ($prepflavor == PclPREPINFO)) ?
				($datalen & 255) : $datascale;
#
#	generate unpackspec for each statement
#	just use the original bytelen for UTF8 CHAR
#
			my $len = ($ttype == SQL_DECIMAL) ? $td_decszs[$prec] : $datalen;
			$packstr .=
				(($ttype == SQL_BINARY) || ($ttype == SQL_DECIMAL)) ? "a$len " :
				(($ttype == SQL_CHAR)								? "A$len " :
					($td_type_dbi2pack{$ttype}) . ' ');
#
#	modify format spec for BYTE/VARBYTE
#
			$sthargs->{tdat_FORMAT}[$nextcol] = "6($prec)"
				if ($ttype == SQL_BINARY) || ($ttype == SQL_VARBINARY);
#
#	generate type string
#
			my $typestr = $td_type_dbi2str{$ttype};
			$typestr .= '(' . $prec .
				(($ttype == SQL_DECIMAL) ? ", $scale)" : ')')
				if $td_type_str2baseprec{$typestr};

			$sthargs->{tdat_TYPESTR}[$nextcol] = $typestr;

			DBI->trace_msg(
				($ttype != SQL_DECIMAL) ?
		"$sthargs->{NAME}[$nextcol]\: $ttype LENGTH $prec\n" :
		"$sthargs->{NAME}[$nextcol]\: DECIMAL($prec, $scale) LENGTH $td_decszs[$len]\n",
					1)
				if $debug;
		}	# end foreach column
#
#	save output packstring at starting column entry for current statement
#	or summary
#
		my $packidx = $nextsum ? $stmtinfo->{SummaryStarts}[$nextsum - 1] : $stmtinfo->{StartsAt};

		$packstr=~s/\*//g;
		$sthargs->{_unpackstr}[$packidx] = $packstr;
		$packstr = '';
		last unless $sumcnt;
#
#	skip the extra column count for summary columns
#
		$colcnt = unpack('S', substr($pcl, $curpos));
		$stmtinfo->{SummaryStarts}[$nextsum] = $nextcol;
		$stmtinfo->{SummaryEnds}[$nextsum] = $nextcol + $colcnt - 1;
		$nextsum++;
		$curpos += 2;
		$sumcnt--;
	}
	return $nextcol;
}
#pragma begin_full
#
#	init request buffer for bulk request
#
sub io_init_bulk_request {
	my ($obj, $numparams, $usephs) = @_;
#
# SQL in $_[3]
#	and init it with stmt, options, and datainfo if needed
#
	($obj->[TD_IMPL_SESBUFPOS], $obj->[TD_IMPL_SESBUFF]) =
		(TDAT_HDRSZ, "\0" x ($obj->[TD_IMPL_SESREQSZ] + TDAT_HDRSZ));
#
#	write pcloptions, request, and datainfo
#
	my $dip;
	($obj->[TD_IMPL_SESBUFPOS], $dip) =
		$obj->[TD_IMPL_REQFAC]->bulkRequest($numparams, $usephs, $obj->[TD_IMPL_SESBUFF], $_[3]);
	return $dip;
}

#
#	execute a previously prepared stmt, using current bound params
#
sub io_array_execute {
#
#	$sth - DBI handle of statement we're executing
#
	my ($obj, $sth) = @_;
	$obj->[TD_IMPL_LASTERR] = 0;
	$obj->[TD_IMPL_LASTSTATE] = '00000';
	$obj->[TD_IMPL_LASTEMSG] = '';
	$obj->[TD_IMPL_SESBUFPOS] = $obj->[TD_IMPL_REQFAC]->closeBulkRequest(
		$obj->[TD_IMPL_SESBUFPOS],
		$obj->[TD_IMPL_SESRSPSZ],
		$obj->[TD_IMPL_SESBUFF]);

	my $reqmsg = $obj->io_buildtdhdr(COPKINDSTART, $obj->[TD_IMPL_SESBUFPOS] - TDAT_HDRSZ,
		undef, $obj->[TD_IMPL_SESBUFF]);
	return undef unless $reqmsg;

##########################
	delete $obj->[TD_IMPL_REQ2STH]{$_}
		foreach (keys %{$obj->[TD_IMPL_REQ2STH]});
############################
	my $treqno = $obj->[TD_IMPL_CURREQ];	# assoc. stmt handle w/ reqno
	$obj->io_quicksend($obj->[TD_IMPL_SESBUFPOS], undef, $obj->[TD_IMPL_SESBUFF])
		or return undef;
	$obj->[TD_IMPL_REQ2STH]{$treqno} = $sth;
	$sth->{_private}[TD_STH_REQNO] = $treqno;
	return 1;
}
#pragma end_full
#
#	execute a previously prepared stmt, using current bound params
#
sub io_execute {
#
#	$sth - DBI handle of statement we're executing
#	$datainfo - the DATAINFO parcel for PH's
#	$indicdata - data to send to DBMS
#	$rowid - CURSORHOST rowid for positioned update/delete
#
	my ($obj, $sth, $datainfo, $indicdata, $rowid) = @_;
	$obj->[TD_IMPL_LASTERR] = 0;
	$obj->[TD_IMPL_LASTSTATE] = '00000';
	$obj->[TD_IMPL_LASTEMSG] = '';
	my $stmtinfo = $sth->{tdat_stmt_info};
	my $stmtno = $sth->{tdat_stmtno};
	my $nowait = $sth->{tdat_nowait};
	my $stmt = $sth->{Statement} || '';
	my $keepresp = ($sth->{tdat_keepresp} || ($stmt=~/\s+FOR\s+CURSOR\s*$/i));
	my $rawmode = $sth->{tdat_raw_in};
	my $reqmsg = '';
	my $reqlen = 0;
	my $modepcl = ($rawmode && defined($rawmodes{$rawmode})) ?
		$rawmodes{$rawmode} : PclINDICDATA;
	my $partition = $obj->[TD_IMPL_SESPART];
	my $reqfac = $obj->[TD_IMPL_REQFAC];

#pragma begin_full
	$sth->{tdat_stmt_info}[1]{Prompt} = undef
		if ($partition == TD_PART_DBCCONS);
#pragma end_full
#
#	trim positioned update/delete
#
	$stmt=~s/\s+WHERE\s+CURRENT\s+OF\s+([^\s;]+)\s*;?\s*$/ WHERE CURRENT/i;
	my $forCursor = ($stmt=~/\s+WHERE\s+CURRENT$/i) && $rowid;

#	DBC/SQL default
	if (($partition == TD_PART_SQL) ||
		(($partition == TD_PART_DBCCONS) && ($stmt ne ';'))) {
		$reqlen = 4 + length($stmt) + 6 +
			((defined($datainfo) && length($datainfo)) ? 4 + length($datainfo) : 0) +
			((defined($indicdata) && length($indicdata)) ? 4 + length($indicdata) : 0) +
			($forCursor ? 4 + length($rowid) : 0) +
			($sth->{tdat_mload} ? 14 : 0);
#
#	if we exceeded the max limit, then adjust for the bigrequest parcels
#
		if ($reqlen > 65535) {
			$reqlen += 4 + 4 + (length($datainfo) ? 4 : 0) +
				(length($indicdata) ? 4 : 0) + (($forCursor && $rowid) ? 4 : 0);
			$obj->[TD_IMPL_BIGREQ] = 1;
		}
		else {
			$obj->[TD_IMPL_BIGREQ] = undef;
		}
	}
#pragma begin_full
#
#	for now, we assume DBCCONS doesn't need any big msg support
#
	elsif (($partition == TD_PART_DBCCONS) && ($stmt eq ';')) {
		$reqlen = 6 + 4 + length($indicdata);
	}
#pragma end_full
#
#	handle the big SPs
#
	if ($sth->{Statement}=~/^\s*(CREATE|REPLACE)\s+PROCEDURE\s+/i) {
#
# add MultiTSR and SPOptions
# build array of request buffers with each TSR
#	we may need to make this support bigreq but I don't see
#	why at this point...
#
		my $pos = 0;
		my $segment = 1;

		my $len = length($sth->{Statement});

		my $sz = 0;
		while ($len) {
			$sz = ($len > 64000) ? 64000 : $len;
			$len -= $sz;

			($reqmsg, $pos) = $reqfac->spRequest($obj, $sz, $pos, $segment, $sth, $obj->[TD_IMPL_SESRSPSZ]);
			$segment++;
			my $treqno = $obj->[TD_IMPL_CURREQ];	# assoc. stmt handle w/ reqno
			$obj->io_quicksend(length($reqmsg), undef, $reqmsg) or return undef;
			$obj->[TD_IMPL_REQ2STH]{$treqno} = $sth;
			$sth->{_private}[TD_STH_REQNO] = $treqno;
			last unless $len;
#
#	nowait mode only applies to last segment for CREATE SP
#
			return undef unless defined($obj->io_Realize($sth));
		}
		return ($nowait ? -1 : $obj->io_Realize($sth));
	}
#
#	if our request length is too big, return error
#
	return $obj->io_set_error('Maximum request size exceeded.')
		if ($obj->[TD_IMPL_VERSNUM] < 5000000) && ($reqlen > 65535);

	if ($partition == TD_PART_SQL) {
		$reqmsg = $reqfac->sqlRequest($obj, $sth, $forCursor, $rowid, $modepcl, $keepresp,
			$obj->[TD_IMPL_SESRSPSZ], $stmt, $datainfo, $indicdata);
	}
#pragma begin_full
	elsif ($partition == TD_PART_EXPORT) {
		$reqmsg = $reqfac->exportRequest($obj, $obj->[TD_IMPL_SESRSPSZ], $stmt);
	}
	elsif (($partition == TD_PART_FASTLOAD) || ($partition == TD_PART_MLOAD)) {
		$reqfac->loadRequest($obj, (($partition == TD_PART_FASTLOAD) ? 512 : 4096));
	}
	elsif ($partition == TD_PART_MONITOR) {
		$reqmsg = $reqfac->monitorRequest($obj, $modepcl, $keepresp, $obj->[TD_IMPL_SESRSPSZ], $stmt, $indicdata);
	}
	elsif ($partition == TD_PART_DBCCONS) {
		$reqmsg = $reqfac->consoleRequest($obj, $obj->[TD_IMPL_SESRSPSZ], $stmt, $datainfo, $indicdata);
	}
#pragma end_full

##########################
	delete $obj->[TD_IMPL_REQ2STH]{$_}
		foreach (keys %{$obj->[TD_IMPL_REQ2STH]});
############################
	my $treqno = $obj->[TD_IMPL_CURREQ];	# assoc. stmt handle w/ reqno
	if (($partition == TD_PART_FASTLOAD) || ($partition == TD_PART_MLOAD)) {
		$obj->io_quicksend($obj->[TD_IMPL_SESBUFPOS], undef, $obj->[TD_IMPL_SESBUFF])
			or return undef;
	}
	else {
		$obj->io_tdsend($reqmsg) or return undef;
	}
	$obj->[TD_IMPL_REQ2STH]{$treqno} = $sth;
	$sth->{_private}[TD_STH_REQNO] = $treqno;
#
#	in nowait mode, return immediately
#
	return ($nowait) ? -1 : $obj->io_Realize($sth);
}
#
#	fetch a single row
#
sub io_fetch { # sessno
	my ($obj, $sth, $ary, $retstr) = @_;
	$obj->[TD_IMPL_LASTERR] = 0;
	$obj->[TD_IMPL_LASTSTATE] = '00000';
	$obj->[TD_IMPL_LASTEMSG] = '';

	my $sthpriv = $sth->{_private};

	return 0 unless $sthpriv->[TD_STH_RSPBUF];	# request exhausted
	my $maxlen = ($ary) ? $sthpriv->[TD_STH_MAXCOLARY] : 1;
	my $stmtno = $sth->{tdat_stmt_num};
	my ($tdflavor, $tdlen, $failed) = (0,0,0,0);
#
#	recover the stmt's response buffer info
#
	my $rspmsg = $sthpriv->[TD_STH_RSPBUF];
	my $pos = $sthpriv->[TD_STH_RSPBUFPOS];
	my $rsplen = ($$rspmsg) ? length($$rspmsg) : 0;
	my $partition = $obj->[TD_IMPL_SESPART];
	my $stmtinfo = $sth->{tdat_stmt_info};
	my $stmthash = ($stmtno) ? $$stmtinfo[$stmtno] : undef;
	my $endstmt = 0;
	my $total_activity = 0;
	my $arycnt = 0;
	if ($pos >= $rsplen) {
#
#	current buffer exhausted, get the next one
#
		$rspmsg = $obj->io_tdcontinue($sth, 0);
		return 0 unless $$rspmsg;
		$sthpriv->[TD_STH_RSPBUF] = $rspmsg;
		$rsplen = length($$rspmsg);
#
#	startup the next continue if not end of request and in auto-continue mode
#
		$obj->io_tdcontinue($sth, 1)
			if (! $sthpriv->[TD_STH_LASTRESP]) && $obj->[TD_IMPL_TWOBUF];
		$pos = TDAT_HDRSZ;
	}
#
#	read response data until a RECORD parcel is recv'd;
#
#	case PclENDREQUEST:
#		clean up buffers
#		commit if ANSI & autocommit
#		remove the stmt handle from the reqno map
#
#	case PclFAILURE, PclERROR:
#		extract & save error msg
#		if not ANSI
#			clean up buffers,
#			remove stmt handle from reqno map,
#			mark xaction complete,
#			return
#
#	case PclOK
#	case PclSUCCESS:
#		extract and save stmtno, activity, warning (if any)
#		lookup stmtinfo for stmtno and update stmt info hash
#
#	case PclWITH, PclENDWITH, PclPOSSTART, PclPOSITION, PclPOSEND
#		update summary info
#
#	case PclENDSTATEMENT
#		cleanup summary info
#
#	case PclDATAINFO
#		if MONITOR partition or a HELP, then
#			process the DATAINFO
#
#	case PclCURSORDBC:
#		extract and save rowid in sth
#
#
#	FM parcel sequence is:
#
#	case PclTITLESTART:
#	case PclFORMATSTART:
#	case PclSIZESTART:
#		enable ignore mode
#
#	case PclTITLEEND:
#	case PclFORMATEND:
#	case PclSIZEEND:
#		disable ignore mode
#
#	case PclFIELD:
#		next if (ignore);
#		push @current_record, value;
#
#	case PclNULLFIELD:
#		next if (ignore);
#		push @current_record, undef;
#
#	case PclRECSTART:
#		open new record
#
#	case PclRECEND
#		end new record ???
#
#	default:
#		cleanup stmt handle buffers
#		return Bad Parcel failure
#
#	we keep track of warnings and activity info on a per-statment
#	basis
#
	my ($pclhdrsz, $rowcnt, $tderr, $fldcount, $activity, $tdelen, $tdemsg);
	($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, $pos);
	$sth->{tdat_more_results} = 1;
#
#	may have been pending multipart
#
	my $multipart = $sthpriv->[TD_STH_MULTIPART] || '';

	while (($tdflavor != PclRECORD) && ($tdflavor != PclENDMULTIPARTREC) &&
		($tdflavor != PclERRORCNT)) {
		$sthpriv->[TD_STH_RSPBUFPOS] = $pos + $tdlen;
		if ($tdflavor == PclENDREQUEST) {

			$sthpriv->[TD_STH_RSPBUF] = undef;
			$sthpriv->[TD_STH_RSPBUFPOS] = 0;
			$sth->{tdat_more_results} = 0;
#
#	in ANSI + AutoCommit mode, send COMMIT
#	NOTE: let response msg set xaction state
#
			$obj->io_tddo('COMMIT WORK'),
			$obj->[TD_IMPL_SESINXACT] = 0
				if ($obj->[TD_IMPL_SESMODE] eq 'ANSI') &&
					$obj->[TD_IMPL_SESINXACT] && $obj->[TD_IMPL_DBH]{AutoCommit};
			$sth->{tdat_more_results} = 0,
# unmap reqno-to-sth assoc. if not EXPORT or KEEPRESP non-updatable cursor
			delete $obj->[TD_IMPL_REQ2STH]{$sthpriv->[TD_STH_REQNO]},
			$sthpriv->[TD_STH_REQNO] = 0,
				unless ($obj->[TD_IMPL_SESPART] == TD_PART_EXPORT) ||
					$sth->{tdat_keepresp};
#					($sth->{tdat_keepresp} && (! $sthpriv->[TD_STH_UPDATABLE]));
			return $total_activity;
		}
		elsif (($tdflavor == PclFAILURE) || ($tdflavor == PclERROR)) {
#
#	extract/save error code msg
#
			($stmtno, $rowcnt, $tderr, $tdelen) =
				unpack('SSSS', substr($$rspmsg, $pos+$pclhdrsz, 8));
			$tdemsg = substr($$rspmsg, $pos+$pclhdrsz+8, $tdelen);
			DBI->trace_msg("ERROR $tderr\: $tdemsg\n", 2);
			$obj->io_set_error($tderr,
				(($tdflavor == PclFAILURE) ? 'Failure' : 'Error') .
					" $tderr\: $tdemsg on Statement $stmtno.");

			$sth->{tdat_stmt_num} = $stmtno;
			$stmthash = $$stmtinfo[$stmtno];
			$stmthash->{ErrorCode} = $tderr;
			$stmthash->{ErrorMessage} = $tdemsg;
#
#	let response msgs set xaction state
#
			$obj->[TD_IMPL_SESINXACT] = 0
				if ($obj->[TD_IMPL_SESMODE] ne 'ANSI');
			delete $obj->[TD_IMPL_REQ2STH]{$sthpriv->[TD_STH_REQNO]},
			$sthpriv->[TD_STH_REQNO] = 0,
			$sthpriv->[TD_STH_RSPBUF] = undef,
			$sthpriv->[TD_STH_RSPBUFPOS] = 0,
			$sth->{tdat_more_results} = 0,
			$sthpriv->[TD_STH_LASTRESP] = 1,
			return undef
				if ($obj->[TD_IMPL_SESMODE] ne 'ANSI') ||
					($stmtno == $#{$sth->{tdat_stmt_info}});
		}
		elsif ($tdflavor == PclSUCCESS) {
			($stmtno, $rowcnt, $tderr, $fldcount, $activity, $tdelen) =
				unpack('SLSSSS', substr($$rspmsg, $pos+$pclhdrsz, 14));
			$stmtno++ unless $stmtno;
			$stmthash = $$stmtinfo[$stmtno];
			$stmthash->{ActivityType} = ($td_activity_types{$activity}) ?
				$td_activity_types{$activity} : 'Unknown';
			$stmthash->{ActivityCount} = $rowcnt;
			$total_activity += $rowcnt;

			delete $stmthash->{Warning};
			$stmthash->{Warning} =
	"Warning $tderr\: " . substr($$rspmsg, $pos+$pclhdrsz+14, $tdelen)
				if $tdelen;
			$sth->{tdat_stmt_num} = $stmtno;
		}
		elsif ($tdflavor == PclOK) {
			($stmtno, $fldcount, $rowcnt, $activity, $tderr, $tdelen) =
				unpack('SSLSSS', substr($$rspmsg, $pos+$pclhdrsz, 14));
			$stmtno++ unless $stmtno;
			$stmthash = $$stmtinfo[$stmtno];
			$stmthash->{ActivityType} = ($td_activity_types{$activity}) ?
				$td_activity_types{$activity} : 'Unknown';
			$stmthash->{ActivityCount} = $rowcnt;
			$total_activity += $rowcnt;

			delete $stmthash->{Warning};
			$stmthash->{Warning} =
		"Warning $tderr\: " . substr($$rspmsg, $pos+$pclhdrsz+14, $tdelen)
				if $tdelen;
			$sth->{tdat_stmt_num} = $stmtno;
		}
		elsif ($tdflavor == PclWITH) {
			$stmthash->{IsSummary} = (unpack('S',
				substr($$rspmsg, $pos+$pclhdrsz)) - 1);
			$stmthash->{SummaryPosition} = [ ],
			$stmthash->{SummaryPosStart} = [ ]
				unless defined($stmthash->{SummaryPosition});
		}
		elsif ($tdflavor == PclENDWITH) {
			delete $stmthash->{IsSummary};
		}
		elsif ($tdflavor == PclPOSSTART) {
			my $sumpos = $stmthash->{SummaryPosition};
			my $sumstart = $stmthash->{SummaryPosStart};
			push(@$sumstart, scalar(@$sumpos));
		}
		elsif ($tdflavor == PclPOSITION) {
			my $sumpos = $stmthash->{SummaryPosition};
			push(@$sumpos, (unpack('S', substr($$rspmsg, $pos+$pclhdrsz))));
		}
		elsif ($tdflavor == PclPROMPT) {
			$stmthash->{Prompt} = 1;
			$obj->[TD_IMPL_PROMPTED] = 1;
		}
		elsif ($tdflavor == PclENDSTATEMENT) {
			delete $stmthash->{SummaryPosStart};
			delete $stmthash->{SummaryPosition};
			$endstmt = 1;
		}
		elsif ((($tdflavor == PclDATAINFO) || ($tdflavor == PclDATAINFOX)) &&
			(exists $stmthash->{ActivityType}) &&
			(($stmthash->{ActivityType} eq 'Help') ||
			($stmthash->{ActivityType} eq 'Call') ||
			($stmthash->{ActivityType} eq 'Monitor SQL') ||
			(($stmthash->{ActivityType} eq 'Monitor Session') &&
				($stmtno > 1)))) {
			my $rc = $obj->io_proc_datainfo($sth,
				substr($$rspmsg, $pos+$pclhdrsz, $tdlen - $pclhdrsz),
					$stmtno, ($tdflavor == PclDATAINFOX));

			DBI->trace_msg("ERROR -1: DATAINFO for unexpected statement\n", 2),
			return $obj->io_set_error("Failure -1: DATAINFO for unexpected Statement $stmtno.")
				if ($rc == -1);

			DBI->trace_msg("ERROR -2: DATAINFO does not match defined fields.\n", 2),
			return $obj->io_set_error(-2,
				"Failure -2: DATAINFO does not match defined fields for Statement $stmtno.")
				if ($rc == -2);
		}
		elsif ($tdflavor == PclCURSORDBC) {
#
#	save as used by CURSORHOST
#
			$sthpriv->[TD_STH_ROWID] = substr($$rspmsg, $pos+$pclhdrsz,
				$tdlen - $pclhdrsz) . pack('L', $sthpriv->[TD_STH_REQNO]);
		}
		elsif (($tdflavor == PclTITLESTART) ||
			($tdflavor == PclFORMATSTART) ||
			($tdflavor == PclSIZESTART)) {
			$sthpriv->[TD_STH_IGNORE_PCLS] = 1;
		}
		elsif (($tdflavor == PclTITLEEND) ||
			($tdflavor == PclFORMATEND) ||
			($tdflavor == PclSIZEEND)) {
			$sthpriv->[TD_STH_IGNORE_PCLS] = undef;
		}
		elsif ($tdflavor == PclFIELD) {
			push @{$sthpriv->[TD_STH_FM_RECORD]},
				substr($$rspmsg, $pos+$pclhdrsz, $tdlen - $pclhdrsz)
				unless $sthpriv->[TD_STH_IGNORE_PCLS];
		}
		elsif ($tdflavor == PclNULLFIELD) {
			push @{$sthpriv->[TD_STH_FM_RECORD]}, undef
				unless $sthpriv->[TD_STH_IGNORE_PCLS];
		}
		elsif ($tdflavor == PclRECSTART) {
			last unless $retstr;	# if just realizing...
			$sthpriv->[TD_STH_FM_RECORD] = [ ];
			$arycnt++ if defined($ary);
		}
		elsif ($tdflavor == PclRECEND) {
#
#	terminate loop if pending end stmt
#
			last if $endstmt;
#
#	if single record mode, return the field array
#
			$$retstr = $sthpriv->[TD_STH_FM_RECORD],
			$sthpriv->[TD_STH_RSPBUFPOS] = $pos + $tdlen,
			$sthpriv->[TD_STH_FM_RECORD] = undef,
			return 1
				unless defined($ary);
#
#	if array mode, push the fields and continue
#
			push (@$ary, $sthpriv->[TD_STH_FM_RECORD]);
			$tdflavor = PclNOP;
			$sthpriv->[TD_STH_FM_RECORD] = undef;
			$sthpriv->[TD_STH_RSPBUFPOS] = $pos + $tdlen,
			$#$ary = $maxlen - 1,
			return $maxlen
				if ($arycnt == $maxlen);
		}
		elsif ($tdflavor == PclMULTIPARTREC) {
#
#	accumulate parts until ENDMULTIPARTREC, then treat as a complete
#	record
#
			$multipart .= substr($$rspmsg, $pos+$pclhdrsz, $tdlen - $pclhdrsz);
		}
		elsif (($tdflavor != PclDATAINFO) && ($tdflavor != PclDATAINFOX) &&
			($tdflavor != PclPOSEND) && ($tdflavor != PclSIZE)) {
			$obj->io_set_error("Received bad parcel $tdflavor.");
			delete $obj->[TD_IMPL_REQ2STH]{$sthpriv->[TD_STH_REQNO]};	# unmap reqno-to-sth assoc.
			$sthpriv->[TD_STH_REQNO] = 0;
			$sthpriv->[TD_STH_RSPBUF] = undef;
			$sthpriv->[TD_STH_RSPBUFPOS] = 0;
			$sth->{tdat_more_results} = 0;
			return undef;
		}

		$pos += $tdlen;
		if (($pos >= $rsplen) && ( ! $sthpriv->[TD_STH_LASTRESP])) {
			$rspmsg = $obj->io_tdcontinue($sth, 0);
			return 0 unless $rspmsg;
			$sthpriv->[TD_STH_RSPBUF] = $rspmsg;
			$rsplen = length($$rspmsg);
			$pos = TDAT_HDRSZ;
			$obj->io_tdcontinue($sth, 1)
				if (! $sthpriv->[TD_STH_LASTRESP]) && $obj->[TD_IMPL_TWOBUF];
			$pos = TDAT_HDRSZ;
		}
		($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, $pos);
	}
#
#	return here if being Realize'd or got ENDSTMT
#
	$sthpriv->[TD_STH_RSPBUFPOS] = $pos,
	$sthpriv->[TD_STH_MULTIPART] = $multipart,
	return $total_activity
		unless $retstr;

	if ($endstmt) {
		$sthpriv->[TD_STH_RSPBUFPOS] = $pos;
		return 0
			unless defined($ary) && $arycnt;
#
#	field mode, finalize the array
#
		push (@$ary, $sthpriv->[TD_STH_FM_RECORD]);
		$sthpriv->[TD_STH_FM_RECORD] = undef;
		$sthpriv->[TD_STH_RSPBUFPOS] = $pos + $tdlen;
		$#$ary = $arycnt - 1;
		return $arycnt;
	}

	if (defined($ary)) {
#
#	field mode, in midst of record, so just return
#	what we have
#
		$#$ary = $arycnt - 2,
		return $arycnt
			if $sthpriv->[TD_STH_FM_RECORD];
#
#	otherwise, accumulate records til full or end of buffer
#
		my $i = 0;

		while (($i < $maxlen) && ($pos < $rsplen) &&
			(($tdflavor == PclRECORD) || ($tdflavor == PclENDMULTIPARTREC))) {
#
#	we've got possible overflow issues here! multipart could overflow the length field
#
	 		$$ary[$i++] = ($tdflavor == PclRECORD) ?
		 		join('', pack('S', $tdlen - $pclhdrsz), substr($$rspmsg, $pos+$pclhdrsz, $tdlen - $pclhdrsz), "\12") :
		 		join('', pack('S', 0xFFFF & length($multipart)), $multipart, "\12");

			$pos += $tdlen;
			$multipart = '';
			if ($pos < $rsplen) {
				($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, $pos);
				if (($i < $maxlen) && ($tdflavor == PclMULTIPARTREC)) {
#
#	absorb next multipart; if not followed by ENDMULTIPART,
#	then set pending to the fragment and exit with what we've got
#
					$multipart = substr($$rspmsg, $pos+$pclhdrsz, $tdlen - $pclhdrsz);
					$pos += $tdlen;
					($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, $pos)
						if ($pos < $rsplen);
				}
			}
		}

		$sthpriv->[TD_STH_RSPBUFPOS] = $pos;
		$sthpriv->[TD_STH_MULTIPART] = $multipart;
		$#$ary = $i-1;
		return $i;
	}
#
#	field mode w/ incomplete record, just return nothing
#
	$sthpriv->[TD_STH_RSPBUFPOS] = $pos + $tdlen,
	return 0
		if $sthpriv->[TD_STH_FM_RECORD];

	$$retstr = ($tdflavor == PclENDMULTIPARTREC) ?
		$multipart :
		substr($$rspmsg, $pos+$pclhdrsz, $tdlen - $pclhdrsz);
	$sthpriv->[TD_STH_RSPBUFPOS] = $pos + $tdlen;
	$sthpriv->[TD_STH_MULTIPART] = '';
	return 1;
}
#pragma begin_full
#
#	fetch all results for an array bound request
#
sub io_array_fetch { # sessno
	my ($obj, $sth, $stsary) = @_;
	$obj->[TD_IMPL_LASTERR] = 0;
	$obj->[TD_IMPL_LASTSTATE] = '00000';
	$obj->[TD_IMPL_LASTEMSG] = '';

	my $sthpriv = $sth->{_private};

	return 0 unless $sthpriv->[TD_STH_RSPBUF];	# request exhausted
	my $stmtno = 1;
	my ($tdflavor, $tdlen, $pclhdrsz, $failed) = (0,0,0,0);
#
#	recover the stmt's response buffer info
#
	my $rspmsg = $sthpriv->[TD_STH_RSPBUF];
	my $pos = $sthpriv->[TD_STH_RSPBUFPOS];
	my $rsplen = defined($$rspmsg) ? length($$rspmsg) : 0;
	my $stmtinfo = $sth->{tdat_stmt_info};
	my $stmthash = $stmtinfo->[1];
	my $total_activity = 0;
	if ($pos >= $rsplen) {
#
#	current buffer exhausted, get the next one
#
		$rspmsg = $obj->io_tdcontinue($sth, 0);
		return 0 unless $$rspmsg;
		$sthpriv->[TD_STH_RSPBUF] = $rspmsg;
		$rsplen = length($$rspmsg);
#
#	startup the next continue if not end of request and in auto-continue mode
#
		$obj->io_tdcontinue($sth, 1)
			if (! $sthpriv->[TD_STH_LASTRESP]) && $obj->[TD_IMPL_TWOBUF];
		$pos = TDAT_HDRSZ;
	}
#
#	read response data until a RECORD parcel is recv'd;
#
#	case PclENDREQUEST:
#		clean up buffers
#		commit if ANSI & autocommit
#		remove the stmt handle from the reqno map
#
#	case PclFAILURE, PclERROR:
#		extract & save error msg
#		if not ANSI
#			clean up buffers,
#			remove stmt handle from reqno map,
#			mark xaction complete,
#			return
#
#	case PclSUCCESS:
#		extract and save stmtno, activity, warning (if any)
#		lookup stmtinfo for stmtno and update stmt info hash
#
#	default:
#		cleanup stmt handle buffers
#		return Bad Parcel failure
#
#	we keep track of warnings and activity info on a per-statment
#	basis, but only keep the first occurance of either
#
	my ($rowcnt, $tderr, $fldcount, $activity, $tdelen, $tdemsg);
	($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, $pos);
	$sth->{tdat_more_results} = 1;
	my $stsidx = scalar @$stsary;
#
#	this bit isn't needed, since mstmts aren't supported
#
#	my $stmtsperentry = $#$stmtinfo;
	my $tuple_act = 0;

	while ($tdflavor != PclENDREQUEST) {
		$sthpriv->[TD_STH_RSPBUFPOS] = $pos + $tdlen;

		if (($tdflavor == PclFAILURE) || ($tdflavor == PclERROR)) {
#
#	extract/save error code msg
#
			($stmtno, $rowcnt, $tderr, $tdelen) =
				unpack('SSSS', substr($$rspmsg, $pos+$pclhdrsz, 8));
			$tdemsg = substr($$rspmsg, $pos+$pclhdrsz+8, $tdelen);
			DBI->trace_msg("ERROR $tderr\: $tdemsg\n", 2);
#
#	stmt numbers monotonically increase within the request
#	we modulo the stmtno by the stmts-per-request to find the
#	stmthash entry, and divide by the stmts-per-request to find the
#	tuple status entry (relative to the tuple status array size on entry)
#
			$#$stsary = $stsidx - 1;
			push @$stsary, ((-2) x $stmtno);
#			$stmtno = 1 + ($stmtno % $stmtsperentry)
#				if ($stmtno > $stmtsperentry);
#			$stmthash = $stmtinfo->[$stmtno];

#			$stmtno = unpack('S', substr($$rspmsg, $pos+$pclhdrsz, 2));
#			$stmtno = $stsidx + int(($stmtno - 1)/$stmtsperentry);
			$stsary->[$stsidx + $stmtno - 1] = [ $tderr, $tdemsg,
				(exists $td_sqlstates{$tderr} ? $td_sqlstates{$tderr} : "T$tderr") ];
#				unless $stsary->[$stmtno] && ref $stsary->[$stmtno];
#
#	Any error causes the entire batch to be rolled back
#	(both Teradata and ANSI mode); we assume ANSI mode
#	doesn't rollback entire transaction
#
			$obj->[TD_IMPL_SESINXACT] = 0
				if ($obj->[TD_IMPL_SESMODE] ne 'ANSI');
			delete $obj->[TD_IMPL_REQ2STH]{$sthpriv->[TD_STH_REQNO]};
			$sthpriv->[TD_STH_REQNO] = 0;
			$sthpriv->[TD_STH_RSPBUF] = undef;
			$sthpriv->[TD_STH_RSPBUFPOS] = 0;
			$sth->{tdat_more_results} = 0;
			$sthpriv->[TD_STH_LASTRESP] = 1;
			return undef;
		}
		elsif ($tdflavor == PclSUCCESS) {
			($stmtno, $rowcnt, $tderr, $fldcount, $activity, $tdelen) =
				unpack('SLSSSS', substr($$rspmsg, $pos+$pclhdrsz, 14));

#			$stmtno = $stsidx + int(($stmtno - 1)/$stmtsperentry);
#			if (defined($stsary->[$stmtno])) {
#				$stsary->[$stmtno] += $rowcnt
#					unless ref $stsary->[$stmtno];
#			}
#			else {
				$stsary->[$stsidx + $stmtno - 1] = $rowcnt;
#			}

#			$stmtno = unpack('S', substr($$rspmsg, $pos+$pclhdrsz, 2));

#			$stmtno = 1 + ($stmtno % $stmtsperentry)
#				if ($stmtno > $stmtsperentry);
#			$stmthash = $stmtinfo->[$stmtno];
			$stmthash->{ActivityCount} += $rowcnt;
			$total_activity += $rowcnt;

			$stmthash->{Warning} =
				"Warning $tderr\: " . substr($$rspmsg, $pos+$pclhdrsz+14, $tdelen)
				unless ($tdelen == 0) || exists $stmthash->{Warning};
		}

		$pos += $tdlen;
		if (($pos >= $rsplen) && ( ! $sthpriv->[TD_STH_LASTRESP])) {
			$rspmsg = $obj->io_tdcontinue($sth, 0);
			return 0 unless $rspmsg;
			$sthpriv->[TD_STH_RSPBUF] = $rspmsg;
			$rsplen = length($$rspmsg);
			$pos = TDAT_HDRSZ;
			$obj->io_tdcontinue($sth, 1)
				if (! $sthpriv->[TD_STH_LASTRESP]) && $obj->[TD_IMPL_TWOBUF];
			$pos = TDAT_HDRSZ;
		}
		($tdflavor, $tdlen, $pclhdrsz) = _getPclHeader($$rspmsg, $pos);
	}
	$sthpriv->[TD_STH_RSPBUF] = undef;
	$sthpriv->[TD_STH_RSPBUFPOS] = 0;
	$sth->{tdat_more_results} = 0;
#
#	in ANSI + AutoCommit mode, send COMMIT
#	NOTE: let response msg set xaction state
#
	$obj->io_tddo('COMMIT WORK'),
	$obj->[TD_IMPL_SESINXACT] = 0
		if ($obj->[TD_IMPL_SESMODE] eq 'ANSI') &&
			$obj->[TD_IMPL_SESINXACT] && $obj->[TD_IMPL_DBH]{AutoCommit};
	$sth->{tdat_more_results} = 0,
# unmap reqno-to-sth assoc. if not EXPORT or KEEPRESP non-updatable cursor
	delete $obj->[TD_IMPL_REQ2STH]{$sthpriv->[TD_STH_REQNO]},
	$sthpriv->[TD_STH_REQNO] = 0
		unless ($obj->[TD_IMPL_SESPART] == TD_PART_EXPORT) ||
			($sth->{tdat_keepresp} && (! $sthpriv->[TD_STH_UPDATABLE]));
	return $total_activity;
}
#pragma end_full

sub io_commit {
#
#	since we now support relaxed mode, we must loop
#	until the response indicates no transaction
#	*and* we shouldn't issue anything unless we're
#	in a transaction!
#
	while ($_[0][TD_IMPL_SESINXACT]) {
		return undef
			unless defined($_[0]->io_tddo($_[0][TD_IMPL_COMMITSQL]));
#
#	ridiculous hack because CLI is such a pile of crap...
#
		$_[0][TD_IMPL_SESINXACT] = 0
			if (ref $_[0][TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli');
	}
	return 1;
}

sub io_rollback {
#
#	no loop required, and no xaction test required
#
	return $_[0]->io_tddo($_[0][TD_IMPL_ABORTSQL]);
}

sub io_err {
#
#	return last error code for session
#
	return $_[0][TD_IMPL_LASTERR];
}

sub io_errstr {
#
#	return last error msg for session
#
	return $_[0][TD_IMPL_LASTEMSG];
}

sub io_state {
#
#	return last error msg for session
#
	return $_[0][TD_IMPL_LASTSTATE];
}
#
#	close the current request
#
sub io_finish {
	my ($obj, $sth) = @_;
#
#	lookup the stmt's request num
#	if I/O object currently servicing the request,
#		build and send ABORT
#	else if ENDREQUEST not recv'd
#		send CANCEL
#
	my $reqno = $sth->{_private}[TD_STH_REQNO];
	my $running = (($reqno) && ($obj->[TD_IMPL_ACTIVE] == $reqno));
	return 1 if (! $running) && $sth->{_private}[TD_STH_LASTRESP] &&
		(! $sth->{tdat_keepresp});

	if ($obj->[TD_IMPL_CONNFD]) {
#pragma begin_full
		if (ref $obj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli') {
#pragma end_full
			$obj->[TD_IMPL_CONNFD]->cli_end_request($reqno);
#pragma begin_full
		}
		else {

			my $reqmsg = $obj->[TD_IMPL_REQFAC]->finishRequest($obj, $running ? COPKINDABORT : COPKINDCONTINUE, $reqno);
			return undef unless $reqmsg;

			$obj->io_tdsend($reqmsg) or
				(close($obj->[TD_IMPL_CONNFD]) && return 1);
#
#	wait for response, then clean out the stmt
#
			$reqmsg = $obj->io_gettdresp;
		}
#pragma end_full
	}
	delete $obj->[TD_IMPL_REQ2STH]{$reqno};
	return 1;
}

#
#	cancel the current request, supporting async cancel
#
sub io_cancel {
	my ($obj, $sth) = @_;
#
#	lookup the stmt's request num
#	if I/O object currently servicing the request,
#		build and send ABORT
#	else if ENDREQUEST not recv'd
#		send CANCEL
#
	my $reqno = $sth->{_private}[TD_STH_REQNO];
	my $running = ($reqno && ($obj->[TD_IMPL_ACTIVE] == $reqno));
	return 1 if (! $running) && $sth->{_private}[TD_STH_LASTRESP] &&
		(! $sth->{tdat_keepresp});

#pragma begin_full
	if (ref $obj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli') {
#pragma end_full
		($running) ? $obj->[TD_IMPL_CONNFD]->cli_abort_request($reqno) :
			$obj->[TD_IMPL_CONNFD]->cli_end_request($reqno);
#pragma begin_full
	}
	else {
		my $reqmsg = $obj->finishRequest($obj, $running ? COPKINDABORT : COPKINDCONTINUE, $reqno);
		return undef unless $reqmsg;

		$obj->io_tdsend($reqmsg) or
			(close($obj->[TD_IMPL_CONNFD]) && return 1);
	}
#pragma end_full

#
#	wait for response, then clean out the stmt
#
	$obj->io_gettdresp,
	delete $obj->[TD_IMPL_REQ2STH]{$reqno}
		unless $sth->{tdat_nowait};

	return 1;
}

sub io_FirstAvailList {
# static method returns list of all ready dbh's
#
	my ($sesslist, $timeout) = @_;
	my $i = 0;
	my $rmask = '';
	my $wmask = '';
	my $emask = '';
	my ($rout, $wout, $eout);
	my %fdhash = ();
	my @avails = ();
	my @clilist = ();	# for any CLi based connections
	my %seshash = ();
#
#	build hashmap of fd's to input array indexes
#	and the select() vector
#
	foreach my $obj (@$sesslist) {
		$i++, next unless defined($obj);

		$fdhash{$obj} = $i++,
		vec($rmask, $obj, 1) = 1,
		next
			unless (ref $obj);

		push(@avails, $i++),
		next
			unless $obj->[TD_IMPL_ACTIVE];

#pragma begin_full
		$fdhash{fileno($obj->[TD_IMPL_CONNFD])} = $i++,
		vec($rmask, fileno($obj->[TD_IMPL_CONNFD]), 1) = 1,
		next
			unless (ref $obj->[TD_IMPL_CONNFD] eq 'DBD::Teradata::Cli');
#pragma end_full
		push (@clilist, $i++);
	}
#
#	no active handles/objects, just return
#	what we've got
#
	return ($#avails < 0) ? undef : @avails
		if ($rmask eq '') && ($#clilist < 0);
#
#	since CLI is such stinking pile of dogsh*t,
#	we can only poll the other handles once
#	before we wait on the CLI handles
#
	my $started = time();
	$timeout = undef
		if defined($timeout) && ($timeout < 0);
	my $lcltimeout = ($#clilist >= 0) ? 0 : $timeout;

	if ($rmask ne '') {
#
#	if we've got some file/PP objects, check em out
#
		$wmask = 0;
		$emask = $rmask;
		my $n = select($rout=$rmask, undef, $eout=$emask, $lcltimeout);

		return ($#avails < 0) ? undef : @avails
			if ($n <= 0) && ($#clilist < 0);

		foreach (keys(%fdhash)) {
			push(@avails, $fdhash{$_})
				if vec($rout, $_, 1) || vec($eout, $_, 1);
		}
	}
#
#	if (thankfully) theres no cli's, return whatever we've got
#
	return (($#avails >= 0) ? @avails : undef)
		unless ($#clilist >= 0);
#
#	check the CLI objects
#	!!!NOTE: We need to be careful about resp v. keepresp here!
#	NOTE: CLI does NOT honor timeouts...when we get here, we're going
#	to wait until something happens...if we do manage to get a completion,
#	we check all the connections before returning
#
#		print "Waiting on ", join(', ', @clilist), "\n";
	my @sesobjs = ();
	my ($obj, $reqid, $keepresp);

	$obj = $sesslist->[$_],
	$reqid = $obj->[TD_IMPL_ACTIVE],
	$keepresp = ($obj->[TD_IMPL_REQ2STH] && (exists $obj->[TD_IMPL_REQ2STH]{$reqid})) ?
		$obj->[TD_IMPL_REQ2STH]{$reqid}{tdat_keepresp} : undef,
	push(@sesobjs,
		$_,
		$obj->[TD_IMPL_CONNFD],
		$reqid,
		$keepresp)
		foreach (@clilist);
#
#	returns a list of session numbers
#
	my @list = DBD::Teradata::Cli::cli_wait_for_response($timeout, @sesobjs);
#		print "Got responses on ", join(', ', @avails), "\n";
	push @avails, @list;
	return ($#avails >= 0) ? @avails : undef;
}

sub io_Realize {
	my ($obj, $sth) = @_;
#
#	suck in whatever's waiting
#
	my $rspmsg = $obj->io_gettdresp($sth);
#	return -1;

	return undef unless $rspmsg;
#
#	startup the next continue if not end of request
#
	$obj->io_tdcontinue($sth, 1)
		if (! $sth->{_private}[TD_STH_LASTRESP]) && $obj->[TD_IMPL_TWOBUF];
#pragma begin_full
#
#	for fastexport, emulate double buffering by firing up the next
#	request here if there was no failure on the prior request
#	and the querynumber and block number have been provided
#
	$sth->execute
		if (!$sth->{_private}[TD_STH_FAILED]) &&
			($obj->[TD_IMPL_SESPART] == TD_PART_EXPORT);
#pragma end_full
#
#	use fetch() to scan for first actual record
#
	my $rc = $obj->io_fetch($sth, undef, undef);
	$sth->{Active} = undef,
	return undef
		unless defined($rc);
#
#	return the activity count of whatever statement(s) we currently have
#
	return $rc;
}

sub io_proc_datainfo {	# processes DATAINFO parcels on the fly
	my ($obj, $sth, $pcl, $stmtno, $is_datainfox) = @_;
	my $flds = $is_datainfox ? unpack('L', $pcl) : unpack('S', $pcl);
#pragma begin_full
#
#	notify stmt handle for MONITOR SESSION which unpack string to use
	$sth->{_private}[TD_STH_MONSES] = $flds,
	return $flds
		if ($obj->[TD_IMPL_SESPART] == TD_PART_MONITOR);
#pragma end_full
	my $descr = $is_datainfox 
		? (($platform == COPFORMATATT_3B2) ? 'Sx4L' : 'SLx4') x $flds 
		: 'SS' x $flds;
	my $offset = $is_datainfox ? 4 : 2;
	my @diflds = unpack($descr, substr($pcl, $offset));

	my @ftype = ();		# field type
	my @ftypestr = ();		# field type strings
	my @fprec = ();		# field precision/length
	my @fscale = ();	# field scale (DECIMAL type only)
	my @fnullable = ();	# is field nullable
	my @packstrs = ();	# output data unpack string
	my $prec = 0;
#
#	use fixed start/end info previously defined during
#	prepare()
#
	my $stmtinfo = $sth->{tdat_stmt_info};
	my $names = $sth->{NAME};
	my $stmthash = $$stmtinfo[$stmtno];
	return -1 unless $stmthash;
	my ($i, $j, $t);
	my $starts = $stmthash->{StartsAt};
	for ($i = $starts, $j = 0; $i < ($starts + $flds); $i++, $j += 2) {
#
#	funny little 'feature': everything after the first descriptor
#	is always in Intel format, so we need to swap bytes if things
#	look funny here..
#
#	NOTE: also need to look for SP IN/OUT params
#
		$t = $diflds[$j],
		$diflds[$j] = (($t & 0xFF)<<8) + ($t>>8),
		$t = $diflds[$j + 1],
		$diflds[$j + 1] = (($t & 0xFF)<<8) + ($t>>8)
			if ($diflds[$j] > 1300);
#
#	NOTE: also need to look for SP IN/OUT params
#
		$diflds[$j] -= TDAT_SP_IND if ($diflds[$j] > 800);
		$diflds[$j] &= 0xfffd;
		last unless defined($td_type_code2str{($diflds[$j] & tdat_NULL_MASK)});

		if ($i > $stmthash->{EndsAt}) {
#			print "Got $flds valid fields, but expected only ",
#				($stmthash->{EndsAt} - $starts + 1),
#				" expected.\n";
			return -2;
		}
		$ftype[$i] = $td_type_code2dbi{($diflds[$j] & tdat_NULL_MASK)};
		$ftypestr[$i] = $td_type_code2str{($diflds[$j] & tdat_NULL_MASK)};
		$fnullable[$i] = ($diflds[$j] & 1);
		my $len = $diflds[$j+1];
		$fprec[$i] = ($ftype[$i] == SQL_DECIMAL) ?
			(($len >> 8) & 0xFF) : $len;
		$fscale[$i] = ($ftype[$i] == SQL_DECIMAL) ? ($len & 0xFF) : 0;
#
#	generate packstring for the output fields
#
		$prec = ($ftype[$i] == SQL_DECIMAL) ?
			$td_decszs[(($len >> 8) & 0xFF)] : $len;
		$packstrs[$starts] .=
			(($ftype[$i] == SQL_BINARY) || ($ftype[$i] == SQL_DECIMAL)) ?
				"a$prec " :
			($ftype[$i] == SQL_CHAR) ? "A$prec " :
				$td_type_dbi2pack{$ftype[$i]} . ' ';
#
#	update typestring
#
		$ftypestr[$i] .= '(' . $fprec[$i] .
			(($ftype[$i] == SQL_DECIMAL) ? "$fscale[$i])" : ')')
			if defined($td_type_str2baseprec{$ftypestr[$i]});
	}

	$sth->{TYPE} = \@ftype;
	$sth->{PRECISION} = \@fprec;
	$sth->{SCALE} = \@fscale;
	$sth->{NULLABLE} = \@fnullable;
	$sth->{tdat_TYPESTR} = \@ftypestr;
	$sth->{_private}[TD_STH_UNPACKSTRS] = \@packstrs;

	return $flds;
}
#
#	rewind an open request
sub io_rewind {
	my ($obj, $sth) = @_;
	return undef
		unless ($obj->[TD_IMPL_SESPART] == TD_PART_SQL) &&
			$sth->{_private}[TD_STH_ACTIVE] && $sth->{tdat_keepresp};

	my $reqno = $sth->{_private}[TD_STH_REQNO];
#
#	no more 2 buffer once we rewind...
#
	$obj->[TD_IMPL_TWOBUF] = 0;
#
#	somethings in procgress, wait for completion
#
	return undef if $obj->[TD_IMPL_ACTIVE] && (! $obj->io_gettdresp);
#
#	clean our buffer info
#
	$sth->{_private}[TD_STH_RSPBUF] = undef;
	$sth->{_private}[TD_STH_RSPBUFPOS] = undef;

	my $reqmsg = $obj->[TD_IMPL_REQFAC]->rewindRequest($obj, $reqno, $obj->[TD_IMPL_SESRSPSZ])
		or return undef;

	$obj->io_tdsend($reqmsg) or return undef;

	return $sth->{tdat_nowait} ? -1 : $obj->io_Realize($sth);
}

sub io_clone {
	$inited = 0;
	return 1;
}
#pragma begin_full
sub io_clearPrompt {
	$_[0][TD_IMPL_PROMPTED] = undef;
}

sub io_create_passthru {
	$_[0][TD_IMPL_PASSTHRU] = DBD::Teradata::PassThru->pt_new($_[0]);
	return $_[0];
}
#pragma end_full

1;
