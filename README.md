# DBD::Teradata - DBI driver for Teradata

For detailed documentation refer to the included doc directory, or
[the official website](http://www.presicient.com/tdatdbd).
