package TdTestSSVText;

use Exporter;
use base ('Exporter');

@EXPORT = qw(ssvtexttests);

use strict;
use warnings;

my %ssvqueries = (
simplevt =>
"sel (TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), ''))
) as vartext FROM  ssvtexttst",

wildfirst =>
"select distinct (TRIM(LEADING FROM col1) || '^^' ||
TRIM(LEADING FROM col2) || '^^' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '^^' ||
COALESCE(col4, '') || '^^' ||
COALESCE(col5, '') || '^^' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '^^' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '^^' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '^^' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), '')) || '^^' ||
TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '^^' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), ''))
) as vartext FROM  ssvtexttst a",

wildlast =>
"select all (TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), '')) || '|' ||
TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), ''))
) as vartext FROM  ssvtexttst a",

withwhere =>
"select all (TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), '')) || '|' ||
TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), ''))
) as vartext FROM  ssvtexttst a
where col1 = 3453
order by col2",

withgroup =>
"select all (TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), '')) || '|' ||
TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), ''))
) as vartext FROM  ssvtexttst a
GROUP BY col1
order by col2",

withhaving =>
"select all (TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), '')) || '|' ||
TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), ''))
) as vartext FROM  ssvtexttst
having col1 < col2
order by col2",

withqualify =>
"select all (TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), '')) || '|' ||
TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), ''))
) as vartext FROM  ssvtexttst
qualify csum(col1) < 30
order by col2",

withsample =>
"select distinct top 15.7 percent with ties (TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), ''))
) as vartext FROM  ssvtexttst
sample 20
order by col2",

withorder =>
"select (TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), ''))
) as vartext FROM  ssvtexttst
order by col2"
);


#******** BEGIN BIGINT/BIGDEC QUERIES ***********

my %bigssvqueries = (
simplevt =>
"sel (TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), '')) || '|' ||
TRIM(LEADING FROM COALESCE(col14, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col15, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col16, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col17, ''))
) as vartext FROM  ssvtexttst",

wildfirst =>
"select distinct (TRIM(LEADING FROM col1) || '^^' ||
TRIM(LEADING FROM col2) || '^^' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '^^' ||
COALESCE(col4, '') || '^^' ||
COALESCE(col5, '') || '^^' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '^^' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '^^' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '^^' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col14, '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col15, '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col16, '')) || '^^' ||
TRIM(LEADING FROM COALESCE(col17, '')) || '^^' ||
TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '^^' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), ''))
) as vartext FROM  ssvtexttst a",

wildlast =>
"select all (TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), '')) || '|' ||
TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), '')) || '|' ||
TRIM(LEADING FROM COALESCE(col14, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col15, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col16, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col17, ''))
) as vartext FROM  ssvtexttst a",

withwhere =>
"select all (TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), '')) || '|' ||
TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), '')) || '|' ||
TRIM(LEADING FROM COALESCE(col14, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col15, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col16, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col17, ''))
) as vartext FROM  ssvtexttst a
where col1 = 3453
order by col2",

withgroup =>
"select all (TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), '')) || '|' ||
TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), '')) || '|' ||
TRIM(LEADING FROM COALESCE(col14, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col15, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col16, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col17, ''))
) as vartext FROM  ssvtexttst a
GROUP BY col1
order by col2",

withhaving =>
"select all (TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), '')) || '|' ||
TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), '')) || '|' ||
TRIM(LEADING FROM COALESCE(col14, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col15, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col16, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col17, ''))
) as vartext FROM  ssvtexttst
having col1 < col2
order by col2",

withqualify =>
"select all (TRIM(LEADING FROM COALESCE((current_date(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((current_time(VARCHAR(24))), '')) || '|' ||
TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), '')) || '|' ||
TRIM(LEADING FROM COALESCE(col14, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col15, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col16, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col17, ''))
) as vartext FROM  ssvtexttst
qualify csum(col1) < 30
order by col2",

withsample =>
"select distinct top 15.7 percent with ties (TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), '')) || '|' ||
TRIM(LEADING FROM COALESCE(col14, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col15, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col16, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col17, ''))
) as vartext FROM  ssvtexttst
sample 20
order by col2",

withorder =>
"select (TRIM(LEADING FROM col1) || '|' ||
TRIM(LEADING FROM col2) || '|' ||
TRIM(LEADING FROM COALESCE(col3, '')) || '|' ||
COALESCE(col4, '') || '|' ||
COALESCE(col5, '') || '|' ||
TRIM(LEADING FROM COALESCE(col6, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col7, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col8, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col9, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col10, '')) || '|' ||
TRIM(LEADING FROM COALESCE((col11(VARCHAR(10))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col12(VARCHAR(20))), '')) || '|' ||
TRIM(LEADING FROM COALESCE((col13(VARCHAR(30))), '')) || '|' ||
TRIM(LEADING FROM COALESCE(col14, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col15, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col16, '')) || '|' ||
TRIM(LEADING FROM COALESCE(col17, ''))
) as vartext FROM  ssvtexttst
order by col2"
);

#******** END BIGINT/BIGDEC QUERIES ***********

sub _report {
	if ($_[1]) {
	    print "ok $_[0] # $_[2]\n";
	}
	else {
	    print "notok $_[0] # $_[2]\n";
	}
	$_[0]++;
}

sub ssvtexttests {
	my $dbh = shift;

	my $testno = 1;

	print STDERR "Test Server Side Vartext Translation...\n";
	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	if (!$use_bigdata) {
	$dbh->do(
"CREATE VOLATILE TABLE ssvtexttst(
      col1 INTEGER not null,
      col2 SMALLINT not null,
      col3 BYTEINT,
      col4 CHAR(20),
      col5 VARCHAR(100),
      col6 FLOAT,
      col7 DECIMAL(2,1),
      col8 DECIMAL(4,2),
      col9 DECIMAL(8,4),
      col10 DECIMAL(14,5),
      col11 DATE FORMAT 'YY/MM/DD',
      col12 TIME(6),
      col13 TIMESTAMP(0))
UNIQUE PRIMARY INDEX ( col1 )
ON COMMIT PRESERVE ROWS")
or die $dbh->errstr;
	}
	else {
	$dbh->do(
"CREATE VOLATILE TABLE ssvtexttst(
      col1 INTEGER not null,
      col2 SMALLINT not null,
      col3 BYTEINT,
      col4 CHAR(20),
      col5 VARCHAR(100),
      col6 FLOAT,
      col7 DECIMAL(2,1),
      col8 DECIMAL(4,2),
      col9 DECIMAL(8,4),
      col10 DECIMAL(14,5),
      col11 DATE FORMAT 'YY/MM/DD',
      col12 TIME(6),
      col13 TIMESTAMP(0),
      col14 bigint,
      col15 decimal(18),
      col16 decimal(27,14),
      col17 decimal(38,16)
      )
UNIQUE PRIMARY INDEX ( col1 )
ON COMMIT PRESERVE ROWS")
or die $dbh->errstr;
	}

	$dbh->do('insert into ssvtexttst select * from alltypetst')
		or die $dbh->errstr;

	my $qryhash = $use_bigdata ? \%bigssvqueries : \%ssvqueries;
	my $failed = 0;
#
#	test simple wildcard, no separator
#
my $vtsql = $dbh->tdat_ServerSideVartext('sel * from ssvtexttst');
if ($vtsql) {
$failed++,
warn "Simple wildcard: not a match; out len if ", length($vtsql), " should be ", length($qryhash->{simplevt}), "\noutput was\n
$vtsql\n
*** should be\n
$qryhash->{simplevt}\n"
	unless ($vtsql eq $qryhash->{simplevt});
	_report($testno, 1, 'simple wildcard');
}
else {
	$failed++;
	warn 'Simple wildcard: ' . $dbh->errstr;
}
#
#	test wildcard + columns
#
$vtsql = $dbh->tdat_ServerSideVartext('select distinct a.*, current_date, current_time from ssvtexttst a', '^^');
if ($vtsql) {
$failed++,
warn "Wildcard + columns: not a match; output was\n
$vtsql\n
*** should be\n
$qryhash->{wildfirst}\n"
	unless ($vtsql eq $qryhash->{wildfirst});
	_report($testno, 1, 'Wildcard + columns');
}
else {
	$failed++;
	warn 'Wildcard + columns: ' . $dbh->errstr;
}
#
#	test columns + wildcard
#
$vtsql = $dbh->tdat_ServerSideVartext('select all current_date, current_time, a.* from ssvtexttst a');
if ($vtsql) {
$failed++,
warn "Columns + wildcard: not a match; output was\n
$vtsql\n
*** should be\n
$qryhash->{wildlast}\n"
	unless ($vtsql eq $qryhash->{wildlast});
	_report($testno, 1, 'Columns + wildcard');
}
else {
	$failed++;
	warn 'Columns + wildcard: ' . $dbh->errstr;
}
#
#	test columns + wildcard, with WHERE
#
$vtsql = $dbh->tdat_ServerSideVartext(
'select all current_date, current_time, a.*
from ssvtexttst a
where col1 = 3453
order by col2');
if ($vtsql) {
$failed++,
warn "Columns + wildcard w/ where: not a match; output was\n
$vtsql\n
*** should be\n
$qryhash->{withwhere}\n"
	unless ($vtsql eq $qryhash->{withwhere});

	_report($testno, 1, 'Columns + wildcard w/ where');
}
else {
	$failed++;
	warn 'Columns + wildcard w/ where: ' . $dbh->errstr;
}
#
#	test columns + wildcard, with group
#
$vtsql = $dbh->tdat_ServerSideVartext(
'select all current_date, current_time, a.*
from ssvtexttst a
GROUP BY col1
order by col2');
if ($vtsql) {
$failed++,
warn "Columns + wildcard w/ group: not a match; output was\n
$vtsql\n
*** should be\n
$qryhash->{withgroup}\n"
	unless ($vtsql eq $qryhash->{withgroup});
	_report($testno, 1, 'Columns + wildcard w/ group');
}
else {
	$failed++;
	warn 'Columns + wildcard w/ group: ' . $dbh->errstr;
}
#
#	test columns + wildcard, with HAVING
#
$vtsql = $dbh->tdat_ServerSideVartext(
'select all current_date, current_time, ssvtexttst.*
from ssvtexttst
having col1 < col2
order by col2');
if ($vtsql) {
$failed++,
warn "Columns + wildcard w/ having: not a match; output was\n
$vtsql\n
*** should be\n
$qryhash->{withhaving}\n"
	unless ($vtsql eq $qryhash->{withhaving});

	_report($testno, 1, 'Columns + wildcard w/ having');
}
else {
	$failed++;
	warn 'Columns + wildcard w/ having: ' . $dbh->errstr;
}
#
#	test columns + wildcard, with QUALIFY
#
$vtsql = $dbh->tdat_ServerSideVartext(
'select all current_date, current_time, ssvtexttst.*
from ssvtexttst
qualify csum(col1) < 30
order by col2');
if ($vtsql) {
$failed++,
warn "Columns + wildcard w/ qualify: not a match; output was\n
$vtsql\n
*** should be\n
$qryhash->{withqualify}\n"
	unless ($vtsql eq $qryhash->{withqualify});

	_report($testno, 1, 'Columns + wildcard w/ qualify');
}
else {
	$failed++;
	warn 'Columns + wildcard w/ qualify: ' . $dbh->errstr;
}
#
#	test columns, with SAMPLE
#
$vtsql = $dbh->tdat_ServerSideVartext(
'select distinct top 15.7 percent with ties ssvtexttst.* from ssvtexttst
sample 20
order by col2');
if ($vtsql) {
$failed++,
warn "Columns + wildcard w/ sample: not a match; output was\n
$vtsql\n
*** should be\n
$qryhash->{withsample}\n"
	unless ($vtsql eq $qryhash->{withsample});

	_report($testno, 1, 'Columns + wildcard w/ sample');
}
else {
	$failed++;
	warn 'Columns + wildcard w/ sample: ' . $dbh->errstr;
}
#
#	test columns, with ORDER
#
$vtsql = $dbh->tdat_ServerSideVartext(
'select * from ssvtexttst
order by col2');
if ($vtsql) {
$failed++,
warn "Columns + wildcard w/ order: not a match; output was\n
$vtsql\n
*** should be\n
$qryhash->{withorder}\n"
	unless ($vtsql eq $qryhash->{withorder});
	_report($testno, 1, 'Columns + wildcard w/ order');
}
else {
	$failed++;
	warn 'Columns + wildcard w/ order: ' . $dbh->errstr;
}

if ($failed) {
print STDERR "Server side vartext failed with $failed errors.\n";
}
else {
print STDERR "Server side vartext translate OK.\n";
}
	return 1;
}

1;
