package TdTestUnicode;

use DBI qw(:sql_types);

use Exporter;
use base ('Exporter');

@EXPORT = qw(unitests);

use strict;
use warnings;

my @teststrs = (
'hello in unicode',
'goodbye in unicode',
'this is in unicode'
);

my @testlatinstrs = (
'hello in latin',
'goodbye in latin',
'this is in latin'
);

my $unilen = 21300;
my $latinlen = 100;

sub unitests {
	my $dbh = shift;

	print STDERR "Test UNICODE conversion w/ UTF8 charset...\n";

#$dbh->{tdat_respsize} = 65000 * 2;

	$dbh->do(
'create volatile table tdat_dbd_unitest(
	col1 int,
	col2 char(100) character set unicode,
	col3 varchar(100) character set unicode,
	col4 char(100) character set latin,
	col5 varchar(100) character set latin
) on commit preserve rows;')
		or die "Can't create test table: " . $dbh->errstr . "\n";

	my $sth = $dbh->prepare('insert into tdat_dbd_unitest values(?, ?, ?, ?, ?)')
		or die "Can't prepare: " . $dbh->errstr . "\n";

	$sth->execute(1, 'hello in unicode', 'hello in unicode', 'hello in latin', 'hello in latin')
		or die "Can't execute: " . $sth->errstr . "\n";
	$sth->execute(2, 'goodbye in unicode', 'goodbye in unicode', 'goodbye in latin', 'goodbye in latin')
		or die "Can't execute: " . $sth->errstr . "\n";
	$sth->execute(3, 'this is in unicode', 'this is in unicode', 'this is in latin', 'this is in latin')
		or die "Can't execute: " . $sth->errstr . "\n";

	$sth = $dbh->prepare('select * from tdat_dbd_unitest order by col1')
		or die "Can't prepare: " . $dbh->errstr . "\n";

	unless (($sth->{tdat_CHARSET}[1] eq 'UNICODE') &&
		($sth->{PRECISION}[1] == 100) &&
		($sth->{tdat_CHARSET}[2] eq 'UNICODE') &&
		($sth->{PRECISION}[2] == 100) &&
		($sth->{tdat_CHARSET}[3] eq 'LATIN') &&
		($sth->{PRECISION}[3] == 100) &&
		($sth->{tdat_CHARSET}[4] eq 'LATIN') &&
		($sth->{PRECISION}[4] == 100)) {
		print $sth->{tdat_CHARSET}[$_], ' ', $sth->{PRECISION}[$_], "\n"
			for (1..4);
		die "Invalid character set or precision";
	}

	$sth->execute() or die $sth->errstr;
	my $rows = $sth->fetchall_arrayref()
		or die $sth->errstr;

	my @latinstrs = @testlatinstrs;
	my @unistrs = @teststrs;
	foreach (@$rows) {
		die "Unexpected length at row $_->[0]"
			unless (length($_->[1]) == 100) &&
				(length($_->[2]) <= 100) &&
				(length($_->[3]) == 100) &&
				(length($_->[4]) <= 100);
		my $i = $_->[0] - 1;

		unless (($unistrs[$i] eq $_->[2]) && ($latinstrs[$i] eq $_->[4])) {
			print $unistrs[$i], ": length ", length($unistrs[$i]), "\n",
				$_->[2], " : length ", length($_->[2]), "\n",
				$latinstrs[$i], ": length ", length($latinstrs[$i]), "\n",
				$_->[4], " : length ", length($_->[4]), "\n";
			die "Unexpected varchar string at row $_->[0]";
		}

		$unistrs[$i] .= ' ' x (100 - length($unistrs[$i]));
		$latinstrs[$i] .= ' ' x (100 - length($latinstrs[$i]));
		die "Unexpected char string at row $_->[0]"
			unless ($unistrs[$i] eq $_->[1]) && ($latinstrs[$i] eq $_->[3]);
	}
	print "ok...unicode and latin mix OK\n";
	$dbh->do('drop table tdat_dbd_unitest');
#
#	tests for long UNICODE strings
#
	$dbh->do(
"create volatile table tdat_dbd_unitest(
	col1 int,
	col2 char($unilen) character set unicode,
	col3 char($latinlen) character set latin
) on commit preserve rows")
		or die "Can't create test table: " . $dbh->errstr . "\n";


	$sth = $dbh->prepare('insert into tdat_dbd_unitest values(?, ?, ?)')
		or die "Can't prepare: " . $dbh->errstr . "\n";

	$sth->execute(1, 'hello in unicode', 'hello in latin')
		or die "Can't execute: " . $sth->errstr . "\n";
	$sth->execute(2, 'goodbye in unicode', 'goodbye in latin')
		or die "Can't execute: " . $sth->errstr . "\n";
	$sth->execute(3, 'this is in unicode', 'this is in latin')
		or die "Can't execute: " . $sth->errstr . "\n";

	$sth = $dbh->prepare('select * from tdat_dbd_unitest order by col1')
		or die "Can't prepare: " . $dbh->errstr . "\n";

	@latinstrs = @testlatinstrs;
	@unistrs = @teststrs;
	die "Invalid character set or precision"
		unless ($sth->{tdat_CHARSET}[1] eq 'UNICODE') &&
			($sth->{PRECISION}[1] == $unilen) &&
			($sth->{tdat_CHARSET}[2] eq 'LATIN') &&
			($sth->{PRECISION}[2] == $latinlen);

	$sth->execute() or die $sth->errstr;
	$rows = $sth->fetchall_arrayref()
		or die $sth->errstr;

	foreach (@$rows) {
		print length($_->[1]), ' ', length($_->[2]), "\n" and
		die "Unexpected length at row $_->[0]"
			unless (length($_->[1]) == $unilen) &&
				(length($_->[2]) == $latinlen);

		my $i = $_->[0] - 1;

		$latinstrs[$i] .= ' ' x ($latinlen - length($latinstrs[$i]));
		die "Unexpected latin string at row $_->[0]"
			unless ($latinstrs[$i] eq $_->[2]);
#
#	may need to truncate base if longer than expanded transfer length
#
		if (($sth->{PRECISION}[1] * 3) > 65535) {
			my $trim = ($sth->{PRECISION}[1] * 3) & 0xFFFF;
			$unistrs[$i] = substr($unistrs[$i], 0, $trim)
				if (length($unistrs[$i]) > $trim);
		}
		$unistrs[$i] .= ' ' x ($unilen - length($unistrs[$i]));
		die "Unexpected char string at row $_->[0]"
			unless ($unistrs[$i] eq $_->[1]);
	}

	$dbh->do('drop table tdat_dbd_unitest');
	print "ok...long unicode and latin mix OK\n";
#
#	tests for long UNICODE strings
#
	$dbh->do(
"create volatile table tdat_dbd_unitest(
	col1 int,
	col2 char($unilen) character set unicode,
	col3 char($latinlen) character set latin
) on commit preserve rows")
		or die "Can't create test table: " . $dbh->errstr . "\n";


	$sth = $dbh->prepare('insert into tdat_dbd_unitest values(?, ?, ?)')
		or die "Can't prepare: " . $dbh->errstr . "\n";

	$sth->execute(1, 'hello in unicode', 'hello in latin')
		or die "Can't execute: " . $sth->errstr . "\n";
	$sth->execute(2, 'goodbye in unicode', 'goodbye in latin')
		or die "Can't execute: " . $sth->errstr . "\n";
	$sth->execute(3, 'this is in unicode', 'this is in latin')
		or die "Can't execute: " . $sth->errstr . "\n";

	$sth = $dbh->prepare('select * from tdat_dbd_unitest order by col1', { tdat_formatted => 1 })
		or die "Can't prepare: " . $dbh->errstr . "\n";

	@latinstrs = @testlatinstrs;
	@unistrs = @teststrs;
	die "Invalid character set or precision"
		unless ($sth->{tdat_CHARSET}[1] eq 'UNICODE') &&
			($sth->{PRECISION}[1] == $unilen) &&
			($sth->{tdat_CHARSET}[2] eq 'LATIN') &&
			($sth->{PRECISION}[2] == $latinlen);

	$sth->execute() or die $sth->errstr;
	$rows = $sth->fetchall_arrayref()
		or die $sth->errstr;

	foreach (@$rows) {
		print length($_->[1]), ' ', length($_->[2]), "\n" and
		die "Unexpected length at row $_->[0]"
			unless (length($_->[1]) == $unilen) &&
				(length($_->[2]) == $latinlen);

		my $i = $_->[0] - 1;

		$latinstrs[$i] .= ' ' x ($latinlen - length($latinstrs[$i]));
		die "Unexpected latin string at row $_->[0]"
			unless ($latinstrs[$i] eq $_->[2]);
#
#	may need to truncate base if longer than expanded transfer length
#
		if (($sth->{PRECISION}[1] * 3) > 65535) {
			my $trim = ($sth->{PRECISION}[1] * 3) & 0xFFFF;
			$unistrs[$i] = substr($unistrs[$i], 0, $trim)
				if (length($unistrs[$i]) > $trim);
		}
		$unistrs[$i] .= ' ' x ($unilen - length($unistrs[$i]));
		die "Unexpected char string at row $_->[0]"
			unless ($unistrs[$i] eq $_->[1]);
	}

	$dbh->do('drop table tdat_dbd_unitest');
	print "ok...formatted long unicode and latin mix OK\n";
#
#	test for timestamp and time types
#
	$sth = $dbh->prepare('select current_time, current_timestamp')
		or die $dbh->errstr;

#	print join(', ', @{$sth->{tdat_CHARSET}}), "\n";
	$sth->execute() or die $sth->errstr;
	$rows = $sth->fetchrow_arrayref()
		or die $sth->errstr;

#	print join(', ', @$rows), ' ' , length($rows->[0]), ' ', length($rows->[1]), "\n" and
	die "Unexpected time/timestamp length "
		unless (length($rows->[0]) == 14) && (length($rows->[1]) == 32);

	print "ok...timestamps OK\n";
#
#	tests for long varchar UNICODE strings
#
	$dbh->do(
"create volatile table tdat_dbd_unitest(
	col1 int,
	col2 varchar($unilen) character set unicode,
	col3 varchar($latinlen) character set latin
) on commit preserve rows")
		or die "Can't create test table: " . $dbh->errstr . "\n";

	$sth = $dbh->prepare('insert into tdat_dbd_unitest values(?, ?, ?)')
		or die "Can't prepare: " . $dbh->errstr . "\n";

	my $longuni = 'asdf' x ($unilen >> 2);
	$sth->execute(1, $longuni, 'hello in latin')
		or die "Can't execute: " . $sth->errstr . "\n";
	$sth->execute(2, $longuni, 'goodbye in latin')
		or die "Can't execute: " . $sth->errstr . "\n";
	$sth->execute(3, $longuni, 'this is in latin')
		or die "Can't execute: " . $sth->errstr . "\n";

	$sth = $dbh->prepare('select * from tdat_dbd_unitest order by col1')
		or die "Can't prepare: " . $dbh->errstr . "\n";

	@latinstrs = @testlatinstrs;
	die "Invalid character set or precision"
		unless ($sth->{tdat_CHARSET}[1] eq 'UNICODE') &&
			($sth->{PRECISION}[1] == $unilen) &&
			($sth->{tdat_CHARSET}[2] eq 'LATIN') &&
			($sth->{PRECISION}[2] == $latinlen);

	$sth->execute() or die $sth->errstr;
	$rows = $sth->fetchall_arrayref()
		or die $sth->errstr;

	foreach (@$rows) {
		my $i = $_->[0] - 1;

		print length($_->[1]), ' ', length($_->[2]), "\n" and
		die "Unexpected length at row $_->[0]"
			unless (length($_->[1]) == length($longuni)) &&
				(length($_->[2]) == length($latinstrs[$i]));

		die "Unexpected latin string at row $_->[0]"
			unless ($latinstrs[$i] eq $_->[2]);

		die "Unexpected char string at row $_->[0]"
			unless ($longuni eq $_->[1]);
	}

	$dbh->do('drop table tdat_dbd_unitest');
	print "ok...varchar long unicode and latin mix OK\n";
#
#	tests for too long UNICODE strings
#
	$dbh->do(
"create volatile table tdat_dbd_unitest(
	col1 int,
	col2 char(21850) character set unicode,
	col3 char(100) character set latin
) on commit preserve rows")
		or die "Can't create test table: " . $dbh->errstr . "\n";

	$sth = $dbh->prepare('select * from tdat_dbd_unitest order by col1');
	die "Unexpected successful result\n"
		if $sth || ($dbh->errstr ne "Invalid server truncation of UNICODE field col2");

	$dbh->do('drop table tdat_dbd_unitest');
	print "ok...too long unicode OK\n";

	print STDERR "UNICODE + UTF8 ok\n";
	return 1;
}

1;