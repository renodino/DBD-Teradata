BEGIN {
	push @INC, './t';
}

use Config;
use DBI;
use DBD::Teradata;
use DBD::Teradata::Utility;
use TdTestFastload qw(floadtest);
use TdTestLoopback qw(fllbtest mllbtest);

use strict;
use warnings;

$| = 1;

my $sessions = 2;
my $versnum;
while (substr($ARGV[0], 0, 1) eq '-') {
	my $op = shift @ARGV;
	$sessions = shift @ARGV, next
		if ($op eq '-s');

	$versnum = shift @ARGV, next
		if ($op eq '-v');

	$ENV{TDAT_DBD_NO_CLI} = 1,
	next
		if ($op eq '-c');

	$ENV{TDAT_DBD_DEBUG} = 1,
	DBI->trace(2, shift @ARGV),
	next
		if ($op eq '-d');
}

my ($dsn, $userid, $passwd, $dfltdb) = @ARGV;
$dsn .= ";DATABASE=$dfltdb"
	if $dfltdb;

my $dbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		tdat_charset => 'UTF8',
		tdat_mode => 'TERADATA',
	}
) || die "Can't connect to $dsn: $DBI::errstr. Exiting...\n";

print STDERR "Logon to $dsn ver. " . $dbh->{tdat_version} . '(' . $dbh->{tdat_mode} . " mode) ok.\n";
my $drh = $dbh->{Driver};
print STDERR "DBD::Teradata v. $drh->{Version}\n";

die "Fastexport not supported with CLI adapter, check your configuration."
	if $dbh->{tdat_uses_cli};

print STDERR "Connected via ", ($dbh->{tdat_uses_cli} ? 'CLI' : 'pure Perl'), "\n";

$dbh->{tdat_versnum} = $versnum if $versnum;
#
#	force dateform to integer
#
$dbh->do('set session dateform=integerdate');

print STDERR "Reload table\n";
floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef);

my $fllbstarted = undef;
my $mllbstarted = undef;
my $tfllbstarted = undef;
my $tmllbstarted = undef;

if ($^O ne 'MSWin32') {
	print STDERR "Testing fastload loopback...\n";
	$fllbstarted = fllbtest($dsn, $userid, $passwd, $sessions, $versnum);
	print STDERR "Testing multiload loopback...\n";
	$mllbstarted = mllbtest($dsn, $userid, $passwd, $sessions, $versnum);
}
if ($Config{useithreads}) {
	print STDERR "Testing threaded fastload loopback...\n";
	$tfllbstarted = fllbtest($dsn, $userid, $passwd, $sessions, $versnum, 1);
	print STDERR "Testing threaded multiload loopback...\n";
	$tmllbstarted = mllbtest($dsn, $userid, $passwd, $sessions, $versnum, 1);
}
