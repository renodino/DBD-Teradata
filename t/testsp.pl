BEGIN {
	push @INC, './t';
}

use DBI;
use DBD::Teradata;
use TdTestProcs qw(sptests);

while (substr($ARGV[0], 0, 1) eq '-') {
	my $opt = shift @ARGV;

	$ENV{TDAT_DBD_NO_CLI} = 1,
	next
		if ($opt eq '-c');

	DBI->trace(2, shift @ARGV),
	$ENV{TDAT_DBD_DEBUG} = 1
		if ($opt eq '-d');
}

my ($dsn, $userid, $passwd, $dfltdb) = @ARGV;
$dsn .= ";DATABASE=$dfltdb"
	if $dfltdb;

my $dbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd) || die $DBI::errstr;

sptests($dbh);

$dbh->disconnect();
