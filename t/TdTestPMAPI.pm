package TdTestPMAPI;

use DBI;
use DBI qw(:sql_types);
use Exporter;
use base ('Exporter');

@EXPORT = qw(montest);

use strict;
use warnings;

my %typestr = (
	SQL_VARCHAR, 'VARCHAR',
	SQL_CHAR, 'CHAR',
	SQL_FLOAT, 'FLOAT',
	SQL_DECIMAL, 'DECIMAL',
	SQL_INTEGER, 'INTEGER',
	SQL_SMALLINT, 'SMALLINT',
	SQL_TINYINT, 'TINYINT',
	SQL_VARBINARY, 'VARBINARY',
	SQL_BINARY, 'BINARY',
	SQL_LONGVARBINARY, 'LONG VARBINARY',
	SQL_DATE, 'DATE',
	SQL_TIMESTAMP, 'TIMESTAMP',
	SQL_TIME, 'TIME'
	);

sub montest {
	my ($dbh, $dsn, $user, $passwd, $versnum) = @_;
#
#	logon privileged user
#
	my $mondbh = DBI->connect("dbi:Teradata:$dsn", $user, $passwd, {
		tdat_utility => 'MONITOR',
		tdat_mode => 'TERADATA',
		tdat_charset => 'UTF8'
		});

	$mondbh->{tdat_versnum} = $versnum if $versnum;
	my $sth;
	my $rate = 30;
	my $vers = $dbh->{tdat_versnum};
	my $monver = ($vers >= 6000000) ? 5 : ($vers >= 4010000) ? 4 : 3;
	print "Using version $monver\n";
#
#	set session rate first
#
	my $sessth = $mondbh->prepare('SET SESSION RATE');
	print "Setting session rate to $rate\n";
	$sessth->bind_param(1, $monver);	# version id
	$sessth->bind_param(2, $rate);	# rate
	$sessth->bind_param(3, undef);	# system wide
	dump_results($sessth)
		if $sessth->execute;
#
#	need to set resource monitor rate
#
	my $ressth = $mondbh->prepare('SET RESOURCE RATE');
	print "Setting resource rate to 60...\n";
	$ressth->bind_param(1, $monver);	# version id
	$ressth->bind_param(2, 60);	# rate
	$ressth->bind_param(3, 'N');	# monitor rate
	$ressth->bind_param(4, 'Y');	# for vprocs
	dump_results($ressth)
		if $ressth->execute;

	$ressth->bind_param(1, $monver);	# version id
	$ressth->bind_param(2, 60);	# rate
	$ressth->bind_param(3, 'N');	# monitor only
	$ressth->bind_param(4, 'N');	# for node
	dump_results($ressth)
		if $ressth->execute;
#
#	then wait a while for data to be collected
#
	print "Waiting for sample...\n";
	sleep($rate+2);
#
#	try new version if available
#
	if ($monver > 3) {
		$sth = $mondbh->prepare('MONITOR VERSION');
		$sth->bind_param(1, $monver);	# version id
		print "Fetching MONITOR VERSION results\n";
		dump_results($sth)
			if $sth->execute;

		$sth = $mondbh->prepare('MONITOR SESSION', { tdat_formatted => 0, ChopBlanks => 1 } );
		$sth->bind_param(1, $monver);	# version id
		$sth->bind_param(2, undef);	# rate
		$sth->bind_param(3, undef);	# system wide
		$sth->bind_param(4, undef);	# system wide
		print "Fetching MONITOR SESSION results\n";
		dump_results($sth)
			if $sth->execute;
	}

	print "Waiting for resource sample...\n";
    sleep(100);
#
#	MONITOR PHYS SUM
#
	$sth = $mondbh->prepare('MONITOR PHYSICAL SUMMARY', { tdat_formatted => 0, ChopBlanks => 1 } );
	$sth->bind_param(1, $monver);	# version id
	print "Fetching MONITOR PHYSICAL SUMMARY results\n";
	dump_results($sth)
		if $sth->execute;
#
#	MONITOR PHYS CFG
#
	$sth = $mondbh->prepare('MONITOR PHYSICAL CONFIG', { tdat_formatted => 0, ChopBlanks => 1 } );
	$sth->bind_param(1, $monver);	# version id
	print "Fetching MONITOR PHYSICAL CONFIG results\n";
	dump_results($sth)
		if $sth->execute;
#
#	MONITOR PHYSICAL RESOURCE
#
	$sth = $mondbh->prepare('MONITOR PHYSICAL RESOURCE', { tdat_formatted => 0, ChopBlanks => 1 } );
	$sth->bind_param(1, $monver);	# version id
	print "Fetching MONITOR PHYSICAL RESOURCE results\n";
	dump_results($sth)
		if $sth->execute;
#
#	MONITOR VIRT SUM
#
	$sth = $mondbh->prepare('MONITOR VIRTUAL SUMMARY', { tdat_formatted => 0, ChopBlanks => 1 } );
	$sth->bind_param(1, $monver);	# version id
	print "Fetching MONITOR VIRTUAL SUMMARY results\n";
	dump_results($sth)
		if $sth->execute;
#
#	MONITOR VIRTUAL CFG
#
	$sth = $mondbh->prepare('MONITOR VIRTUAL CONFIG', { tdat_formatted => 0, ChopBlanks => 1 } );
	$sth->bind_param(1, $monver);	# version id
	print "Fetching MONITOR VIRTUAL CONFIG results\n";
	dump_results($sth)
		if $sth->execute;
#
#	MONITOR VIRTUAL RESOURCE
#
	$sth = $mondbh->prepare('MONITOR VIRTUAL RESOURCE', { tdat_formatted => 0, ChopBlanks => 1 } );
	$sth->bind_param(1, $monver);	# version id
	print "Fetching MONITOR VIRTUAL RESOURCE results\n";
	dump_results($sth)
		if $sth->execute;
#
#	test SET ACCOUNT
#
	my $tstdbh = DBI->connect("dbi:Teradata:$dsn", $user, $passwd);

	my $sessno = $tstdbh->{tdat_sessno};
#
#	get our hostid for the subsequent operations
#
	my $s = $tstdbh->prepare('select hostno from dbc.sessioninfo where sessionno=?');
	$s->execute($sessno);
	my $row = $s->fetchrow_arrayref;
	$sth = $mondbh->prepare('SET SESSION ACCOUNT');
	$sth->bind_param(1, $monver);	# version id
	$sth->bind_param(2, $$row[0]);	# hostid
	$sth->bind_param(3, $sessno);	# sessionno
	$sth->bind_param(4, '$H');	# account
	$sth->bind_param(5, 'Y');	# all requests
	print "Fetching SET SESSION ACCOUNT results\n";
	dump_results($sth)
		if $sth->execute;
#
#	verify the session has been updated
#
	print "Wait for session collection\n";
	sleep(32);
	$sth = $mondbh->prepare('MONITOR SESSION');
	$sth->bind_param(1, $monver);	# version id
	$sth->bind_param(2, $$row[0]);	# hostid
	$sth->bind_param(3, undef);	# name
	$sth->bind_param(4, undef);	# sessionno
	print "Fetching MONITOR SESSION results\n";
	dump_results($sth)
		if $sth->execute;
#
#	test IDENTIFY SESSION
#
	$sth = $mondbh->prepare('IDENTIFY SESSION');
	$sth->bind_param(1, $monver);	# version id
	$sth->bind_param(2, $$row[0]);	# hostid
	$sth->bind_param(3, $sessno);	# sessionno
	print "Fetching IDENTIFY SESSION results\n";
	dump_results($sth)
		if $sth->execute;
#
#	test TDWM DELAY REQUEST CHANGE
#
	if ($vers >= 6010000) {
		$sth = $mondbh->prepare('TDWM DELAY REQUEST CHANGE');
		$sth->bind_param(1, $monver);	# version id
		$sth->bind_param(2, 1);			# request flag
		$sth->bind_param(3, $dbh->{tdat_hostid}); # hostid
		$sth->bind_param(4, 0); 		# reserved: must be zero!
		$sth->bind_param(5, $dbh->{tdat_sessno});	# sessionno
		$sth->bind_param(6, 1234);		# requestno
		$sth->bind_param(7, 0);			# workclass id
		print "Fetching TDWM DELAY REQUEST CHANGE results\n";
	# probably won't get any results, since we're using a bogus requestno
		dump_results($sth)
			if $sth->execute;
	}
#
#	test ABORT SESSION
#
	$sth = $mondbh->prepare('ABORT SESSION');
	$sth->bind_param(1, $monver);	# version id
	$sth->bind_param(2, $$row[0]);	# hostid
	$sth->bind_param(3, $sessno);	# sessionno
	$sth->bind_param(4, undef);	# user
	$sth->bind_param(5, 'Y');	# return list
	$sth->bind_param(6, 'Y');	# logoff
	$sth->bind_param(7, undef);	# fail override
	print "Fetching ABORT SESSION results\n";
	dump_results($sth)
		if $sth->execute;
#
#	catch SIGPIPE or UNIXen may die unexpectedly here
#
	$SIG{PIPE} = sub { print "Got SIGPIPE!\n"; };
	$tstdbh->disconnect;
#
#	clear session rate when done
#
	print "Turning off session and resource sampling...\n";
	$ressth->bind_param(1, $monver);	# version id
	$ressth->bind_param(2, 0);	# rate
	$ressth->bind_param(3, 'N');	# monitor rate
	$ressth->bind_param(4, 'Y');	# for vprocs
	dump_results($ressth)
		if $ressth->execute;

	$ressth->bind_param(1, $monver);	# version id
	$ressth->bind_param(2, 0);	# rate
	$ressth->bind_param(3, 'N');	# monitor only
	$ressth->bind_param(4, 'N');	# for node
	dump_results($ressth)
		if $ressth->execute;

	$sessth->bind_param(1, $monver);	# version id
	$sessth->bind_param(2, 0);	# rate
	$sessth->bind_param(3, undef);	# system wide
	dump_results($sessth)
		if $sessth->execute;
	$mondbh->disconnect;
	print "MONITOR testing complete.\n";
}

sub dump_results {
	my ($sth) = @_;
	my $names = $sth->{NAME};
	my $types = $sth->{TYPE};
	my $stmthash;
	my $row;
	while ($sth->{tdat_more_results}) {
		$stmthash = $sth->{tdat_stmt_info}->[$sth->{tdat_stmt_num}];
		print 'For statement ', $sth->{tdat_stmt_num}, ":\n";

		while ($row = $sth->fetchrow_arrayref) {
			print $$names[$_], '(', $typestr{$$types[$_]}, '): ',
				(defined($$row[$_]) ? $$row[$_] : 'NULL'), "\n"
				foreach ($$stmthash{StartsAt}..$$stmthash{EndsAt});
			print "\n";
		}
	}
}

1;
