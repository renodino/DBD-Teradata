BEGIN {
	push @INC, './t';
}

use Config;
use DBI;
use DBD::Teradata;
use DBD::Teradata::Utility;
use TdTestFastload qw(floadtest);
use TdTestExport qw(fexptest);

use strict;
use warnings;

$| = 1;

my $sessions = 2;
my $versnum;
while (substr($ARGV[0], 0, 1) eq '-') {
	my $op = shift @ARGV;
	$sessions = shift @ARGV, next
		if ($op eq '-s');

	$versnum = shift @ARGV, next
		if ($op eq '-v');

	$ENV{TDAT_DBD_NO_CLI} = 1,
	next
		if ($op eq '-c');

	$ENV{TDAT_DBD_DEBUG} = 1,
	DBI->trace(2, shift @ARGV),
	next
		if ($op eq '-d');
}

my ($dsn, $userid, $passwd, $dfltdb) = @ARGV;
$dsn .= ";DATABASE=$dfltdb"
	if $dfltdb;

my $dbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		tdat_charset => 'UTF8',
		tdat_mode => 'TERADATA',
	}
) || die "Can't connect to $dsn: $DBI::errstr. Exiting...\n";

print STDERR "Logon to $dsn ver. " . $dbh->{tdat_version} . '(' . $dbh->{tdat_mode} . " mode) ok.\n";
my $drh = $dbh->{Driver};
print STDERR "DBD::Teradata v. $drh->{Version}\n";

#die "Fastexport not supported with CLI adapter, check your configuration."
#	if $dbh->{tdat_uses_cli};

print STDERR "Connected via ", ($dbh->{tdat_uses_cli} ? 'CLI' : 'pure Perl'), "\n";

$dbh->{tdat_versnum} = $versnum if $versnum;
#
#	force dateform to integer
#
$dbh->do('set session dateform=integerdate');

print STDERR "Reload table\n";
floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef);

###################################################
#
#	test FEXP
#
###################################################

my $sfestarted = undef;
my $rfestarted = undef;
my $vfestarted = undef;
my $mpsfestarted = undef;
my $mpvfestarted = undef;
my $tmpsfestarted = undef;
my $tmpvfestarted = undef;

print STDERR "Testing fastexport...\n";
$sfestarted = fexptest($dsn, $userid, $passwd, $sessions, $versnum);

print STDERR "Testing rawmode fastexport...\n";
$rfestarted = fexptest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode');

print STDERR "Testing vartext fastexport...\n";
$vfestarted = fexptest($dsn, $userid, $passwd, $sessions, $versnum, 'varout.txt');

print STDERR "Testing server-side vartext fastexport...\n";
$vfestarted = fexptest($dsn, $userid, $passwd, $sessions, $versnum, 'varout.txt', undef, undef, 1);

if ($^O ne 'MSWin32') {
#
#	process based
#
	print STDERR "Testing rawmode MP fastexport...\n";
	$mpsfestarted = fexptest($dsn, $userid, $passwd, $sessions, $versnum, undef, 1);
}
if ($Config{useithreads}) {
#
#	thread based
#
	print STDERR "Testing threaded rawmode MP fastexport...\n";
	$tmpsfestarted = fexptest($dsn, $userid, $passwd, $sessions, $versnum, undef, 1, 1);
}

$dbh->disconnect;

sub cleanup {
	my $dbh = shift;

$dbh->do( 'DROP TABLE alltypetst');
die $dbh->errstr
	if $dbh->err && ($dbh->err != 3807);

my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

if (!$use_bigdata) {
$dbh->do( 'CREATE TABLE alltypetst, NO FALLBACK (
col1 integer,
col2 smallint,
col3 byteint,
col4 char(20) character set unicode,
col5 varchar(100) character set unicode,
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0))
unique primary index(col1);'
) || die ($dbh->errstr . "\n");
}
else {
$dbh->do( 'CREATE TABLE alltypetst, NO FALLBACK (
col1 integer,
col2 smallint,
col3 byteint,
col4 char(20) character set unicode,
col5 varchar(100) character set unicode,
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0),
col14 bigint,
col15 decimal(18,0),
col16 decimal(27,14),
col17 decimal(38,16)
)
unique primary index(col1);'
) || die ($dbh->errstr . "\n");
}

}