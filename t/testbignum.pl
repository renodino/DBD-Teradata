BEGIN {
	push @INC, './t';
}

use DBI;
use DBD::Teradata;
use TdTestBigInt qw(numtests);

use strict;
use warnings;

$| = 1;

numtests();

print STDERR "Tests completed OK.\n";