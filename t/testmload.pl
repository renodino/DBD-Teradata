BEGIN {
	push @INC, './t';
}

use Config;
use DBI;
use DBD::Teradata;
use DBD::Teradata::Utility;
use TdTestFastload qw(floadtest);
use TdTestMultiload qw(mloadtest mloadtest_ups);
use TdTestDataGen qw(gen_test_data gen_big_test_data);

use strict;
use warnings;

$| = 1;

my $sessions = 2;
my $versnum;
my $upsert;
while (substr($ARGV[0], 0, 1) eq '-') {
	my $op = shift @ARGV;
	$sessions = shift @ARGV, next
		if ($op eq '-s');

	$versnum = shift @ARGV, next
		if ($op eq '-v');

	$ENV{TDAT_DBD_NO_CLI} = 1,
	next
		if ($op eq '-c');

	$ENV{TDAT_DBD_DEBUG} = 1,
	DBI->trace(2, shift @ARGV),
	next
		if ($op eq '-d');

	$upsert = 1, next
		if ($op eq '-u');

}

my ($dsn, $userid, $passwd, $dfltdb) = @ARGV;
$dsn .= ";DATABASE=$dfltdb"
	if $dfltdb;

###################################################
#
#	start multiload test
#
###################################################
my $smlstarted = undef;
my $rmlstarted = undef;
my $vmlstarted = undef;
my $mpsmlstarted = undef;
my $mprmlstarted = undef;
my $mpvmlstarted = undef;
my $tmpsmlstarted = undef;
my $tmprmlstarted = undef;
my $tmpvmlstarted = undef;
my $mlstarted = undef;
my $supsstarted = undef;
my $rupsstarted = undef;
my $vupsstarted = undef;
my $mpsupsstarted = undef;
my $mprupsstarted = undef;
my $mpvupsstarted = undef;
my $tmpsupsstarted = undef;
my $tmprupsstarted = undef;
my $tmpvupsstarted = undef;

my $dbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd, {PrintError => 0}) or die $DBI::errstr;

$dbh->do('RELEASE MLOAD ALLTYPETST');

die "Can't release: " . $dbh->errstr . "\n"
	if $dbh->err && ($dbh->err != 2580);

my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

(!$use_bigdata) ? gen_test_data() : gen_big_test_data()
	unless (-e 'rawdata.dat') && (-e 'utf8data.txt') &&
		(-s 'rawdata.dat') && (-s 'utf8data.txt');

print STDERR "Testing simple multiload...\n";
$smlstarted = mloadtest($dsn, $userid, $passwd, $sessions, $versnum, undef);
sleep 5;

print STDERR "Testing simple rawmode multiload...\n";
$rmlstarted = mloadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode');
sleep 5;

print STDERR "Testing simple vartext multiload...\n";
$vmlstarted = mloadtest($dsn, $userid, $passwd, $sessions, $versnum, 'mlvardata.txt');
sleep 5;

if ($^O ne 'MSWin32') {
#
#	process based
#
	print STDERR "Testing simple MP multiload...\n";
	$mpsmlstarted = mloadtest($dsn, $userid, $passwd, $sessions, $versnum, undef, 1);
	sleep 5;

	print STDERR "Testing simple MP rawmode multiload...\n";
	$mprmlstarted = mloadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', 1);
	sleep 5;

	print STDERR "Testing simple MP vartext multiload...\n";
	$mpvmlstarted = mloadtest($dsn, $userid, $passwd, $sessions, $versnum, 'mlvardata.txt', 1);
	sleep 5;
}

if ($Config{useithreads}) {
#
#	thread based
#
	print STDERR "Testing threaded simple MP multiload...\n";
	$tmpsmlstarted = mloadtest($dsn, $userid, $passwd, $sessions, $versnum, undef, 1, 1);
	sleep 5;

	print STDERR "Testing threaded simple MP rawmode multiload...\n";
	$tmprmlstarted = mloadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', 1, 1);
	sleep 5;

	print STDERR "Testing threaded simple MP vartext multiload...\n";
	$tmpvmlstarted = mloadtest($dsn, $userid, $passwd, $sessions, $versnum, 'mlvardata.txt', 1, 1);
	sleep 5;
}

if ($upsert) {
print STDERR "Testing upsert multiload...\n";
print STDERR "Reloading table...\n";
floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef);
$supsstarted = mloadtest_ups($dsn, $userid, $passwd, $sessions, $versnum, undef);
sleep 5;

print STDERR "Testing upsert rawmode multiload...\n";
print STDERR "Reloading table...\n";
floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef);
$rupsstarted = mloadtest_ups($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode');
sleep 5;

print STDERR "Testing upsert vartext multiload...\n";
print STDERR "Reloading table...\n";
floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef);
$vupsstarted = mloadtest_ups($dsn, $userid, $passwd, $sessions, $versnum, 'mlvardata.txt');
sleep 5;

if ($^O ne 'MSWin32') {
#
#	process based
#
	print STDERR "Testing upsert MP multiload...\n";
	print STDERR "Reloading table...\n";
	floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef, 1);
	$mpsupsstarted = mloadtest_ups($dsn, $userid, $passwd, $sessions, $versnum, undef, 1);
	sleep 5;

	print STDERR "Testing upsert MP rawmode multiload...\n";
	print STDERR "Reloading table...\n";
	floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef, 1);
	$mprupsstarted = mloadtest_ups($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', 1);
	sleep 5;

	print STDERR "Testing upsert MP vartext multiload...\n";
	print STDERR "Reloading table...\n";
	floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef, 1);
	$mpvupsstarted = mloadtest_ups($dsn, $userid, $passwd, $sessions, $versnum, 'mlvardata.txt', 1);
	sleep 5;
}

if ($Config{useithreads}) {
#
#	thread based
#
	print STDERR "Testing threaded upsert MP multiload...\n";
	print STDERR "Reloading table...\n";
	floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef, 1, 1);
	$tmpsupsstarted = mloadtest_ups($dsn, $userid, $passwd, $sessions, $versnum, undef, 1, 1);
	sleep 5;

	print STDERR "Testing threaded upsert MP rawmode multiload...\n";
	print STDERR "Reloading table...\n";
	floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef, 1, 1);
	$tmprupsstarted = mloadtest_ups($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', 1, 1);
	sleep 5;

	print STDERR "Testing threaded upsert MP vartext multiload...\n";
	print STDERR "Reloading table...\n";
	floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef, 1, 1);
	$tmpvupsstarted = mloadtest_ups($dsn, $userid, $passwd, $sessions, $versnum, 'mlvardata.txt', 1, 1);
	sleep 5;
}
}