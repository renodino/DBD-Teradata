package TdTestBigInt;

use DBI qw(:sql_types);
use DBD::Teradata::BigNum qw(
	cvt_dec2flt 
	cvt_dec2bigint 
	cvt_int64tobigint 
	cvt_flt2dec
	cvt_bigint2int64
	cvt_bigint2dec);

use Exporter;
use base ('Exporter');

@EXPORT = qw(dectests numtests);

use strict;
use warnings;
use bytes;

my @decstrs = (
# 2,0
'0',
'1',
'-1',
'10',
'-10',
'99',
'-99',

# 2,1
'0.0',
'0.1',
'-0.1',
'1.0',
'-1.0',
'9.9',
'-9.9',

# 4,0
'0',
'1',
'-1',
'100',
'-100',
'9999',
'-9999',

# 4,2
'0.00',
'0.01',
'-0.01',
'1.00',
'-1.00',
'99.99',
'-99.99',

# 9,0
'0',
'1',
'-1',
'10000',
'-10000',
'999999999',
'-999999999',

# 9,4
'0.0000',
'0.0001',
'-0.0001',
'1.0000',
'-1.0000',
'99999.9999',
'-99999.9999',

# 15,0
'0',
'1',
'-1',
'10000000000',
'-10000000000',
'999999999999999',
'-999999999999999',

# 15, 10
'0.0000000000',
'0.0000000001',
'-0.0000000001',
'1.0000000000',
'-1.0000000000',
'99999.9999999999',
'-99999.9999999999',

# 18,0
'0',
'1',
'-1',
'10000000000',
'-10000000000',
'999999999999999999',
'-999999999999999999',

# 18, 10
'0.0000000000',
'0.0000000001',
'-0.0000000001',
'1.0000000000',
'-1.0000000000',
'99999999.9999999999',
'-99999999.9999999999',

# 19,0
'0',
'1',
'-1',
'10000000000',
'-10000000000',
'9999999999999999999',
'-9999999999999999999',

# 19,10
'0.0000000000',
'0.0000000001',
'-0.0000000001',
'1.0000000000',
'-1.0000000000',
'999999999.9999999999',
'-999999999.9999999999',

# 38,18
'0',
'1',
'-1',
'1000000000000000000',
'-1000000000000000000',
'99999999999999999999999999999999999999',
'-99999999999999999999999999999999999999',

# 38,18
'0.000000000000000000',
'0.000000000000000001',
'-0.000000000000000001',
'1.000000000000000000',
'-1.000000000000000000',
'99999999999999999999.999999999999999999',
'-99999999999999999999.999999999999999999',


# bigint
0,
1,
-1,
'1234567890123456789',
'-1234567890123456789',
'9223372036854775807',
'-9223372036854775808',
);

my @decvals = (
# 2,0
"\0",
"\1",
"\377",
"\12",
"\366",
"\143",
"\235",

# 2,1
"\0",
"\1",
"\377",
"\12",
"\366",
"\143",
"\235",

# 4,0
"\0\0",
"\0\1",
"\377\377",
"\0\144",
"\377\234",
"\047\017",
"\330\361",

# 4,2
"\0\0",
"\0\1",
"\377\377",
"\0\144",
"\377\234",
"\047\017",
"\330\361",

# 9,0
"\0\0\0\0",
"\0\0\0\1",
"\377\377\377\377",
"\0\0\047\020",
"\377\377\330\360",
"\73\232\311\377",
"\304\145\66\1",

# 9,4
"\0\0\0\0",
"\0\0\0\1",
"\377\377\377\377",
"\0\0\047\020",
"\377\377\330\360",
"\73\232\311\377",
"\304\145\66\1",

# 15, 0
"\0" x 8,
("\0" x 7) . "\1",
"\377\377\377\377\377\377\377\377",
"\0\0\0\2\124\13\344\0",
"\377\377\377\375\253\364\34\0",
"\0\3\215\176\244\306\177\377",
"\377\374\162\201\133\71\200\1",

#"\15\340\266\263\247\143\377\377",
#"\362\37\111\114\130\234\0\1",

# 15, 10
"\0" x 8,
("\0" x 7) . "\1",
"\377\377\377\377\377\377\377\377",
"\0\0\0\2\124\13\344\0",
"\377\377\377\375\253\364\34\0",
"\0\3\215\176\244\306\177\377",
"\377\374\162\201\133\71\200\1",

#"\15\340\266\263\247\143\377\377",
#"\362\37\111\114\130\234\0\1",

# 18, 0
"\0" x 8,
("\0" x 7) . "\1",
"\377\377\377\377\377\377\377\377",
"\0\0\0\2\124\13\344\0",
"\377\377\377\375\253\364\34\0",
"\15\340\266\263\247\143\377\377",
"\362\37\111\114\130\234\0\1",

# 18, 10
"\0" x 8,
("\0" x 7) . "\1",
"\377\377\377\377\377\377\377\377",
"\0\0\0\2\124\13\344\0",
"\377\377\377\375\253\364\34\0",
"\15\340\266\263\247\143\377\377",
"\362\37\111\114\130\234\0\1",

# 19,0
"\0" x 16,
("\0" x 15) . "\1",
"\377" x 16,
("\0" x 11) . "\2\124\13\344\0",
("\377" x 11) . "\375\253\364\34\0",
"\0\0\0\0\0\0\0\0\212\307\43\4\211\347\377\377",
"\377\377\377\377\377\377\377\377\165\70\334\373\166\30\0\1",
  
# 19,10
"\0" x 16,
("\0" x 15) . "\1",
"\377" x 16,
("\0" x 11) . "\2\124\13\344\0",
("\377" x 11) . "\375\253\364\34\0",
"\0\0\0\0\0\0\0\0\212\307\43\4\211\347\377\377",
"\377\377\377\377\377\377\377\377\165\70\334\373\166\30\0\1",
  
# 38,0
"\0" x 16,
("\0" x 15) . "\1",
"\377" x 16,
"\0\0\0\0\0\0\0\0\15\340\266\263\247\144\0\0",
"\377\377\377\377\377\377\377\377\362\37\111\114\130\234\0\0",
"\113\73\114\250\132\206\304\172\11\212\42\77\377\377\377\377", 
"\264\304\263\127\245\171\73\205\366\165\335\300\0\0\0\1",
  
# 38,18
"\0" x 16,
("\0" x 15) . "\1",
"\377" x 16,
"\0\0\0\0\0\0\0\0\15\340\266\263\247\144\0\0",
"\377\377\377\377\377\377\377\377\362\37\111\114\130\234\0\0",
"\113\73\114\250\132\206\304\172\11\212\42\77\377\377\377\377", 
"\264\304\263\127\245\171\73\205\366\165\335\300\0\0\0\1",
  
# bigint
"\0" x 8,
("\0" x 7) . "\1",
"\377" x 8,
"\21\42\20\364\175\351\201\25",
"\356\335\357\13\202\26\176\353",
"\177" . ("\377" x 7), 
"\200" . ("\0" x 7),
);

my @bigdec_qrys = (
'create volatile table dectest(
	col1 int, 
	col2 decimal(2,0),
	col3 decimal(2,1),
	col4 decimal(4,0),
	col5 decimal(4,2),
	col6 decimal(9,0),
	col7 decimal(9,4),
	col8 decimal(15,0),
	col9 decimal(15,10),
	col10 decimal(18,0),
	col11 decimal(18,10),
	col12 decimal(19,0),
	col13 decimal(19,10),
	col14 decimal(38,0),
	col15 decimal(38,18),
	col16 bigint)
	on commit preserve rows',
'insert into dectest values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
);

my @dec15_qrys = (
'create volatile table dectest(
	col1 int, 
	col2 decimal(2,0),
	col3 decimal(2,1),
	col4 decimal(4,0),
	col5 decimal(4,2),
	col6 decimal(9,0),
	col7 decimal(9,4),
	col8 decimal(15,0),
	col9 decimal(15,10))
	on commit preserve rows',
'insert into dectest values(?, ?, ?, ?, ?, ?, ?, ?, ?)'
);

my @dec18_qrys = (
'create volatile table dectest(
	col1 int, 
	col2 decimal(2,0),
	col3 decimal(2,1),
	col4 decimal(4,0),
	col5 decimal(4,2),
	col6 decimal(9,0),
	col7 decimal(9,4),
	col8 decimal(15,0),
	col9 decimal(15,10),
	col10 decimal(18,0),
	col11 decimal(18,10))
	on commit preserve rows',
'insert into dectest values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
);


sub dectests {
	my $dbh = shift;
#
#	Stored procedures tests
#
	my $has_bigdec = $dbh->{tdat_max_dec_prec};
	my $limit_prec = 0;
#
#	check if this Perl can handle large floats
#
	print STDERR "Test DECIMAL conversion w/ and wo/ Math::BigInt with max precision $has_bigdec \n";
	$limit_prec = 1,
	print STDERR "Due to float conversion limits, no_bigint tests are limited to max precision 15\n"
		unless ('999999999999999999' eq cvt_flt2dec('999999999999999999', 0x1200));

	if (unpack('S', pack('n', 1234)) != 1234) {
		$decvals[$_] = reverse($decvals[$_])
			for (7..104);
	}
	my $queries = ($has_bigdec > 18) ? \@bigdec_qrys : ($has_bigdec > 15) ? \@dec18_qrys : \@dec15_qrys;
	my $checker = ($has_bigdec > 18) ? \&bigdec_check : ($has_bigdec > 15) ? \&dec18_check : \&dec15_check;
	my @offsets = ($has_bigdec > 18) ? map 7 * $_, 0..14 : ($has_bigdec > 15) ? map 7 * $_, 0..9 : map 7 * $_, 0..7;

	foreach my $flag (0..1) {
		print "Setting tdat_no_bigint to $flag\n";
		$dbh->{tdat_no_bigint} = $flag;
		
		if ($flag && $limit_prec) {
			$queries = \@dec15_qrys;
			$checker = \&dec15_check;
			@offsets = map 7 * $_, 0..7;
			$has_bigdec = 0;
		}

		$dbh->do( $queries->[0] )
			or die "Can't create test table: " . $dbh->errstr . "\n";
		my $sth = $dbh->prepare($queries->[1])
			or die "Can't prepare: " . $dbh->errstr . "\n";

		$sth->bind_param(1, undef, SQL_INTEGER);
		$sth->bind_param(2, undef, { TYPE => SQL_DECIMAL, PRECISION => 2, SCALE => 0});
		$sth->bind_param(3, undef, { TYPE => SQL_DECIMAL, PRECISION => 2, SCALE => 1});
		$sth->bind_param(4, undef, { TYPE => SQL_DECIMAL, PRECISION => 4, SCALE => 0});
		$sth->bind_param(5, undef, { TYPE => SQL_DECIMAL, PRECISION => 4, SCALE => 2});
		$sth->bind_param(6, undef, { TYPE => SQL_DECIMAL, PRECISION => 9, SCALE => 0});
		$sth->bind_param(7, undef, { TYPE => SQL_DECIMAL, PRECISION => 9, SCALE => 4});
		$sth->bind_param(8, undef, { TYPE => SQL_DECIMAL, PRECISION => 15, SCALE => 0});
		$sth->bind_param(9, undef, { TYPE => SQL_DECIMAL, PRECISION => 15, SCALE => 10});

		if ($has_bigdec > 15) {
			$sth->bind_param(10, undef, { TYPE => SQL_DECIMAL, PRECISION => 18, SCALE => 0});
			$sth->bind_param(11, undef, { TYPE => SQL_DECIMAL, PRECISION => 18, SCALE => 10});
		}
		if ($has_bigdec > 18) {
			eval {
			$sth->bind_param(12, undef, { TYPE => SQL_DECIMAL, PRECISION => 19, SCALE => 0})
				or die $sth->errstr;
			$sth->bind_param(13, undef, { TYPE => SQL_DECIMAL, PRECISION => 19, SCALE => 10})
				or die $sth->errstr;
			$sth->bind_param(14, undef, { TYPE => SQL_DECIMAL, PRECISION => 38, SCALE => 0})
				or die $sth->errstr;
			$sth->bind_param(15, undef, { TYPE => SQL_DECIMAL, PRECISION => 38, SCALE => 18})
				or die $sth->errstr;
			$sth->bind_param(16, undef, SQL_BIGINT)
				or die $sth->errstr;
			};
			if ($@) {
				die $@ unless $flag;
				next;
			}
		}

		for my $i (0..6) {
			my @parms = map $decstrs[$i + $_], @offsets;
			$sth->execute($i, @parms)
				or die "Can't execute: " . $sth->errstr . "\nStatement:", $sth->{Statement}, 
					"\nNum Params: ", scalar @parms, "\nParams: ", join(',', @parms), "\n";
		}

		my $rows = $dbh->selectall_arrayref('select * from dectest order by col1')
			or die $dbh->errstr;
#
#	very strange stuff...0.00789 does not equal 0.00789...*until* we assign it to
#	another variable ???? and *only* when we use float conversion!!!
#	so maybe its a float precision issue ???
#	anyway don't validate float convert here, since its a bit different
#
		my $failed = join('', map $checker->($_), @$rows);

		$dbh->do('drop table dectest');

		die "Row $failed did not match\n" if $failed ne '';
		if ($flag) {
			print "*** nobigint ok\n";
		}
		else {
			print "*** bigint ok\n";
		}
	}
	$dbh->{tdat_no_bigint} = 0;
	print STDERR "DECIMAL conversion ok\n";
	return 1;
}

sub bigdec_check {
	my $row = shift;	
	my $s = $row->[0];
	return (($decstrs[$s] == $row->[1]) &&
			($decstrs[$s + 7] == $row->[2]) &&
			($decstrs[$s + 14] == $row->[3]) &&
			($decstrs[$s + 21] == $row->[4]) &&
			($decstrs[$s + 28] == $row->[5]) &&
			($decstrs[$s + 35] == $row->[6]) &&
			($decstrs[$s + 42] eq $row->[7]) &&
			($decstrs[$s + 49] eq $row->[8]) &&
			($decstrs[$s + 56] eq $row->[9]) &&
			($decstrs[$s + 63] eq $row->[10]) &&
			($decstrs[$s + 70] eq $row->[11]) &&
			($decstrs[$s + 77] eq $row->[12]) &&
			($decstrs[$s + 84] eq $row->[13]) &&
			($decstrs[$s + 91] eq $row->[14]) &&
			($decstrs[$s + 98] eq $row->[15]))
			? ''
			: "$s: wanted " . join(', ', map $decstrs[$s + ($_ * 7)], 0..14) .
				"\n\tgot " . join(', ', map $row->[$_], 1..15) . "\n";
}

sub dec18_check {
	my $row = shift;	
	my $s = $row->[0];
	return (($decstrs[$s] == $row->[1]) &&
			($decstrs[$s + 7] == $row->[2]) &&
			($decstrs[$s + 14] == $row->[3]) &&
			($decstrs[$s + 21] == $row->[4]) &&
			($decstrs[$s + 28] == $row->[5]) &&
			($decstrs[$s + 35] == $row->[6]) &&
			($decstrs[$s + 42] eq $row->[7]) &&
			($decstrs[$s + 49] eq $row->[8]) &&
			($decstrs[$s + 56] eq $row->[9]) &&
			($decstrs[$s + 63] eq $row->[10]))
			? ''
			: "$s: wanted " . join(', ', map $decstrs[$s + ($_ * 7)], 0..9) .
				"\n\tgot " . join(', ', map $row->[$_], 1..10) . "\n";
}

sub dec15_check {
	my $row = shift;	
	my $s = $row->[0];
	return (($decstrs[$s] == $row->[1]) &&
			($decstrs[$s + 7] == $row->[2]) &&
			($decstrs[$s + 14] == $row->[3]) &&
			($decstrs[$s + 21] == $row->[4]) &&
			($decstrs[$s + 28] == $row->[5]) &&
			($decstrs[$s + 35] == $row->[6]) &&
			($decstrs[$s + 42] eq $row->[7]) &&
			($decstrs[$s + 49] eq $row->[8]))
			? ''
			: "$s: wanted " . join(', ', map $decstrs[$s + ($_ * 7)], 0..7) .
				"\n\tgot " . join(', ', map $row->[$_], 1..8) . "\n";
}

#
#	test the BigNum package
#
sub numtests {
	my @precs = (2, 2, 4, 4, 9, 9, 15, 15, 18, 18, 19, 19, 38, 38);
	my @scales = (0, 1, 0, 2, 0, 4, 0, 10, 0, 10, 0, 10, 0, 18);
	my @packprecs = (0x200, 0x201, 0x400, 0x402, 0x900, 0x904, 0xF00, 0xF0A, 0x1200, 0x120A, 0x1300, 0x130A, 0x2600, 0x2612);
	my @offsets = map $_ * 7, 0..13;
	my $val;	

	if (unpack('S', pack('n', 1234)) != 1234) {
		$decvals[$_] = reverse($decvals[$_])
			for (7..104);
	}

	for my $i (0..13) {
#
#	NOTE: large precision unsupported by float conversion method
#
		for (0..6) {
#		print ">>> $decstrs[$_ + $offsets[$i]], $precs[$i], $scales[$i]:\n";
			$val = cvt_dec2flt($decvals[$_ + $offsets[$i]], $precs[$i], $scales[$i]);
			print "cvt_dec2flt($decstrs[$_ + $offsets[$i]], $precs[$i], $scales[$i]) failed\n(got $val, expected $decstrs[$_ + $offsets[$i]])\n"
				unless ($i >= 6) || 
					($decstrs[$_ + $offsets[$i]] == $val) ||
					($decstrs[$_ + $offsets[$i]] eq $val);

			$val = cvt_dec2bigint($decvals[$_ + $offsets[$i]], $precs[$i], $scales[$i]);
			die "cvt_dec2bigint($decstrs[$_ + $offsets[$i]], $precs[$i], $scales[$i]) failed\n(got $val, expected $decstrs[$_ + $offsets[$i]])\n"
				unless ($decstrs[$_ + $offsets[$i]] eq $val);

			$val = cvt_flt2dec($decstrs[$_ + $offsets[$i]], $packprecs[$i]);
			dumpval($val, $decstrs[$_ + $offsets[$i]], $packprecs[$i]),
			die "cvt_flt2dec($decstrs[$_ + $offsets[$i]], $precs[$i], $scales[$i]) failed\n"
				unless ($i >= 6) || ($decvals[$_ + $offsets[$i]] eq $val);

			$val = cvt_bigint2dec($decstrs[$_ + $offsets[$i]], $packprecs[$i]);
			die "cvt_bigint2dec($decstrs[$_ + $offsets[$i]], $precs[$i], $scales[$i]) failed\n"
				unless ($decvals[$_ + $offsets[$i]] eq $val);
		}
	}

	for (98..104) {
#	print ">>> cvt_int64tobigint($decstrs[$_]):\n";
		$val = cvt_int64tobigint($decvals[$_]);
		die "cvt_int64tobigint($decstrs[$_]) failed\n(got $val, expected $decstrs[$_])\n"
			unless ($decstrs[$_] eq $val);

#	print ">>> cvt_bigint2int64($decstrs[$_]):\n";
		$val = cvt_bigint2int64($decstrs[$_]);
		die "cvt_bigint2int64($decstrs[$_]) failed\n"
			unless ($decvals[$_] eq $val);
	}
}

sub dumpval {
	print join(',', map oct($_), split '', $_[0]), "\n";
	cvt_flt2dec($_[1], $_[2]);
}
1;