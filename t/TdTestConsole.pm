package TdTestConsole;

use DBI;
use Exporter;
use base ('Exporter');

@EXPORT = qw(consoletest);

use strict;
use warnings;

sub consoletest {
	my ($dbh, $dsn, $user, $passwd) = @_;
#
#	if no DBSCONTROL macro, create it
#
	my $sth = $dbh->prepare("select 1 from dbc.tablesx where databasename='CONSOLE' and
	tablename='DBSCONTROL' and tablekind='M'");
	my $rc = $sth->execute;
	return undef unless defined($rc);
	unless ($rc != 0) {
		print "Creating DBSCONTROL in CONSOLE";
		print STDERR "Can't create DBSCONTROL: " . $dbh->errstr . "\n" and
		return undef
			unless $dbh->do('create macro CONSOLE.DBSCONTROL AS (;);');
		print STDERR "Can't grant execute on DBSCONTROL: " . $dbh->errstr . "\n" and
		return undef
			unless $dbh->do("grant execute on CONSOLE.DBSCONTROL to $user");
	}

	my $consdbh = DBI->connect('dbi:Teradata:' . $dsn, $user, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		AutoCommit => 1,
		tdat_utility => 'DBCCONS',
		tdat_charset => 'UTF8'
	});

	my $consth = $consdbh->prepare("start DBSCONTROL");
	$consth->execute;
#
#	now prepare an empty statement to be used to
#	interact with the console utility
#
	my $coniosth = $consdbh->prepare(';');
#
#	retrieve and display any utility startup banner
#
	my ($line, $row, $stmtinfo);
	$line = $$row[0],
	$line=~tr/\r/\n/,
	print $line
		while ($row=$sth->fetchrow_arrayref);

	my $resp;
	my @resps = ('help', 'quit');
	while (1) {
#
#	execute the empty statement to solicit
#	utility output until a Prompt indication is received
#
		$rc = $resp ? $coniosth->execute($resp) : $coniosth->execute;
		$resp = undef;
		print "Utility ending\n",
		last
			unless defined($rc);

		$stmtinfo = $coniosth->{tdat_stmt_info}->[1];
#
#	fetch the result and print
#
		$line = $$row[0],
		$line=~tr/\r/\n/,
		print $line
			while ($row=$coniosth->fetchrow_arrayref);
#
#	if we got a Prompt indication, then its time
#	to get some input
#
		next unless $stmtinfo->{Prompt};
		$resp = shift @resps ;
	}

	$consdbh->disconnect;
}

1;