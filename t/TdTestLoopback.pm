package TdTestLoopback;

use DBI;
use Time::HiRes qw(time);

use Exporter;
use base ('Exporter');

@EXPORT = qw(fllbtest mllbtest);

use strict;
use warnings;

sub fllbtest {
	my ($dsn, $userid, $passwd, $sessions, $versnum, $usethrd) = @_;
	my $lbstarted = 0;
	print "Test loopback...\n";
	my $ctldbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		AutoCommit => 1,
		tdat_lsn => 0,
		tdat_charset => 'UTF8'
	});

	$ctldbh->{tdat_versnum} = $versnum if $versnum;
	my $use_bigdata = ($ctldbh->{tdat_has_bignum} && ($ctldbh->{tdat_max_dec_prec} > 18));

	my $fedbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		AutoCommit => 1,
		tdat_lsn => 0,
		tdat_charset => 'UTF8'
	});

	$fedbh->{tdat_versnum} = $versnum if $versnum;

	$ctldbh->do( 'DROP TABLE lbtypetst');
	die $ctldbh->errstr if (defined($ctldbh->err) && ($ctldbh->err != 3807));
	print STDERR $ctldbh->errstr . "\n" if defined($ctldbh->err);

	$ctldbh->do('SET SESSION DATEFORM=INTEGERDATE');
	$fedbh->do('SET SESSION DATEFORM=INTEGERDATE');
	$ctldbh->do( 'DROP TABLE lbtypetst_err1');
	$ctldbh->do( 'DROP TABLE lbtypetst_err2');

	if (!$use_bigdata) {
	$ctldbh->do(
'CREATE TABLE lbtypetst, NO FALLBACK (
	col1 integer,
	col2 smallint,
	col3 byteint,
	col4 char(20) CHARACTER SET UNICODE,
	col5 varchar(100) CHARACTER SET UNICODE,
	col6 float,
	col7 decimal(2,1),
	col8 decimal(4,2),
	col9 decimal(8,4),
	col10 decimal(14,5),
	col11 date,
	col12 time,
	col13 timestamp(0)
) unique primary index(col1);'
	) || die ($ctldbh->errstr . "\n");
	}
	else {
	$ctldbh->do(
'CREATE TABLE lbtypetst, NO FALLBACK (
	col1 integer,
	col2 smallint,
	col3 byteint,
	col4 char(20) CHARACTER SET UNICODE,
	col5 varchar(100) CHARACTER SET UNICODE,
	col6 float,
	col7 decimal(2,1),
	col8 decimal(4,2),
	col9 decimal(8,4),
	col10 decimal(14,5),
	col11 date,
	col12 time,
	col13 timestamp(0),
	col14 bigint,
	col15 decimal(18),
	col16 decimal(27,14),
	col17 decimal(38,16)
) unique primary index(col1);'
	) || die ($ctldbh->errstr . "\n");
	}	
	my $count = 0;

	my $flargs = {
		Utility => 'FASTLOAD',
		Sessions => $sessions,
#
#	NOTE: since our src table defined col5 as UNICODE,
#	we get back 3 * sizeof(col4) bytes for it;
#	however, the varchar field col5 is still limited to
#	2 * sizeof(col5), since we'll always get UTF8
#	encoding back, which should guarantee no more than 2 * sizeof(col5)
#
		SQL => (!$use_bigdata) ?
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(60),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0))
INSERT INTO lbtypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
			:col7, :col8, :col9, :col10, :col11, :col12, :col13);' :

'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(60),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0),
col14 bigint,
col15 decimal(18),
col16 varchar(30),
col17 varchar(40)
)
INSERT INTO lbtypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
	:col7, :col8, :col9, :col10, :col11, :col12, :col13, :col14, :col15, :col16, :col17);',
		Checkpoint => 2000,
		Loopback => $use_bigdata
			? 'SELECT col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16(varchar(30)), col17(varchar(40)) FROM alltypetst'
			: 'SELECT * FROM alltypetst',
		Source => $fedbh,
		Report => \&report_cb,
		MP => ($usethrd ? 'threads' : 'nothreads')
		};
#
#	use large buffers for newer versions
#
	$flargs->{RequestSize} = ($ctldbh->{tdat_versnum} >= 6000000) ? 600000 : 64256;

	$lbstarted = time;
	my $total = $ctldbh->tdat_UtilitySetup($flargs);

	$lbstarted = int((time - $lbstarted) * 1000)/1000;
	print "Loopback complete, $total rows received in $lbstarted secs\n"
		if defined($total);
	print $ctldbh->errstr, "\n"
		unless defined($total);
	$ctldbh->disconnect;
	print "Loopback OK.\n";
	return $lbstarted;
}

sub mllbtest {
	my ($dsn, $userid, $passwd, $sessions, $versnum, $usethrd) = @_;
	my $lbstarted = 0;
	print "Test mload loopback...\n";
	my $ctldbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		AutoCommit => 1,
		tdat_lsn => 0,
		tdat_charset => 'UTF8'
	});
	$ctldbh->{tdat_versnum} = $versnum if $versnum;
	my $use_bigdata = ($ctldbh->{tdat_has_bignum} && ($ctldbh->{tdat_max_dec_prec} > 18));

	my $fedbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		AutoCommit => 1,
		tdat_lsn => 0,
		tdat_charset => 'UTF8'
	});
	$fedbh->{tdat_versnum} = $versnum if $versnum;

	$ctldbh->do( 'DROP TABLE lbtypetst');
	die $ctldbh->errstr if (defined($ctldbh->err) && ($ctldbh->err != 3807));
	print STDERR $ctldbh->errstr . "\n" if defined($ctldbh->err);

	$ctldbh->do( 'DROP TABLE lbtypetst2');
	die $ctldbh->errstr if (defined($ctldbh->err) && ($ctldbh->err != 3807));
	print STDERR $ctldbh->errstr . "\n" if defined($ctldbh->err);

	$ctldbh->do('SET SESSION DATEFORM=INTEGERDATE');
	$fedbh->do('SET SESSION DATEFORM=INTEGERDATE');

	if (!$use_bigdata) {
	$ctldbh->do(
'CREATE TABLE lbtypetst, NO FALLBACK (
	col1 integer,
	col2 smallint,
	col3 byteint,
	col4 char(20) CHARACTER SET UNICODE,
	col5 varchar(100) CHARACTER SET UNICODE,
	col6 float,
	col7 decimal(2,1),
	col8 decimal(4,2),
	col9 decimal(8,4),
	col10 decimal(14,5),
	col11 date,
	col12 time,
	col13 timestamp(0),
) unique primary index(col1);'
	) || die ($ctldbh->errstr . "\n");

	$ctldbh->do(
'CREATE TABLE lbtypetst2, NO FALLBACK (
	col1 integer,
	col2 smallint,
	col3 byteint,
	col4 char(20) CHARACTER SET UNICODE,
	col5 varchar(100) CHARACTER SET UNICODE,
	col6 float,
	col7 decimal(2,1),
	col8 decimal(4,2),
	col9 decimal(8,4),
	col10 decimal(14,5),
	col11 date,
	col12 time,
	col13 timestamp(0)
) unique primary index(col1);'
	) || die ($ctldbh->errstr . "\n");
	}
	else {
	$ctldbh->do(
'CREATE TABLE lbtypetst, NO FALLBACK (
	col1 integer,
	col2 smallint,
	col3 byteint,
	col4 char(20) CHARACTER SET UNICODE,
	col5 varchar(100) CHARACTER SET UNICODE,
	col6 float,
	col7 decimal(2,1),
	col8 decimal(4,2),
	col9 decimal(8,4),
	col10 decimal(14,5),
	col11 date,
	col12 time,
	col13 timestamp(0),
	col14 bigint,
	col15 decimal(18),
	col16 decimal(27,14),
	col17 decimal(38,16)
) unique primary index(col1);'
	) || die ($ctldbh->errstr . "\n");

	$ctldbh->do(
'CREATE TABLE lbtypetst2, NO FALLBACK (
	col1 integer,
	col2 smallint,
	col3 byteint,
	col4 char(20) CHARACTER SET UNICODE,
	col5 varchar(100) CHARACTER SET UNICODE,
	col6 float,
	col7 decimal(2,1),
	col8 decimal(4,2),
	col9 decimal(8,4),
	col10 decimal(14,5),
	col11 date,
	col12 time,
	col13 timestamp(0),
	col14 bigint,
	col15 decimal(18),
	col16 decimal(27,14),
	col17 decimal(38,16)
) unique primary index(col1);'
	) || die ($ctldbh->errstr . "\n");
	}
	my $count = 0;

	my $flargs = {
		Utility => 'MLOAD',
		Sessions => $sessions,
		SQL => (!$use_bigdata) ?
		[
			'IGNORE DUPLICATE INSERT ROWS;
			DO INSERT FOR MISSING UPDATE ROWS;
			UPDATE lbtypetst SET col5 = :col5 WHERE col1 = :col1;
			INSERT INTO lbtypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
			:col7, :col8, :col9, :col10, :col11, :col12, :col13);',

			'MARK DUPLICATE INSERT ROWS;
			INSERT INTO lbtypetst2 VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
			:col7, :col8, :col9, :col10, :col11, :col12, :col13);'
		] :
		[
			'IGNORE DUPLICATE INSERT ROWS;
			DO INSERT FOR MISSING UPDATE ROWS;
			UPDATE lbtypetst SET col5 = :col5 WHERE col1 = :col1;
			INSERT INTO lbtypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
			:col7, :col8, :col9, :col10, :col11, :col12, :col13, :col14, :col15, :col16, :col17);',

			'MARK DUPLICATE INSERT ROWS;
			INSERT INTO lbtypetst2 VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
			:col7, :col8, :col9, :col10, :col11, :col12, :col13, :col14, :col15, :col16, :col17);'
		],
#
#	NOTE: since our src table defined col5 as UNICODE,
#	we get back 3 * sizeof(col4) bytes for it;
#	however, the varchar field col5 is still limited to
#	2 * sizeof(col5), since we'll always get UTF8
#	encoding back, which should guarantee no more than 2 * sizeof(col5)
#
		SourceFields => (!$use_bigdata) ?
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(60),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0))' :
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(60),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0),
col14 bigint,
col15 decimal(18),
col16 varchar(30),
col17 varchar(40)
)',
		Checkpoint => 2000,
		Loopback => $use_bigdata
			? 'SELECT col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16(varchar(30)), col17(varchar(40)) FROM alltypetst'
			: 'SELECT * FROM alltypetst',
		Source => $fedbh,
		Report => \&report_cb,
		MP => ($usethrd ? 'threads' : 'nothreads')
		};

	$lbstarted = time;
	my $total = $ctldbh->tdat_UtilitySetup($flargs);

	$lbstarted = int((time - $lbstarted) * 1000)/1000;
	print "Loopback complete, $total rows received in $lbstarted secs\n"
		if defined($total);
	print $ctldbh->errstr, "\n"
		unless defined($total);
	$ctldbh->disconnect;
	print "Mload Loopback OK.\n";
	return $lbstarted;
}

sub report_cb {	print $_[0], "\n"; }

1;