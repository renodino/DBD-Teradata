BEGIN {
	push @INC, './t';
}

use Config;
use DBI;
use DBD::Teradata;
use DBD::Teradata::Utility;
use TdTestFastload qw(floadtest);

use strict;
use warnings;

$| = 1;

my $sessions = 2;
my $versnum;
while (substr($ARGV[0], 0, 1) eq '-') {
	my $op = shift @ARGV;
	$sessions = shift @ARGV, next
		if ($op eq '-s');

	$versnum = shift @ARGV, next
		if ($op eq '-v');

	$ENV{TDAT_DBD_NO_CLI} = 1,
	next
		if ($op eq '-c');

	$ENV{TDAT_DBD_DEBUG} = 1,
	DBI->trace(2, shift @ARGV),
	next
		if ($op eq '-d');
}

my ($dsn, $userid, $passwd, $dfltdb) = @ARGV;
$dsn .= ";DATABASE=$dfltdb"
	if $dfltdb;

print STDERR "\n *** Testing fastload...\n";
my $sflstarted = floadtest($dsn, $userid, $passwd, $sessions, $versnum, undef, undef);

print STDERR "\n *** Testing rawmode fastload...\n";
my $rflstarted = floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', undef);

print STDERR "\n *** Testing vartext fastload...\n";
my $vflstarted = floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'utf8data.txt', undef);
#
#	utility module decides whether to use
#	process or thread level parallelism
#
if ($^O ne 'MSWin32') {
#
#	process based
#
	print STDERR "\n *** Testing MP fastload...\n";
	my $mpsflstarted = floadtest($dsn, $userid, $passwd, $sessions, $versnum, undef, 1);
	print STDERR "\n *** Testing MP rawmode fastload...\n";
	my $mprflstarted = floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', 1);
	print STDERR "\n *** Testing MP vartext fastload...\n";
	my $mpvflstarted = floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'utf8data.txt', 1);
}

if ($Config{useithreads}) {
#
#	thread based
#
	print STDERR "\n *** Testing threaded MP fastload...\n";
	my $tmpsflstarted = floadtest($dsn, $userid, $passwd, $sessions, $versnum, undef, 1, 1);

	print STDERR "\n *** Testing threaded MP rawmode fastload...\n";
	my $tmprflstarted = floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'IndicatorMode', 1, 1);

	print STDERR "\n *** Testing threaded MP vartext fastload...\n";
	my $tmpvflstarted = floadtest($dsn, $userid, $passwd, $sessions, $versnum, 'utf8data.txt', 1, 1);
}
