package TdTestDataGen;

use DBD::Teradata::BigNum qw(big_rand big_rand_bin);
use Exporter;
use base ('Exporter');

@EXPORT = qw(gen_test_data collect_recs_each gen_big_test_data collect_big_recs_each);

use strict;
use warnings;

my $alphas = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ ';

my @vtlim = qw(18 12 8 40 200 60 8 14 20 60 10 15 19);

my $bigend;

BEGIN {
	$alphas .= chr($_) foreach (160..250);
	$bigend = (unpack('S', pack('n', 1234)) == 1234);
}

sub gen_test_data {
	print STDERR "Generating test data; this may take a while...\n";
	my ($rawfd, $vtfd);
	open($rawfd, '>rawdata.dat') || die "Can't open rawdata.dat: $!";
	binmode $rawfd;
	open($vtfd, '>:utf8', 'utf8data.txt') || die "Can't open utf8data.txt: $!";
	_write_data(0, 24999, $rawfd, $vtfd);
	close $rawfd;
	close $vtfd;
}

sub gen_rand_data {
#
# col1 integer
# col2 smallint
# col3 byteint
# col4 char(20)
# col5 varchar(100)
# col6 float
# col7 decimal(2,1)
# col8 decimal(4,2)
# col9 decimal(8,4)
# col10 decimal(14,5)
# col11 char(10) date
# col12 char(8) time
# col13 char(19) timestamp
#
	my $inp = shift;
	my @x = (
		$inp,
		$inp%32767,
		$inp%127,
		rndstring(20),
		($inp%20 ? rndstring(int(rand(99))+1) : ''),
#		rndstring(int(rand(99))+1),
		rand(100000),
		int(rand(10)),
		int(rand(100)),
		int(rand(10000)),
		big_rand_bin(14),
		1021121,
		'20:02:58.002',
		'2002-12-04 12:09:47'
	);
#
#	force some negative numbers
#
	my $signum = int(rand(2048));
	for (0..2, 5..8) {
		$x[$_] *= -1 if ($signum & (1 << $_));
	}
	return @x;
}

#
#	updated to reflect UNICODE string sizing
#

sub _write_data {
	my ($start, $end, $rawfd, $vtfd) = @_;

	my $prelen = 2 + 4 + 2 + 1 + 40 + 2 + 8 + 1 + 2 + 4 + 8 + 4 + 15 + 19;
	for ($start..$end) {
		my @x = gen_rand_data($_);
#
#	updated to reflect UNICODE string sizing
#
		use bytes;
		my $len = $prelen + length($x[4]);
		no bytes;

		if ($len > $prelen) {
			print $rawfd pack('SS l s c A40 S/A* d c s l a8 L A15 A19 c', $len, 0, @x, 10);
		}
		else {
			$x[4] = 0;
			print $rawfd pack('Scc l s c A40 S d c s l a8 L A15 A19 c', $len, 8, 0, @x, 10);
			$x[4] = '';
		}

		$x[10] = '2002-11-21';
		$x[9] = big_rand(14,5);
		print $vtfd join('|', @x), "\n";

		print STDERR "Generated $_ rows...\n" unless $_%5000;
	}
}

sub rndstring {
	my($len) = pop(@_);
#
#	input len is number of *chars*
#	we generate string in singlebyte mode, then
#	apply encoding to convert to UTF8, which may return a string
#	with a bytelength up to 2x our reuqested charlen
#
	my $s = pack("A$len", '');
	my $j = 0;

	substr($s, $j, 1) = substr($alphas, rand(length($alphas)), 1),
	$j++
		while ($j < $len);
#
#	make it a UTF string
#
    Encode::from_to($s, "iso-8859-1", "utf-8"); # from legacy to utf-8
    $s = Encode::decode_utf8($s);
	return $s;
}

sub collect_recs {
	my ($fh, $base, $count, $sz) = @_;

	my @ary = ();
	my $i = 0;
	my $s = 0;

	$ary[$i] = readraw($fh),
	$s += length($ary[$i]) + 1,
	$i++
		while (($s < $sz) && ($i < $count));

	$$base += $i;
	return \@ary;
}

sub collect_recs_each {
	my ($base, $count, $mload, $delta) = @_;

	my @arys = ( [], [], [], [], [], [], [], [], [], [], [], [], [] );
	$#$_ = $count-1
		foreach (@arys);

	my $i = 0;
#
# col1 integer
# col2 smallint
# col3 byteint
# col4 char(20)
# col5 varchar(100)
# col6 float
# col7 decimal(2,1)
# col8 decimal(4,2)
# col9 decimal(8,4)
# col10 decimal(14,5)
# col11 char(10) date
# col12 char(8) time
# col13 char(19) timestamp
#
#	compute base record size in the output msg buffer
	my $reclen = 14 + 4 + 2 + 1 + 40 + 2 + 0 + 8 + 1 + 2 + 4 + 8 + 4 + 15 + 26;
	my $ttlsz = 0;
	use bytes;
	while (($i < $count) && ($ttlsz < 63000)) {
		$arys[0][$i] = ($$base + $delta + 300000),
		$arys[1][$i] =  ($$base + $delta  + 300000)%32767,
		$arys[2][$i] =  ($$base + $delta  + 300000)%128,
		$arys[3][$i] =  rndstring(20),
		$arys[4][$i] =  ($$base%20 == 0) ? undef : rndstring(int(rand(99))+1),
		$arys[5][$i] =  rand(100000),
		$arys[6][$i] =  int(rand(99)) * 0.1,
		$arys[7][$i] =  int(rand(9999)) * 0.01,
		$arys[8][$i] =  int(rand(99999999)) * 0.0001,
		$arys[9][$i] =  big_rand(14,5),
		$arys[10][$i] =  1021121,
		$arys[11][$i] =  '20:02:58.002',
		$arys[12][$i] =  '2002-12-04 12:09:47',
		$ttlsz += (($reclen + (defined($arys[4][$i]) ? length($arys[4][$i]) : 0)) * $mload),
		$i++,
		$$base++,
		next
			if (($i%2) && $mload);

		$arys[0][$i] = $$base + $delta ;
		$arys[1][$i] =  ($$base + $delta)%32767;
		$arys[2][$i] =  ($$base + $delta)%128;
		$arys[3][$i] =  $mload ? 'callback mload test ' : rndstring(20);
		$arys[4][$i] =  ($$base%20 == 0) ? undef : rndstring(int(rand(99))+1);
		$arys[5][$i] =  rand(100000);
		$arys[6][$i] =  int(rand(99)) * 0.1;
		$arys[7][$i] =  int(rand(9999)) * 0.01;
		$arys[8][$i] =  int(rand(99999999)) * 0.0001;
		$arys[9][$i] =  big_rand(14,5);
		$arys[10][$i] =  1021121;
		$arys[11][$i] =  '20:02:58.002';
		$arys[12][$i] =  '2002-12-04 12:09:47';
		$ttlsz += (($reclen + (defined($arys[4][$i]) ? length($arys[4][$i]) : 0)) * $mload);
		$i++;
		$$base++;
	}
	no bytes;
#
#	remove empty slots
#
	$i--;
	$#{$arys[$_]} = $i for (0..12);
	return \@arys;
}

#*********************************
#
#	BEGIN BIG DATA GEN
#
#*********************************

sub gen_big_test_data {
	print STDERR "Generating test data w/ bigint and large decimals; this may take a while...\n";
	my ($rawfd, $vtfd);
	open($rawfd, '>rawdata.dat') || die "Can't open rawdata.dat: $!";
	binmode $rawfd;
	open($vtfd, '>:utf8', 'utf8data.txt') || die "Can't open utf8data.txt: $!";
	_write_big_data(0, 24999, $rawfd, $vtfd);
	close $rawfd;
	close $vtfd;
}

sub gen_big_rand_data {
#
# col1 integer
# col2 smallint
# col3 byteint
# col4 char(20)
# col5 varchar(100)
# col6 float
# col7 decimal(2,1)
# col8 decimal(4,2)
# col9 decimal(8,4)
# col10 decimal(14,5)
# col11 char(10) date
# col12 char(8) time
# col13 char(19) timestamp
# col14 bigint
# col15 decimal(18,0)
# col16 decimal(27,14)
# col17 decimal(38,16)
#
	my $inp = shift;
	my @x = (
		$inp,
		$inp%32767,
		$inp%127,
		rndstring(20),
		($inp%20 ? rndstring(int(rand(99))+1) : ''),
#		rndstring(int(rand(99))+1),
		rand(100000),
		int(rand(10)),
		int(rand(100)),
		int(rand(10000)),
		big_rand_bin(14),	# getting packed version anyway, so no scale
		1021121,
		'20:02:58.002',
		'2002-12-04 12:09:47',
		big_rand_bin(),
		big_rand_bin(18),	# getting packed version anyway, so no scale
		big_rand_bin(27),	# getting packed version anyway, so no scale
		big_rand_bin(38)	# getting packed version anyway, so no scale
	);
#
#	force some negative numbers
#
	my $signum = int(rand(65536));
	for (0..2, 5..8) {
		$x[$_] *= -1 if ($signum & (1 << $_));
	}
	return @x;
}

sub collect_big_recs_each {
	my ($base, $count, $mload, $delta) = @_;

	my @arys = ( [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [] );
	$#$_ = $count-1
		foreach (@arys);

	my $i = 0;
#
# col1 integer
# col2 smallint
# col3 byteint
# col4 char(20)
# col5 varchar(100)
# col6 float
# col7 decimal(2,1)
# col8 decimal(4,2)
# col9 decimal(8,4)
# col10 decimal(14,5)
# col11 char(10) date
# col12 char(8) time
# col13 char(19) timestamp
# col14 bigint
# col15 decimal(18,0)
# col16 decimal(27,14)
# col17 decimal(38,16)
#
#	compute base record size in the output msg buffer
	my $reclen = 15 + 4 + 2 + 1 + 40 + 2 + 0 + 8 + 1 + 2 + 4 + 8 + 4 + 15 + 26 + 8 + 8 + 16 + 16;
	my $ttlsz = 0;
	use bytes;
	while (($i < $count) && ($ttlsz < 63000)) {
		$arys[0][$i] = ($$base + $delta + 300000),
		$arys[1][$i] =  ($$base + $delta  + 300000)%32767,
		$arys[2][$i] =  ($$base + $delta  + 300000)%128,
		$arys[3][$i] =  rndstring(20),
		$arys[4][$i] =  ($$base%20 == 0) ? undef : rndstring(int(rand(99))+1),
		$arys[5][$i] =  rand(100000),
		$arys[6][$i] =  int(rand(99)) * 0.1,
		$arys[7][$i] =  int(rand(9999)) * 0.01,
		$arys[8][$i] =  int(rand(99999999)) * 0.0001,
		$arys[9][$i] =  big_rand(14,5),
		$arys[10][$i] =  1021121,
		$arys[11][$i] =  '20:02:58.002',
		$arys[12][$i] =  '2002-12-04 12:09:47',
		$arys[13][$i] =  big_rand(),
		$arys[14][$i] =  big_rand(18),
		$arys[15][$i] =  big_rand(27,14),
		$arys[16][$i] =  big_rand(38,16),
		$ttlsz += (($reclen + (defined($arys[4][$i]) ? length($arys[4][$i]) : 0)) * $mload),
		$i++,
		$$base++,
		next
			if (($i%2) && $mload);

		$arys[0][$i] = $$base + $delta ;
		$arys[1][$i] =  ($$base + $delta)%32767;
		$arys[2][$i] =  ($$base + $delta)%128;
		$arys[3][$i] =  $mload ? 'callback mload test ' : rndstring(20);
		$arys[4][$i] =  ($$base%20 == 0) ? undef : rndstring(int(rand(99))+1);
		$arys[5][$i] =  rand(100000);
		$arys[6][$i] =  int(rand(99)) * 0.1;
		$arys[7][$i] =  int(rand(9999)) * 0.01;
		$arys[8][$i] =  int(rand(99999999)) * 0.0001;
		$arys[9][$i] =  big_rand(14,5);
		$arys[10][$i] =  1021121;
		$arys[11][$i] =  '20:02:58.002';
		$arys[12][$i] =  '2002-12-04 12:09:47';
		$arys[13][$i] =  big_rand();
		$arys[14][$i] =  big_rand(18);
		$arys[15][$i] =  big_rand(27,14);
		$arys[16][$i] =  big_rand(38,16);
		$ttlsz += (($reclen + (defined($arys[4][$i]) ? length($arys[4][$i]) : 0)) * $mload);
		$i++;
		$$base++;
	}
	no bytes;
#
#	remove empty slots
#
	$i--;
	$#$_ = $i foreach (@arys);
	return \@arys;
}

sub _write_big_data {
	my ($start, $end, $rawfd, $vtfd) = @_;

	my $prelen = 3 + 4 + 2 + 1 + 40 + 2 + 8 + 1 + 2 + 4 + 8 + 4 + 15 + 19 + 8 + 8 + 16 + 16;
	for ($start..$end) {
		my @x = gen_big_rand_data($_);
#
#	updated to reflect UNICODE string sizing
#
		use bytes;
		my $len = $prelen + length($x[4]);
		no bytes;

		if ($len > $prelen) {
			print $rawfd pack('SSc l s c A40 S/A* d c s l a8 L A15 A19 a8 a8 a16 a16 c', $len, 0, 0, @x, 10);
		}
		else {
			$x[4] = 0;
			print $rawfd pack('Sccc l s c A40 S d c s l a8 L A15 A19 a8 a8 a16 a16 c', $len, 8, 0, 0, @x, 10);
			$x[4] = '';
		}

		$x[9] =  big_rand(14,5);
		$x[10] = '2002-11-21';
		$x[13] =  big_rand();
		$x[14] =  big_rand(18);
		$x[15] =  big_rand(27,14);
		$x[16] =  big_rand(38,16);

		print $vtfd join('|', @x), "\n";

		print STDERR "Generated $_ rows...\n" unless $_%5000;
	}
}


1;