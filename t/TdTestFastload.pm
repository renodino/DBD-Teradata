package TdTestFastload;

use DBI;
use Time::HiRes qw(time);
use TdTestDataGen qw(collect_recs_each gen_test_data collect_big_recs_each gen_big_test_data);

use Exporter;
use base ('Exporter');

@EXPORT = qw(floadtest);

use strict;
use warnings;

sub floadtest {
	my ($dsn, $userid, $passwd, $sessions, $versnum, $mode, $mp, $usethrd) = @_;
#
#	generate test data if none
#
	my $ctldbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
		{
			PrintError => 0,
			RaiseError => 0,
			AutoCommit => 1,
			tdat_lsn => 0,
			tdat_charset => 'UTF8'
		}
	) || die 'Can\'t logon control session: ' . $DBI::errstr . "\n";

	if ($versnum) {
		$ctldbh->{tdat_versnum} = $versnum;
	}
	else {
		$versnum = $ctldbh->{tdat_versnum};
	}

	my $use_bigdec = ($ctldbh->{tdat_has_bignum} && ($ctldbh->{tdat_max_dec_prec} > 18));
	$use_bigdec ? gen_big_test_data() 
		: gen_test_data()
		unless (-e 'rawdata.dat') && (-e 'utf8data.txt') &&
			(-s 'rawdata.dat') && (-s 'utf8data.txt');

	$ctldbh->do( 'DROP TABLE alltypetst');
	die $ctldbh->errstr if (defined($ctldbh->err) && ($ctldbh->err != 3807));
	print STDERR $ctldbh->errstr . "\n" if defined($ctldbh->err);

	$ctldbh->do('SET SESSION DATEFORM=INTEGERDATE');
	$ctldbh->do( 'DROP TABLE alltypetst_err1');
	$ctldbh->do( 'DROP TABLE alltypetst_err2');

	my $ct = $use_bigdec ?
	'CREATE TABLE alltypetst, NO FALLBACK (
	col1 integer,
	col2 smallint,
	col3 byteint,
	col4 char(20) character set UNICODE,
	col5 varchar(100) character set UNICODE,
	col6 float,
	col7 decimal(2,1),
	col8 decimal(4,2),
	col9 decimal(8,4),
	col10 decimal(14,5),
	col11 date,
	col12 time,
	col13 timestamp(0),
	col14 bigint,
	col15 decimal(18,0),
	col16 decimal(27,14),
	col17 decimal(38,16)
	) unique primary index(col1);'
	:
	'CREATE TABLE alltypetst, NO FALLBACK (
	col1 integer,
	col2 smallint,
	col3 byteint,
	col4 char(20) character set UNICODE,
	col5 varchar(100) character set UNICODE,
	col6 float,
	col7 decimal(2,1),
	col8 decimal(4,2),
	col9 decimal(8,4),
	col10 decimal(14,5),
	col11 date,
	col12 time,
	col13 timestamp(0)
	) unique primary index(col1);';

	$ctldbh->do($ct) || die ($ctldbh->errstr . "\n");
	my $count = 0;

	my $flargs = {
		Utility => 'FASTLOAD',
		Sessions => $sessions,
		SQL => (!$use_bigdec) ?
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0))
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
			:col7, :col8, :col9, :col10, :col11, :col12, :col13);' :

'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0),
col14 bigint,
col15 decimal(18,0),
col16 decimal(27, 14),
col17 decimal(38,16)
)
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
			:col7, :col8, :col9, :col10, :col11, :col12, :col13, 
			:col14, :col15, :col16, :col17);',

		Checkpoint => 2000,
		Report => \&report_cb
		};

	$flargs->{SQL} = (!$use_bigdec) ?
'USING (col1 varchar(9),
col2 varchar(6),
col3 varchar(4),
col4 varchar(40),
col5 varchar(200),
col6 varchar(30),
col7 varchar(4),
col8 varchar(7),
col9 varchar(10),
col10 varchar(30),
col11 varchar(10),
col12 varchar(15),
col13 varchar(19))
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5,
:col6, :col7, :col8, :col9, :col10, :col11, :col12, :col13)' :

'USING (col1 varchar(9),
col2 varchar(6),
col3 varchar(4),
col4 varchar(40),
col5 varchar(200),
col6 varchar(30),
col7 varchar(4),
col8 varchar(7),
col9 varchar(10),
col10 varchar(30),
col11 varchar(10),
col12 varchar(15),
col13 varchar(19),
col14 varchar(40),
col15 varchar(20),
col16 varchar(30),
col17 varchar(40)
)
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5,
:col6, :col7, :col8, :col9, :col10, :col11, :col12, :col13, 
:col14, :col15, :col16, :col17)'
		if defined($mode) && ($mode!~/^Indicator|Record/);
#
#	if direct file based, set file name/type
#
	my $datagen = ($versnum < 6020000) ? \&get_data : \&get_big_data;
	$flargs->{Source} = defined($mode) ?
		(($mode=~/^Indicator|Record/) ? 'INDICDATA rawdata.dat' :
			"VARTEXT '|' $mode") : $datagen;
#
#	only specify callbacks and context if subref based
#
	my $flstarted;
	$flargs->{CheckpointCallback} = \&checkpoint,
	$flargs->{Context} = {
		Count => \$count,
#		Runtime => \$flstarted,
		Base => [ 0, 1000000 ] }
		unless defined($mode);

	$flargs->{MP} = ($usethrd ? 'threads' : 'nothreads') if $mp;
#
#	use large buffers for newer versions
#
	$flargs->{RequestSize} = 600000
		unless $ctldbh->{tdat_uses_cli} || ($ctldbh->{tdat_versnum} < 6000000);

	$flstarted = time;
	my $total = $ctldbh->tdat_UtilitySetup($flargs);
	print "Total is $total\n" if defined($total);
	print $ctldbh->errstr, "\n"
		unless ($total && ($total > 0));

	$flstarted = int((time - $flstarted) * 1000)/1000;
	print "$total rows loaded in $flstarted secs...\n" if $total;
	print "Fastload completed successfully. $total loaded.\n" if $total;
	$ctldbh->disconnect;
#	close $fh if $fh;
	return $flstarted;
}

sub checkpoint {
	my ($function, $rowcount, $ctxt) = @_;

	print "$rowcount FASTLOAD sessions logged on.\n" and
	return 1
	if ($function eq 'INIT');

	print "$rowcount rows loaded.\n" and
	return 1
	if ($function eq 'FINISH');

	print "Check point at $rowcount rows\n" and
	return 1
	if ($function eq 'CHECKPOINT');
	1;
}

sub get_data {
	my ($function, $sth, $sessnum, $maxrows, $ctxt) = @_;
	my ($ary, $rc, $rowcnt);

	if ($function eq 'INIT') {
		$sth->{tdat_raw_in} = $ctxt->{Mode};
		return -1;
	}

	return 0 if (($function eq 'MOREDATA') && (${$ctxt->{Count}} >= 5000));

	if ($function eq 'MOREDATA') {
		$ary = collect_recs_each($ctxt->{Count}, ($maxrows > 280) ? 280 : $maxrows, 0, $ctxt->{Base}[$sessnum]);
		print "Sending " . ($#{$$ary[0]}+1) . " rows for session $sessnum...\n";

		$rc = $sth->bind_param_array($_+1, $$ary[$_])
			foreach (0..$#$ary);

		return $#{$$ary[0]}+1;
	}

	print "Got IO Finish for $sessnum\n" and return -1
		if ($function eq 'FINISH');

	print "Got CHECKPOINT for $sessnum\n" and return -1
		if ($function eq 'CHECKPOINT');

	return 0;
}

sub get_big_data {
	my ($function, $sth, $sessnum, $maxrows, $ctxt) = @_;
	my ($ary, $rc, $rowcnt);

	if ($function eq 'INIT') {
		$sth->{tdat_raw_in} = $ctxt->{Mode};
		return -1;
	}

	return 0 if (($function eq 'MOREDATA') && (${$ctxt->{Count}} >= 5000));

	if ($function eq 'MOREDATA') {
		$ary = collect_big_recs_each($ctxt->{Count}, ($maxrows > 240) ? 240 : $maxrows, 0, $ctxt->{Base}[$sessnum]);
		print "Sending " . ($#{$$ary[0]}+1) . " rows for session $sessnum...\n";

		$rc = $sth->bind_param_array($_+1, $$ary[$_])
			foreach (0..$#$ary);

		return $#{$$ary[0]}+1;
	}

	print "Got IO Finish for $sessnum\n" and return -1
		if ($function eq 'FINISH');

	print "Got CHECKPOINT for $sessnum\n" and return -1
		if ($function eq 'CHECKPOINT');

	return 0;
}

sub report_cb {	print $_[0], "\n"; }

1;