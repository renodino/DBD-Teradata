BEGIN {
	push @INC, './t';
}

use DBI;
use DBD::Teradata;
use TdTestArrayBind qw(
	ary_using_load
	ary_named_using_load
	ary_sth_using_load
	ary_ph_load
	ary_sth_ph_load
	ary_load_using_raw_tuples
	ary_load_using_raw_fetch
	ary_load_using_vartext_tuples
	ary_load_using_vartext_fetch
	ary_error_load
	);
use TdTestDataGen qw(gen_test_data gen_big_test_data);

use strict;
use warnings;

$| = 1;
my $rowcnt;
my $versnum;
my $interact;
while (substr($ARGV[0], 0, 1) eq '-') {
	my $opt = shift @ARGV;

	$ENV{TDAT_DBD_NO_CLI} = 1,
	next
		if ($opt eq '-c');

	DBI->trace(2, shift @ARGV),
	$ENV{TDAT_DBD_DEBUG} = 1
		if ($opt eq '-d');

	$rowcnt = shift @ARGV, next
		if ($opt eq '-r');

	$versnum = shift @ARGV, next
		if ($opt eq '-v');

	$interact = 1, next
		if ($opt eq '-i');
}
my $dbh;
my ($dsn, $userid, $passwd, $dfltdb) = @ARGV;

$dsn .= ";DATABASE=$dfltdb"
	if $dfltdb;

$dbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		tdat_charset => 'UTF8',
		tdat_mode => 'TERADATA',
	}
) || die "Can't connect to $dsn: $DBI::errstr. Exiting...\n";

print STDERR "Logon to $dsn ver. " . $dbh->{tdat_version} . '(' . $dbh->{tdat_mode} . " mode) ok.\n";
my $drh = $dbh->{Driver};
print STDERR "DBD::Teradata v. $drh->{Version}\n";

die "Did not connect with CLI adapter, check your configuration."
	unless $dbh->{tdat_uses_cli} || $ENV{TDAT_DBD_NO_CLI};

print STDERR "Connected via ", ($dbh->{tdat_uses_cli} ? 'CLI' : 'pure Perl'), "\n";

$dbh->{tdat_versnum} = $versnum if $versnum;
#
#	force dateform to integer
#
$dbh->do('set session dateform=integerdate');
#
#	pre-gen some data
#
my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));
(!$use_bigdata) ? gen_test_data() : gen_big_test_data()
	unless (-e 'rawdata.dat') && (-e 'utf8data.txt') &&
		(-s 'rawdata.dat') && (-s 'utf8data.txt');
#
#	clear out the table
#
my @row;
cleanup($dbh);
#
#	force big requests if possible
#
$dbh->{tdat_reqsize} = 500000
	if ($dbh->{tdat_versnum} >= 6000000);
#
#	adjust rowcount for version due to speed difference
#
$rowcnt = ($dbh->{tdat_versnum} >= 6000000) ? 10000 : 1000
	unless $rowcnt;

my $austarted  = ary_using_load($dbh, $rowcnt);

@row = $dbh->selectrow_array('select count(*) from alltypetst');
die $dbh->errstr unless scalar @row;
print "Table has $row[0] rows\n";

#
#	create a copy of the table for use w/ sth sourced methods
#
my $srcdbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		tdat_charset => 'UTF8',
		tdat_mode => 'TERADATA',
	}
) || die "Can't connect to $dsn: $DBI::errstr. Exiting...\n";

$srcdbh->{tdat_versnum} = $versnum if $versnum;
$srcdbh->do(
'create volatile table alltypetst20 as
	(select * from alltypetst) with data
	on commit preserve rows') or die $srcdbh->errstr . "\n";

my $srcsth = $srcdbh->prepare('select * from alltypetst20') or die $srcdbh->errstr . "\n";
my ($rc, $apstarted, $aunstarted, $ausstarted, $apsstarted, );

$apstarted  = ary_ph_load($dbh, $rowcnt);
@row = $dbh->selectrow_array('select count(*) from alltypetst');
die $dbh->errstr unless scalar @row;
print "Table has $row[0] rows\n";

prompt() if $interact;
$aunstarted  = ary_named_using_load($dbh, $rowcnt);
@row = $dbh->selectrow_array('select count(*) from alltypetst');
die $dbh->errstr unless scalar @row;
print "Table has $row[0] rows\n";

prompt() if $interact;
$rc = $srcsth->execute() or die "Execute source query: " . $srcsth->errstr . "\n";
$ausstarted  = ary_sth_using_load($dbh, $srcsth, $rc);
@row = $dbh->selectrow_array('select count(*) from alltypetst');
die $dbh->errstr unless scalar @row;
print "Table has $row[0] rows\n";

prompt() if $interact;
$rc = $srcsth->execute() or die $srcsth->errstr . "\n";
$apsstarted  = ary_sth_ph_load($dbh, $srcsth, $rc);
@row = $dbh->selectrow_array('select count(*) from alltypetst');
die $dbh->errstr unless scalar @row;
print "Table has $row[0] rows\n";

prompt() if $interact;
my $aitstarted  = ary_load_using_raw_tuples($dbh, $rowcnt);
@row = $dbh->selectrow_array('select count(*) from alltypetst');
die $dbh->errstr unless scalar @row;
print "Table has $row[0] rows\n";

prompt() if $interact;
my $aifstarted  = ary_load_using_raw_fetch($dbh, $rowcnt);
@row = $dbh->selectrow_array('select count(*) from alltypetst');
die $dbh->errstr unless scalar @row;
print "Table has $row[0] rows\n";

prompt() if $interact;
my $avtstarted  = ary_load_using_vartext_tuples($dbh, $rowcnt);
@row = $dbh->selectrow_array('select count(*) from alltypetst');
die $dbh->errstr unless scalar @row;
print "Table has $row[0] rows\n";

prompt() if $interact;
my $avfstarted  = ary_load_using_vartext_fetch($dbh, $rowcnt);
@row = $dbh->selectrow_array('select count(*) from alltypetst');
die $dbh->errstr unless scalar @row;
print "Table has $row[0] rows\n";

prompt() if $interact;
ary_error_load($dbh, $rowcnt, 0);	# AutoCommit off

prompt() if $interact;
ary_error_load($dbh, $rowcnt, 1);	# AutoCommit on

$srcdbh->disconnect;

$dbh->disconnect();
#
#	now run error tests in ANSI mode
#
$dbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		tdat_charset => 'UTF8',
		tdat_mode => 'ANSI',
	}
) || die "Can't connect to $dsn: $DBI::errstr. Exiting...\n";

$dbh->{tdat_versnum} = $versnum if $versnum;
#
#	force big requests if possible
#
$dbh->{tdat_reqsize} = 500000
		if ($dbh->{tdat_versnum} >= 6000000);
#
#	force dateform to integer
#
$dbh->do('set session dateform=integerdate');

prompt() if $interact;
ary_error_load($dbh, $rowcnt, 0);	# AutoCommit off

prompt() if $interact;
ary_error_load($dbh, $rowcnt, 1);	# AutoCommit on

########## multistatements not yet supported
if (0) {
my $aumstarted  = ary_using_multistmt($dbh, $rowcnt);

@row = $dbh->selectrow_array('select count(*) from alltypetst');
die $dbh->errstr unless scalar @row;
print "Table has $row[0] rows\n";

my $apmstarted  = ary_ph_multistmt($dbh, $rowcnt);

@row = $dbh->selectrow_array('select count(*) from alltypetst');
die $dbh->errstr unless scalar @row;
print "Table has $row[0] rows\n";
}
########## end multistatements not yet supported

$dbh->disconnect();

sub prompt {
	print "Press enter to continue...\n";
	my $x = <STDIN>;
}

sub cleanup {
	my $dbh = shift;

$dbh->do( 'DROP TABLE alltypetst');
die $dbh->errstr
	if $dbh->err && ($dbh->err != 3807);

if (!$use_bigdata) {
$dbh->do( 'CREATE TABLE alltypetst, NO FALLBACK (
col1 integer,
col2 smallint,
col3 byteint,
col4 char(20) character set unicode,
col5 varchar(100) character set unicode,
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0))
unique primary index(col1);'
) || die ($dbh->errstr . "\n");

$dbh->do( 'CREATE VOLATILE TABLE alltypetst10 (
col1 integer,
col2 smallint,
col3 byteint,
col4 char(20) character set unicode,
col5 varchar(100) character set unicode,
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0))
unique primary index(col1)
on commit preserve rows;'
) || die ($dbh->errstr . "\n");
}
else {
$dbh->do( 'CREATE TABLE alltypetst, NO FALLBACK (
col1 integer,
col2 smallint,
col3 byteint,
col4 char(20) character set unicode,
col5 varchar(100) character set unicode,
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0),
col14 bigint,
col15 decimal(18,0),
col16 decimal(27,14),
col17 decimal(38,16)
)
unique primary index(col1);'
) || die ($dbh->errstr . "\n");

$dbh->do( 'CREATE VOLATILE TABLE alltypetst10 (
col1 integer,
col2 smallint,
col3 byteint,
col4 char(20) character set unicode,
col5 varchar(100) character set unicode,
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0),
col14 bigint,
col15 decimal(18,0),
col16 decimal(27,14),
col17 decimal(38,16)
)
unique primary index(col1)
on commit preserve rows;'
) || die ($dbh->errstr . "\n");
}
}