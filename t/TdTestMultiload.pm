package TdTestMultiload;

use DBI;
use Time::HiRes qw(time);
use TdTestDataGen qw(collect_recs_each collect_big_recs_each);

use Exporter;
use base ('Exporter');

@EXPORT = qw(mloadtest mloadtest_ups);

use strict;
use warnings;

sub mloadtest {
	my ($dsn, $userid, $passwd, $sessions, $versnum, $mode, $mp, $usethrd) = @_;
	my $ctldbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
		{
			PrintError => 0,
			RaiseError => 0,
			AutoCommit => 1,
			tdat_lsn => 0,
			tdat_charset => 'UTF8'
		}
	) || die 'Can\'t logon control session: ' . $DBI::errstr . "\n";

	$ctldbh->{tdat_versnum} = $versnum if $versnum;

#
#	generate mload data if needed
#
	my $use_bigdata = ($ctldbh->{tdat_has_bignum} && ($ctldbh->{tdat_max_dec_prec} > 18));
	gen_mload_data($use_bigdata)
		unless (-e 'mlrawdata.dat' && -e 'mlvardata.txt');
#
#	force integerdate
#
	$ctldbh->do('SET SESSION DATEFORM=INTEGERDATE');
	$ctldbh->do('DELETE FROM ALLTYPETST');

	my $count = 0;
	my %act_counts = ();
	my $mlargs = {
		Utility => 'MLOAD',
		Sessions => $sessions,
		SQL => [ (!$use_bigdata) ?
			'IGNORE DUPLICATE INSERT ROWS;
			INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
				:col7, :col8, :col9, :col10, :col11, :col12, :col13);' :
			'IGNORE DUPLICATE INSERT ROWS;
			INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
				:col7, :col8, :col9, :col10, :col11, :col12, :col13, 
				:col14, :col15, :col16, :col17);' ],
		SourceFields => (!$use_bigdata) ?
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0))' :
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0),
col14 bigint,
col15 decimal(18),
col16 decimal(27,14),
col17 decimal(38,16)
)',
		Checkpoint => 2000,
		ActivityCounts => \%act_counts,
		Report => \&report_cb
		};

	$mlargs->{SourceFields} = (!$use_bigdata) ?
'USING (col1 varchar(9),
col2 varchar(6),
col3 varchar(4),
col4 varchar(40),
col5 varchar(200),
col6 varchar(30),
col7 varchar(4),
col8 varchar(7),
col9 varchar(10),
col10 varchar(30),
col11 varchar(10),
col12 varchar(15),
col13 varchar(19))' :
'USING (col1 varchar(9),
col2 varchar(6),
col3 varchar(4),
col4 varchar(40),
col5 varchar(200),
col6 varchar(30),
col7 varchar(4),
col8 varchar(7),
col9 varchar(10),
col10 varchar(30),
col11 varchar(10),
col12 varchar(15),
col13 varchar(19),
col14 varchar(38),
col15 varchar(20),
col16 varchar(30),
col17 varchar(40)
)'
		if (defined($mode) && ($mode!~/^Indicator|Record/));
#
#	if direct file based, set file name/type
#
	my $datagen = (!$use_bigdata) ? \&ml_get_data : \&ml_get_big_data;
	$mlargs->{Source} = defined($mode) ?
		(($mode=~/^Indicator|Record/) ? 'INDICDATA mlrawdata.dat' :
			"VARTEXT '|' $mode") : $datagen;
#
#	only specify callbacks abd context if subref based
#
	my $mlstarted;

	$mlargs->{CheckpointCallback} = \&mlcheckpoint,
	$mlargs->{Context} = {
		Count => \$count,
#		Runtime => \$mlstarted,
		Base => [ 0, 1000000 ],
		JobCount => 1
	}
		unless defined($mode);

	$mlargs->{MP} = ($usethrd ? 'threads' : 'nothreads') if $mp;

#
#	use large buffers for newer versions
#
	$mlargs->{RequestSize} = 600000
		unless $ctldbh->{tdat_uses_cli} || ($ctldbh->{tdat_versnum} < 6000000);

	$mlstarted = time;

	my $total = $ctldbh->tdat_UtilitySetup($mlargs);

	print $ctldbh->errstr
		unless ($total && ($total > 0));

	$mlstarted = int((time - $mlstarted) * 1000)/1000;
#
#	only track acq. phase time; thats done inside report_cb
#
	print "$total rows inserted via multiload in $mlstarted secs...\n" if $total;

	if ($total) {
		if ($total > 0) {
		print '
Table	Inserted        Updated        Deleted
----------------------------------------------
';
		print "$_: ", $act_counts{$_}->{Inserts},
		"\t", $act_counts{$_}->{Updates},
		"\t", $act_counts{$_}->{Deletes}, "\n"
			foreach (keys %act_counts);
		}

		print "Multiload completed successfully. $total loaded.\n";
	}
	else {
		print $ctldbh->errstr, "\n";
	}
	$ctldbh->disconnect;
	return $mlstarted;
}

sub mloadtest_ups {
	my ($dsn, $userid, $passwd, $sessions, $versnum, $mode, $mp, $usethrd) = @_;
	my $ctldbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
		{
			PrintError => 0,
			RaiseError => 0,
			AutoCommit => 1,
			tdat_lsn => 0,
			tdat_charset => 'UTF8'
		}
	) || die 'Can\'t logon control session: ' . $DBI::errstr . "\n";

	$ctldbh->{tdat_versnum} = $versnum if $versnum;
	my $use_bigdata = ($ctldbh->{tdat_has_bignum} && ($ctldbh->{tdat_max_dec_prec} > 18));
#
#	force integerdate
#
	$ctldbh->do('SET SESSION DATEFORM=INTEGERDATE');
#
#	create our extra table
#
	$ctldbh->do('DROP TABLE ALLTYPETST2');
	$ctldbh->do('DROP TABLE ALLTYPETST3');
	$ctldbh->do('CREATE TABLE ALLTYPETST2 AS (SELECT * FROM ALLTYPETST) WITH NO DATA')
		|| die $ctldbh->errstr;
	$ctldbh->do('CREATE TABLE ALLTYPETST3 AS (SELECT * FROM ALLTYPETST) WITH DATA')
		|| die $ctldbh->errstr;

	my $count = 0;
	my %act_counts = ();
	my $mlargs = {
		Utility => 'MLOAD',
		Sessions => $sessions,
		SQL => (!$use_bigdata) ?
		[
			'DO INSERT FOR MISSING UPDATE ROWS;
			UPDATE alltypetst SET col4 = :col4 WHERE col1 = :col1;
			INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
				:col7, :col8, :col9, :col10, :col11, :col12, :col13);',

			"MARK DUPLICATE INSERT ROWS;
			INSERT INTO alltypetst2 VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
				:col7, :col8, :col9, :col10, :col11, TIME '10:20:30', :col13);",

			'MARK MISSING DELETE ROWS;
			DELETE FROM alltypetst3 WHERE col1 = :col1;'
		] :
		[
			'DO INSERT FOR MISSING UPDATE ROWS;
			UPDATE alltypetst SET col4 = :col4 WHERE col1 = :col1;
			INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
				:col7, :col8, :col9, :col10, :col11, :col12, :col13, :col14, :col15, :col16, :col17);',

			"MARK DUPLICATE INSERT ROWS;
			INSERT INTO alltypetst2 VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
				:col7, :col8, :col9, :col10, :col11, TIME '10:20:30', :col13, :col14, :col15, :col16, :col17);",

			'MARK MISSING DELETE ROWS;
			DELETE FROM alltypetst3 WHERE col1 = :col1;'
		],
		SourceFields => (!$use_bigdata) ?
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0))' :
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0),
col14 bigint,
col15 decimal(18),
col16 decimal(27,14),
col17 decimal(38,16)
)',
		Checkpoint => 2000,
		LogTables => {
			alltypetst => [ 'wt_alltt', 'et_alltt', 'uv_alltt' ],
			alltypetst2 => [ 'wt_alltt2', 'et_alltt2', 'uv_alltt2' ],
			alltypetst3 => [ 'wt_alltt3', 'et_alltt3', 'uv_alltt3' ],
			},
		ActivityCounts => \%act_counts,
		Report => \&report_cb
		};

	$mlargs->{SourceFields} = (!$use_bigdata) ?
'USING (col1 varchar(9),
col2 varchar(6),
col3 varchar(4),
col4 varchar(40),
col5 varchar(200),
col6 varchar(30),
col7 varchar(4),
col8 varchar(7),
col9 varchar(10),
col10 varchar(30),
col11 varchar(10),
col12 varchar(15),
col13 varchar(19))' :
'USING (col1 varchar(9),
col2 varchar(6),
col3 varchar(4),
col4 varchar(40),
col5 varchar(200),
col6 varchar(30),
col7 varchar(4),
col8 varchar(7),
col9 varchar(10),
col10 varchar(30),
col11 varchar(10),
col12 varchar(15),
col13 varchar(19),
col14 varchar(38),
col15 varchar(20),
col16 varchar(30),
col17 varchar(40)
)'
		if (defined($mode) && ($mode!~/^Indicator|Record/));
#
#	if direct file based, set file name/type
#
	my $datagen = (!$use_bigdata) ? \&ml_get_data : \&ml_get_big_data;
	$mlargs->{Source} = defined($mode) ?
		(($mode=~/^Indicator|Record/) ? 'INDICDATA mlrawdata.dat' :
			"VARTEXT '|' $mode") : $datagen;
#
#	only specify callbacks and context if subref based
#
	my $mlstarted;
	$mlargs->{CheckpointCallback} = \&mlcheckpoint,
	$mlargs->{Context} = {
		Count => \$count,
#		Runtime => \$mlstarted,
		Base => [ 0, 1000000 ],
		JobCount => 3
	}
		unless defined($mode);

	$mlargs->{MP} = ($usethrd ? 'threads' : 'nothreads') if $mp;
#
#	use large buffers for newer versions
#
	$mlargs->{RequestSize} = 600000
		unless $ctldbh->{tdat_uses_cli} || ($ctldbh->{tdat_versnum} < 6000000);

	$mlstarted = time;

	my $total = $ctldbh->tdat_UtilitySetup($mlargs);

	print $ctldbh->errstr
		unless ($total && ($total > 0));

	$mlstarted = int((time - $mlstarted) * 1000)/1000;
#
#	only track acq phase time, done inreport_cb
#
	if ($total) {
		print "$total rows multiloaded in $mlstarted secs...\n";
		print "Multiload completed successfully. $total loaded.\n";
#
#	print explicit insert/update/delete counts for each job
#	should use a format here!!!
#
		if ($total > 0) {
		print '
Table	Inserted        Updated        Deleted
----------------------------------------------
';
		print "$_: ", $act_counts{$_}->{Inserts},
		"\t", $act_counts{$_}->{Updates},
		"\t", $act_counts{$_}->{Deletes}, "\n"
			foreach (keys %act_counts);
		}
	}
	else {
		print $ctldbh->errstr, "\n";
	}

	$ctldbh->disconnect;
	return $mlstarted;
}

sub mlcheckpoint {
	my ($function, $rowcount, $ctxt) = @_;

	print "$rowcount MLOAD sessions logged on.\n" and
	return 1
	if ($function eq 'INIT');

	print "$rowcount rows loaded.\n" and
	return 1
	if ($function eq 'FINISH');

	print "Check point at $rowcount rows\n" and
	return 1
	if ($function eq 'CHECKPOINT');
	1;
}

sub ml_get_data {
	my ($function, $sth, $sessnum, $maxrows, $ctxt) = @_;
	my ($ary, $rc, $rowcnt);

	$sth->{tdat_raw} = $ctxt->{Mode}, return -1
		if ($function eq 'INIT');

	return 0 if (($function eq 'MOREDATA') && (${$ctxt->{Count}} >= 10000));

	if ($function eq 'MOREDATA') {
		$ary = collect_recs_each($ctxt->{Count},
			($maxrows > 280) ? 280 : $maxrows, $ctxt->{JobCount}, $ctxt->{Base}->[$sessnum]);
		print "Sending " . ($#{$$ary[0]}+1) . " rows for session $sessnum...\n";
		foreach my $i (0..$#$ary) {
			$rc = $sth->bind_param_array($i+1, $$ary[$i]);
		}

		return $#{$$ary[0]}+1;
	}

	print "Got IO Finish for $sessnum\n" and return -1
		if ($function eq 'FINISH');

	print "Got CHECKPOINT for $sessnum\n" and return -1
		if ($function eq 'CHECKPOINT');

	return 0;
}

sub ml_get_big_data {
	my ($function, $sth, $sessnum, $maxrows, $ctxt) = @_;
	my ($ary, $rc, $rowcnt);

	$sth->{tdat_raw} = $ctxt->{Mode}, return -1
		if ($function eq 'INIT');

	return 0 if (($function eq 'MOREDATA') && (${$ctxt->{Count}} >= 10000));

	if ($function eq 'MOREDATA') {
		$ary = collect_big_recs_each($ctxt->{Count},
			($maxrows > 240) ? 240 : $maxrows, $ctxt->{JobCount}, $ctxt->{Base}->[$sessnum]);
		print "Sending " . ($#{$$ary[0]}+1) . " rows for session $sessnum...\n";
		foreach my $i (0..$#$ary) {
			$rc = $sth->bind_param_array($i+1, $$ary[$i]);
		}

		return $#{$$ary[0]}+1;
	}

	print "Got IO Finish for $sessnum\n" and return -1
		if ($function eq 'FINISH');

	print "Got CHECKPOINT for $sessnum\n" and return -1
		if ($function eq 'CHECKPOINT');

	return 0;
}

sub report_cb {	print $_[0], "\n"; }

sub gen_mload_data {
	my $has_bignum = shift;
	my $pack = $has_bignum
		? 'a3 L S C a40 S/a* d C S L LL L a15 a19 a8 a8 a16 a16 c'
		: 'S L S C a40 S/a* d C S L LL L a15 a19 c';

	print " *** Generating mload data...\n";
	open(RAW, 'rawdata.dat');
	binmode RAW;
	open (MLRAW, '>mlrawdata.dat');
	binmode MLRAW;

	my ($v, $len);
	my @p;
	while (1) {
		last unless read RAW, $len, 2, 0;
		$len = unpack('S', $len);
		last unless read RAW, $v, $len+1, 0;
		@p = unpack($pack, $v);
		$p[4] = 'weedoggies!!!!      ' unless ($p[1]%3);
		$p[1] += 100000 if ($p[1]%3);
		$v = pack($pack, @p);
		$len = length($v) -1;
		$v = pack('Sa*', $len, $v);
		print MLRAW $v;
	}
	close RAW;
	close MLRAW;

	open(VTXT, '<:utf8', 'utf8data.txt');
	open(MLVTXT, '>:utf8', 'mlvardata.txt');

	while (<VTXT>) {
		chomp;
		@p = split '\|';
		$p[0] += 200000 if ($p[0]%2);
		$p[3] = 'mload vartext test' unless ($p[0]%2);
		print MLVTXT join('|', @p), "\n";
	}
	close VTXT;
	close MLVTXT;
	print " *** Mload data generated\n";
}



1;