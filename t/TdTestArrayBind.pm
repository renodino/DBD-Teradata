package TdTestArrayBind;

use DBI;
use DBI qw(:sql_types);
use threads;
use threads::shared;	# so we can share data
use Thread::Queue;	# a thread-safe shared queue
use Time::HiRes qw(time);
use TdTestDataGen qw(collect_recs_each collect_big_recs_each);

use Exporter;
use base ('Exporter');

@EXPORT = qw(
	ary_using_load
	ary_named_using_load
	ary_sth_using_load
	ary_ph_load
	ary_sth_ph_load
	ary_load_using_raw_tuples
	ary_load_using_raw_fetch
	ary_load_using_vartext_tuples
	ary_load_using_vartext_fetch
	ary_using_multistmt
	ary_ph_multistmt
	ary_error_load
	);

use strict;
use warnings;

my $usingload =
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 DATE,
col12 TIME,
col13 TIMESTAMP(0))
LOCKING TABLE alltypetst FOR ACCESS
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5,
:col6, :col7, :col8, :col9, :col10, :col11, :col12, :col13)';

my $bigusingload =
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 DATE,
col12 TIME,
col13 TIMESTAMP(0),
col14 bigint,
col15 decimal(18,0),
col16 decimal(27,14),
col17 decimal(38,16)
)
LOCKING TABLE alltypetst FOR ACCESS
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5,
:col6, :col7, :col8, :col9, :col10, :col11, :col12, :col13, 
:col14, :col15, :col16, :col17)';

my $phload = 
'LOCKING TABLE alltypetst FOR ACCESS
INSERT INTO alltypetst VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

my $bigphload = 
'LOCKING TABLE alltypetst FOR ACCESS
INSERT INTO alltypetst VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

my $vtload = 
'USING (col1 varchar(18),
col2 varchar(12),
col3 varchar(8),
col4 varchar(40),
col5 varchar(200),
col6 varchar(60),
col7 varchar(8),
col8 varchar(14),
col9 varchar(20),
col10 varchar(60),
col11 varchar(10),
col12 varchar(15),
col13 varchar(19))
LOCKING TABLE alltypetst FOR ACCESS
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5,
:col6, :col7, :col8, :col9, :col10, :col11, :col12, :col13)';

my $bigvtload = 
'USING (col1 varchar(18),
col2 varchar(12),
col3 varchar(8),
col4 varchar(40),
col5 varchar(200),
col6 varchar(60),
col7 varchar(8),
col8 varchar(14),
col9 varchar(20),
col10 varchar(60),
col11 varchar(10),
col12 varchar(15),
col13 varchar(19),
col14 varchar(20),
col15 varchar(20),
col16 varchar(30),
col17 varchar(41)
)
LOCKING TABLE alltypetst FOR ACCESS
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5,
:col6, :col7, :col8, :col9, :col10, :col11, :col12, :col13,
:col14, :col15, :col16, :col17)';

my $multiload = 
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 DATE,
col12 TIME,
col13 TIMESTAMP(0))
LOCKING TABLE alltypetst FOR ACCESS
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5,
:col6, :col7, :col8, :col9, :col10, :col11, :col12, :col13);
INSERT INTO alltypetst10 VALUES(:col1, :col2, :col3, :col4, :col5,
:col6, :col7, :col8, :col9, :col10, :col11, :col12, :col13)';

my $bigmultiload = 
'USING (col1 integer,
col2 smallint,
col3 byteint,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 DATE,
col12 TIME,
col13 TIMESTAMP(0),
col14 bigint,
col15 decimal(18,0),
col16 decimal(27,14),
col17 decimal(38,16)
)
LOCKING TABLE alltypetst FOR ACCESS
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5,
:col6, :col7, :col8, :col9, :col10, :col11, :col12, :col13,
:col14, :col15, :col16, :col17);
INSERT INTO alltypetst10 VALUES(:col1, :col2, :col3, :col4, :col5,
:col6, :col7, :col8, :col9, :col10, :col11, :col12, :col13,
:col14, :col15, :col16, :col17);';

###################################################
#
#	test simple USING array binding
#
###################################################
sub ary_using_load {
	my ($dbh, $rowcnt) = @_;

	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	print STDERR "Testing array binding for USING...\n";

	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;
	print STDERR "Generating data...\n";
	my $base = 0;
	my $ary = $use_bigdata
		? collect_big_recs_each(\$base, $rowcnt, 0, 0)
		: collect_recs_each(\$base, $rowcnt, 0, 0);
	print STDERR "Data generated, starting execution...\n";
	my $ristarted = time;

	my @tuple_status = ();
	my $sql = $use_bigdata ? $bigusingload : $usingload;
	my $sth = $dbh->prepare($sql)
		|| die ("While preparing: " . $dbh->errstr . "\n");

	my $rownum = -1;
	my $rc = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		ArrayTupleFetch => sub {
			$rownum++;
			return undef unless ($rownum < $rowcnt);
			print "\rSending row $rownum..." unless $rownum%100;
			return [ map { $_->[$rownum]; } @$ary ];
			}
		})
		or die ("While executing: " . $sth->errstr . "\n");

	print "\n";	# advance from last progress report
	$ristarted = int((time - $ristarted) * 1000)/1000;
	print "$rc rows inserted in $ristarted secs.\n";

	die "Unexpected tuplestatus size " . (scalar @tuple_status) . " ne $rowcnt\n"
		unless (scalar @tuple_status == $rowcnt);

	my $ok = 0;
	$ok += $_
		foreach (@tuple_status);

	die "Unexpected tuplestatus values\n"
		unless ($ok == scalar @tuple_status);

	print STDERR "Array binding for USING ok.\n";
	return $ristarted;
}

###################################################
#
#	test named USING array binding
#
###################################################
sub ary_named_using_load {
	my ($dbh, $rowcnt) = @_;

	print STDERR "Testing named array binding for USING...\n";
	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;
	print STDERR "Generating data...\n";
	my $base = 0;
	my $ary = $use_bigdata
		? collect_big_recs_each(\$base, $rowcnt, 0, 0)
		: collect_recs_each(\$base, $rowcnt, 0, 0);
	print STDERR "Data generated, starting execution...\n";
	my $ristarted = time;

	my @tuple_status = ();
	my $sql = $use_bigdata ? $bigusingload : $usingload;
	my $sth = $dbh->prepare($sql)
		|| die ("While preparing: " . $dbh->errstr . "\n");

	$sth->bind_param_array(':col1', shift @$ary, SQL_INTEGER) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col2', shift @$ary, SQL_TINYINT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col3', shift @$ary, SQL_SMALLINT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col4', shift @$ary, { TYPE => SQL_CHAR, PRECISION => 40 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col5', shift @$ary, { TYPE => SQL_VARCHAR, PRECISION => 200 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col6', shift @$ary, SQL_FLOAT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col7', shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 2, SCALE => 1 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col8', shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 4, SCALE => 2 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col9', shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 8, SCALE => 4 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col10', shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 14, SCALE => 5 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col11', shift @$ary, SQL_DATE) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col12', shift @$ary, SQL_TIME) || die ($sth->errstr . "\n");
	$sth->bind_param_array(':col13', shift @$ary, SQL_TIMESTAMP) || die ($sth->errstr . "\n");
	if ($use_bigdata) {
		$sth->bind_param_array(':col14', shift @$ary, SQL_BIGINT) || die ($sth->errstr . "\n");
		$sth->bind_param_array(':col15', shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 18, SCALE => 0 }) || die ($sth->errstr . "\n");
		$sth->bind_param_array(':col16', shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 27, SCALE => 14 }) || die ($sth->errstr . "\n");
		$sth->bind_param_array(':col17', shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 38, SCALE => 16 }) || die ($sth->errstr . "\n");
	}

	my $rc = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		tdat_progress => [ 100, sub { print "\rSent $_[0] tuples..." ; } ]
		})
		or die ("While executing: " . $sth->errstr . "\n");

	print "\n";	# advance from last progress report
	$ristarted = int((time - $ristarted) * 1000)/1000;
	print "$rc rows inserted in $ristarted secs.\n";

	die "Unexpected tuplestatus size " . (scalar @tuple_status) . " ne $rowcnt\n"
		unless (scalar @tuple_status == $rowcnt);

	my $ok = 0;
	$ok += $_
		foreach (@tuple_status);

	die "Unexpected tuplestatus values\n"
		unless ($ok == scalar @tuple_status);

	print STDERR "Named array binding for USING ok.\n";
	return $ristarted;
}

###################################################
#
#	test sth source USING array binding
#
###################################################
sub ary_sth_using_load {
	my ($dbh, $srcsth, $rowcnt) = @_;

	print STDERR "Testing sth sourced array binding for USING...\n";
	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;
	print STDERR "Generating data...\n";

	my $ristarted = time;

	my @tuple_status = ();
	my $sql = $use_bigdata ? $bigusingload : $usingload;
	my $sth = $dbh->prepare($sql)
		|| die ("While preparing: " . $dbh->errstr . "\n");

	my $rc = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		ArrayTupleFetch => $srcsth,
		tdat_progress => [ 100, sub { print "\rSent $_[0] tuples..." ; } ]
		})
		or die ("While executing: " . $sth->errstr . "\n");

	print "\n";	# advance from last progress report
	$ristarted = int((time - $ristarted) * 1000)/1000;
	print "$rc rows inserted in $ristarted secs.\n";

	die "Unexpected tuplestatus size " . (scalar @tuple_status) . " ne $rowcnt\n"
		unless (scalar @tuple_status == $rowcnt);

	my $ok = 0;
	$ok += $_
		foreach (@tuple_status);

	die "Unexpected tuplestatus values\n"
		unless ($ok == scalar @tuple_status);

	print STDERR "Sth sourced Array binding for USING ok.\n";
	return $ristarted;
}

###################################################
#
#	test simple placeholder array binding
#
###################################################
sub ary_ph_load {
	my ($dbh, $rowcnt) = @_;

	print STDERR "Testing array binding for placeholders...\n";
	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;

	print STDERR "Generating data...\n";
	my $base = 0;
	my $ary = $use_bigdata 
		? collect_big_recs_each(\$base, $rowcnt, 0, 0)
		: collect_recs_each(\$base, $rowcnt, 0, 0);
	print STDERR "Data generated, starting execution...\n";

	my $ristarted = time;

	my @tuple_status = ();
	my $sql = $use_bigdata ? $bigphload : $phload;
	my $sth = $dbh->prepare($sql)
		|| die ("While preparing: " . $dbh->errstr . "\n");

	$sth->bind_param_array(1, shift @$ary, SQL_INTEGER) || die ($sth->errstr . "\n");
	$sth->bind_param_array(2, shift @$ary, SQL_SMALLINT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(3, shift @$ary, SQL_TINYINT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(4, shift @$ary, { TYPE => SQL_CHAR, PRECISION => 40 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(5, shift @$ary, { TYPE => SQL_VARCHAR, PRECISION => 200 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(6, shift @$ary, SQL_FLOAT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(7, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 2, SCALE => 1 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(8, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 4, SCALE => 2 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(9, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 8, SCALE => 4 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(10, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 14, SCALE => 5 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(11, shift @$ary, SQL_DATE) || die ($sth->errstr . "\n");
	$sth->bind_param_array(12, shift @$ary, SQL_TIME) || die ($sth->errstr . "\n");
	$sth->bind_param_array(13, shift @$ary, SQL_TIMESTAMP) || die ($sth->errstr . "\n");
	if ($use_bigdata) {
		$sth->bind_param_array(14, shift @$ary, SQL_BIGINT) || die ($sth->errstr . "\n");
		$sth->bind_param_array(15, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 18, SCALE => 0 }) || die ($sth->errstr . "\n");
		$sth->bind_param_array(16, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 27, SCALE => 14 }) || die ($sth->errstr . "\n");
		$sth->bind_param_array(17, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 38, SCALE => 16 }) || die ($sth->errstr . "\n");
	}

	my $rc = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		tdat_progress => [ 100, sub { print "\rSent $_[0] tuples..." ; } ]
		})
		or die ("While executing: " . $sth->errstr . "\n");

	print "\n";	# advance from last progress report
	$ristarted = int((time - $ristarted) * 1000)/1000;
	print "$rc rows inserted in $ristarted secs.\n";

	die "Unexpected tuplestatus size " . (scalar @tuple_status) . " ne $rowcnt\n"
		unless (scalar @tuple_status == $rowcnt);

	my $ok = 0;
	$ok += $_
		foreach (@tuple_status);

	die "Unexpected tuplestatus values\n"
		unless ($ok == scalar @tuple_status);

	print STDERR "Array binding for placeholders ok.\n";
	return $ristarted;
}

###################################################
#
#	test sth placeholder array binding
#
###################################################
sub ary_sth_ph_load {
	my ($dbh, $srcsth, $rowcnt) = @_;

	print STDERR "Testing sth-sourced array binding for placeholders...\n";
	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;

	my $ristarted = time;

	my @tuple_status = ();
	my $sql = $use_bigdata ? $bigphload : $phload;
	my $sth = $dbh->prepare($sql)
		|| die ("While preparing: " . $dbh->errstr . "\n");

	$sth->bind_param_array(1, undef, SQL_INTEGER) || die ($sth->errstr . "\n");
	$sth->bind_param_array(2, undef, SQL_SMALLINT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(3, undef, SQL_TINYINT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(4, undef, { TYPE => SQL_CHAR, PRECISION => 40 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(5, undef, { TYPE => SQL_VARCHAR, PRECISION => 200 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(6, undef, SQL_FLOAT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(7, undef, { TYPE => SQL_DECIMAL, PRECISION => 2, SCALE => 1 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(8, undef, { TYPE => SQL_DECIMAL, PRECISION => 4, SCALE => 2 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(9, undef, { TYPE => SQL_DECIMAL, PRECISION => 8, SCALE => 4 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(10, undef, { TYPE => SQL_DECIMAL, PRECISION => 14, SCALE => 5 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(11, undef, SQL_DATE) || die ($sth->errstr . "\n");
	$sth->bind_param_array(12, undef, SQL_TIME) || die ($sth->errstr . "\n");
	$sth->bind_param_array(13, undef, SQL_TIMESTAMP) || die ($sth->errstr . "\n");
	if ($use_bigdata) {
		$sth->bind_param_array(14, undef, SQL_BIGINT) || die ($sth->errstr . "\n");
		$sth->bind_param_array(15, undef, { TYPE => SQL_DECIMAL, PRECISION => 18, SCALE => 0 }) || die ($sth->errstr . "\n");
		$sth->bind_param_array(16, undef, { TYPE => SQL_DECIMAL, PRECISION => 27, SCALE => 14 }) || die ($sth->errstr . "\n");
		$sth->bind_param_array(17, undef, { TYPE => SQL_DECIMAL, PRECISION => 38, SCALE => 16 }) || die ($sth->errstr . "\n");
	}

	my $rc = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		ArrayTupleFetch => $srcsth,
		tdat_progress => [ 100, sub { print "\rSent $_[0] tuples..." ; } ]
		})
		or die ("While executing: " . $sth->errstr . "\n");

	print "\n";	# advance from last progress report
	$ristarted = int((time - $ristarted) * 1000)/1000;
	print "$rc rows inserted in $ristarted secs.\n";

	die "Unexpected tuplestatus size " . (scalar @tuple_status) . " ne $rowcnt\n"
		unless (scalar @tuple_status == $rowcnt);

	my $ok = 0;
	$ok += $_
		foreach (@tuple_status);

	die "Unexpected tuplestatus values\n"
		unless ($ok == scalar @tuple_status);

	print STDERR "STH sourced Array binding for placeholders ok.\n";
	return $ristarted;
}

###################################################
#
#	test array bind with raw input, implicit bind
#
###################################################
sub ary_load_using_raw_tuples {
	my ($dbh, $rowcnt) = @_;

	print STDERR "Testing Array binding for USING w/ raw tuple binding...\n";
	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	my @tuple_status = ();
	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;

	my $fh;
	open($fh, '<rawdata.dat');
	binmode $fh;
	my @tuples = map { $_ = readraw($fh); } 1..$rowcnt;

	my $ristarted = time;
	my $sql = $use_bigdata ? $bigusingload : $usingload;
	my $sth = $dbh->prepare($sql,
		{
			tdat_raw_in => 'IndicatorMode'
		}) || die ("While preparing: " . $dbh->errstr . "\n");

	my $rownum = -1;
	my $rc = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		tdat_progress => [ 1000, sub { print "\rSent $_[0] tuples..." ; } ]
		}, \@tuples);

	print "\n";	# advance from last progress report
	$ristarted = int((time - $ristarted) * 1000)/1000;
	if ($rc) {
		print "$rc rows inserted in $ristarted secs.\n";
	}
	else {
		print "Rows inserted in $ristarted secs with errors.\n";
	}

	die "Unexpected tuplestatus size " . (scalar @tuple_status) . " ne $rowcnt\n"
		unless (scalar @tuple_status == $rowcnt);

	my $ok = 0;
	foreach (0..$#tuple_status) {
		if (ref $tuple_status[$_]) {
			print "!!! Tuple $_ failed: ", join(':', @{$tuple_status[$_]}), "\n";
		}
		else {
			$ok += $tuple_status[$_];
		}
	}

	die "Unexpected tuplestatus values\n"
		unless ($ok == scalar @tuple_status);

	print STDERR "Array binding for USING w/ raw tuple binding ok.\n";
	return $ristarted;
}

###################################################
#
#	test array bind with raw input
#
###################################################
sub ary_load_using_raw_fetch {
	my ($dbh, $rowcnt) = @_;

	print STDERR "Testing Array binding for USING w/ raw ArrayTupleFetch...\n";
	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	my @tuple_status = ();
	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;

	my $fh;
	open($fh, '<rawdata.dat');
	binmode $fh;
	my $ristarted = time;
	my $sql = $use_bigdata ? $bigusingload : $usingload;
	my $sth = $dbh->prepare($sql,
		{
			tdat_raw_in => 'IndicatorMode'
		}) || die ("While preparing: " . $dbh->errstr . "\n");

	my $rownum = -1;
	my $rc = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		ArrayTupleFetch => sub {
			$rownum++;
			return undef unless ($rownum < $rowcnt);
			print "\rSending row $rownum..." unless $rownum%100;
			return [ readraw($fh) ];
			},
		})
		or die ("While executing: " . $sth->errstr . "\n");

	print "\n";	# advance from last progress report
	$ristarted = int((time - $ristarted) * 1000)/1000;
	print "$rc rows inserted in $ristarted secs.\n";

	die "Unexpected tuplestatus size " . (scalar @tuple_status) . " ne $rowcnt\n"
		unless (scalar @tuple_status == $rowcnt);

	my $ok = 0;
	$ok += $_
		foreach (@tuple_status);

	die "Unexpected tuplestatus values\n"
		unless ($ok == scalar @tuple_status);

	print STDERR "Array binding for USING w/ raw ArrayTupleFetch ok.\n";
	return $ristarted;
}

###################################################
#
#	test array bind with implicit binding vartext input
#
###################################################
sub ary_load_using_vartext_tuples {
	my ($dbh, $rowcnt) = @_;

	print STDERR "Testing Array binding for USING w/ vartext tuple binding ...\n";
	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	my @tuple_status = ();
	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;
#
#	large requests w/ vartext seem to choke small servers,
#	so throttle back
#
	my $oldreqsz = $dbh->{tdat_reqsize};
	$dbh->{tdat_reqsize} = 200000 if ($oldreqsz > 200000);

	my $vtfd;
	open($vtfd, '<:utf8', 'utf8data.txt');

	my $vt;
	my @tuples = map { $_ = readvt($vtfd); } 1..$rowcnt;

	my $rvstarted = time;
	my $sql = $use_bigdata ? $bigvtload : $vtload;
	my $sth = $dbh->prepare($sql,
			{
				tdat_vartext_in => '\|',
			}) || die ("While preparing: " . $dbh->errstr . "\n");

	my $rownum = -1;
	my $rc = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		tdat_progress => [ 100, sub { print "\rSent $_[0] tuples..." ; } ]
		}, \@tuples)
		or die ("While executing: " . $sth->errstr . "\n");

	print "\n";	# advance from last progress report
	$rvstarted = int((time - $rvstarted) * 1000)/1000;
	print "$rowcnt rows inserted in $rvstarted secs.\n";
	close $vtfd;

	die "Unexpected tuplestatus size " . (scalar @tuple_status) . " ne $rowcnt\n"
		unless (scalar @tuple_status == $rowcnt);

	my $ok = 0;
	$ok += $_
		foreach (@tuple_status);

	die "Unexpected tuplestatus values\n"
		unless ($ok == scalar @tuple_status);

	print STDERR "Array binding for USING w/ vartext tuple binding ok.\n";
	$dbh->{tdat_reqsize} = $oldreqsz;
	return $rvstarted;
}

###################################################
#
#	test array bind with vartext input
#
###################################################
sub ary_load_using_vartext_fetch {
	my ($dbh, $rowcnt) = @_;

	print STDERR "Testing Array binding for USING w/ vartext ArrayTupleFetch ...\n";
	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	my @tuple_status = ();
	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;
#
#	large requests w/ vartext seem to choke small servers,
#	so throttle back
#
	my $oldreqsz = $dbh->{tdat_reqsize};
	$dbh->{tdat_reqsize} = 200000 if ($oldreqsz > 200000);

	my $vtfd;
	open($vtfd, '<:utf8', 'utf8data.txt');

	my $vt;

	my $rvstarted = time;
	my $sql = $use_bigdata ? $bigvtload : $vtload;
	my $sth = $dbh->prepare($sql,
			{
				tdat_vartext_in => '\|',
			}) || die ("While preparing: " . $dbh->errstr . "\n");

	my $rownum = -1;
	my $rc = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		ArrayTupleFetch => sub {
			$rownum++;
			return undef unless ($rownum < $rowcnt);
			print "\rSending row $rownum..." unless $rownum%100;
			return [ readvt($vtfd) ];
			},
		})
		or die ("While executing: " . $sth->errstr . "\n");

	print "\n";	# advance from last progress report
	$rvstarted = int((time - $rvstarted) * 1000)/1000;
	print "$rowcnt rows inserted in $rvstarted secs.\n";
	close $vtfd;

	die "Unexpected tuplestatus size " . (scalar @tuple_status) . " ne $rowcnt\n"
		unless (scalar @tuple_status == $rowcnt);

	my $ok = 0;
	$ok += $_
		foreach (@tuple_status);

	die "Unexpected tuplestatus values\n"
		unless ($ok == scalar @tuple_status);

	print STDERR "Array binding for USING w/ vartext ArrayTupleFetch ok.\n";
	$dbh->{tdat_reqsize} = $oldreqsz;
	return $rvstarted;
}

###################################################
#
#	test multistmt USING array binding
#
###################################################
sub ary_using_multistmt {
	my ($dbh, $rowcnt) = @_;

	print STDERR "Testing multistatement array binding for USING...\n";
	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;
	print STDERR "Generating data...\n";
	my $base = 0;
	my $ary = $use_bigdata 
		? collect_big_recs_each(\$base, $rowcnt, 0, 0)
		: collect_recs_each(\$base, $rowcnt, 0, 0);
	print STDERR "Data generated, starting execution...\n";
	my $ristarted = time;

	my @tuple_status = ();
	my $sql = $use_bigdata ? $bigvtload : $vtload;
	my $sth = $dbh->prepare($sql)
		|| die ("While preparing: " . $dbh->errstr . "\n");

	my $rownum = -1;
	my $rc = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		ArrayTupleFetch => sub {
			$rownum++;
			return undef unless ($rownum < $rowcnt);
			print "\rSending row $rownum..." unless $rownum%100;
			return [ map { $_->[$rownum]; } @$ary ];
			}
		})
		or die ("While executing: " . $sth->errstr . "\n");

	print "\n";	# advance from last progress report
	$ristarted = int((time - $ristarted) * 1000)/1000;
	print "$rc rows inserted in $ristarted secs.\n";

	die "Unexpected tuplestatus size " . (scalar @tuple_status) . " ne $rowcnt\n"
		unless (scalar @tuple_status == $rowcnt);

	my $ok = 0;
	$ok += $_
		foreach (@tuple_status);

	die "Unexpected tuplestatus values\n"
		unless ($ok == scalar @tuple_status);

	print STDERR "Array binding for multistatement USING ok.\n";
	return $ristarted;
}

###################################################
#
#	test multistatement placeholder array binding
#	!!!NOT SUPPORTED YET!!!!
#
###################################################
sub ary_ph_multistmt {
	my ($dbh, $rowcnt) = @_;

	print STDERR "Testing multistatement array binding for placeholders...\n";

	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;

	print STDERR "Generating data...\n";
	my $base = 0;
	my $ary = collect_recs_each(\$base, $rowcnt, 0, 0);
	print STDERR "Data generated, starting execution...\n";

	my $ristarted = time;

	my @tuple_status = ();
	my $sth = $dbh->prepare(
'LOCKING TABLE alltypetst FOR ACCESS
INSERT INTO alltypetst VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
INSERT INTO alltypetst10 VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);',
		) || die ("While preparing: " . $dbh->errstr . "\n");

	$sth->bind_param_array(1, shift @$ary, SQL_INTEGER) || die ($sth->errstr . "\n");
	$sth->bind_param_array(2, shift @$ary, SQL_SMALLINT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(3, shift @$ary, SQL_TINYINT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(4, shift @$ary, { TYPE => SQL_CHAR, PRECISION => 40 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(5, shift @$ary, { TYPE => SQL_VARCHAR, PRECISION => 200 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(6, shift @$ary, SQL_FLOAT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(7, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 2, SCALE => 1 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(8, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 4, SCALE => 2 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(9, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 8, SCALE => 4 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(10, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 14, SCALE => 5 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(11, shift @$ary, SQL_DATE) || die ($sth->errstr . "\n");
	$sth->bind_param_array(12, shift @$ary, SQL_TIME) || die ($sth->errstr . "\n");
	$sth->bind_param_array(13, shift @$ary, SQL_TIMESTAMP) || die ($sth->errstr . "\n");

	my $rc = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		tdat_progress => [ 100, sub { print "\rSent $_[0] tuples..." ; } ]
		})
		or die ("While executing: " . $sth->errstr . "\n");

	print "\n";	# advance from last progress report
	$ristarted = int((time - $ristarted) * 1000)/1000;
	print "$rc rows inserted in $ristarted secs.\n";

	die "Unexpected tuplestatus size " . (scalar @tuple_status) . " ne $rowcnt\n"
		unless (scalar @tuple_status == $rowcnt);

	my $ok = 0;
	$ok += $_
		foreach (@tuple_status);

	die "Unexpected tuplestatus values\n"
		unless ($ok == scalar @tuple_status);

	print STDERR "Multistatement Array binding for placeholders ok.\n";
	return $ristarted;
}

###################################################
#
#	test array binding with errors
#
###################################################
sub ary_error_load {
	my ($dbh, $rowcnt, $commit) = @_;

	my $testtype = 	$dbh->{tdat_mode} . ' mode, AutoCommit ' . ($commit ? 'ON' : 'OFF');
	print STDERR "Testing array binding with bad data, $testtype ...\n";
	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;

	print STDERR "Generating data...\n";
	my $base = 0;
	my $ary = $use_bigdata
		? collect_big_recs_each(\$base, $rowcnt, 0, 0)
		: collect_recs_each(\$base, $rowcnt, 0, 0);
	print STDERR "Data generated, starting execution...\n";
#
#	suppress error reports
#
	$dbh->{RaiseError} = 0;
	$dbh->{PrintError} = 0;
#
#	inject some data errors
#
	my $i = 13;
	my $errcnt = 0;
	$ary->[12][$i] = 'a bogus timestamp',
	$i += 30,
	$errcnt++
		while ($i < $#{$ary->[0]});

	my @tuple_status = ();
	my $sql = $use_bigdata ? $bigusingload : $usingload;
	my $sth = $dbh->prepare($sql)
		|| die ("While preparing: " . $dbh->errstr . "\n");

	$dbh->{AutoCommit} = $commit;
	my ($tuplecnt, $rc) = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		tdat_progress => [ 100, sub { print "\rSent $_[0] tuples..." ; } ]
		}, @$ary);

	print "\n";	# advance from last progress report
	die "No errors detected; expected $errcnt\n"
		if defined($rc);

#	print "\n *** Returned errors are ", $sth->errstr, "\n";

	$tuplecnt = scalar @tuple_status;
	if ($dbh->{tdat_mode} eq 'TERADATA') {
		if ($commit) {
			die "Got $tuplecnt tuples, expected $rowcnt\n"
				unless ($tuplecnt == $rowcnt);

			if ($dbh->{tdat_versnum} < 6000000) {
				my $has_err = 0;
				foreach (@tuple_status) {
					$has_err++
						if ref $_;
				}

				die "Error count $has_err ne expected $errcnt\n"
					unless ($has_err == $errcnt);
			}
		}
		else {
			die "Got $tuplecnt tuples, expected 14\n"
				unless ($tuplecnt == 14);
			foreach (0..12) {
				die "Tuple $_: Expected status of -2, got $tuple_status[$_]\n"
					unless ($tuple_status[$_] == -2);
			}
		}
	}
	else { # ANSI
#
#	not certain what to check for here...
#
		die "Got $tuplecnt tuples, expected $rowcnt\n"
			unless ($tuplecnt == $rowcnt);

		if ($dbh->{tdat_versnum} < 6000000) {
			my $has_err = 0;
			foreach (@tuple_status) {
				$has_err++
					if ref $_;
			}

			die "Error count $has_err ne expected $errcnt\n"
				unless ($has_err == $errcnt);
		}
	}

	$dbh->rollback() unless $commit;

	print STDERR "Array binding errors with USING $testtype ok.\n";

	print STDERR "Testing array binding errors with placeholders $testtype ...\n";

	$dbh->{AutoCommit} = 1;

	$dbh->do('DELETE FROM alltypetst') or die $dbh->errstr;

	@tuple_status = ();
	$sql = $use_bigdata ? $bigphload : $phload;
	$sth = $dbh->prepare($sql)
		|| die ("While preparing: " . $dbh->errstr . "\n");

	$sth->bind_param_array(1, shift @$ary, SQL_INTEGER) || die ($sth->errstr . "\n");
	$sth->bind_param_array(2, shift @$ary, SQL_SMALLINT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(3, shift @$ary, SQL_TINYINT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(4, shift @$ary, { TYPE => SQL_CHAR, PRECISION => 40 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(5, shift @$ary, { TYPE => SQL_VARCHAR, PRECISION => 200 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(6, shift @$ary, SQL_FLOAT) || die ($sth->errstr . "\n");
	$sth->bind_param_array(7, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 2, SCALE => 1 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(8, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 4, SCALE => 2 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(9, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 8, SCALE => 4 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(10, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 14, SCALE => 5 }) || die ($sth->errstr . "\n");
	$sth->bind_param_array(11, shift @$ary, SQL_DATE) || die ($sth->errstr . "\n");
	$sth->bind_param_array(12, shift @$ary, SQL_TIME) || die ($sth->errstr . "\n");
	$sth->bind_param_array(13, shift @$ary, SQL_TIMESTAMP) || die ($sth->errstr . "\n");
	if ($use_bigdata) {
		$sth->bind_param_array(14, shift @$ary, SQL_BIGINT) || die ($sth->errstr . "\n");
		$sth->bind_param_array(15, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 18, SCALE => 0 }) || die ($sth->errstr . "\n");
		$sth->bind_param_array(16, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 27, SCALE => 14 }) || die ($sth->errstr . "\n");
		$sth->bind_param_array(17, shift @$ary, { TYPE => SQL_DECIMAL, PRECISION => 38, SCALE => 16 }) || die ($sth->errstr . "\n");
	}

	$dbh->{AutoCommit} = $commit;
	($tuplecnt, $rc) = $sth->execute_array({
		ArrayTupleStatus => \@tuple_status,
		tdat_progress => [ 100, sub { print "\rSent $_[0] tuples..." ; } ]
		});

	print "\n";	# advance from last progress report
	die "No errors detected; expected $errcnt\n"
		if defined($rc);

#	print "\n *** Returned errors are ", $sth->errstr, "\n";

	$tuplecnt = scalar @tuple_status;
	if ($dbh->{tdat_mode} eq 'TERADATA') {
		if ($commit) {
			die "Got $tuplecnt tuples, expected $rowcnt\n"
				unless ($tuplecnt == $rowcnt);

			if ($dbh->{tdat_versnum} < 6000000) {
				my $has_err = 0;
				foreach (@tuple_status) {
					$has_err++
						if ref $_;
				}

				die "Error count $has_err ne expected $errcnt\n"
					unless ($has_err == $errcnt);
			}
		}
		else {
			die "Got $tuplecnt tuples, expected 14\n"
				unless ($tuplecnt == 14);
			foreach (0..12) {
				die "Tuple $_: Expected status of -2, got $tuple_status[$_]\n"
					unless ($tuple_status[$_] == -2);
			}
		}
	}
	else { # ANSI
#
#	not certain what to check for here...
#
		die "Got $tuplecnt tuples, expected $rowcnt\n"
			unless ($tuplecnt == $rowcnt);

		if ($dbh->{tdat_versnum} < 6000000) {
			my $has_err = 0;
			foreach (@tuple_status) {
				$has_err++
					if ref $_;
			}

			die "Error count $has_err ne expected $errcnt\n"
				unless ($has_err == $errcnt);
		}
	}

	$dbh->rollback() unless $commit;

	print STDERR "Array binding errors with placeholders $testtype ok.\n";
	return 1;
}

sub readraw {
	my $fh = shift;
	my $len;
	read $fh, $len, 2, 0;
	return undef
		unless $len;

	$len = unpack('S', $len);
	print STDERR "Bad len $len\n"
		if ($len > 440);

	my $var;
	read $fh, $var, $len+1, 0;
	return undef
		unless $var;

	return pack('S a*', $len, $var);
}

sub readvt {
	my $vtfd = shift;
	my $vt = <$vtfd>;
	$vt=~s/\n$//;
	return $vt || undef;
}

1;