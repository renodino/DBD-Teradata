BEGIN {
	push @INC, './t';
}

use DBI;
use DBD::Teradata;
use TdTestBigInt qw(dectests);

use strict;
use warnings;

$| = 1;

while (substr($ARGV[0], 0, 1) eq '-') {
	my $opt = shift @ARGV;

	$ENV{TDAT_DBD_NO_CLI} = 1,
	next
		if ($opt eq '-c');

	DBI->trace(2, shift @ARGV),
	$ENV{TDAT_DBD_DEBUG} = 1
		if ($opt eq '-d');
}

my $dbh;
my ($dsn, $userid, $passwd, $dfltdb) = @ARGV;

$dsn .= ";DATABASE=$dfltdb"
	if $dfltdb;

$dbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		tdat_charset => 'UTF8',
		tdat_mode => 'TERADATA',
	}
) || die "Can't connect to $dsn: $DBI::errstr. Exiting...\n";

printf STDERR  "Logon to %s ver. %s(%s mode) ok\n", $dsn, $dbh->{tdat_version}, $dbh->{tdat_mode};
my $drh = $dbh->{Driver};
print STDERR "DBD::Teradata v. $drh->{Version}\n";

die "Did not connect with CLI adapter, check your configuration."
	unless $dbh->{tdat_uses_cli} || $ENV{TDAT_DBD_NO_CLI};

print STDERR "Connected via ", ($dbh->{tdat_uses_cli} ? 'CLI' : 'pure Perl'), "\n";

dectests($dbh);

$dbh->disconnect;

print STDERR "Tests completed OK.\n";