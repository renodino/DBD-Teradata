package TdTestDBIAPI;

use DBI qw(:sql_types);

use Exporter;
use base ('Exporter');

@EXPORT = qw(dbiapitests);

use strict;
use warnings;

sub _report {
	if ($_[1]) {
	    print "ok $_[0] # $_[2]\n";
	}
	else {
	    print "notok $_[0] # $_[2]\n";
	}
	$_[0]++;
}

sub dbiapitests {
	my ($dbh, $dsn) = @_;

	my $testno = 1;

	print STDERR "Test DBI API methods...\n";

	my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));

	my $ttl_columns = $use_bigdata ? 17 : 13;
	
	my ($scheme, $driver, $attr_string, $attr_hash, $driver_dsn) = DBI->parse_dsn($dsn)
      or die "Can't parse DBI DSN '$dsn'";
	_report($testno, 1, 'parse_dsn()');

	my $sth = $dbh->prepare('select * from alltypetst order by col1 sample 100')
		or die $dbh->errstr;
	$sth->execute or die $sth->errstr;
#
#	fetchrow_hashref: w/ and wo/ name param
#
	my $rows = $sth->fetchrow_hashref();
	foreach (@{$sth->{NAME}}) {
		die "fetchrow_hashref(): $_ not found\n"
			unless exists $rows->{$_};
	}
	_report($testno, 1, 'fetchrow_hashref()');

	$rows = $sth->fetchrow_hashref('NAME_lc');
	foreach (@{$sth->{NAME_lc}}) {
		die "fetchrow_hashref(NAME_lc): $_ not found\n"
			unless exists $rows->{$_};
	}
	_report($testno, 1, 'fetchrow_hashref(NAME_lc)');
#
#	fetchall_arrayref
#
	$sth->execute or die $sth->errstr;
	$rows = $sth->fetchall_arrayref();
	die "fetchall_arrayref(): not 100 rows\n"
		unless (scalar @$rows == 100);

	die "fetchall_arrayref(): not $ttl_columns columns\n"
		unless (scalar @{$rows->[0]} == $ttl_columns);
	_report($testno, 1, 'fetchall_arrayref()');

	$sth->execute or die $sth->errstr;
	$rows = $sth->fetchall_arrayref([0, 1, -2, -1]);
	die "fetchall_arrayref(\$slice): not 100 rows\n"
		unless (scalar @$rows == 100);
	die "fetchall_arrayref(\$slice): not 4 columns\n"
		unless (scalar @{$rows->[0]} == 4);
	_report($testno, 1, 'fetchall_arrayref([0, 1, -2, -1])');

	$dbh->{FetchHashKeyName} = 'NAME_lc';
	$sth->execute or die $sth->errstr;
	$rows = $sth->fetchall_arrayref({});
	die "fetchall_arrayref({}): not 100 rows\n"
		unless (scalar @$rows == 100);
	die "fetchall_arrayref({}): not a hash\n"
		unless (ref $rows->[0] eq 'HASH');
	foreach (@{$sth->{NAME_lc}}) {
		die "fetchall_arrayref({}): $_ not found\n"
			unless exists $rows->[0]{$_};
	}
	_report($testno, 1, 'fetchall_arrayref({})');

	$sth->execute or die $sth->errstr;
	$rows = $sth->fetchall_arrayref({ col1 => 1, col10 => 1 });
	die "fetchall_arrayref({ col1 => 1, col10 => 1 }): not 100 rows\n"
		unless (scalar @$rows == 100);
	die "fetchall_arrayref({ col1 => 1, col10 => 1 }): not a hash\n"
		unless (ref $rows->[0] eq 'HASH');
	foreach (qw(col1 col10)) {
		die "fetchall_arrayref({ col1 => 1, col10 => 1 }): $_ not found\n"
			unless exists $rows->[0]{$_};
	}
	_report($testno, 1, 'fetchall_arrayref({ col1 => 1, col10 => 1 })');

	$sth->execute or die $sth->errstr;
	$rows = $sth->fetchall_arrayref({ col1 => 1, col10 => 1 }, 20);
	die "fetchall_arrayref({ col1 => 1, col10 => 1 }, 20): not 20 rows\n"
		unless (scalar @$rows == 20);
	die "fetchall_arrayref({ col1 => 1, col10 => 1 }, 20): not a hash\n"
		unless (ref $rows->[0] eq 'HASH');
	foreach (qw(col1 col10)) {
		die "fetchall_arrayref({ col1 => 1, col10 => 1 }, 20): $_ not found\n"
			unless exists $rows->[0]{$_};
	}
	_report($testno, 1, 'fetchall_arrayref({ col1 => 1, col10 => 1 }, 20)');

#
#	fetchall_hashref
#
	$sth->execute or die $sth->errstr;
	$rows = $sth->fetchall_hashref('col1');
	die "fetchall_hashref(col1): not a hash\n"
		unless (ref $rows eq 'HASH');
	die "fetchall_hashref(col1): not 100 rows\n"
		unless (scalar keys %$rows == 100);
	my $key = (keys %$rows)[0];
	my @colnames = qw(col2 col3 col4 col5 col6 col7 col8 col9 col10 col11 col12 col13);
	push @colnames, qw(col14 col15 col16 col17)
		if $use_bigdata;
	foreach (@colnames) {
		die "fetchall_hashref(col1): $_ not found\n"
			unless exists $rows->{$key}{$_};
	}
	_report($testno, 1, "fetchall_hashref('col1')");

	$sth->execute or die $sth->errstr;
	$rows = $sth->fetchall_hashref( [ 'col1', 'col2' ]);
	die "fetchall_hashref([col1, col2]): not a hash\n"
		unless (ref $rows eq 'HASH');
	die "fetchall_hashref([col1, col2]): not 100 rows\n"
		unless (scalar keys %$rows == 100);
	$key = (keys %$rows)[0];
	my $col2 = (keys %{$rows->{$key}})[0];
	shift @colnames;
	foreach (@colnames) {
		die "fetchall_hashref([col1, col2]): $_ not found\n"
			unless exists $rows->{$key}{$col2}{$_};
	}
	_report($testno, 1, "fetchall_hashref([ 'col1', 'col2' ])");
#
#	dump_results
#
	my $dumpvar = '';
    open( my $fh, "+>:scalar", \$dumpvar );
	$sth->execute or die $sth->errstr;
	$rows = $sth->dump_results(50, "\n", '|', $fh);
	close $fh;
	my @lines = split /\n/, $dumpvar;
	_report($testno, (scalar @lines == 101), 'dump_results(50, "\n", \'|\')');
#
#	selectrow_array
#
	my @row = $dbh->selectrow_array(
	'select * from alltypetst order by col1 sample 100'
	);
	die "selectrow_array: " . $dbh->errstr . "\n"
		unless @row;
	die "selectrow_array: not $ttl_columns columns\n"
		unless (scalar @row == $ttl_columns);
	_report($testno, 1, 'selectrow_array()');
#
#	selectrow_arrayref
#
	$rows = $dbh->selectrow_arrayref(
	'select * from alltypetst order by col1 sample 100'
	);
	die "selectrow_arrayref: " . $dbh->errstr . "\n"
		unless $rows;
	die "selectrow_arrayref: not $ttl_columns columns\n"
		unless (scalar @$rows == $ttl_columns);
	_report($testno, 1, 'selectrow_arrayref()');
#
#	selectrow_hashref
#
	$rows = $dbh->selectrow_hashref(
	'select * from alltypetst order by col1 sample 100'
	);
	unshift @colnames, qw(col1 col2);
	foreach (@colnames) {
		die "selectrow_hashref(): $_ not found\n"
			unless exists $rows->{$_};
	}
	_report($testno, 1, 'selectrow_hashref()');
#
#	selectall_arrayref
#
	$rows = $dbh->selectall_arrayref(
	'select * from alltypetst order by col1 sample 100'
	)
	or die "selectall_arrayref(): " . $dbh->errstr . "\n";
	die "selectall_arrayref(): not 100 rows\n"
		unless (scalar @$rows == 100);
	die "selectall_arrayref(): not $ttl_columns columns\n"
		unless (scalar @{$rows->[0]} == $ttl_columns);
	_report($testno, 1, 'selectall_arrayref()');

	$rows = $dbh->selectall_arrayref(
	'select * from alltypetst order by col1 sample 100',
	{ Slice => [0, 1, -2, -1] }
	)
	or die "selectall_arrayref(\$slice): " . $dbh->errstr . "\n";
	die "selectall_arrayref(\$slice): not 100 rows\n"
		unless (scalar @$rows == 100);
	die "selectall_arrayref(\$slice): not 4 columns\n"
		unless (scalar @{$rows->[0]} == 4);
	_report($testno, 1, 'selectall_arrayref({ Slice => [0, 1, -2, -1] })');

	$rows = $dbh->selectall_arrayref(
	'select * from alltypetst order by col1 sample 100',
	{ Slice => {} }
	)
	or die "selectall_arrayref({}): " . $dbh->errstr . "\n";
	die "selectall_arrayref({}): not 100 rows\n"
		unless (scalar @$rows == 100);
	die "selectall_arrayref({}): not a hash\n"
		unless (ref $rows->[0] eq 'HASH');
	foreach (@{$sth->{NAME_lc}}) {
		die "selectall_arrayref({}): $_ not found\n"
			unless exists $rows->[0]{$_};
	}
	_report($testno, 1, 'selectall_arrayref({ Slice => {} })');

	$rows = $dbh->selectall_arrayref(
	'select * from alltypetst order by col1 sample 100',
	{ Slice => { col1 => 1, col10 => 1 } }
	)
	or die "selectall_arrayref({ col1 => 1, col10 => 1 }): " . $dbh->errstr . "\n";
	die "selectall_arrayref({ col1 => 1, col10 => 1 }): not 100 rows\n"
		unless (scalar @$rows == 100);
	die "selectall_arrayref({ col1 => 1, col10 => 1 }): not a hash\n"
		unless (ref $rows->[0] eq 'HASH');
	foreach (qw(col1 col10)) {
		die "selectall_arrayref({ col1 => 1, col10 => 1 }): $_ not found\n"
			unless exists $rows->[0]{$_};
	}
	_report($testno, 1, 'selectall_arrayref({ Slice => { col1 => 1, col10 => 1 } })');

	$rows = $dbh->selectall_arrayref(
	'select * from alltypetst order by col1 sample 100',
	{
		Slice => { col1 => 1, col10 => 1 },
		MaxRows => 20
	}
	)
	or die "selectall_arrayref({ col1 => 1, col10 => 1 }, 20): " . $dbh->errstr . "\n";
	die "selectall_arrayref({ col1 => 1, col10 => 1 }, 20): not 20 rows\n"
		unless (scalar @$rows == 20);
	die "selectall_arrayref({ col1 => 1, col10 => 1 }, 20): not a hash\n"
		unless (ref $rows->[0] eq 'HASH');
	foreach (qw(col1 col10)) {
		die "selectall_arrayref({ col1 => 1, col10 => 1 }, 20): $_ not found\n"
			unless exists $rows->[0]{$_};
	}
	_report($testno, 1, 'selectall_arrayref(Slice => { col1 => 1, col10 => 1 }, MaxRows => 20)');
#
#	selectall_hashref
#
	$rows = $dbh->selectall_hashref(
	'select * from alltypetst order by col1 sample 100',
	'col1'
	)
	or die "selectall_hashref(col1): " . $dbh->errstr . "\n";
	die "selectall_hashref(col1): not a hash\n"
		unless (ref $rows eq 'HASH');
	die "selectall_hashref(col1): not 100 rows\n"
		unless (scalar keys %$rows == 100);
	$key = (keys %$rows)[0];
	shift @colnames;
	foreach (@colnames) {
		die "selectall_hashref(col1): $_ not found\n"
			unless exists $rows->{$key}{$_};
	}
	_report($testno, 1, "selectall_hashref('col1')");

	$rows = $dbh->selectall_hashref(
	'select * from alltypetst order by col1 sample 100',
	[ 'col1', 'col2' ]
	)
	or die "selectall_hashref(col1): " . $dbh->errstr . "\n";
	die "selectall_hashref([col1, col2]): not a hash\n"
		unless (ref $rows eq 'HASH');
	die "selectall_hashref([col1, col2]): not 100 rows\n"
		unless (scalar keys %$rows == 100);
	$key = (keys %$rows)[0];
	$col2 = (keys %{$rows->{$key}})[0];
	shift @colnames;
	foreach (@colnames) {
		die "selectall_hashref([col1, col2]): $_ not found\n"
			unless exists $rows->{$key}{$col2}{$_};
	}
	_report($testno, 1, "selectall_hashref([ 'col1', 'col2' ])");
#
#	selectcol_arrayref
#
	$rows = $dbh->selectcol_arrayref(
	'select * from alltypetst order by col1 sample 100'
	)
	or die "selectcol_arrayref(): " . $dbh->errstr . "\n";
	die "selectcol_arrayref(): not 100 rows\n"
		unless (scalar @$rows == 100);
	_report($testno, 1, 'selectcol_arrayref()');

	$rows = $dbh->selectcol_arrayref(
	'select * from alltypetst order by col1 sample 100',
	{ Columns => [1, 10] }
	)
	or die "selectcol_arrayref([1, 10]): " . $dbh->errstr . "\n";
	die "selectcol_arrayref([1, 10]): not 200 elements\n"
		unless (scalar @$rows == 200);
	_report($testno, 1, 'selectcol_arrayref({ Columns => [1, 10] })');
#
#	need to implement these, deferred for now
#
if (1 == 0) {
    my @sources = $dbh->data_sources();
	$sth = $dbh->column_info();
	$rows = $sth->fetchall_arrayref();

	$sth = $dbh->primary_key_info();
	$rows = $sth->fetchall_arrayref();

	$sth = $dbh->primary_key();
	$rows = $sth->fetchall_arrayref();

	$sth = $dbh->foreign_key_info();
	$rows = $sth->fetchall_arrayref();

	$sth = $dbh->prepare("select * from alltypetst where col1 < 100; select * from alltypetst where col1 > 1000;");
	$sth->execute();
	my $row;
	while (1) {
		while ($row = $sth->fetchrow_arrayref()) {
		};
		last unless $sth->more_results();
	}
}
	print STDERR "DBI API methods ok\n";
	return 1;
}

1;