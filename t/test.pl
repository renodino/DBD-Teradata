#!/usr/local/bin/perl
#
#	testutf8.pl - UTF8 based test suite for DBD::Teradata
#
BEGIN {
	push @INC, './t';
}

use DBI;
use DBI qw(:sql_types);
use FileHandle;
use DBD::Teradata;
#use DBD::Teradata::Utility;
#use DBD::Teradata::Cli;
use Encode;
use Time::HiRes qw(time);
use Config;
use TdTestDataGen qw(gen_test_data gen_big_test_data);
use TdTestBulkload qw(
	load_nb_raw
	load_nb_vartext
	load_thrd_raw
	load_thrd_vartext);
use TdTestArrayBind qw(
	ary_using_load
	ary_named_using_load
	ary_sth_using_load
	ary_ph_load
	ary_sth_ph_load
	ary_load_using_raw_tuples
	ary_load_using_raw_fetch
	ary_load_using_vartext_tuples
	ary_load_using_vartext_fetch
	ary_error_load
	);
use TdTestBigInt qw(dectests);
use TdTestDBIAPI qw(dbiapitests);
use TdTestSSVText qw(ssvtexttests);
use TdTestBigSQL qw(bigsqltest);
use TdTestCursors qw(
	init_for_cursors
	updatable_cursor
	persistent_cursor
	rewind_cursor);
use TdTestUnicode qw(unitests);

*STDERR = *STDOUT;

my %typestr = (
	SQL_VARCHAR, 'VARCHAR',
	SQL_CHAR, 'CHAR',
	SQL_FLOAT, 'FLOAT',
	SQL_DECIMAL, 'DECIMAL',
	SQL_INTEGER, 'INTEGER',
	SQL_SMALLINT, 'SMALLINT',
	SQL_TINYINT, 'TINYINT',
	SQL_VARBINARY, 'VARBINARY',
	SQL_BINARY, 'BINARY',
	SQL_LONGVARBINARY, 'LONG VARBINARY',
	SQL_DATE, 'DATE',
	SQL_TIMESTAMP, 'TIMESTAMP',
	SQL_TIME, 'TIME'
	);

use strict;
use warnings;

no warnings 'threads';

my $dbh;
#
#	process cmdline options
#
my $label;
my %opts = ( '-n', 1, '-f', 2, '-m', 3, '-x', 4, '-p', 5, '-c', 6,
	'-u', 7, '-s', 8, '-d', 9, '-t', 10, '-l', 11, '-v', 12, '-A', 13);
my $logfile = undef;
# do all of normal, fastload, mload, export, loopback, monitor, and console
#	tests 2 session limit, no tracing, threads on, CLI adapter on, default Teradata version
my @specials = (1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 1, 1, undef, undef);

#
#	for mp debug
#$DB::fork_TTY = '/dev/ttyp2';


my $doall = 1;
if ($ARGV[0]=~/^-/) {
	@specials = (0) x 12;
	$specials[8] = 2;	# deflt util sesscount
	$specials[10] = 1;	# deflt threads enabled
	$specials[11] = 1;	# deflt use cli enabled
	while (1) {
		last
			unless ($ARGV[0]=~/^-/);

		$label = shift @ARGV;

		usage() and exit
			if ($label eq '-h');

		die "Unknown option $label; valid options are -[hnfmxpcusdlv]\n"
			unless $opts{$label};

		$doall = 0 if ($opts{$label} < 8);
		$specials[$opts{$label}] = 1;

		$specials[$opts{$label}] = shift @ARGV
			if ($label eq '-l');

		$specials[$opts{$label}] = shift @ARGV
			if (($label eq '-t') && ($ARGV[0]=~/^[012]$/));

		$logfile = shift @ARGV
			if ($label eq '-d');

		$specials[8] = shift @ARGV
			if (($label eq '-s') && ($ARGV[0]=~/^\d+$/));

		$specials[12] = shift @ARGV
			if ($label eq '-v');

		$specials[13] = 1
			if ($label eq '-A');
	}
	@specials[1..7] = (1,1,1,1,1,1,1) if $doall;
}

my ($dsn, $userid, $passwd, $dfltdb) = @ARGV;

$dsn = $ENV{'TDAT_DBD_DSN'},
$userid = $ENV{'TDAT_DBD_USER'},
$passwd = $ENV{'TDAT_DBD_PASSWORD'}
	unless defined($dsn) && defined($userid) && defined($passwd);

die "No host defined...check TDAT_DBD_DSN environment variable\n"
	unless defined($dsn);

die "No userid defined...check TDAT_DBD_USER environment variable\n"
	unless defined($userid);

die "No password defined...check TDAT_DBD_PASSWORD environment variable\n"
	unless defined($passwd);

unlink($logfile),
DBI->trace(2, $logfile),
$ENV{TDAT_DBD_DEBUG} = 1
	if $specials[9];

$ENV{TDAT_DBD_NO_CLI} = 1
	unless $specials[11];

my $versnum = $specials[12];

$dsn .= ";DATABASE=$dfltdb"
	if $dfltdb;

print STDERR "Logging onto $dsn as $userid...\n";
########################
#$DBD::Teradata::Cli::debug = 1;
##########################

$dbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		tdat_charset => 'UTF8',
		tdat_mode => 'TERADATA',
	}
) || die "Can't connect to $dsn: $DBI::errstr. Exiting...\n";
print STDERR join('',
	"Logon to $dsn ver. ",
	$dbh->{tdat_version},
	' (',
	'VERSNUM: ', $dbh->{tdat_versnum},
	' MODE: ', $dbh->{tdat_mode},
	" mode) ok.\n");
my $drh = $dbh->{Driver};
print STDERR "DBD::Teradata v. $drh->{Version}\n";

#$specials[10] = 0,
#print STDERR "Using CLI; thread tests disabled.\n"
#	if $dbh->{tdat_uses_cli};

die "Did not connect with CLI adapter, check your configuration."
	if $specials[11] && (! $dbh->{tdat_uses_cli});

$dbh->{tdat_versnum} = $versnum,
print STDERR "*** Emulating Teradata Version $versnum\n"
	if $versnum && (int($versnum/100) != int($dbh->{tdat_versnum}/100));

print STDERR "The following tests will be performed:\n";
print STDERR "Normal SQL with raw and vartext extensions\n" if $specials[1];
print STDERR "Fastload with raw and vartext extensions\n" if $specials[2];
print STDERR "Multiload with raw and vartext extensions\n" if $specials[3];
print STDERR "Fastexport\n" if $specials[4];
print STDERR "PM/API\n" if $specials[5];
print STDERR "Remote Console\n" if $specials[6];
print STDERR "Utility Loopback\n" if $specials[7];
print STDERR "Thread tests " . ($specials[10] ? ($specials[10] == 1) ? 'enabled' : 'only' : 'disabled') . "\n";
print STDERR "CLI adapter " . ($specials[11] ? 'enabled.' : 'disabled.') . "\n";
print STDERR "\n";

my ($i, $j, $rc, $rowcnt, $row);
my $sescnt = 7;

my ($sth, $ssth, $stmtnum, $stmtinfo,, $stmthash);
my ($updsth, $delsth, $reccnt, $ostarted, $fmostarted);
my ($bcstarted, $threshold);
my ($rows, $ristarted, $tristarted, $rbcstarted, $len);
my ($rvstarted, $trvstarted, $mprfestarted, $tmprfestarted, $rostarted, $vostarted);
my ($austarted, $apstarted, $aunstarted, $ausstarted, $apsstarted,
	$aitstarted, $avtstarted, $aifstarted, $avfstarted);
#
#	force dateform to integer
#
$dbh->do('set session dateform=integerdate');
#
#	pre-gen some data
#
my $use_bigdata = ($dbh->{tdat_has_bignum} && ($dbh->{tdat_max_dec_prec} > 18));
(!$use_bigdata) ? gen_test_data() : gen_big_test_data()
	unless (-e 'rawdata.dat') && (-e 'utf8data.txt') &&
		(-s 'rawdata.dat') && (-s 'utf8data.txt');

if ($specials[1]) {

###################################################
#
#	DSN attribute tests
#
###################################################
print STDERR "Testing DSN attributes...\n";

my $testdsn = "dbi:Teradata:$dsn;CHARSET=UTF8;CLI=NO;MODE=TERADATA;REQBUFSZ=64300;RSPBUFSZ=64300";

my $dsndbh = DBI->connect($testdsn, $userid, $passwd)
	or die "DSN Attributes failed: $DBI::errstr\n";
#
#	verify the attributes took
#	NOTE: can't do DATABASE test, since we don't know if we have one
#	NOTE2: Likewise for ACCOUNT...
#
if (($dsndbh->{tdat_charset} ne 'UTF8') ||
	$dsndbh->{tdat_uses_cli} ||
	($dsndbh->{tdat_mode} ne 'TERADATA') ||
	($dsndbh->{tdat_reqsize} != 64300) ||
	($dsndbh->{tdat_respsize} != 64300)) {
	print STDERR "DSN attributes FAILED.\n*** DSN is $testdsn\n*** Reported values are\n";
	print STDERR "\t$_: ", $dsndbh->{$_}, "\n"
		foreach (qw(tdat_charset tdat_uses_cli tdat_mode tdat_reqsize tdat_respsize));
}
else {
	$dsndbh->disconnect;

	$testdsn = "dbi:Teradata:$dsn;CHARSET=ASCII;CLI=0;MODE=ANSI;REQBUFSZ=64300;RSPBUFSZ=64300";

	$dsndbh = DBI->connect($testdsn, $userid, $passwd)
		or die "DSN Attributes failed: $DBI::errstr\n";
#
#	verify the attributes took
#
	if (($dsndbh->{tdat_charset} ne 'ASCII') ||
		$dsndbh->{tdat_uses_cli} ||
		($dsndbh->{tdat_mode} ne 'ANSI') ||
		($dsndbh->{tdat_reqsize} != 64300) ||
		($dsndbh->{tdat_respsize} != 64300)) {
		print STDERR "DSN attributes FAILED.\n*** DSN is $testdsn\n*** Reported values are\n";
		print STDERR "\t$_: ", $dsndbh->{$_}, "\n"
			foreach (qw(tdat_charset tdat_uses_cli tdat_mode tdat_reqsize tdat_respsize));
	}
	else {
		print STDERR "DSN attributes ok.\n";
	}
}
$dsndbh->disconnect;

###################################################
#
#	buffer size adjustment test
#
###################################################
print STDERR "Testing Buffer adjustment and large responses...\n";
#
#	first, big buffers
#
$dbh->{tdat_reqsize} = (2**17) - 1;
$dbh->{tdat_respsize} = (2**19) - 1;

my $respchk = $dbh->selectall_arrayref('select user, date, time')
	or die "USER/DATE/TIME: Can't adjust buffers: " . $dbh->errstr . "\n";

die "Unexpected results\n"
	unless (scalar @$respchk == 1) &&
		(scalar @{$respchk->[0]} == 3);
#
#	adjust returned length depending on database version;
#	as of R6.1, USER is returned as UNICODE
#
my $userlen = ($dbh->{tdat_versnum} < 5010000) ? 30000 : 10000;
$respchk = $dbh->selectall_arrayref("SELECT user(char($userlen))", {tdat_formatted => 1})
	or die "USER(30000): Can't adjust buffers: " . $dbh->errstr . "\n";

die "Unexpected results\n"
	unless (scalar @$respchk == 1) &&
		(scalar @{$respchk->[0]} == 1) &&
		(length($respchk->[0][0]) == $userlen);

#
#	then, small buffers
#
$dbh->{tdat_reqsize} = (2**8) - 1;
$dbh->{tdat_respsize} = (2**10) - 1;

$sth = $dbh->prepare( "SELECT user(char($userlen))", {tdat_formatted => 1})
	or die "Small buffer/large resp: " . $dbh->errstr . "\n";
$rc = $sth->execute;
die $sth->errstr . "\n"
	unless defined($rc);

die "Unexpected results\n"
	unless (scalar @$respchk == 1) &&
		(scalar @{$respchk->[0]} == 1) &&
		(length($respchk->[0][0]) == $userlen);

print STDERR "Buffer adjustment w/ large response ok.\n";

print STDERR '*** bigint is ', ($dbh->{tdat_no_bigint} ? 'disabled' : 'enabled'), ' and bignum is ',
	($use_bigdata ? '' : 'not '), "available\n";

###################################################
#
#	test metadata
#
###################################################
print STDERR "Test metadata...\n";
my @tbls = $dbh->tables;
my $tblcnt = ($#tbls > 10) ? 10 : $#tbls;
print "Partial table listing:\n" if ($tblcnt < $#tbls);
print join("\n", @tbls[0..$tblcnt]), "\n";

$sth = $dbh->table_info;
my $names = $sth->{NAME};
$tblcnt = 0;
while ($row = $sth->fetchrow_arrayref) {
	$tblcnt++;
	last if ($tblcnt > 10);
	print $$names[$_], ': ', (defined($$row[$_]) ? $$row[$_] : 'NULL'), "\n"
		foreach (0..$#$row);
}

my $typeinfo = $dbh->type_info_all() || die "Can't get type info: " . $dbh->errstr . "\n";

my $srvname = $dbh->get_info(13) || die "Can't get_info(SQL_SERVER_NAME): " . $dbh->errstr . "\n";
$srvname = $dbh->get_info(17) || die "Can't get_info(SQL_DBMS_NAME): " . $dbh->errstr . "\n";
die "Invalid DBMS name $srvname\n" unless ($srvname eq 'Teradata');

print STDERR (($tblcnt > 0) ? "Metadata OK.\n" : "Metadata failed.\n");
###################################################
#
#	test large response
#
###################################################
if ($dbh->{tdat_versnum} < 6000000) {
	print STDERR "Large response not supported, skipping...\n";
}
else {
	print STDERR "Test large response...\n";
	my $lsth = $dbh->prepare('select * from dbc.columnsx order by databasename, tablename, columnname')
		or die "Can't prepare large response request: " . $dbh->errstr . "\n";
	$lsth->execute
		or die "Can't execute large response request: " . $lsth->errstr . "\n";
	my $rowcnt = 0;
	my $row;
	while ($row = $lsth->fetchrow_arrayref) {
		print "\r Recv'd $rowcnt rows..."
			unless ++$rowcnt % 100;
	}
	print STDERR "\n$rowcnt rows returned.\n";
	print STDERR "Large response OK.\n";
}


###################################################
#
#	test DDL
#
###################################################
print STDERR "Testing DDL...\n";
$dbh->do( 'DROP TABLE alltypetst');
($dbh->err != 3807) ? die $dbh->errstr : print STDERR $dbh->errstr . "\n"
	if $dbh->err;

my $mktable = 
'CREATE TABLE alltypetst, NO FALLBACK (
col1 integer,
col2 smallint,
col3 byteint,
col4 char(20) character set unicode,
col5 varchar(100) character set unicode,
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0))
unique primary index(col1);';

my $mkbigtable =
'CREATE TABLE alltypetst, NO FALLBACK (
col1 integer,
col2 smallint,
col3 byteint,
col4 char(20) character set unicode,
col5 varchar(100) character set unicode,
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0),
col14 bigint,
col15 decimal(18,0),
col16 decimal(27,14),
col17 decimal(38,16))
unique primary index(col1);';

my $sql = (!$use_bigdata) ? $mktable : $mkbigtable;
my $ctsth = $dbh->prepare($sql) || die ($dbh->errstr . "\n");

$rc = $ctsth->execute;
die ($ctsth->errstr . "\n") unless defined($rc);
###################################################
#
#	test result when update to empty table
#
###################################################
print STDERR "Update empty table...\n";
$rc = $dbh->do('UPDATE alltypetst SET col2 = 23 WHERE col1 = 10');
die ($ctsth->errstr . "\n") unless defined($rc);
print STDERR "Update empty table OK\n";

print STDERR "SHOW TABLE...\n";
$sth = $dbh->prepare('SHOW TABLE alltypetst') || die ($dbh->errstr . "\n");
$rc = $sth->execute;
die ($sth->errstr . "\n") unless defined($rc);
$names = $sth->{NAME};

while ($row = $sth->fetchrow_arrayref() ) {
	foreach (0..$#$row) {
		if (defined($$row[$_])) {
			$$row[$_]=~s/\r/\n/g;
			print "$$names[$_]:\n$$row[$_]\n";
		}
		else {
			print "$$names[$_]: NULL\n";
		}
	}
	print "\n";
}
print STDERR "SHOW TABLE OK\n";

print STDERR "HELP TABLE...\n";
$sth = $dbh->prepare('HELP TABLE alltypetst') || die ($dbh->errstr . "\n");
$rc = $sth->execute;
die ($sth->errstr . "\n") unless defined($rc);
$names = $sth->{NAME};

while ($row = $sth->fetchrow_arrayref() ) {
	print $$names[$_], ': ', (defined($$row[$_]) ? $$row[$_] : 'NULL'), "\n"
		foreach (0..$#$row);
	print "\n";
}
print STDERR "HELP TABLE OK\n";

print STDERR "EXPLAIN...\n";
$sth = $dbh->prepare('EXPLAIN select * from alltypetst') || die ($dbh->errstr . "\n");
$sth->execute or die ($sth->errstr . "\n");

while ($row = $sth->fetchrow_arrayref() ) {
	foreach (@$row) {
		print "NULL\n" and next
			unless defined($_);

		$_=~s/\r/\n/g;
		print $_;
	}
	print "\n";
}
print STDERR "EXPLAIN OK\n";

###################################################
#
#	test MACRO execution
#
###################################################
print STDERR "Testing Macro creation...\n";

$rc = $dbh->do( 'DROP MACRO dbitest');
die $dbh->errstr unless
	(defined($rc) || ($dbh->err == 3824));
#print STDERR $dbh->errstr . "\n" if $dbh->err;

my $mkmacro = 
'CREATE MACRO dbitest(col1 integer,
col2 smallint,
col3 byteint,
col4 char(20) character set unicode,
col5 varchar(100) character set unicode,
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 DATE,
col12 TIME,
col13 TIMESTAMP(0)) AS (
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
:col7, :col8, :col9, :col10, :col11, :col12, :col13);
 /* now read it back */
SELECT * FROM alltypetst; );';

my $mkbigmacro = 
'CREATE MACRO dbitest(col1 integer,
col2 smallint,
col3 byteint,
col4 char(20) character set unicode,
col5 varchar(100) character set unicode,
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 DATE,
col12 TIME,
col13 timestamp(0),
col14 bigint,
col15 decimal(18,0),
col16 decimal(27,14),
col17 decimal(38,16)
) AS (
INSERT INTO alltypetst VALUES(:col1, :col2, :col3, :col4, :col5, :col6,
:col7, :col8, :col9, :col10, :col11, :col12, :col13, :col14, :col15, :col16, :col17);
 /* now read it back */
SELECT * FROM alltypetst; );';

$sql = (!$use_bigdata) ? $mkmacro : $mkbigmacro;
my $cmsth = $dbh->prepare($sql) || die ($dbh->errstr . "\n");
$cmsth->execute or die ($cmsth->errstr . "\n");

print STDERR "DROP/CREATE MACRO ok.\n";
#
#	now test all datatypes as bound params
#	and placeholders
#
print STDERR "Testing multiple prepared statements, placeholders, and explicit commit...\n";
$dbh->{AutoCommit} =  0;

my $insert = 
'INSERT INTO alltypetst VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?(time(6)), ?(timestamp(0)))';
my $biginsert = 
'INSERT INTO alltypetst VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?(time(6)), ?(timestamp(0)), ?, ?, ?, ?)';

$sql = (!$use_bigdata) ? $insert : $biginsert;
my $isth = $dbh->prepare($sql) || die ($dbh->errstr . "\n");
my $dsth = $dbh->prepare( 'DELETE FROM alltypetst') || die ($dbh->errstr . "\n");
$ssth = $dbh->prepare('SELECT * FROM alltypetst ORDER BY col1',
	{ChopBlanks => 1}) || die ($dbh->errstr . "\n");
#
#	insert a row
#
print STDERR "Test explicit param binding...\n";
my @invals = (123456, 1234, 12, 'perl is great', 'okey dokey',
12.34567, 1.2, 12.34, 1234.5678, 123456789.01234,
1021121, '11:21:02.034500', '2002-11-21 11:21:02',
123456789012345,
123456789012345678,
1234567890123.45678901234567,
1234567890123456789012.3456789012345678
);

$isth->bind_param(1, 123456) || die ($isth->errstr . "\n");
$isth->bind_param(2, 1234) || die ($isth->errstr . "\n");
$isth->bind_param(3, 12) || die ($isth->errstr . "\n");
$isth->bind_param(4, 'perl is great') || die ($isth->errstr . "\n");
$isth->bind_param(5, 'okey dokey') || die ($isth->errstr . "\n");
$isth->bind_param(6, 12.34567) || die ($isth->errstr . "\n");
$isth->bind_param(7, 1.2) || die ($isth->errstr . "\n");
$isth->bind_param(8, 12.34) || die ($isth->errstr . "\n");
$isth->bind_param(9, 1234.5678) || die ($isth->errstr . "\n");
$isth->bind_param(10, 123456789.01234) || die ($isth->errstr . "\n");
$isth->bind_param(11, '2002-11-21') || die ($isth->errstr . "\n");
$isth->bind_param(12, '11:21:02.0345') || die ($isth->errstr . "\n");
$isth->bind_param(13, '2002-11-21 11:21:02') || die ($isth->errstr . "\n");
if ($use_bigdata) {
	$isth->bind_param(14, 123456789012345) || die ($isth->errstr . "\n");
	$isth->bind_param(15, 123456789012345678) || die ($isth->errstr . "\n");
	$isth->bind_param(16, 1234567890123.45678901234567) || die ($isth->errstr . "\n");
	$isth->bind_param(17, 1234567890123456789012.3456789012345678) || die ($isth->errstr . "\n");
}
$isth->execute or die ($isth->errstr . "\n");
#
#	make sure the returned values are the same
#	as we inserted
#
$names = $ssth->{NAME};
$ssth->execute or die ($ssth->errstr . "\n");
while ($row = $ssth->fetchrow_arrayref() ) {
	foreach (0..$#$row) {
		print $$names[$_], ': ', (defined($$row[$_]) ? $$row[$_] : 'NULL'), "\n";
		print "WARNING: field $$names[$_] does not match: src len ", length($invals[$_]), " recv len ", length($$row[$_]), "\n"
			if ($$row[$_] ne $invals[$_]);
	}
	print "\n";
}
print STDERR "Explicit param binding OK\n";

###################################################
#
#	now try passing wo/ bind
#
###################################################
print STDERR "Test default param binding...\n";

if (!$use_bigdata) {
$isth->execute(234567, 2345, 23, 'perl is great',
	'a really long string to test that default bindings get adjusted',
	12.3456, 1.2, 12.34, 1234.5678, -12345679.01234,
	'2002-12-20', '23:43:56.098', undef) or die ($isth->errstr . "\n");
}
else {
$isth->execute(234567, 2345, 23, 'perl is great',
	'a really long string to test that default bindings get adjusted',
	12.3456, 1.2, 12.34, 1234.5678, -12345679.01234,
	'2002-12-20', '23:43:56.098', undef,
	123456789012345,
	123456789012345678,
	1234567890123.45678901234567,
	1234567890123456789012.3456789012345678) or die ($isth->errstr . "\n");
}

$names = $ssth->{NAME};
$ssth->execute || die ($ssth->errstr . "\n");
while ($row = $ssth->fetchrow_arrayref() ) {
	print $$names[$_], ': ', (defined($$row[$_]) ? $$row[$_] : 'NULL'), "\n"
		foreach (0..$#$row);
	print "\n";
}
print STDERR "Default param binding OK\n";
###################################################
#
#	test explicit column binding
#
###################################################
print STDERR "Testing explicit column binding...\n";
my ($bcol1, $bcol2, $bcol3, $bcol4, $bcol5, $bcol6, $bcol7, $bcol8, $bcol9, $bcol10, $bcol11, $bcol12, $bcol13, $bcol14, $bcol15, $bcol16, $bcol17);
$ssth->bind_col(1, \$bcol1);
$ssth->bind_col(2, \$bcol2);
$ssth->bind_col(3, \$bcol3);
$ssth->bind_col(4, \$bcol4);
$ssth->bind_col(5, \$bcol5);
$ssth->bind_col(6, \$bcol6);
$ssth->bind_col(7, \$bcol7);
$ssth->bind_col(8, \$bcol8);
$ssth->bind_col(9, \$bcol9);
$ssth->bind_col(10, \$bcol10);
$ssth->bind_col(11, \$bcol11);
$ssth->bind_col(12, \$bcol12);
$ssth->bind_col(13, \$bcol13);
if ($use_bigdata) {
	$ssth->bind_col(14, \$bcol14);
	$ssth->bind_col(15, \$bcol15);
	$ssth->bind_col(16, \$bcol16);
	$ssth->bind_col(17, \$bcol17);
}
$ssth->execute or die $ssth->errstr;

while ($ssth->fetch) {
	$bcol13 ||= 'NULL';
	if (!$use_bigdata) {
		print join(', ', $bcol1, $bcol2, $bcol3, $bcol4, $bcol5, $bcol6, $bcol7, $bcol8, $bcol9, $bcol10, $bcol11, $bcol12, $bcol13), "\n";
	}
	else {
		print join(', ', $bcol1, $bcol2, $bcol3, $bcol4, $bcol5, $bcol6, $bcol7, $bcol8, $bcol9, $bcol10, $bcol11, $bcol12, $bcol13, $bcol14, $bcol15, $bcol16, $bcol17), "\n";
	}
}
print STDERR "Explicit column binding OK\n";
#
#	clean up
#
$dbh->commit || die ($dbh->errstr . "\n");

$dbh->{AutoCommit} = 1;

$dsth->execute or die ($dsth->errstr . "\n");

print STDERR "DELETE/Parameterized INSERT/SELECT, and commit() ok.\n";

###################################################
#
#	test the MACRO execution
#
###################################################
print STDERR "Testing MACRO execution w/ USING...\n";

my $callmacro = 'USING (col1 INTEGER,
col2 SMALLINT,
col3 BYTEINT,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0))
EXEC dbitest(:col1, :col2, :col3, :col4, :col5, :col6, :col7, :col8, :col9, :col10,
:col11, :col12, :col13)';

my $callbigmacro = 'USING (col1 INTEGER,
col2 SMALLINT,
col3 BYTEINT,
col4 char(40),
col5 varchar(200),
col6 float,
col7 decimal(2,1),
col8 decimal(4,2),
col9 decimal(8,4),
col10 decimal(14,5),
col11 date,
col12 time,
col13 timestamp(0),
col14 bigint,
col15 decimal(18,0),
col16 decimal(27,14),
col17 decimal(38,16)
)
EXEC dbitest(:col1, :col2, :col3, :col4, :col5, :col6, :col7, :col8, :col9, :col10,
:col11, :col12, :col13, :col14, :col15, :col16, :col17)';

$sql = (!$use_bigdata) ? $callmacro : $callbigmacro;
$isth = $dbh->prepare($sql) || die ($dbh->errstr . "\n");
$isth->bind_param(1, 123456) || die ($isth->errstr . "\n");
$isth->bind_param(2, 1234) || die ($isth->errstr . "\n");
$isth->bind_param(3, 12) || die ($isth->errstr . "\n");
$isth->bind_param(4, 'rough and ready') || die ($isth->errstr . "\n");
$isth->bind_param(5, 'okey dokey') || die ($isth->errstr . "\n");
$isth->bind_param(6, 12.34567) || die ($isth->errstr . "\n");
$isth->bind_param(7, 1.2) || die ($isth->errstr . "\n");
$isth->bind_param(8, 12.34) || die ($isth->errstr . "\n");
$isth->bind_param(9, 1234.5678) || die ($isth->errstr . "\n");
$isth->bind_param(10, 123456789.01234) || die ($isth->errstr . "\n");
$isth->bind_param(11, 1021231) || die ($isth->errstr . "\n");
$isth->bind_param(12, '20:32:45.567') || die ($isth->errstr . "\n");
$isth->bind_param(13, '2002-12-20 11:22:33') || die ($isth->errstr . "\n");
if ($use_bigdata) {
	$isth->bind_param(14, 123456789012345) || die ($isth->errstr . "\n");
	$isth->bind_param(15, 123456789012345678) || die ($isth->errstr . "\n");
	$isth->bind_param(16, 1234567890123.45678901234567) || die ($isth->errstr . "\n");
	$isth->bind_param(17, 1234567890123456789012.3456789012345678) || die ($isth->errstr . "\n");
}
$rc = $isth->execute;
die ($isth->errstr . "\n") unless defined($rc);

$names = $isth->{NAME};
my $typestr = $isth->{tdat_TYPESTR};
while ($isth->{tdat_more_results}) {
	$stmthash = $isth->{tdat_stmt_info}->[$isth->{tdat_stmt_num}];
	print 'For statement ', $isth->{tdat_stmt_num}, ":\n";
	print "$_ is ", (defined($$stmthash{$_}) ? $$stmthash{$_} : 'undefined'), "\n"
		foreach (keys(%$stmthash));

	while ($row = $isth->fetchrow_arrayref) {
		print $$names[$_], '(', $$typestr[$_], '): ',
			(defined($$row[$_]) ? $$row[$_] : 'NULL'), "\n"
			foreach ($$stmthash{StartsAt}..$$stmthash{EndsAt});
		print "\n";
	}
}

print STDERR "MACRO execution w/ USING ok.\n";
###################################################
#
#	test summary support
#
###################################################
print STDERR "Testing summarized SELECT...\n";
my $sumsth = $dbh->prepare(
'select col1, col2, col9 from alltypetst with avg(col2), avg(col9) by col1
with sum(col2)') || die ($dbh->errstr . "\n");
$names = $sumsth->{NAME};
$sumsth->execute or die ($ssth->errstr . "\n");
$stmtnum = $sumsth->{'tdat_stmt_num'};
$stmtinfo = $sumsth->{'tdat_stmt_info'};
$stmthash = $$stmtinfo[1];
print "$_ is ", (defined($$stmthash{$_}) ? $$stmthash{$_} : 'undefined'), "\n"
	foreach (keys(%$stmthash));
my $sumstarts = $$stmthash{'SummaryStarts'};
my $sumends = $$stmthash{'SummaryEnds'};
my $colstart = $$stmthash{'StartsAt'};
my $colend = $$stmthash{'EndsAt'};

while ($row = $sumsth->fetchrow_arrayref() ) {
	if (defined($$stmthash{'IsSummary'})) {
		my $issum = $$stmthash{'IsSummary'};
		print "\n-------------------------------------\n";
		my $sumpos = $$stmthash{'SummaryPosition'};
		my $sumposst = $$stmthash{'SummaryPosStart'};
		for ($i = $$sumstarts[$issum], $j = $$sumposst[$issum];
			$i <= $$sumends[$issum]; $i++, $j++) {
			print "\t" x $$sumpos[$j], "$$names[$i] = $$row[$i],\n";
		}
	}
	else {
		print "$$names[$_] = $$row[$_], "
			foreach ($colstart..$colend);
	}
	print "\n";
}

print STDERR "Summarized SELECT ok.\n";

###################################################
#
#	test rows(), $DBI::rows
#
###################################################
print STDERR "Testing \$sth->rows, \$DBI::rows ...\n";
$dbh->do('create volatile table dbdtdat_rowtest(col1 int, col2 char(30))
on commit preserve rows') or die $dbh->errstr;

my $rowsok = 0;
my $sth = $dbh->prepare('insert into dbdtdat_rowtest values(?, ?)')
or die $dbh->errstr;

for (1..200) {
	$sth->execute($_, 'abcdef');
#	print "$_\n" unless $_%100;
}

$sth = $dbh->prepare("update dbdtdat_rowtest set col2 = 'xyz' where col2 = 'abcdef'");

$sth->execute;

#print "sth rows is ", $sth->rows, "\n";
#print "DBI rows is ", $DBI::rows, "\n";

$rowsok++ if ($sth->rows == 200);
$rowsok++ if ($DBI::rows == 200);

$sth = $dbh->prepare('select * from dbdtdat_rowtest');

$sth->execute;

$rowsok++ if ($sth->rows == 200);
$rowsok++ if ($DBI::rows == 200);

if ($rowsok == 4) {
	print STDERR "\$sth->rows, \$DBI::rows ok.\n";
}
else {
	print STDERR "\$sth->rows, \$DBI::rows FAILED.\n";
}

###################################################
#
#	test UNICODE handling
#
###################################################

if ($dbh->{tdat_versnum} < 5010000) {
	print STDERR "UNICODE PREPARE not supported, skipping...\n";
}
else {
	unitests($dbh);
}

###################################################
#
#	test auto-destruct of stmt handles
#
###################################################

print STDERR "Testing destruction of stmt handles ...\n";

my @sths = ();

my $currkids = $dbh->{Kids} + 10;

foreach (1..10) {
	push @sths, $dbh->prepare('select user, date, time')
		or die $dbh->errstr;
}

foreach (0..$#sths) {
	$sths[$_]->execute();
	my $rows = $sths[$_]->fetchall_arrayref();
}

if ($currkids == $dbh->{Kids}) {
	@sths = ();	# should destroy

	$currkids -= 10;
	if ($currkids != $dbh->{Kids}) {
		print STDERR "Destruction of stmt handles FAILED: Kids is ", $dbh->{Kids}, " should be $currkids\n";
	}
	else {
		print STDERR "Destruction of stmt handles OK\n";
	}
}
else {
	print STDERR "Destruction of stmt handles FAILED: Kids is ", $dbh->{Kids}, " should be $currkids\n";
}

###################################################
#
#	test BigInt vs. float decimals
#
###################################################

dectests($dbh);

###################################################
#
#	test Big SQL
#
###################################################

bigsqltest($dbh)
	if ($dbh->{tdat_versnum} >= 5000000);

###################################################
#
#	test stored procedures
#
###################################################
if ($dbh->{tdat_versnum} >= 4000000) {
	eval {
		require TdTestProcs;
		import TdTestProcs qw(sptests);
	};
	die "Unable to load TdTestProcs: $@"
		if $@;
	sptests($dbh);
}
else {
	print STDERR "Unable to test stored procedures: not supported by DBMS.\n";
}

###################################################
#
#	test bulkloads
#
###################################################

$| = 1;

$ristarted  = load_nb_raw($dbh, $dsn, $userid, $passwd, $sescnt, 1000);

$rvstarted  = load_nb_vartext($dbh, $dsn, $userid, $passwd, $sescnt, 1000);
###################################################
#
#	test threaded implementation of above,
#	but only with no-cli
#
###################################################
#if ($Config{useithreads} && $specials[10] && (! $dbh->{tdat_uses_cli})) {
if ($Config{useithreads} && $specials[10]) {
	$tristarted  = load_thrd_raw($dsn, $userid, $passwd, $sescnt, 1000);

	$trvstarted  = load_thrd_vartext($dsn, $userid, $passwd, $sescnt, 1000);
}
else {
	print STDERR "Perl built without thread support, skipping thread tests\n"
		if $specials[10];
}
###################################################
#
#	test array binding
#
###################################################

#
#	force big requests if possible
#
$dbh->{tdat_reqsize} = 500000
		if ($dbh->{tdat_versnum} >= 6000000);
#
#	adjust rowcount for version due to speed difference
#
my $tuplecnt = ($dbh->{tdat_versnum} >= 6000000) ? 10000 : 1000;

if (!$specials[13]) {
$austarted  = ary_using_load($dbh, $tuplecnt);
#
#	create a copy of the table for use w/ sth sourced methods
#
my $srcdbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		tdat_charset => 'UTF8',
		tdat_mode => 'TERADATA',
	}
) || die "Can't connect to $dsn: $DBI::errstr. Exiting...\n";

$srcdbh->{tdat_versnum} = $versnum if $versnum;

$srcdbh->do(
'create volatile table alltypetst20 as
	(select * from alltypetst) with data
	on commit preserve rows') or die $srcdbh->errstr . "\n";

my $srcsth = $srcdbh->prepare('select * from alltypetst20') or die $srcdbh->errstr . "\n";

print "!!! no bigint !!!\n" if $dbh->{tdat_no_bigint};

$apstarted  = ary_ph_load($dbh, $tuplecnt);

$aunstarted  = ary_named_using_load($dbh, $tuplecnt);

$rc = $srcsth->execute() or die $srcsth->errstr . "\n";
$ausstarted  = ary_sth_using_load($dbh, $srcsth, $rc);

$rc = $srcsth->execute() or die $srcsth->errstr . "\n";
$apsstarted  = ary_sth_ph_load($dbh, $srcsth, $rc);

$aitstarted  = ary_load_using_raw_tuples($dbh, $tuplecnt);

$aifstarted  = ary_load_using_raw_fetch($dbh, $tuplecnt);

$avtstarted  = ary_load_using_vartext_tuples($dbh, $tuplecnt);

$avfstarted  = ary_load_using_vartext_fetch($dbh, $tuplecnt);

ary_error_load($dbh, $tuplecnt, 0);	# AutoCommit off

ary_error_load($dbh, $tuplecnt, 1);	# AutoCommit on

$srcdbh->disconnect;
#
#	now run error tests in ANSI mode
#
my $ansidbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		tdat_charset => 'UTF8',
		tdat_mode => 'ANSI',
	}
) || die "Can't connect to $dsn: $DBI::errstr. Exiting...\n";

$ansidbh->{tdat_versnum} = $versnum if $versnum;

#
#	force big requests if possible
#
$ansidbh->{tdat_reqsize} = 500000
		if ($ansidbh->{tdat_versnum} >= 6000000);
#
#	force dateform to integer
#
$ansidbh->do('set session dateform=integerdate');

ary_error_load($ansidbh, $tuplecnt, 0);	# AutoCommit off

ary_error_load($ansidbh, $tuplecnt, 1);	# AutoCommit on

$ansidbh->disconnect();

print STDERR "Array binding tests complete.\n";
}
###################################################
#
#	test updatable cursors
#
###################################################
if ($dbh->{tdat_uses_cli}) {
	print STDERR "Using CLI, skipping persistent/rewindable cursors.\n";
}
else {
	init_for_cursors($dbh, 1000);
	updatable_cursor($dbh, $dsn, $userid, $passwd);

	persistent_cursor($dbh);
	rewind_cursor($dbh);
}

###################################################
#
#	test output mode
#
###################################################
print STDERR "Testing standard output mode...\n";

init_for_cursors($dbh, 10000);

$ostarted = time;
$ssth = $dbh->prepare('SELECT * FROM alltypetst') or die ($dbh->errstr . "\n");
$names = $ssth->{NAME};
print join(' ', @$names), "\n";

$ssth->execute or die ($ssth->errstr . "\n");
$reccnt = 0;
while ($row = $ssth->fetchrow_arrayref() ) {
	$reccnt++;
	print STDERR "Got $reccnt rows\n"
		unless $reccnt%1000;
}
$ostarted = trim_time($ostarted);
print STDERR "$reccnt rows retrieved in $ostarted secs.\n";

print STDERR "Std output ok.\n";
###################################################
#
#	test formatted output mode
#
###################################################
print STDERR "Testing formatted output mode...\n";
$fmostarted = time;
$ssth = $dbh->prepare('SELECT * FROM alltypetst', {tdat_formatted => 1}) ||
	die ($dbh->errstr . "\n");
$names = $ssth->{NAME};
print join(' ', @$names), "\n";

$ssth->execute or die ($ssth->errstr . "\n");
$reccnt = 0;
while ($row = $ssth->fetchrow_arrayref() ) {
	$reccnt++;
	print STDERR "Got $reccnt rows\n"
		unless $reccnt%1000;
}
$fmostarted = trim_time($fmostarted);
print STDERR "$reccnt rows retrieved in $fmostarted secs.\n";

print STDERR "Formatted output ok.\n";
###################################################
#
#	test raw output mode
#
###################################################
print STDERR "Testing raw output mode...\n";
$rostarted = time;
$ssth = $dbh->prepare('SELECT * FROM alltypetst', {
	tdat_raw_out => 'IndicatorMode'
	}) || die ($dbh->errstr . "\n");
$names = $ssth->{NAME};
#print join(' ', @$names), "\n";

$ssth->execute or die ($ssth->errstr . "\n");
$reccnt = 0;
while ($row = $ssth->fetchrow_arrayref() ) {
	$reccnt++;
	print STDERR "Got $reccnt rows\n" unless $reccnt%1000;
}
$rostarted = trim_time($rostarted);
print STDERR "$reccnt rows retrieved in $rostarted secs.\n";

print STDERR "Raw output ok.\n";
###################################################
#
#	test vartext output mode
#
###################################################
print STDERR "Testing vartext output mode...\n";
$vostarted = time;
$ssth = $dbh->prepare('SELECT * FROM alltypetst', {
	tdat_vartext_out => '|'
	}) || die ($dbh->errstr . "\n");
$names = $ssth->{NAME};
#print join(' ', @$names), "\n";

$ssth->execute or die ($ssth->errstr . "\n");
$reccnt = 0;
while ($row = $ssth->fetchrow_arrayref() ) {
	$reccnt++;
	print "Got $reccnt rows\n" and
	print $$row[0], "\n"
		unless $reccnt%1000;
}
$vostarted = trim_time($vostarted);
print STDERR "$reccnt rows retrieved in $vostarted secs.\n";

print STDERR "Vartext output ok.\n";
###################################################
#
#	test tdat_BindColArray()
#
###################################################
print STDERR "Testing tdat_BindColArray()...\n";
my @col1 = ();
my @col2 = ();
my @col3 = ();
my @col4 = ();
my @col5 = ();
my @col6 = ();
my @col7 = ();
my @col8 = ();
my @col9 = ();
my @col10 = ();
my @col11 = ();
my @col12 = ();
my @col13 = ();
my @col14 = ();
my @col15 = ();
my @col16 = ();
my @col17 = ();

$sth = $dbh->prepare('SELECT * from alltypetst') or die $dbh->errstr;
$sth->tdat_BindColArray(1, \@col1, 300);
$sth->tdat_BindColArray(2, \@col2, 300);
$sth->tdat_BindColArray(3, \@col3, 300);
$sth->tdat_BindColArray(4, \@col4, 300);
$sth->tdat_BindColArray(5, \@col5, 300);
$sth->tdat_BindColArray(6, \@col6, 300);
$sth->tdat_BindColArray(7, \@col7, 300);
$sth->tdat_BindColArray(8, \@col8, 300);
$sth->tdat_BindColArray(9, \@col9, 300);
$sth->tdat_BindColArray(10, \@col10, 300);
$sth->tdat_BindColArray(11, \@col11, 300);
$sth->tdat_BindColArray(12, \@col12, 300);
$sth->tdat_BindColArray(13, \@col13, 300);
if ($use_bigdata) {
	$sth->tdat_BindColArray(14, \@col14, 300);
	$sth->tdat_BindColArray(15, \@col15, 300);
	$sth->tdat_BindColArray(16, \@col16, 300);
	$sth->tdat_BindColArray(17, \@col17, 300);
}

$bcstarted = time;
$sth->execute or die $sth->errstr;
$rowcnt = 0;
$threshold = 1000;
while ($sth->fetch) {
	$rowcnt += scalar(@col1);
	print STDERR "Got $rowcnt rows...\n" and
	$threshold += 1000
		if ($rowcnt >= $threshold);
	$#col1 = -1;
}

$bcstarted = trim_time($bcstarted);
print STDERR "Recvd $rowcnt rows in $bcstarted secs.\n";
print STDERR "tdat_BindColArray OK.\n";
###################################################
#
#	test rawmode tdat_BindColArray()
#
###################################################
print STDERR "Testing rawmode tdat_BindColArray()...\n";
my @cols = ();
$sth = $dbh->prepare('SELECT * from alltypetst',
	{ tdat_raw_out => 'IndicatorMode' });
$sth->tdat_BindColArray(1, \@cols, 300);
$rbcstarted = time;
$sth->execute or die "Can't execute:" . $sth->errstr . "\n";
$rowcnt = 0;
$threshold = 1000;
while ($sth->fetch) {
	$rowcnt += scalar(@cols);
	print STDERR "Got $rowcnt rows...\n" and
	$threshold += 1000
		if ($rowcnt >= $threshold);
	@cols = ();
}

$rbcstarted = trim_time($rbcstarted);
print STDERR "Recvd $rowcnt rows in $rbcstarted secs.\n";
print STDERR "Rawmode tdat_BindColArray OK.\n";

###################################################
#
#	test Server Side Vartext Translation
#
###################################################

ssvtexttests($dbh)
	if ($dbh->{tdat_versnum} < 12000000);

###################################################
#
#	test various DBI API calls
#
###################################################

dbiapitests($dbh, $testdsn);

}	#end if specials[1]

###################################################
#
#	test MONITOR
#
###################################################
if ($specials[5]) {
	print STDERR "Testing PM/API...\n";
	eval {
		require TdTestPMAPI;
		import TdTestPMAPI qw(montest);
	};
	die "Can't load TdTestPMAPI: $@"
		if $@;

	montest($dbh, $dsn, $userid, $passwd, $versnum);
}
###################################################
#
#	test console
#
###################################################
if ($specials[6]) {
	print STDERR "Testing Remote Console...\n";
	eval {
		require TdTestConsole;
		import TdTestConsole qw(consoletest);
	};
	die "Can't load TdTestConsole: $@"
		if $@;
	consoletest($dbh, $dsn, $userid, $passwd);
}
###################################################
#
#	start fastload test
#
###################################################
my $sflstarted = undef;
my $rflstarted = undef;
my $vflstarted = undef;
my $mpsflstarted = undef;
my $mprflstarted = undef;
my $mpvflstarted = undef;
my $tmpsflstarted = undef;
my $tmprflstarted = undef;
my $tmpvflstarted = undef;
if ($specials[2]) {
	eval {
		require TdTestFastload;
		import TdTestFastload qw(floadtest);
	};
	die "Can't load TdTestFastload: $@"
		if $@;

	unless ($specials[10] && ($specials[10] == 2)) {
		print STDERR "Testing fastload...\n";
		$sflstarted = floadtest($dsn, $userid, $passwd, $specials[8], $versnum, undef, undef);
		print STDERR "Testing rawmode fastload...\n";
		$rflstarted = floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', undef);
		print STDERR "Testing vartext fastload...\n";
		$vflstarted = floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'utf8data.txt', undef);
#
#	utility module decides whether to use
#	process or thread level parallelism
#
		if ($^O ne 'MSWin32') {
#
#	process based
#
			print STDERR "Testing MP fastload...\n";
			$mpsflstarted = floadtest($dsn, $userid, $passwd, $specials[8], $versnum, undef, 1);
			print STDERR "Testing MP rawmode fastload...\n";
			$mprflstarted = floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', 1);
			print STDERR "Testing MP vartext fastload...\n";
			$mpvflstarted = floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'utf8data.txt', 1);
		}
		else {
			print STDERR "Skipping multiprocess fastload..\n";
		}
	}
	if ($Config{useithreads} && $specials[10]) {
#
#	thread based
#
		print STDERR "Testing threaded MP fastload...\n";
		$tmpsflstarted = floadtest($dsn, $userid, $passwd, $specials[8], $versnum, undef, 1, 1);
		print STDERR "Testing threaded MP rawmode fastload...\n";
		$tmprflstarted = floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', 1, 1);
		print STDERR "Testing threaded MP vartext fastload...\n";
		$tmpvflstarted = floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'utf8data.txt', 1, 1);
	}
}
###################################################
#
#	test FEXP
#
###################################################
my $sfestarted = undef;
my $rfestarted = undef;
my $vfestarted = undef;
my $mpsfestarted = undef;
my $mpvfestarted = undef;
my $tmpsfestarted = undef;
my $tmpvfestarted = undef;
if ($specials[4]) {
	eval {
		require TdTestExport;
		import TdTestExport qw(fexptest);
	};
	die "Can't load TdTestExport: $@"
		if $@;

	unless ($specials[10] && ($specials[10] == 2)) {
		print STDERR "Testing fastexport...\n";
		$sfestarted = fexptest($dsn, $userid, $passwd, $specials[8], $versnum, undef, undef);
		print STDERR "Testing rawmode fastexport...\n";
		$rfestarted = fexptest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', undef);
		print STDERR "Testing vartext fastexport...\n";
		$vfestarted = fexptest($dsn, $userid, $passwd, $specials[8], $versnum, 'varout.txt', undef);
		if ($^O ne 'MSWin32') {
#
#	process based
#
			print STDERR "Testing rawmode MP fastexport...\n";
			$mpsfestarted = fexptest($dsn, $userid, $passwd, $specials[8], $versnum, undef, 1);
		}
		else {
			print STDERR "Skipping multiprocess fastexport..\n";
		}
	}
	if ($Config{useithreads} && $specials[10]) {
#
#	thread based
#
		print STDERR "Testing threaded rawmode MP fastexport...\n";
		$tmpsfestarted = fexptest($dsn, $userid, $passwd, $specials[8], $versnum, undef, 1, 1);
	}
#		print STDERR "Testing vartext MP fastexport...\n";
#		$vfestarted = fexptest($dsn, $userid, $passwd, $specials[8], $versnum, 'varout.txt', 1);
}
###################################################
#
#	start multiload test
#
###################################################
my $smlstarted = undef;
my $rmlstarted = undef;
my $vmlstarted = undef;
my $mpsmlstarted = undef;
my $mprmlstarted = undef;
my $mpvmlstarted = undef;
my $tmpsmlstarted = undef;
my $tmprmlstarted = undef;
my $tmpvmlstarted = undef;
my $mlstarted = undef;
my $supsstarted = undef;
my $rupsstarted = undef;
my $vupsstarted = undef;
my $mpsupsstarted = undef;
my $mprupsstarted = undef;
my $mpvupsstarted = undef;
my $tmpsupsstarted = undef;
my $tmprupsstarted = undef;
my $tmpvupsstarted = undef;
if ($specials[3]) {
	eval {
		require TdTestMultiload;
		import TdTestMultiload qw(mloadtest mloadtest_ups);
	};
	die "Can't load TdTestMultiload: $@"
		if $@;

	eval {
		require TdTestFastload;
		import TdTestFastload qw(floadtest);
	};
	die "Can't load TdTestFastload: $@"
		if $@;

	unless ($specials[10] && ($specials[10] == 2)) {
		print STDERR "Testing simple multiload...\n";
		$smlstarted = mloadtest($dsn, $userid, $passwd, $specials[8], $versnum, undef);
		sleep 5;
		print STDERR "Testing simple rawmode multiload...\n";
		$rmlstarted = mloadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode');
		sleep 5;
		print STDERR "Testing simple vartext multiload...\n";
		$vmlstarted = mloadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'mlvardata.txt');
		sleep 5;

		if ($^O ne 'MSWin32') {
#
#	process based
#
			print STDERR "Testing simple MP multiload...\n";
			$mpsmlstarted = mloadtest($dsn, $userid, $passwd, $specials[8], $versnum, undef, 1);
			sleep 5;
			print STDERR "Testing simple MP rawmode multiload...\n";
			$mprmlstarted = mloadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', 1);
			sleep 5;
			print STDERR "Testing simple MP vartext multiload...\n";
			$mpvmlstarted = mloadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'mlvardata.txt', 1);
			sleep 5;
		}
		else {
			print STDERR "Skipping multiprocess multiloads..\n";
		}
	}
	if ($Config{useithreads} && $specials[10]) {
#
#	thread based
#
		print STDERR "Testing threaded simple MP multiload...\n";
		$tmpsmlstarted = mloadtest($dsn, $userid, $passwd, $specials[8], $versnum, undef, 1, 1);
		sleep 5;
		print STDERR "Testing threaded simple MP rawmode multiload...\n";
		$tmprmlstarted = mloadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', 1, 1);
		sleep 5;
		print STDERR "Testing threaded simple MP vartext multiload...\n";
		$tmpvmlstarted = mloadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'mlvardata.txt', 1, 1);
		sleep 5;
	}

	print STDERR "Testing upsert multiload...\n";
	unless ($specials[10] && ($specials[10] == 2)) {
		print STDERR "Reloading table...\n";
		floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', undef);
		$supsstarted = mloadtest_ups($dsn, $userid, $passwd, $specials[8], $versnum, undef);
		sleep 5;
		print STDERR "Testing upsert rawmode multiload...\n";
		print STDERR "Reloading table...\n";
		floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', undef);
		$rupsstarted = mloadtest_ups($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode');
		sleep 5;
		print STDERR "Testing upsert vartext multiload...\n";
		print STDERR "Reloading table...\n";
		floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', undef);
		$vupsstarted = mloadtest_ups($dsn, $userid, $passwd, $specials[8], $versnum, 'mlvardata.txt');
		sleep 5;

		if ($^O ne 'MSWin32') {
#
#	process based
#
			print STDERR "Testing upsert MP multiload...\n";
			print STDERR "Reloading table...\n";
			floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', undef, 1);
			$mpsupsstarted = mloadtest_ups($dsn, $userid, $passwd, $specials[8], $versnum, undef, 1);
			sleep 5;
			print STDERR "Testing upsert MP rawmode multiload...\n";
			print STDERR "Reloading table...\n";
			floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', undef, 1);
			$mprupsstarted = mloadtest_ups($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', 1);
			sleep 5;
			print STDERR "Testing upsert MP vartext multiload...\n";
			print STDERR "Reloading table...\n";
			floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', undef, 1);
			$mpvupsstarted = mloadtest_ups($dsn, $userid, $passwd, $specials[8], $versnum, 'mlvardata.txt', 1);
			sleep 5;
		}
		else {
			print STDERR "Skipping multiprocess multiload upserts..\n";
		}
	}
	if ($Config{useithreads} && $specials[10]) {
#
#	thread based
#
		print STDERR "Testing threaded upsert MP multiload...\n";
		print STDERR "Reloading table...\n";
		floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', undef, 1, 1);
		$tmpsupsstarted = mloadtest_ups($dsn, $userid, $passwd, $specials[8], $versnum, undef, 1, 1);
		sleep 5;
		print STDERR "Testing threaded upsert MP rawmode multiload...\n";
		print STDERR "Reloading table...\n";
		floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', undef, 1, 1);
		$tmprupsstarted = mloadtest_ups($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', 1, 1);
		sleep 5;
		print STDERR "Testing threaded upsert MP vartext multiload...\n";
		print STDERR "Reloading table...\n";
		floadtest($dsn, $userid, $passwd, $specials[8], $versnum, 'IndicatorMode', undef, 1, 1);
		$tmpvupsstarted = mloadtest_ups($dsn, $userid, $passwd, $specials[8], $versnum, 'mlvardata.txt', 1, 1);
		sleep 5;
	}
}
###################################################
#
#	start utility loopback test
#
###################################################
my $fllbstarted = undef;
my $mllbstarted = undef;
my $tfllbstarted = undef;
my $tmllbstarted = undef;
if ($specials[7]) {
	eval {
		require TdTestLoopback;
		import TdTestLoopback qw(fllbtest mllbtest);
	};
	die "Can't load TdTestLoopback: $@"
		if $@;

	unless ($specials[10] && ($specials[10] == 2)) {
		if ($^O ne 'MSWin32') {
			print STDERR "Testing fastload loopback...\n";
			$fllbstarted = fllbtest($dsn, $userid, $passwd, $specials[8], $versnum);
			print STDERR "Testing multiload loopback...\n";
			$mllbstarted = mllbtest($dsn, $userid, $passwd, $specials[8], $versnum);
		}
		else {
			print STDERR "Skipping multiprocess loopbacks..\n";
		}
	}
	if ($Config{useithreads} && $specials[10]) {
		print STDERR "Testing threaded fastload loopback...\n";
		$tfllbstarted = fllbtest($dsn, $userid, $passwd, $specials[8], $versnum, 1);
		print STDERR "Testing threaded multiload loopback...\n";
		$tmllbstarted = mllbtest($dsn, $userid, $passwd, $specials[8], $versnum, 1);
	}
}

##################################
#
#	All done, clean up and report results
#
##################################
print STDERR "Cleaning up...\n";
#$dbh->do('DROP TABLE alltypetst');
$rc = $dbh->do('DROP MACRO dbitest');

my $out =
"Raw input:                 $ristarted secs
Vartext input:             $rvstarted secs
"
	if $specials[1];

$out .=
"Threaded Raw input:        $tristarted secs
Threaded Vartext input:    $trvstarted secs
"
	if $specials[1] && $specials[10];

print
"$out
USING Array bound input:   $austarted secs
PH Array bound input:      $apstarted secs
Named USING Array bound input: $aunstarted secs
USING Array bound from sth: $ausstarted
PH Array bound from sth:   $apsstarted
Raw Array tuple input:     $aitstarted secs
Raw Array fetched input:   $aifstarted secs
Vartext Array tuple input: $avtstarted secs
Vartext Array fetched input: $avfstarted secs
Std output:                $ostarted secs
Formatted output:          $fmostarted secs
Raw output:                $rostarted secs
Vartext output:            $vostarted secs
tdat_BindColArry:          $bcstarted secs
Rawmode tdat_BindColArray: $rbcstarted secs
\n"
	if $specials[1];

print
"Fastload:                  $sflstarted secs
Rawmode Fastload:          $rflstarted secs
Vartext Fastload:          $vflstarted secs\n"
	if $sflstarted;

print
"MP Fastload:               $mpsflstarted secs
MP Rawmode Fastload:       $mprflstarted secs
MP Vartext Fastload:       $mpvflstarted secs\n"
	if $mpsflstarted;

print
"Threaded MP Fastload:         $tmpsflstarted secs
Threaded MP Rawmode Fastload: $tmprflstarted secs
Threaded MP Vartext Fastload: $tmpvflstarted secs\n"
	if $tmpsflstarted;

print
"Multiload:                    $smlstarted secs
Rawmode Multiload:            $rmlstarted secs
Vartext Multiload:            $vmlstarted secs\n"
	if $smlstarted;

print
"MP Multiload:                 $mpsmlstarted secs
MP Rawmode Multiload:         $mprmlstarted secs
MP Vartext Multiload:         $mpvmlstarted secs\n"
	if $mpsmlstarted;

print
"Threaded MP Multiload:         $tmpsmlstarted secs
Threaded MP Rawmode Multiload: $tmprmlstarted secs
Threaded MP Vartext Multiload: $tmpvmlstarted secs\n"
	if $tmpsmlstarted;

print
"Multijob Multiload:            $supsstarted secs
Rawmode Multijob Multiload:    $rupsstarted secs
Vartext Multijob Multiload:    $vupsstarted secs\n"
	if $supsstarted;

print
"MP Multijob Multiload:         $mpsupsstarted secs
MP Rawmode Multijob Multiload: $mprupsstarted secs
MP Vartext Multijob Multiload: $mpvupsstarted secs\n"
	if $mpsupsstarted;

print
"Threaded MP Multijob Multiload:         $tmpsupsstarted secs
Threaded MP Rawmode Multijob Multiload: $tmprupsstarted secs
Threaded MP Vartext Multijob Multiload: $tmpvupsstarted secs\n"
	if $tmpsupsstarted;

print
"Fastexport:                    $sfestarted secs
Rawmode Fastexport:            $rfestarted secs
Vartext Fastexport:            $vfestarted secs\n"
	if $sfestarted;

print
"MP Rawmode Fastexport:         $mprfestarted secs
MP Vartext Fastexport:         $mpvfestarted secs\n"
	if $mprfestarted;

print
"Threaded MP Rawmode Fastexport: $tmprfestarted secs
Threaded MP Vartext Fastexport: $tmpvfestarted secs\n"
	if $tmprfestarted;

print
"Fastload loopback:              $fllbstarted secs
Multiload loopback:             $mllbstarted secs\n"
	if $fllbstarted;

print
"Threaded Fastload loopback:     $tfllbstarted secs
Threaded Multiload loopback:    $tmllbstarted secs\n"
	if $tfllbstarted;

print STDERR "Logging off...\n";
$dbh->disconnect();
print STDERR "Tests completed ok, exitting...\n";

sub usage {
	print
"test.pl [options] [ hostname userid password[,account] [default_database] ]
where [options] are any number of instances of
	-h : print this message
	-n : do all normal SQL tests
	-f : do fastload tests
	-m : do multiload tests
	-x : do fastexport tests
	-p : do PMAPI tests
	-c : do remote console tests
	-u : do utility loopback tests
	-s count : set max sessions for utilities (default 2)
	-d logfile : turn on diagnostic tracing and log to logfile
	-t [2|1|0] : only/enable/disable thread testing (default enabled)
	-l [0|1] : use CLI adapter (default on)
	-v <version> : force behavior for specified integer Teradata version
		(e.g., 6000127 eq 'V2R6.0.1.27')
	-A : skip (long running) array binding tests

Default is all tests, no trace, 2 sessions, enable thread testing,
CLI adapter enabled, array binding included

If no host/user/password are given, then the environment variables
TDAT_DBD_DSN, TDAT_DBD_USER, and TDAT_DBD_PASSWORD are used.

Example:

perl test.pl -n -f -p -d bugtest.txt localhost dbitst dbitst

will use the localhost, user dbitst password dbitst and perform
only SQL, fastload, and PMAPI tests, logging traces to bugtest.txt.
";

}

sub trim_time {
	return int((time - $_[0]) * 1000)/1000;
}
