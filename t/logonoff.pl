use DBI;
use DBD::Teradata;
use DBD::Teradata::Crypt;
use DBD::Teradata::Diagnostic;
use DBD::Teradata::GetInfo;
use DBD::Teradata::PMAPI;
use DBD::Teradata::TypeInfo;
use DBD::Teradata::Utility;
use Time::HiRes qw(time);

my $logfile;
my $usecli;
my $count = 1;
while ($ARGV[0] && (substr($ARGV[0], 0, 1) eq '-')) {
	my $op = shift @ARGV;

	$logfile = shift @ARGV,
	next
		if ($op eq '-d');

	$usecli = 1, next
		if ($op eq '-c');

	$count = shift @ARGV,
	next
		if ($op eq '-n');
}

if ($logfile) {
	unlink $logfile;
	DBI->trace(2, $logfile);
	$ENV{TDAT_DBD_DEBUG} = 2;
}

#$ENV{TDAT_DBD_NO_CLI} = 1
#	unless $usecli;

my $dsn = "dbi:Teradata:$ARGV[0]";
$dsn .= ";DATABASE=$ARGV[3]"
	if $ARGV[3];

foreach my $i (1..$count) {
my $started = time();
my $dbh1 = DBI->connect($dsn, $ARGV[1], $ARGV[2],
{ PrintError => 0, RaiseError => 0, tdat_charset => 'UTF8', tdat_no_cli => (!$usecli)});

$started = time() - $started;

warn "No connection: " . $DBI::errstr and next
	unless $dbh1;

printf "Logged on to vers. %s(%s) in %6.3f secs\n", $dbh1->{tdat_version}, $dbh1->{tdat_versnum}, $started;

print "Using CLI...\n"
	if $dbh1->{tdat_uses_cli};

print "BIGINT supported\n" if $dbh1->{tdat_has_bignum};
print "Max decimal precision ", $dbh1->{tdat_max_dec_prec}, "\n";

my $sth;
if (1 == 0) {
$sth = $dbh1->prepare('sel user,date,time')
	or die $dbh->errstr;
}
else {
$sth = $dbh1->prepare('sel logonsource from dbc.sessioninfo')
	or die $dbh->errstr;
}
print join(', ', $sth->{NUM_OF_PARAMS}, $sth->{NUM_OF_FIELDS}, @{$sth->{NAME}}), "\n",
join(', ', @{$sth->{TYPE}}), "\n",
join(', ', @{$sth->{PRECISION}}), "\n",
join(', ', @{$sth->{NULLABLE}}), "\n",
join(', ', @{$sth->{tdat_TYPESTR}}), "\n";

	$sth->execute || die $sth->errstr;
	while ($row = $sth->fetchrow_arrayref) {
		print join(', ', @$row), "\n";
	}

$dbh1->disconnect;

sleep 2;
#print "\n";
}
#$dbh2->disconnect;
