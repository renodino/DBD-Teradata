package TdTestExport;

use DBI;
use Time::HiRes qw(time);

use Exporter;
use base ('Exporter');

@EXPORT = qw(fexptest);

use strict;
use warnings;

sub fexptest {
	my ($dsn, $userid, $passwd, $sessions, $versnum, $mode, $mp, $usethrd, $servervt) = @_;
	my $ctldbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		AutoCommit => 1,
		tdat_lsn => 0,
		tdat_charset => 'UTF8'
	});
	$ctldbh->{tdat_versnum} = $versnum if $versnum;
	my $use_bigdata = ($ctldbh->{tdat_has_bignum} && ($ctldbh->{tdat_max_dec_prec} > 18));

	my $qry = $use_bigdata 
			? 'SELECT col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16(varchar(30)), col17(varchar(40)) FROM alltypetst'
			: 'SELECT * FROM alltypetst';

	my $feargs = {
		Utility => 'EXPORT',
		SQL => $qry,
		Sessions => $sessions,
		MP => ($mp ? ($usethrd ? 'threads' : 'nothreads') : undef),
		Report => \&report_cb
	};

	if ($servervt) {
		$feargs->{SQL} = $ctldbh->tdat_ServerSideVartext('SELECT * FROM alltypetst', '|');
		print "**** server side query failed: ", $ctldbh->errstr, "\n" and
		return undef
			unless $feargs->{SQL};
	}
#
#	if direct file based, set file name/type
#
	$feargs->{Target} = defined($mode) ?
		(($mode=~/^Indicator|Record/) ? 'INDICDATA rawout.dat' :
			"VARTEXT '|' $mode") : \&save_data;
#
#	only specify callbacks abd context if subref based
#
	my $festarted;
	$feargs->{CheckpointCallback} = \&fexp_checkpoint,
	$feargs->{Context} = {
		MyContext => 'test string',
#		Runtime => \$festarted
	},
	$feargs->{Context}->{BoundArys} = [ ([ ]) x $sessions ]
		unless defined($mode);

	$festarted = time;

	my $rc = $ctldbh->tdat_UtilitySetup($feargs);

	$festarted = int((time - $festarted) * 1000)/1000;
	print "Export complete, $rc rows received in $festarted secs\n"
		if defined($rc);
	print $ctldbh->errstr, "\n"
		unless defined($rc);
	$ctldbh->disconnect;
	print "Fastexport OK.\n";
	return $festarted;
}

sub fexp_checkpoint {
	my ($function, $rowcount, $ctxt) = @_;

	return 1
		if ($function eq 'FINISH');
#
#	must be init

	my @arys = ([ ]) x $rowcount;
	$ctxt->{BoundArys} = \@arys;
	print "$rowcount EXPORT sessions logged on\n";
	return 1;
}

sub save_data {
	my ($function, $sth, $sessnum, $maxrows, $ctxt) = @_;

	$sth->{tdat_raw} = 'IndicatorMode',
	$sth->tdat_BindColArray(1, $ctxt->{BoundArys}->[$sessnum], 1000),
	return 1
		if ($function eq 'INIT');

	print "Got $maxrows rows on session $sessnum\n" and
	return $maxrows
		if ($function eq 'MOREDATA');
	1;
}

sub report_cb {	print $_[0], "\n"; }

1;