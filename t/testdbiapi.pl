BEGIN {
	push @INC, './t';
}

use DBI;
use DBD::Teradata;
use TdTestDBIAPI qw(dbiapitests);

use strict;
use warnings;

$| = 1;

my $nocli;
while (substr($ARGV[0], 0, 1) eq '-') {
	my $opt = shift @ARGV;

	$nocli = 1,
	next
		if ($opt eq '-c');

	DBI->trace(2, shift @ARGV),
	$ENV{TDAT_DBD_DEBUG} = 1
		if ($opt eq '-d');
}

my $dbh;
my ($dsn, $userid, $passwd, $dfltdb) = @ARGV;

$dsn .= ";DATABASE=$dfltdb"
	if $dfltdb;

$dbh = DBI->connect("dbi:Teradata:$dsn", $userid, $passwd,
	{
		PrintError => 0,
		RaiseError => 0,
		tdat_charset => 'UTF8',
		tdat_mode => 'TERADATA',
		tdat_no_cli => $nocli
	}
) || die "Can't connect to $dsn: $DBI::errstr. Exiting...\n";

print STDERR "Logon to $dsn ver. " . $dbh->{tdat_version} . '(' . $dbh->{tdat_mode} . " mode) ok.\n";
my $drh = $dbh->{Driver};
print STDERR "DBD::Teradata v. $drh->{Version}\n";

die "Did not connect with CLI adapter, check your configuration."
	unless $nocli || $dbh->{tdat_uses_cli};

print STDERR "Connected via ", ($dbh->{tdat_uses_cli} ? 'CLI' : 'pure Perl'), "\n";

dbiapitests($dbh, "dbi:Teradata:$dsn");

$dbh->disconnect;

print STDERR "Tests completed OK.\n";