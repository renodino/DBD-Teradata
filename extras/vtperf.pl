use DBI;
use Time::HiRes qw(time);

use strict;
use warnings;

my $dbh = DBI->connect("dbi:Teradata:$ARGV[0]", $ARGV[1], $ARGV[2])
or die $DBI::errstr;

my $sth = $dbh->prepare('select * from alltypetst',
	{ tdat_vartext_out => '|' }) or die $dbh->errstr;

#
#	prime the pump to avoid cache skews
#
my $cnt = $sth->execute or die $sth->errstr;
my $row;
1 while ($row = $sth->fetchrow_arrayref);

open OUTF, ">vtext.txt";

my $started = time();
$cnt = $sth->execute or die $sth->errstr;

print OUTF $row->[0], "\n"
	while ($row = $sth->fetchrow_arrayref);

print "*** Regular takes ", time() - $started, " secs for $cnt \n";
close OUTF;

my $sql = $dbh->tdat_ServerSideVartext('select * from alltypetst', '|')
	or die $dbh->errstr;
#$sth = $dbh->prepare($sql, { tdat_vartext_out => '|' }) or die $dbh->errstr;
$sth = $dbh->prepare($sql) or die $dbh->errstr;

open OUTF, ">ssvtext.txt";

$started = time();

$cnt = $sth->execute or die $sth->errstr;

print OUTF $row->[0], "\n"
	while ($row = $sth->fetchrow_arrayref);

print "*** Regular takes ", time() - $started, " secs for $cnt \n";
close OUTF;
