#!/usr/bin/perl
#
#	dbccons.pl - Teradata Remote Console app
#
#	command syntax:
#	dbccons.pl host userid password [ utility ]
#
#	if utility not given, DBSCONTROL is assumed.
#
#	first, lets use PurePerl to keep installation simple
#
#BEGIN { $ENV{DBI_PUREPERL} = 2 }

use DBI;

print "Sorry, this version of remote console is not compatible
with MS Windows due to a lack of nonblocking support for STDIN.
A version with piped redirection, or using Perl/Tk,
maybe possible.\n" and
exit 1
if ($^O eq 'MSWin32');
#
#	connect with the DBCCONS utility attribute
#
#$ENV{TDAT_DBD_DEBUG} = 2;
#DBI->trace(2, 'dbccons.log');
$dbh = DBI->connect("dbi:Teradata:$ARGV[0]", $ARGV[1], $ARGV[2], { 
	tdat_utility => 'DBCCONS',
	RaiseError => 0,
	PrintError => 0 });
print "Connected...\n";
$drh = $dbh->{Driver};
#
#	prepare and execute the "start" request to get
#	our console utility started
#
$utility = $ARGV[3] ? $ARGV[3] : 'DBSCONTROL';
$sth = $dbh->prepare("start $utility");
$sth->execute;
#
#	now prepare an empty statement to be used to
#	interact with the console utility
#
$iosth = $dbh->prepare(';', { tdat_nowait => 1 });
#
#	retrieve and display any utility startup banner
#
$line = $$row[0],
$line=~tr/\r/\n/,
$line = strip_esc($line),
print $line
	while ($row=$sth->fetchrow_arrayref);

my $resp;
my $rc;
my $prompted = 0;
#
#	prime the pump by executing an empty stmt
#
$rc = $iosth->execute;
$resp = undef;
print "Utility ending\n",
last
	unless defined($rc);
	
my $stdfn = fileno(STDIN);

while (1) {
#
#	execute the empty statement to solicit
#	utility output until a Prompt indication is received
#
	@avail = $drh->func( ($prompted ? [ $dbh, $stdfn] : [ $dbh ]), -1, 'tdat_FirstAvailList');
	$fd = shift @avail;
	last unless defined($fd);
	if ($fd == 0) {
		$rc = $iosth->func('tdat_Realize');
		print "Utility ending\n",
		$dbh->disconnect,
		exit 1
			unless defined($rc);

		$stmtinfo = $iosth->{tdat_stmt_info}->[1];
#
#	fetch the result and print
#
		while ($row = $iosth->fetchrow_arrayref) {
			$line = $$row[0];
			$line=~tr/\r/\n/;
			$line = strip_esc($line);
			print $line;
		}
#
#	if we got a Prompt indication, then its time
#	to check for some input
#
		if ($stmtinfo->{Prompt}) {
			$prompted = 1 
		}
		$fd = shift @avail;
	}
	if ($prompted && ($fd == 1)) {
#
#	we must cancel if we've already started a request
#
		$iosth->finish 
			if $dbh->{tdat_active};
		$resp = <STDIN> ;
		chop $resp;
		$prompted = 0;
	}
	next if $dbh->{tdat_active};
#
#	now restart the execution
#
	$rc = ($resp) ? $iosth->execute($resp) : $iosth->execute;
	print "Utility ending\n",
	last
		unless defined($rc);
	$resp = undef;
}

$dbh->disconnect;

sub strip_esc {
	my ($str) = @_;
	
$str=~s/\e\[\d?[ABCDJKghlnc]//g;

$str=~s/\e\[\d?;\d?[Hf]//g;

$str=~s/\e\[.+?[mq]//g;

$str=~s/\e\[?\d?[hl]//g;

$str=~s/\e[\d+;\d+[rRy]//g;

$str=~s/\e[ABCDEFGHIJKMZXc=<>\d]//g;

$str=~s/\eY\d+//g;

$str=~s/\e\#\d//g;

$str=~s/\e\([AB\d]//g;

return $str;
}
